#!/usr/bin/perl -w 

$file = shift @ARGV or die "USAGE: tsv-filter.pl ivar.tsv > ivar.filt.tsv\n";

open IFILE, $file or die "$!\n";
$header = <IFILE>;
print $header;
while ($line = <IFILE>) {
    chomp $line;
    @line = split /\t/, $line;
    $pos = $line[1];
    if (($line[10] > .5) && ($line[13] eq "TRUE")) {
	if (($pos > 54) && ($pos < 29836)) {
	    print "$line\n";
	}
    }
}
close IFILE;

