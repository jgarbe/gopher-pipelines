#!/usr/bin/perl -w

# BED file from Daryl has primer coordinates, this converts to amplicon coordinates

#MN908947.3	30	54	nCoV-2019_1_LEFT	nCoV-2019_1	
#MN908947.3	385	410	nCoV-2019_1_RIGHT	nCoV-2019_1	
#MN908947.3	320	342	nCoV-2019_2_LEFT	nCoV-2019_2	

while ($line = <>) {
    chomp $line;
    @line = split /\t/, $line;
    $ref = $line[0];
    $start = $line[1];
    $name = $line[3];

    $line = <>;
    chomp $line;
    @line = split /\t/, $line;
    $end = $line[2];
    $name =~ s/_LEFT//;
    print "$ref\t$start\t$end\t$name\n";
}
