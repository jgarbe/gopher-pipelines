#!/usr/bin/perl
# loadControlMetrics.pl
use strict;

my ($infile) = @ARGV;
unless ($infile) { die "Usage: perl loadControlMetrics.pl <input file>\n"; }

open (FILE, "< :raw", $infile) or die "Couldn't open input metrics file $infile. $!\n";

# operational variables
my $file_version;
my $buffer;
my $typedef1 = "v v v v";
my $typedef2 = "";
my $cbytes;
my $ibytes;

# output variables
my $lane;
my $tile;
my $readNum;
my $controlName;
my $indexName;
my $ncc;

read(FILE,$buffer,1) or die "Couldn't read the first byte! $!\n";
$file_version = unpack("C",$buffer);
unless($file_version == 1) { die "This file version of ControlMetricsOut.bin is not yet supported by this script.\n" }

# print header
print "Lane\tTile\tReadNum\tControlName\tIndexName\tNumControlClusters\n";

while (read(FILE,$buffer,length(pack($typedef1,())))) { 
	($lane,$tile,$readNum,$cbytes) = unpack($typedef1,$buffer);
	print "$lane\t$tile\t$readNum";

	$typedef2 = "a" . $cbytes . " v";
	read(FILE,$buffer,$cbytes+2);
	($controlName,$ibytes) = unpack($typedef2,$buffer);
	print "\t$controlName";

	$typedef2 = "a" . $ibytes . " V";
	read(FILE,$buffer,$ibytes+4);
	($indexName,$ncc) = unpack($typedef2,$buffer);
	print "\t$indexName\t$ncc\n";
}

close (FILE);
