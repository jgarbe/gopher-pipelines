#!/usr/bin/perl
# loadIndexMetrics.pl
use strict;

my ($infile) = @ARGV;
unless ($infile) { die "Usage: perl loadIndexMetrics.pl <input file>\n"; }

open (FILE, "< :raw", $infile) or die "Couldn't open input metrics file $infile. $!\n";

# operational variables
my $file_version;
my $buffer;
my $typedef1 = "v v v v";
my $typedef2 = "";
my $ibytes;
my $sbytes;
my $pbytes;

# output variables
my $lane;
my $tile;
my $readNum;
my $indexName;
my $ncc;
my $sampleName;
my $projectName;

read(FILE,$buffer,1) or die "Couldn't read the first byte! $!\n";
$file_version = unpack("C",$buffer);
unless($file_version == 1) { die "This file version of IndexMetricsOut.bin is not yet supported by this script.\n" }

# print header
print "Lane\tTile\tReadNum\tIndexName\tNumControlClusters\tSampleName\tProjectName\n";

while (read(FILE,$buffer,length(pack($typedef1,())))) { 
	($lane,$tile,$readNum,$ibytes) = unpack($typedef1,$buffer);
	print "$lane\t$tile\t$readNum";

	$typedef2 = "a" . $ibytes . " V v";
	read(FILE,$buffer,$ibytes+6);
	($indexName,$ncc,$sbytes) = unpack($typedef2,$buffer);
	print "\t$indexName\t$ncc";

	$typedef2 = "a" . $sbytes . " v";
	read(FILE,$buffer,$sbytes+2);
	($sampleName,$pbytes) = unpack($typedef2,$buffer);
	print "\t$sampleName";

	$typedef2 = "a" . $pbytes;
	read(FILE,$buffer,$pbytes);
	($projectName) = unpack($typedef2,$buffer);
	print "\t$projectName\n";
}

close (FILE);
