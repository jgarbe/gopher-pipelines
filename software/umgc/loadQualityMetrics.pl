#!/usr/bin/perl
# loadQualityMetrics.pl
use strict;

my ($infile) = @ARGV;
unless ($infile) { die "Usage: perl loadQualityMetrics.pl <input file>\n"; }

open (FILE, "< :raw", $infile) or die "Couldn't open input metrics file $infile. $!\n";

# operational variables
my $file_version;
my $record_size;
my $buffer;
my $typedef = "v3 V50"; # three two-byte integers and 50 4-byte integers

# binning variables
my $bindef;
my $quality_score_binning;
my $number_bins;
my $bins_lower_bound;
my $bins_upper_bound;
my $bins_reported_values;

# output variables
my $lane;
my $tile;
my $cycle;
my @qscores;

read(FILE,$buffer,1) or die "Couldn't read the first byte! $!\n";
$file_version = unpack("C",$buffer);
unless($file_version == 4 || $file_version == 5) { die "This file version of QMetricsOut.bin is not yet supported by this script.\n" }


read(FILE,$buffer,1) or die "Couldn't read the second byte! $!\n";
$record_size = unpack("C",$buffer);

if ($file_version == 5) {
	read(FILE,$buffer,1) or die "Couldn't read the third byte! $!\n";
	$quality_score_binning = unpack("C",$buffer);
	if ($quality_score_binning == 1) {
		read(FILE,$buffer,1) or die "Couldn't read the fourth byte! $!\n";
		$number_bins = unpack("C",$buffer);
		$bindef = "C" . $number_bins;
		read(FILE,$buffer,$number_bins);
		$bins_lower_bound = unpack($bindef,$buffer);
		read(FILE,$buffer,$number_bins);
		$bins_upper_bound = unpack($bindef,$buffer);
		read(FILE,$buffer,$number_bins);
		$bins_reported_values = unpack($bindef,$buffer);
	}
}

# print header line
print "Lane\tTile\tCycle";
for (my $i = 1; $i < 10; $i++) { print "\tQ0" . $i; } 
for (my $i = 10; $i <= 50; $i++) { print "\tQ" . $i; } 
print "\n";

while (read(FILE,$buffer,$record_size)) { # read will return 0 at EOF
	($lane,$tile,$cycle,@qscores) = unpack($typedef,$buffer);
	print "$lane\t$tile\t$cycle";
	foreach (@qscores) { print "\t" . $_; }
	print "\n";
}

close (FILE);
