#!/usr/bin/perl
# loadImageMetrics.pl
use strict;

my ($infile) = @ARGV;
unless ($infile) { die "Usage: perl loadImageMetrics.pl <input file>\n"; }

open (FILE, "< :raw", $infile) or die "Couldn't open input metrics file $infile. $!\n";

# operational variables
my $file_version;
my $record_size;
my $buffer;
my $typedef = "v6"; # six two-byte integers

# output variables
my $lane;
my $tile;
my $cycle;
my $channel;
my $minContrast;
my $maxContrast;

read(FILE,$buffer,1) or die "Couldn't read the first byte! $!\n";
$file_version = unpack("C",$buffer);
unless($file_version == 1) { die "This file version of ImageMetricsOut.bin is not yet supported by this script.\n" }

read(FILE,$buffer,1) or die "Couldn't read the second byte! $!\n";
$record_size = unpack("C",$buffer);

print "Lane\tTile\tCycle\tChannelID\tminContrast\tmaxContrast\n"; # print header line

while (read(FILE,$buffer,$record_size)) { # read will return 0 at EOF
	($lane,$tile,$cycle,$channel,$minContrast,$maxContrast) = unpack($typedef,$buffer);
	print "$lane\t$tile\t$cycle\t$channel\t$minContrast\t$maxContrast\n";
}

close (FILE);
