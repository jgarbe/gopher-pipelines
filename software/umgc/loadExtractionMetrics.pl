#!/usr/bin/perl
# loadExtractionMetrics.pl
use strict;

my ($infile) = @ARGV;
unless ($infile) { die "Usage: perl loadExtractionMetrics.pl <input file>\n"; }

open (FILE, "< :raw", $infile) or die "Couldn't open input metrics file $infile. $!\n";

# operational variables
my $file_version;
my $record_size;
my $buffer;
# three two-byte integers, 4 4-byte floats, 4 2-byte integers, 8 8-bit strings
my $typedef = "v v v f f f f v v v v B8 B8 B8 B8 B8 B8 B8 B8"; 
my @dtarray;
my $fullbin;
my $ticksbin;
my $ticks;

# output variables
my $lane;
my $tile;
my $cycle;
my $fwhm_a;
my $fwhm_c;
my $fwhm_g;
my $fwhm_t;
my $int_a;
my $int_c;
my $int_g;
my $int_t;
my $dt;

read(FILE,$buffer,1) or die "Couldn't read the first byte! $!\n";
$file_version = unpack("C",$buffer);
unless($file_version == 2) { die "This file version of ExtractionMetricsOut.bin is not yet supported by this script.\n" }

read(FILE,$buffer,1) or die "Couldn't read the second byte! $!\n";
$record_size = unpack("C",$buffer);

# print header line
print "Lane\tTile\tCycle\tFWHM_A\tFWHM_C\tFWHM_G\tFWHM_T\tIntensity_A\tIntensity_C\tIntensity_G\tIntensity_T\tDate_Time\n";

while (read(FILE,$buffer,$record_size)) { # read will return 0 at EOF
	$fullbin = '';
	($lane,$tile,$cycle,$fwhm_a,$fwhm_c,$fwhm_g,$fwhm_t,$int_a,$int_c,$int_g,$int_t,@dtarray) = unpack($typedef,$buffer);
	
	# Reconstruct the full binary string. Reverse is used to reflect little-endian architecture.
	foreach (reverse @dtarray) { $fullbin = $fullbin . $_; }

	# Date/Time is represented as one 64-bit integer used in .NET to represent "ticks"
	# the .NET representation uses the first two bits to store a separate field called "Kind", 
	# so we truncate and replace these bits with "00"
	$ticksbin = "00" . substr($fullbin,2);  

	# convert the resulting binary back into decimal format
	$ticks = oct("0b".$ticksbin);

	# convert .NET "ticks" to epoch time, and output a sensible time format
	$dt = localtime(($ticks - 621355968000000000) / 10000000);

	print "$lane\t$tile\t$cycle\t$fwhm_a\t$fwhm_c\t$fwhm_g\t$fwhm_t\t$int_a\t$int_c\t$int_g\t$int_t\t$dt\n";
}

close (FILE);