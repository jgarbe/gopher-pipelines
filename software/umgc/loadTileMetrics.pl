#!/usr/bin/perl
# loadTileMetrics.pl
use strict;

my ($infile) = @ARGV;
unless ($infile) { die "Usage: perl loadTileMetrics.pl <input file>\n"; }

open (FILE, "< :raw", $infile) or die "Couldn't open input metrics file $infile. $!\n";

# operational variables
my $file_version;
my $record_size;
my $buffer;
my $typedef = "v v v f"; # three two-byte integers and one 4-byte float

# output variables
my $lane;
my $tile;
my $code;
my $value;

read(FILE,$buffer,1) or die "Couldn't read the first byte! $!\n";
$file_version = unpack("C",$buffer);
unless($file_version == 2) { die "This file version of TileMetricsOut.bin is not yet supported by this script.\n" }

read(FILE,$buffer,1) or die "Couldn't read the second byte! $!\n";
$record_size = unpack("C",$buffer);


print "Lane\tTile\tCode\tValue\n"; # print header line

while (read(FILE,$buffer,$record_size)) { # read will return 0 at EOF
	($lane,$tile,$code,$value) = unpack($typedef,$buffer);
	print "$lane\t$tile\t$code\t$value\n";
}

close (FILE);
