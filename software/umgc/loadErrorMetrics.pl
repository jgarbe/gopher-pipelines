#!/usr/bin/perl
# loadErrorMetrics.pl
use strict;

my ($infile) = @ARGV;
unless ($infile) { die "Usage: perl loadErrorMetrics.pl <input file>\n"; }

open (FILE, "< :raw", $infile) or die "Couldn't open input metrics file $infile. $!\n";

# operational variables
my $file_version;
my $record_size;
my $buffer;
my $typedef = "v3 f V5"; # three two-byte integers, one float, and 5 4-byte integers

# output variables
my $lane;
my $tile;
my $cycle;
my $errorRate;
my $perfReads;
my $singleError;
my $doubleError;
my $tripleError;
my $quadError;

read(FILE,$buffer,1) or die "Couldn't read the first byte! $!\n";
$file_version = unpack("C",$buffer);
unless($file_version == 3) { die "This file version of ErrorMetricsOut.bin is not yet supported by this script.\n" }

read(FILE,$buffer,1) or die "Couldn't read the second byte! $!\n";
$record_size = unpack("C",$buffer);

# print header line
print "Lane\tTile\tCycle\tErrorRate\tNumPerfectReads\tNumSingleError\tNumDoubleError\tNumTripleError\tNumQuadrupleError\n";

while (read(FILE,$buffer,$record_size)) { # read will return 0 at EOF
	($lane,$tile,$cycle,$errorRate,$perfReads,$singleError,$doubleError,$tripleError,$quadError) = unpack($typedef,$buffer);
	print "$lane\t$tile\t$cycle\t$errorRate\t$perfReads\t$singleError\t$doubleError\t$tripleError\t$quadError\n";
}

close (FILE);
