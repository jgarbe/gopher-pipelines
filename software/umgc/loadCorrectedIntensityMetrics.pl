#!/usr/bin/perl
# loadCorrectedIntensityMetrics.pl
use strict;

my ($infile) = @ARGV;
unless ($infile) { die "Usage: perl loadCorrectedIntensityMetrics.pl <input file>\n"; }

open (FILE, "< :raw", $infile) or die "Couldn't open input metrics file $infile. $!\n";

# operational variables
my $file_version;
my $record_size;
my $buffer;
my $typedef = "v12 V5 f"; # twelve two-byte integers, five 4-byte integers, and one 4-byte float

# output variables
my $lane;
my $tile;
my $cycle;
my $avInt;
my $aciA;
my $aciC;
my $aciG;
my $aciT;
my $aciccA;
my $aciccC;
my $aciccG;
my $aciccT;
my $numNC;
my $numA;
my $numC;
my $numG;
my $numT;
my $s2nratio;

read(FILE,$buffer,1) or die "Couldn't read the first byte! $!\n";
$file_version = unpack("C",$buffer);
unless($file_version == 2) { die "This file version of CorrectedIntMetricsOut.bin is not yet supported by this script.\n" }

read(FILE,$buffer,1) or die "Couldn't read the second byte! $!\n";
$record_size = unpack("C",$buffer);


# print header line
print "Lane\tTile\tCycle\tAveIntensity\tAveCorrectedIntensity_A\tAveCorrectedIntensity_C\tAveCorrectedIntensity_G";
print "\tAveCorrectedIntensity_T\tAveCorrectedIntensityCalledClusters_A\tAveCorrectedIntensityCalledClusters_C";
print "\tAveCorrectedIntensityCalledClusters_G\tAveCorrectedIntensityCalledClusters_T\tNumNoCalls\tNum_A\tNum_C";
print "\tNum_G\tNum_T\tSignal2NoiseRatio\n";

while (read(FILE,$buffer,$record_size)) { # read will return 0 at EOF
	($lane,$tile,$cycle,$avInt,$aciA,$aciC,$aciG,$aciT,$aciccA,$aciccC,$aciccG,$aciccT,
		$numNC,$numA,$numC,$numG,$numT,$s2nratio) = unpack($typedef,$buffer);
	print "$lane\t$tile\t$cycle\t$avInt\t$aciA\t$aciC\t$aciG\t$aciT\t$aciccA\t$aciccC\t$aciccG\t$aciccT";
	print "\t$numNC\t$numA\t$numC\t$numG\t$numT\t$s2nratio\n";
}

close (FILE);
