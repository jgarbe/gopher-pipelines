#!/usr/bin/perl -w

#####################################
# genename.pl
# John Garbe
# January 2020
#####################################

=head1 DESCRIPTION

genename.pl - convert ensembl IDs to gene names

=head1 SYNOPSIS

genename.pl --gtf annotation.gtf --input subread.txt > subread.genenames.txt

=head1 OPTIONS

Options:

 --input file : input tab-delimited text file, with header and Ensembl IDs in the first column
 --gtf file : ensembl gtf file, which has data on ensembl IDs and gene names
 --help : Print usage instructions and exit
 --verbose : Print more information while running

=cut

##################### Initialize ###############################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;
use File::Temp qw( tempdir );

# set defaults
$args{threads} = 10;
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "threads=i" => \$args{threads},
	   "input=s" => \$args{input},
	   "gtf=s" => \$args{gtf},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}

### Handle Parameters ###

die "--input is required\n" unless ($args{input}); 
die "--gtf is required\n" unless ($args{gtf}); 


### read through gtf file, parsing out gene_name
open IFILE, $args{gtf} or die "cannot open $args{gtf}: $!\n";
while ($line = <IFILE>) {
    chomp $line;
    @line = split /\t/, $line;
    @attributes = split /;/, $line[8];
    my $id;
    my $name;
    foreach $attribute (@attributes) {
	if ($attribute =~ /gene_id/) {
	    if ($attribute =~ /\"(.+)\"/) {
		$id = $1;
	    }
	}
	if ($attribute =~ /gene_name/) {
	    if ($attribute =~ /\"(.+)\"/) {
		$name = $1;
	    }
	}
    }
    $name = $id if ($name eq ""); # use ensemble ID if no name
    if ($id eq "") { # skip if no ID
	next;
    } elsif (defined($names{$id}) && ($names{$id} ne $name)) { # check for multipled names for ID
	print STDERR "Duplicate: $id $names{$id} $name\n";
    } else {
	$names{$id} = $name;
    }
}

foreach $id (keys %names) {
    print "$id\t$names{$id}\n";
}




################################ Helper subs ###############################

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

sub mean {
    my($data) = @_;
    if (not @$data) {
	die("Empty array\n");
    }
    my $total = 0;
    foreach (@$data) {
	$total += $_;
    }
    my $average = $total / @$data;
    return $average;
}

sub stdev {
    my($data) = @_;
    if(@$data == 1) {
	return 0;
    }
    my $average = &average($data);
    my $sqtotal = 0;
    foreach(@$data) {
	$sqtotal += ($average-$_) ** 2;
    }
    my $std = ($sqtotal / (@$data-1)) ** 0.5;
    return $std;
}



