#!/usr/bin/perl -w

#####################################
# gbstrim.pl 
# John Garbe
# June 2016
# 
#####################################

=head1 NAME

gbstrim.pl - 
 - Calculate star activity (if --star)
 - Trim padding and/or cut site residue from beginning of reads
 - Trim sequencing adapter from ends of reads
 - Trim all reads to length (if --minlength and/or --maxlength specified)

NOTE: this doesn't handle paired-end data

=head1 SYNOPSIS

gbstrim.pl [--overhang AGCT OR --enzyme SbfI] [--padding ,A,AG,TGC] [--adapter CTGTCTCTT] [--minlength length] [maxlength length] --fastqfile file.fastq --outputfile out.fastq

=head1 DESCRIPTION

Options:
    --enzyme enzyme : Enzyme used to digest sample [not implemented]
    --overhang AGCT : Overhang sequence left by restriction enzyme
    --help : Display usage information
b
    --padding C,TG,AAG,GCTC : comma-separated list of phasing pads. To include a pad of zero start with a comma: ,C,TG,AAG,GCTC
    --minlength 95 : discard reads shorter than minlength
    --maxlength 95 : trim all longer than maxlength to maxlength
    --adapter CTGTCTCTTATACACATCTCCGAG : sequencing primer adapter to trim from 3' end
    --fastqfile sample.fastq : fastq file of reads to process
    --outputfile sample.gbstrim.fastq : fastq file of processed reads
    --star : calculate star activity
=cut

##################### Initialize ###############################

use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib "$FindBin::RealBin";
use enzymes;

$usage = "USAGE: gbstrim.pl [--overhang AGCT OR --enzyme SbfI] [--padding ,A,AG,TGC] [--adapter CTGTCTCTT] [--minlength length] [--maxlength length] [--star] --fastqfile file.fastq --outputfile out.fastq\n";

# set defaults
$adapter = "CTGTCTCTTATACACATCTCCGAG";
GetOptions("help" => \$help,
	   "verbose" => \$verbose,
#	   "threads=i" => \$threads,
	   "enzyme=s" => \$enzyme,
	   "overhang=s" => \$overhang,
	   "minlength=i" => \$minlength,
	   "maxlength=i" => \$maxlength,
	   "padding=s" => \$padding,
	   "adapter=s" => \$adapter,
	   "fastqfile=s" => \$fastqfile,
	   "outputfile=s" => \$outputfile,
	   "star" => \$star,
    ) or die $usage;
pod2usage(q(-verbose) => 3) if ($help);
die $usage unless ($fastqfile and $outputfile);
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    die $usage;
}
die "Cannot find fastq file $fastqfile\n" if (! -e $fastqfile);

die "Unknown enzyme $enzyme\n" unless ($enzymes{lc($enzyme)});
$overhang = $enzymes{lc($enzyme)}{overhang};

$ifile = $fastqfile;

if ($star) {
    print "Calculating star activity with staractitivy.pl...\n" if ($verbose);
    $paddingoption = ($padding) ? "--padding $padding" : "";
    $verboseoption = ($verbose) ? "--verbose" : "";
    $command = "staractivity.pl $verboseoption --fastqfile $ifile --enzyme $enzyme $paddingoption";
    print "COMMAND: $command\n" if ($verbose);
    $result = `$command`;
    print $result if ($verbose);
    if ($result =~ /(\d*\.?\d*)% star activity\n/) {
	$stats{starpct} = $1;
	print "starpct $stats{starpct}\n" if ($verbose);
    }
}   

### Trim restriction enzyme site + phasing padding ###
# build sequences to trim (each padding sequence with the overhang added on)
$maxpad = 0;
if ($padding) {
    @paddings = split /,/, $padding;
    if ($overhang) {
	for $i (0..$#paddings) {
	    $trimseqs[$i] = $paddings[$i] . $overhang;
	    $maxpad = length($trimseqs[$i]) if (length($trimseqs[$i]) > $maxpad);
	}
    }
}

### Identify phasing padding and cut site overhang ###
$pattern = `adapterpattern.pl $overhang $padding`;
chomp $pattern;
print "searching for pattern /$pattern/\n" if ($verbose);

# initialize cutsite counts to zero
#@padding = split /,/, $padding;
#foreach $padding (@padding) {
#    $cutsitecount{$padding} = 0;
#}

# trim padding/overhang sequence
if ($overhang and $padding) {

    print "Trimming from 5' end...\n" if ($verbose);

    $ofile = $outputfile . ".5trim.fastq";
    die "Cannot find input file $ifile\n" unless (-e $ifile);

    if ($ifile =~ /\.gz$/) {
	open IFILE, "gunzip -c $ifile |" or die "cannot open gzip pipe of $ifile: $!\n";	
    } else {
	open IFILE, $ifile or die "cannot open $ifile: $!\n";
    }
    open OFILE, ">$ofile" or die "cannot open $ofile: $!\n";
    open OFILE2, ">$ofile.untrimmed" or die "cannot open $ofile.untrimmed: $!\n";

    while ($id = <IFILE>) {
	$seq = <IFILE>;
	my $plus = <IFILE>;
	$qual = <IFILE>;

	if ($seq =~ /$pattern/) {
	    $thispadding = $1;
	    $thisoverhang = $2;
	    $thisadapter = $thispadding . $thisoverhang;
	    $trimlength = length($thisadapter);
	    $seq = substr($seq,$trimlength);
	    $qual = substr($qual, $trimlength);
	    $cutsitecount{$thisadapter}++;
	    print OFILE "$id$seq$plus$qual";
	} else {
	    print OFILE2 "$id$seq$plus$qual";	    
	    $stats{nocutsite}++;
	}
	$stats{inputreads}++;
    }

    `cat $ofile.untrimmed | sed -n '2~4p' | cut -c 1-11 | sort | uniq -c | sort -n | tail -n 14 > $ofile.untrimmed.adapters`; # identify untrimmed adapters 

    # compute stats
    $stats{"%nocutsite"} = &round100($stats{nocutsite} / $stats{inputreads} * 100);
    foreach $sequence (sort keys %cutsitecount) {
	$cutsitecount{$sequence} = 0 unless ($cutsitecount{$sequence});
	$percents{$sequence} = &round100($cutsitecount{$sequence} / $stats{inputreads} * 100);
    }
    $ifile = $ofile;

    foreach $sequence (sort keys %percents) {
	$stats{"\%" . "$sequence"} = $percents{$sequence};
    }

#    $key = "\%";
#    $value = "";
#    foreach $sequence (sort keys %percents) {
#	$key .= "$sequence-";
#	$value .= $percents{$sequence} . "-";
#    }
#    chop $key;
#    chop $value;
#    $stats{$key} = $value;
}

### Trim any trailing adapter sequence ##
if ($adapter) {
    
    print "Trimming $adapter from 3' end...\n" if ($verbose);

    $ofile = $outputfile . ".3trim.fastq";

    $command = "cutadapt -a $adapter --minimum-length=20 -o $ofile $ifile";
    print "$command\n" if ($verbose);
    @result = `$command`;

    # parse stats from output
    foreach $result (@result) {
	if ($result =~ /^Total reads processed:\s+([\d,]+)/) {
	    $total = $1;
	    $total =~ s/,//g;
#	print "Total reads\t$total\n";
	    next;
	}
	if ($result =~ /^Reads with adapters:\s+([\d,]+)/) {
	    $good = $1;
	    $good =~ s/,//g;
	    $stats{readswithtrailingadapter} = $good;
#	    print "readswithtrailingadapter\t$good\n";
	    $percent = &round100($good / $total * 100);
	    $stats{"%readswithtrailingadapter"} = $percent;
#	    print "%readswithtrailingadapter\t$percent\n";
	    next;
	}
    }
    $ifile = $ofile;
}

### Trim reads to length ###
if ($minlength or $maxlength) {

    print "Trimming all reads down to $maxlength bases...\n" if ($verbose and $maxlength);
    print "Discarding reads shorter than $minlength bases...\n" if ($verbose and $minlength);

    $ofile = $outputfile . ".crop.fastq";

    $min = ($minlength) ? "MINLEN:$minlength" : "";
    $max = ($maxlength) ? "CROP:$maxlength" : "";
    $command = "java -jar /panfs/roc/itascasoft/trimmomatic/0.33/trimmomatic.jar SE $ifile $ofile $min $max"; # removed -trimlog $ofile.log 2/23/18
    print "$command\n" if ($verbose);
    $result = `$command`;
    $ifile = $ofile;

    if (0) { # TODO: fix this so it works
    $result =~ /Input Reads: (\d+) Surviving: (\d+)/;
    $inputreads = $1;
    $survivingreads = $2;
    $shortreads = $inputreads - $survivingreads;
    $pctshortreads = &round100($shortreads / $inputreads * 100);
#print "Reads before trimmomatic\t$inputreads\n";
    $stats{shortreads} = $shortreads;
#    print "shortreads\t$shortreads\n";
    $stats{"%shortreads"} = $pctshortreads;
#    print "%shortreads\t$pctshortreads\n";
    }
}

print "STATS Start\n";
foreach $key (keys %stats) {
    print "$key\t$stats{$key}\n";
}
print "STATS End\n";

### name output file properly, clean up tmp files ###
`mv $ofile $outputfile`;
print "mv $ofile $outputfile\n" if ($verbose);
foreach $file ("$outputfile.3trim.fastq","$outputfile.5trim.fastq","$outputfile.crop.fastq","$outputfile.crop.fastq.log") {
    `rm $file` if (-e $file);
}

exit;

########################################################

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}
