#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

coverageplot.pl - generate a fragment-length plot from Picard output

=head1 SYNOPSIS

coverageplot.pl insert_summary1.txt [insert_summary2.txt ...]
coverageplot.pl -f filelist.txt

=head1 DESCRIPTION

Generate a plot summarizing coverage-summary output files. R is required, as well as the R package ggplot2. 

Options:
    -f filelist.txt : provide a file with a list of coverage-summary.pl output files, one per line. A second tab-delimited column may be included containing sample names
    -h : Print usage instructions and exit
    -v : Print more information whie running (verbose)

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h, $opt_v);

$usage = "USAGE: coverageplot.pl coverage.txt [coverage2.txt ...]\n";
die $usage unless ($#ARGV >= 0);
die $usage unless getopts('f:hv');
pod2usage(q(-verbose) => 3) if ($opt_h);
$verbose = $opt_v // 0;

# Get list of coverage files
if (defined($opt_f)) {
    open IFILE, "$opt_f" or die "Cannot open file list $opt_f: $!\n";
    while (<IFILE>) {
	chomp;
	next unless length;
	@line = split /\t/;
	push @files, $line[0];
	push @samples, $line[1] || $line[0];
    } 
} else { 
    @files = @ARGV; 
    @samples = @ARGV;
}

if ($verbose) {
    print "File\tSample\n";
    for $i (0..$#files) { 
	print "$files[$i] $samples[$i]\n";
    }
}
#print "@files\n";
#print "@samples\n";

open OFILE, ">tmp.dat" or die "Cannot open output file tmp.dat: $!\n";
print OFILE "Value\tCount\tSample\n";
# run through the insert metric files, saving stats
print "STATS Start\n";
for $i (0..$#files) {
    if (not -e $files[$i]) { # skip files that don't exist
	print STDERR "File $files[$i] not found, skipping\n";
	next;
    }
    open IFILE, "$files[$i]" or die "Cannot open input file $files[$i] $!\n";
    while (<IFILE>) {
	chomp;
	next unless length;
	next if ($_ =~ /#/);
	($depth, $loci) = split /\t/;
	print OFILE "$loci\t$depth\t$samples[$i]\n";
    }
    print "$samples[$i]\tloci\t$loci\n";
    close IFILE;
}
print "STATS End\n";

# Run R to generate plot
my $r_script = "coverageplot.r";
my $width = 480;
my $numsamples = $#samples + 1;
#if ($numsamples > 6) {
#    $width = 480 + (20 * ($numsamples-6));
#}
open(Rcmd,">", $r_script) or die "Cannot open $r_script \n\n";
print Rcmd "options(warn=-1)" unless ($verbose);
print Rcmd qq(
  library("ggplot2");
  png(filename="coverageplot.png",width=$width);
  datat <- read.table("tmp.dat", header=T, colClasses=c("Sample"="factor"));

ggplot(datat, aes(Count, Value, color=Sample)) + geom_line() + ggtitle("Title") + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Number of Loci') + xlab('Minimum Number of Reads') + xlim(c(0,100))

# + facet_wrap(~Metric, scales='free_y', ncol=1) 

  #eof) . "\n";

close Rcmd;
system("R --no-restore --no-save --no-readline < $r_script > $r_script.out");

exit;

################# subs ###################

sub median {
    $data = shift;
    $total = shift;
    $sum = 0;
    foreach $size (sort keys %{$data}) {
	$sum += $data->{$size};
	return $size if ($sum >= $total / 2);
    }	
}
