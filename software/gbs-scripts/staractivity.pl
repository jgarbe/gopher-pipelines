#!/usr/bin/perl -w 

#####################################
# staractivity.pl
# John Garbe
# February 2017
# 
# Calcualte star activity percentage
#
#####################################

=head1 NAME

staractivity.pl - Calculate star activity percentage

=head1 SYNOPSIS

staractivity.pl --overhang GATC --padding ,A,AG,ACGT --fastqfile file.fastq

=head1 DESCRIPTION

Options:
    --enzyme enzyme : Enzyme use to digest sample
b
    --padding C,TG,AAG,GCTC : comma-separated list of phasing pads. To include a pad of zero start with a comma: ,C,TG,AAG,GCTC
    --fastqfile sample.fastq : fastq file of reads to process, can be .gz file
    --sequences 1000000 : Number of sequences from fastq file to process
    --help : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib "$FindBin::RealBin";
use enzymes;

$usage = "USAGE: staractivity.pl --enzyme GATC --padding ,A,AG,TGC --fastqfile file.fastq [ --sequences 1000000 ]\n";

# set defaults
$verbose = 0;
$seqlimit = 1000000;
GetOptions("help" => \$help,
	   "verbose" => \$verbose,
	   "enzyme=s" => \$enzyme,
	   "padding=s" => \$padding,
	   "sequences=i" => \$seqlimit,
	   "fastqfile=s" => \$fastqfile,
    ) or die $usage;
pod2usage(q(-verbose) => 3) if ($help);
die $usage unless ($fastqfile and $enzyme);
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    die $usage;
}
die "Cannot find fastq file $fastqfile\n" if (! -e $fastqfile);

die "Unknown enzyme $enzyme\n" unless ($enzymes{lc($enzyme)});
$overhang = $enzymes{lc($enzyme)}{overhang};

# deal with degenerate bases in overhang
$iupac{N} = ["A", "T", "C", "G"];
$iupac{R} = ["A", "G"];
$iupac{Y} = ["C", "T"];
$iupac{M} = ["A", "C"];
$iupac{K} = ["G", "T"];
$iupac{S} = ["G", "C"];
$iupac{W} = ["A", "T"];
$iupac{H} = ["A", "T", "C"];
$iupac{B} = ["G", "T", "C"];
$iupac{V} = ["G", "A", "C"];
$iupac{D} = ["G", "A", "T"];

@bases = split '', $overhang;
for $i (0..$#bases) {
    $base = $bases[$i];
    if ($iupac{$base}) {
	foreach $iupacbase (@{$iupac{$base}}) {
	    $seq = $overhang;
	    substr($seq,$i,1) = $iupacbase;
	    push @overhangs, $seq;
	}
    }
}
@overhangs = ($overhang) unless (@overhangs);;

foreach $overhang (@overhangs) {
    print "overhang $overhang\n" if ($verbose);
}

# determine all of the overhang sequences with 1 base mismatch
foreach $overhang (@overhangs) {
    @bases = split '', $overhang;
    for $i (0..$#bases) {
	foreach $base ("A", "C", "T", "G") {
	    $seq = $overhang;
	    substr($seq,$i,1) = $base;
	    $mismatches{$seq} = 1;
	}
    }
}

# remove the 0 mismatch overhangs from list of 1 mismatch overhangs
foreach $overhang (@overhangs) {
    delete $mismatches{$overhang};
}

$mismatchcount = keys %mismatches;
print "$mismatchcount mismatch overhangs\n" if ($verbose);

# Generate all combinations of padding sequences and overhang sequences
@paddings = split /,/, $padding;
$maxprimerlength = 0;
foreach $pad (@paddings) {
    foreach $mismatch (sort keys %mismatches) {
	$seq = "$pad$mismatch"; # 1 base mismatch overhang
	$primers{$seq} = 0;
#	print "$seq\n";
    }
    foreach $overhang (@overhangs) {
	$seq = "$pad$overhang"; # matching overhang
	$primers{$seq} = 1;
	$maxprimerlength = length($seq) if (length($seq) > $maxprimerlength);
    }
#    print "$seq\n\n";
}

$primercount = keys %primers;
print "$primercount primers\n" if ($verbose);
print "Longest primer is $maxprimerlength bases\n" if ($verbose);

# Grab start of every read, return count of unique sequences
$linelimit = $seqlimit * 4;
if ($fastqfile =~ /\.gz$/) {
    @results = `gunzip -c $fastqfile | head -n $linelimit | sed -n '2~4p' | cut -c -$maxprimerlength | sort | uniq -c | sort -n`;
} else {
    @results = `head -n $linelimit $fastqfile | sed -n '2~4p' | cut -c -$maxprimerlength | sort | uniq -c | sort -n`;
}

# go through unique sequences, look for match to primer
$readmatchcount = 0;
$seqmatchcount = 0;
$readmismatchcount = 0;
$seqmismatchcount = 0;
$readcount = 0;
$seqcount = 0;
foreach $result (@results) {
    chomp $result;
    $result =~ s/^\s+//; # strip leading whitespace
    ($count, $sequence) = split ' ', $result;
#    print "$count $sequence\n";
    $seqcount++;
    $readcount += $count;

#    foreach $seq ($sequence, &reverse_complement($sequence)) {
    $seq = $sequence;
    $match = 0;
    foreach $primer (keys %primers) {
	if ($seq =~ /^$primer/) {
	    if ($primers{$primer}) {
		$seqmatchcount++;
		$readmatchcount += $count;
	    } else {
		$seqmismatchcount++;
		$readmismatchcount += $count;
	    }
	    $match = 1;
	    last;
	}
    }
    $seqnomatch{$seq} = $count unless ($match);
}

if ($verbose) {
    print "High-frequency unmatched sequences:\n";
    foreach $seq (keys %seqnomatch) {
	print "$seq $seqnomatch{$seq}\n" if ($seqnomatch{$seq} > 1000);
    }
}

print "$seqcount sequences\n" if ($verbose);
print "$readcount reads\n" if ($verbose);
$readmatchpct = $readmatchcount / $readcount * 100;
print "$seqmatchcount matched sequences\n" if ($verbose);
print "$readmatchcount matched reads ($readmatchpct%)\n" if ($verbose);
$readmismatchpct = $readmismatchcount / $readcount * 100;
print "$seqmismatchcount mismatched sequences\n" if ($verbose);
print "$readmismatchcount mismatched reads ($readmismatchpct%)\n" if ($verbose);

$seqnomatchcount = $seqcount - $seqmatchcount - $seqmismatchcount;
$readnomatchcount = $readcount - $readmatchcount - $readmismatchcount;
$readnomatchpct = $readnomatchcount / $readcount * 100;
print "$seqnomatchcount no match sequences\n" if ($verbose); 
print "$readnomatchcount no match reads ($readnomatchpct%)\n" if ($verbose);

$starpct = &round100($readmismatchcount / ($readmismatchcount + $readmatchcount) * 100);
print "$starpct% star activity\n";

sub reverse_complement {
    my $dna = shift;

    # reverse the DNA sequence
    my $revcomp = reverse($dna);

    # complement the reversed DNA sequence
#    $revcomp =~ tr/ACGTacgt/TGCAtgca/;
    $revcomp =~ tr/ABCDGHMNRSTUVWXYabcdghmnrstuvwxy/TVGHCDKNYSAABWXRtvghcdknysaabwxr/;
    return $revcomp;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}
