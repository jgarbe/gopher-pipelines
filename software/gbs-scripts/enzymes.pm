
package enzymes;

# Don't edit this file directly: edit enzymes.txt then run "create-enzyme-package.pl > enzymes.pm" to generate this file

our @ISA = qw( Exporter );
our @EXPORT = qw( %enzymes %enzymeclasses cutter %padding);

%enzymes = (
"asisi" => {
name => "AsiSI",
sequence => "GCGAT|CGC",
overhang => "ATCGC",
},
"apeki" => {
name => "ApeKI",
sequence => "G|CWGC",
overhang => "CWGC",
},
"sexai" => {
name => "SexAI",
sequence => "A|CCWGGT",
overhang => "CCWGGT",
},
"tsp45i" => {
name => "Tsp45I",
sequence => "|GTSAC",
overhang => "GTSAC",
},
"naei" => {
name => "NaeI",
sequence => "GCC|GGC",
overhang => "GGC",
},
"hpych4iii" => {
name => "HpyCH4III",
sequence => "ACN|GT",
overhang => "NGT",
},
"stui" => {
name => "StuI",
sequence => "AGG|CCT",
overhang => "CCT",
},
"ngomiv" => {
name => "NgoMIV",
sequence => "G|CCGGC",
overhang => "CCGGC",
},
"fsei" => {
name => "FseI",
sequence => "GGCCGG|CC",
overhang => "CCGGCC",
},
"psti" => {
name => "PstI",
sequence => "CTGCA|G",
overhang => "TGCAG",
},
"afei" => {
name => "AfeI",
sequence => "AGC|GCT",
overhang => "GCT",
},
"pvui" => {
name => "PvuI",
sequence => "CGAT|CG",
overhang => "ATCG",
},
"bstyi" => {
name => "BstYI",
sequence => "R|GATCY",
overhang => "GATCY",
},
"haeiii" => {
name => "HaeIII",
sequence => "GG|CC",
overhang => "CC",
},
"bsshii" => {
name => "BssHII",
sequence => "G|CGCGC",
overhang => "CGCGC",
},
"bfuci" => {
name => "BfuCI",
sequence => "|GATC",
overhang => "GATC",
},
"xmai" => {
name => "XmaI",
sequence => "C|CCGGG",
overhang => "CCGGG",
},
"mboi" => {
name => "MboI",
sequence => "|GATC",
overhang => "GATC",
},
"dpni" => {
name => "DpnI",
sequence => "GA|TC",
overhang => "TC",
},
"msei" => {
name => "MseI",
sequence => "T|TAA",
overhang => "TAA",
},
"ecori" => {
name => "EcoRI",
sequence => "G|AATTC",
overhang => "AATTC",
},
"styi" => {
name => "StyI",
sequence => "C|CWWGG",
overhang => "CWWGG",
},
"swai" => {
name => "SwaI",
sequence => "ATTT|AAAT",
overhang => "AAAT",
},
"bstz17i" => {
name => "BstZ17I",
sequence => "GTA|TAC",
overhang => "TAC",
},
"avrii" => {
name => "AvrII",
sequence => "C|CTAGG",
overhang => "CTAGG",
},
"nsii" => {
name => "NsiI",
sequence => "ATGCA|T",
overhang => "TGCAT",
},
"haeii" => {
name => "HaeII",
sequence => "RGCGC|Y",
overhang => "GCGCY",
},
"rsrii" => {
name => "RsrII",
sequence => "CG|GWCCG",
overhang => "GWCCG",
},
"pspgi" => {
name => "PspGI",
sequence => "|CCWGG",
overhang => "CCWGG",
},
"nspi" => {
name => "NspI",
sequence => "RCATG|Y",
overhang => "CATGY",
},
"ppumi" => {
name => "PpuMI",
sequence => "RG|GWCCY",
overhang => "GWCCY",
},
"cviki-1" => {
name => "CviKI-1",
sequence => "RG|CY",
overhang => "CY",
},
"blpi" => {
name => "BlpI",
sequence => "GC|TNAGC",
overhang => "TNAGC",
},
"noti" => {
name => "NotI",
sequence => "GC|GGCCGC",
overhang => "GGCCGC",
},
"bsihkai" => {
name => "BsiHKAI",
sequence => "GWGCW|C",
overhang => "WGCWC",
},
"hinfi" => {
name => "HinfI",
sequence => "G|ANTC",
overhang => "ANTC",
},
"sfoi" => {
name => "SfoI",
sequence => "GGC|GCC",
overhang => "GCC",
},
"apoi" => {
name => "ApoI",
sequence => "R|AATTY",
overhang => "AATTY",
},
"bsaai" => {
name => "BsaAI",
sequence => "YAC|GTR",
overhang => "GTR",
},
"psii" => {
name => "PsiI",
sequence => "TTA|TAA",
overhang => "TAA",
},
"hpy166ii" => {
name => "Hpy166II",
sequence => "GTN|NAC",
overhang => "NAC",
},
"zrai" => {
name => "ZraI",
sequence => "GAC|GTC",
overhang => "GTC",
},
"asei" => {
name => "AseI",
sequence => "AT|TAAT",
overhang => "TAAT",
},
"ecot22i" => {
name => "EcoT22I",
sequence => "ATGCA|T",
overhang => "TGCAT",
},
"drai" => {
name => "DraI",
sequence => "TTT|AAA",
overhang => "AAA",
},
"sali" => {
name => "SalI",
sequence => "G|TCGAC",
overhang => "TCGAC",
},
"bcli" => {
name => "BclI",
sequence => "T|GATCA",
overhang => "GATCA",
},
"apai" => {
name => "ApaI",
sequence => "GGGCC|C",
overhang => "GGCCC",
},
"saci" => {
name => "SacI",
sequence => "GAGCT|C",
overhang => "AGCTC",
},
"hpai" => {
name => "HpaI",
sequence => "GTT|AAC",
overhang => "AAC",
},
"rsai" => {
name => "RsaI",
sequence => "GT|AC",
overhang => "AC",
},
"cac8i" => {
name => "Cac8I",
sequence => "GCN|NGC",
overhang => "NGC",
},
"kasi" => {
name => "KasI",
sequence => "G|GCGCC",
overhang => "GCGCC",
},
"nlaiii" => {
name => "NlaIII",
sequence => "CATG|",
overhang => "CATG",
},
"srfi" => {
name => "SrfI",
sequence => "GCCC|GGGC",
overhang => "GGGC",
},
"xhoi" => {
name => "XhoI",
sequence => "C|TCGAG",
overhang => "TCGAG",
},
"bsawi" => {
name => "BsaWI",
sequence => "W|CCGGW",
overhang => "CCGGW",
},
"spei" => {
name => "SpeI",
sequence => "A|CTAGT",
overhang => "CTAGT",
},
"alwni" => {
name => "AlwNI",
sequence => "CAGNNN|CTG",
overhang => "NNNCTG",
},
"taqai" => {
name => "TaqaI",
sequence => "T|CGA",
overhang => "CGA",
},
"bsiwi" => {
name => "BsiWI",
sequence => "C|GTACG",
overhang => "GTACG",
},
"alui" => {
name => "AluI",
sequence => "AG|CT",
overhang => "CT",
},
"avai" => {
name => "AvaI",
sequence => "C|YCGRG",
overhang => "YCGRG",
},
"scrfi" => {
name => "ScrFI",
sequence => "CC|NGG",
overhang => "NGG",
},
"taqalphai" => {
name => "TaqalphaI",
sequence => "T|CGA",
overhang => "CGA",
},
"bfai" => {
name => "BfaI",
sequence => "C|TAG",
overhang => "TAG",
},
"pvuii" => {
name => "PvuII",
sequence => "CAG|CTG",
overhang => "CTG",
},
"fati" => {
name => "FatI",
sequence => "|CATG",
overhang => "CATG",
},
"asci" => {
name => "AscI",
sequence => "GG|CGCGCC",
overhang => "CGCGCC",
},
"mspa1i" => {
name => "MspA1I",
sequence => "CMG|CKG",
overhang => "CKG",
},
"ncii" => {
name => "NciI",
sequence => "CC|SGG",
overhang => "SGG",
},
"bani" => {
name => "BanI",
sequence => "G|GYRCC",
overhang => "GYRCC",
},
"afliii" => {
name => "AflIII",
sequence => "A|CRYGT",
overhang => "CRYGT",
},
"sspi" => {
name => "SspI",
sequence => "AAT|ATT",
overhang => "ATT",
},
"acc65i" => {
name => "Acc65I",
sequence => "G|GTACC",
overhang => "GTACC",
},
"nhei" => {
name => "NheI",
sequence => "G|CTAGC",
overhang => "CTAGC",
},
"smli" => {
name => "SmlI",
sequence => "C|TYRAG",
overhang => "TYRAG",
},
"bsiei" => {
name => "BsiEI",
sequence => "CGRY|CG",
overhang => "RYCG",
},
"pcii" => {
name => "PciI",
sequence => "A|CATGT",
overhang => "CATGT",
},
"btgi" => {
name => "BtgI",
sequence => "C|CRYGG",
overhang => "CRYGG",
},
"clai" => {
name => "ClaI",
sequence => "AT|CGAT",
overhang => "CGAT",
},
"pluti" => {
name => "PluTI",
sequence => "GGCGC|C",
overhang => "GCGCC",
},
"avaii" => {
name => "AvaII",
sequence => "G|GWCC",
overhang => "GWCC",
},
"bsphi" => {
name => "BspHI",
sequence => "T|CATGA",
overhang => "CATGA",
},
"sacii" => {
name => "SacII",
sequence => "CCGC|GG",
overhang => "GCGG",
},
"pspomi" => {
name => "PspOMI",
sequence => "G|GGCCC",
overhang => "GGCCC",
},
"pmli" => {
name => "PmlI",
sequence => "CAC|GTG",
overhang => "GTG",
},
"banii" => {
name => "BanII",
sequence => "GRGCY|C",
overhang => "RGCYC",
},
"bstbi" => {
name => "BstBI",
sequence => "TT|CGAA",
overhang => "CGAA",
},
"pmei" => {
name => "PmeI",
sequence => "GTTT|AAAC",
overhang => "AAAC",
},
"hpy188i" => {
name => "Hpy188I",
sequence => "TCN|GA",
overhang => "NGA",
},
"taqi" => {
name => "TaqI",
sequence => "T|CGA",
overhang => "CGA",
},
"nlaiv" => {
name => "NlaIV",
sequence => "GGN|NCC",
overhang => "NCC",
},
"pasi" => {
name => "PasI",
sequence => "CC|CWGGG",
overhang => "CWGGG",
},
"fnu4hi" => {
name => "Fnu4HI",
sequence => "GC|NGC",
overhang => "NGC",
},
"ecoo109i" => {
name => "EcoO109I",
sequence => "RG|GNCCY",
overhang => "GNCCY",
},
"smai" => {
name => "SmaI",
sequence => "CCC|GGG",
overhang => "GGG",
},
"bamhi" => {
name => "BamHI",
sequence => "G|GATCC",
overhang => "GATCC",
},
"bstni" => {
name => "BstNI",
sequence => "CC|WGG",
overhang => "WGG",
},
"tth111i" => {
name => "Tth111I",
sequence => "GACN|NNGTC",
overhang => "NNGTC",
},
"ddei" => {
name => "DdeI",
sequence => "C|TNAG",
overhang => "TNAG",
},
"aflii" => {
name => "AflII",
sequence => "C|TTAAG",
overhang => "TTAAG",
},
"eco53ki" => {
name => "Eco53kI",
sequence => "GAG|CTC",
overhang => "CTC",
},
"aatii" => {
name => "AatII",
sequence => "GACGT|C",
overhang => "ACGTC",
},
"sau96i" => {
name => "Sau96I",
sequence => "G|GNCC",
overhang => "GNCC",
},
"bmti" => {
name => "BmtI",
sequence => "GCTAG|C",
overhang => "CTAGC",
},
"hinp1i" => {
name => "HinP1I",
sequence => "G|CGC",
overhang => "CGC",
},
"hpy188iii" => {
name => "Hpy188III",
sequence => "TC|NNGA",
overhang => "NNGA",
},
"bsaji" => {
name => "BsaJI",
sequence => "C|CNNGG",
overhang => "CNNGG",
},
"mspi" => {
name => "MspI",
sequence => "C|CGG",
overhang => "CGG",
},
"paci" => {
name => "PacI",
sequence => "TTAAT|TAA",
overhang => "ATTAA",
},
"cviqi" => {
name => "CviQI",
sequence => "G|TAC",
overhang => "TAC",
},
"scai" => {
name => "ScaI",
sequence => "AGT|ACT",
overhang => "ACT",
},
"ecorv" => {
name => "EcoRV",
sequence => "GAT|ATC",
overhang => "ATC",
},
"cviaii" => {
name => "CviAII",
sequence => "C|ATG",
overhang => "ATG",
},
"avaiii" => {
name => "AvaIII",
sequence => "ATGCA|T",
overhang => "TGCAT",
},
"bsrgi" => {
name => "BsrGI",
sequence => "T|GTACA",
overhang => "GTACA",
},
"bsski" => {
name => "BssKI",
sequence => "|CCNGG",
overhang => "CCNGG",
},
"msci" => {
name => "MscI",
sequence => "TGG|CCA",
overhang => "CCA",
},
"acli" => {
name => "AclI",
sequence => "AA|CGTT",
overhang => "CGTT",
},
"kpni" => {
name => "KpnI",
sequence => "GGTAC|C",
overhang => "GTACC",
},
"mluci" => {
name => "MluCI",
sequence => "|AATT",
overhang => "AATT",
},
"ncoi" => {
name => "NcoI",
sequence => "C|CATGG",
overhang => "CATGG",
},
"bsrfi" => {
name => "BsrFI",
sequence => "R|CCGGY",
overhang => "CCGGY",
},
"xbai" => {
name => "XbaI",
sequence => "T|CTAGA",
overhang => "CTAGA",
},
"ndei" => {
name => "NdeI",
sequence => "CA|TATG",
overhang => "TATG",
},
"hpych4iv" => {
name => "HpyCH4IV",
sequence => "A|CGT",
overhang => "CGT",
},
"sbfi" => {
name => "SbfI",
sequence => "CCTGCA|GG",
overhang => "TGCAGG",
},
"apali" => {
name => "ApaLI",
sequence => "G|TGCAC",
overhang => "TGCAC",
},
"hhai" => {
name => "HhaI",
sequence => "GCG|C",
overhang => "CGC",
},
"tspri" => {
name => "TspRI",
sequence => "NNCASTGNN|",
overhang => "NNCASTGNN",
},
"pspxi" => {
name => "PspXI",
sequence => "VC|TCGAGB",
overhang => "TCGAGB",
},
"bglii" => {
name => "BglII",
sequence => "A|GATCT",
overhang => "GATCT",
},
"tsei" => {
name => "TseI",
sequence => "G|CWGC",
overhang => "CWGC",
},
"acci" => {
name => "AccI",
sequence => "GT|MKAC",
overhang => "MKAC",
},
"nrui" => {
name => "NruI",
sequence => "TCG|CGA",
overhang => "CGA",
},
"bstui" => {
name => "BstUI",
sequence => "CG|CG",
overhang => "CG",
},
"draiii" => {
name => "DraIII",
sequence => "CACNNN|GTG",
overhang => "NNNGTG",
},
"sphi" => {
name => "SphI",
sequence => "GCATG|C",
overhang => "CATGC",
},
"bsp1286i" => {
name => "Bsp1286I",
sequence => "GDGCH|C",
overhang => "DGCHC",
},
"bsahi" => {
name => "BsaHI",
sequence => "GR|CGYC",
overhang => "CGYC",
},
"hincii" => {
name => "HincII",
sequence => "GTY|RAC",
overhang => "RAC",
},
"sgrai" => {
name => "SgrAI",
sequence => "CR|CCGGYG",
overhang => "CCGGYG",
},
"agei" => {
name => "AgeI",
sequence => "A|CCGGT",
overhang => "CCGGT",
},
"baegi" => {
name => "BaeGI",
sequence => "GKGCM|C",
overhang => "KGCKC",
},
"hpych4v" => {
name => "HpyCH4V",
sequence => "TG|CA",
overhang => "CA",
},
"tfii" => {
name => "TfiI",
sequence => "G|AWTC",
overhang => "AWTC",
},
"eaei" => {
name => "EaeI",
sequence => "Y|GGCCR",
overhang => "GGCCR",
},
"hindiii" => {
name => "HindIII",
sequence => "A|AGCTT",
overhang => "AGCTT",
},
"snabi" => {
name => "SnaBI",
sequence => "TAC|GTA",
overhang => "GTA",
},
"hpy99i" => {
name => "Hpy99I",
sequence => "CGWCG|",
overhang => "CGWCG",
},
"bsu36i" => {
name => "Bsu36I",
sequence => "CC|TNAGG",
overhang => "TNAGG",
},
"nari" => {
name => "NarI",
sequence => "GG|CGCC",
overhang => "CGCC",
},
"bspei" => {
name => "BspEI",
sequence => "T|CCGGA",
overhang => "CCGGA",
},
"mlui" => {
name => "MluI",
sequence => "A|CGCGT",
overhang => "CGCGT",
},
"sfci" => {
name => "SfcI",
sequence => "C|TRYAG",
overhang => "TRYAG",
},
"eagi" => {
name => "EagI",
sequence => "C|GGCCG",
overhang => "GGCCG",
},
"bsteii" => {
name => "BstEII",
sequence => "G|GTNACC",
overhang => "GTNACC",
},
"fspi" => {
name => "FspI",
sequence => "TGC|GCA",
overhang => "GCA",
},
"mfei" => {
name => "MfeI",
sequence => "C|AATTG",
overhang => "AATTG",
},
);

%enzymeclasses = (
"all" => "aatii,acc65i,acci,acli,afei,aflii,afliii,agei,alui,alwni,apai,apali,apeki,apoi,asci,asei,asisi,avai,avaii,avaiii,avrii,baegi,bamhi,bani,banii,bcli,bfai,bfuci,bglii,blpi,bmti,bsaai,bsahi,bsaji,bsawi,bsiei,bsihkai,bsiwi,bsp1286i,bspei,bsphi,bsrfi,bsrgi,bsshii,bsski,bstbi,bsteii,bstni,bstui,bstyi,bstz17i,bsu36i,btgi,cac8i,clai,cviaii,cviki-1,cviqi,ddei,dpni,drai,draiii,eaei,eagi,eco53ki,ecoo109i,ecori,ecorv,ecot22i,fati,fnu4hi,fsei,fspi,haeii,haeiii,hhai,hincii,hindiii,hinfi,hinp1i,hpai,hpy166ii,hpy188i,hpy188iii,hpy99i,hpych4iii,hpych4iv,hpych4v,kasi,kpni,mboi,mfei,mluci,mlui,msci,msei,mspa1i,mspi,naei,nari,ncii,ncoi,ndei,ngomiv,nhei,nlaiii,nlaiv,noti,nrui,nsii,nspi,paci,pasi,pcii,pluti,pmei,pmli,ppumi,psii,pspgi,pspomi,pspxi,psti,pvui,pvuii,rsai,rsrii,saci,sacii,sali,sau96i,sbfi,scai,scrfi,sexai,sfci,sfoi,sgrai,smai,smli,snabi,spei,sphi,srfi,sspi,stui,styi,swai,taqai,taqalphai,taqi,tfii,tsei,tsp45i,tspri,tth111i,xbai,xhoi,xmai,zrai",

"blunt" => "afei,alui,bsaai,bstui,bstz17i,cac8i,cviki-1,dpni,drai,eco53ki,ecorv,fspi,haeiii,hincii,hpai,hpy166ii,hpych4v,msci,mspa1i,naei,nlaiv,nrui,pmei,pmli,psii,pvuii,rsai,scai,sfoi,smai,snabi,srfi,sspi,stui,swai,zrai",

"blunteightcutters" => "pmei,srfi,swai",

"bluntfourcutters" => "alui,bstui,cviki-1,dpni,haeiii,hpych4v,rsai",

"bluntsixcutters" => "afei,bsaai,bstz17i,cac8i,drai,eco53ki,ecorv,fspi,hincii,hpai,hpy166ii,msci,mspa1i,naei,nlaiv,nrui,pmli,psii,pvuii,scai,sfoi,smai,snabi,sspi,stui,zrai",

"eightcutters" => "asci,asisi,fsei,noti,paci,pspxi,sbfi,sgrai",

"fivecutters" => "apeki,avaii,bsski,bstni,ddei,fnu4hi,hinfi,hpy188i,hpy99i,hpych4iii,ncii,pspgi,sau96i,scrfi,tfii,tsei,tsp45i",

"fourcutters" => "bfai,bfuci,cviaii,cviqi,fati,hhai,hinp1i,hpych4iv,mboi,mluci,msei,mspi,nlaiii,taqai,taqalphai,taqi",

"ninepluscutters" => "alwni,draiii,tspri,tth111i",

"sevencutters" => "blpi,bsteii,bsu36i,ecoo109i,pasi,ppumi,rsrii,sexai",

"sixcutters" => "aatii,acc65i,acci,acli,aflii,afliii,agei,apai,apali,apoi,asei,avai,avaiii,avrii,baegi,bamhi,bani,banii,bcli,bglii,bmti,bsahi,bsaji,bsawi,bsiei,bsihkai,bsiwi,bsp1286i,bspei,bsphi,bsrfi,bsrgi,bsshii,bstbi,bstyi,btgi,clai,eaei,eagi,ecori,ecot22i,haeii,hindiii,hpy188iii,kasi,kpni,mfei,mlui,nari,ncoi,ndei,ngomiv,nhei,nsii,nspi,pcii,pluti,pspomi,psti,pvui,saci,sacii,sali,sfci,smli,spei,sphi,styi,xbai,xhoi,xmai",

);

%padding = (
"nsii-r1" => ",A,TG,CAG,GCTA,ACAGA,CTCCGA,GAATCGA,AGGTCCAA,TCACGCTCA",
"psti-r1" => ",A,TG,CAG,GCTA,ACAGA,CTCCGA,GAATCGA,AGGTCCAA,TCACGCTCA",
"sbfi-r1" => ",A,TG,CAG,GCTA,ACAGA,CTCCGA,GAATCGA,AGGTCCAA,TCACGCTCA",
"apali-r1" => ",A,TG,CAG,GCTA,ACAGA,CTCCGA,GAATCGA,AGGTCCAA,TCACGCTCA",
"taqi-r1" => ",A,TG,CAG,GCTA,ACAGA,CTCCGA,GAATCGA,AGGTCCAA,TCACGCTCA",
"taqai-r1" => ",A,TG,CAG,GCTA,ACAGA,CTCCGA,GAATCGA,AGGTCCAA,TCACGCTCA",
"taqalphai-r1" => ",A,TG,CAG,GCTA,ACAGA,CTCCGA,GAATCGA,AGGTCCAA,TCACGCTCA",
"ecot22i-r1" => ",A,TG,CAG,GCTA,ACAGA,CTCCGA,GAATCGA,AGGTCCAA,TCACGCTCA",
"nsii-r2" => ",C,AC,GAC,AGAC,GAGAC,CGAGAC,ACGAGAC,CACGAGAC,CCACGAGAC",
"psti-r2" => ",C,AC,GAC,AGAC,GAGAC,CGAGAC,ACGAGAC,CACGAGAC,CCACGAGAC",
"sbfi-r2" => ",C,AC,GAC,AGAC,GAGAC,CGAGAC,ACGAGAC,CACGAGAC,CCACGAGAC",
"apali-r2" => ",C,AC,GAC,AGAC,GAGAC,CGAGAC,ACGAGAC,CACGAGAC,CCACGAGAC",
"taqi-r2" => ",C,AC,GAC,AGAC,GAGAC,CGAGAC,ACGAGAC,CACGAGAC,CCACGAGAC",
"taqai-r2" => ",C,AC,GAC,AGAC,GAGAC,CGAGAC,ACGAGAC,CACGAGAC,CCACGAGAC",
"taqalphai-r2" => ",C,AC,GAC,AGAC,GAGAC,CGAGAC,ACGAGAC,CACGAGAC,CCACGAGAC",
"ecot22i-r2" => ",C,AC,GAC,AGAC,GAGAC,CGAGAC,ACGAGAC,CACGAGAC,CCACGAGAC",
"bamhi-r1" => ",C,TC,ATT,CATT,GTATC,CGCATC,TCGGGTT,GATAAGCT,ACAAGTCGT,GTCCAATCGT",
"bstyi-r1" => ",C,TC,ATT,CATT,GTATC,CGCATC,TCGGGTT,GATAAGCT,ACAAGTCGT,GTCCAATCGT",
"bglii-r1" => ",C,TC,ATT,CATT,GTATC,CGCATC,TCGGGTT,GATAAGCT,ACAAGTCGT,GTCCAATCGT",
"bfuci-r1" => ",C,TC,ATT,CATT,GTATC,CGCATC,TCGGGTT,GATAAGCT,ACAAGTCGT,GTCCAATCGT",
"mboi-r1" => ",C,TC,ATT,CATT,GTATC,CGCATC,TCGGGTT,GATAAGCT,ACAAGTCGT,GTCCAATCGT",
"bamhi-r2" => ",G,AG,TCA,AAGT,ACGAA,ACTCTG,GTACGGT,TTCGACAT,CGATGTGCT",
"bstyi-r2" => ",G,AG,TCA,AAGT,ACGAA,ACTCTG,GTACGGT,TTCGACAT,CGATGTGCT",
"bglii-r2" => ",G,AG,TCA,AAGT,ACGAA,ACTCTG,GTACGGT,TTCGACAT,CGATGTGCT",
"bfuci-r2" => ",G,AG,TCA,AAGT,ACGAA,ACTCTG,GTACGGT,TTCGACAT,CGATGTGCT",
"mboi-r2" => ",G,AG,TCA,AAGT,ACGAA,ACTCTG,GTACGGT,TTCGACAT,CGATGTGCT",
"ncoi-r1" => ",C,TC,ATC,GGTC,GTATC,CGTATC,TCGGATC",
"ncoi-r2" => ",T,AC,TGT,AGTC,GCGTC,CTAGAC,GTCAGTC",
"pasi-r1" => "C,TC,ATC,GATC,GGATC,CGCATC,TCGTATC,AGGCTATC,CGTGTT,TTGTGTT,AAGCTATG",
"apeki-r1" => "C,TC,ATC,GATC,GGATC,CGCATC,TCGTATC,AGGCTATC,CGTGTT,TTGTGTT,AAGCTATG",
"pasi-r2" => "C,TC,GTC,AGTC,ATGAC,GACGAT,GAAGTAC,ATGATCGT,ATGGT,GATGTT,TGATATT,GAATAGCGT",
"apeki-r2" => "C,TC,GTC,AGTC,ATGAC,GACGAT,GAAGTAC,ATGATCGT,ATGGT,GATGTT,TGATATT,GAATAGCGT",
"btgi-r1" => ",C,TC,ATC,GGTC,GTATC,CGTATC,TCGGATC,ATG,GGTT,GTATG,TCGGATG,GATT,GTAAC,TCAGAAA,GATC,CGTAAC",
"btgi-r2" => ",T,AC,TGT,AGTC,GCGTC,CTAGAC,GTCAGTC,AGTG,GCTGT,CTGGAC,GTCAGTT,TAC,GCGTA,GTCAGTA,AGAC,GCGAA,GTCACCA",
"mspi-r1" => "T,TG,ACG,CGTA,AACTA,GTACTA,TCAGCTA,CGTCACTA,GACTAGCTA,CTGAGCACAA",
"nspi-r1" => "A,TG,CAG,GCTA,ACAGA,CTCCGA,GAATCGA,AGGTCCAA,TCACGCTCA",
);


%charcut = (
    "N" => .5,
    "R" => .5,
    "Y" => .5,
    "M" => .5,
    "K" => ,5,
    "S" => .5,
    "W" => .5,
    "H" => .25,
    "B" => .25,
    "V" => .25,
    "D" => .25,
    "A" => 1,
    "G" => 1,
    "C" => 1,
    "T" => 1,
    );

sub cutter {
    ($enzyme) = @_;
    if ($enzyme =~ /-/) { # double digest
	($e1, $e2) = split /-/, $enzyme;
	$x = &cutter($e1);
	$y = &cutter(\$e2);
	$cutter = ( (4**$x * 4**$y) / (4**$x + 4**$y) )**(1/4); # not correct
	$cutter = 0;
    } else {
	$sequence = $enzymes{$enzyme}{sequence};
	$sequence =~ s/\|//; # remove |
	@bases = split '', $sequence;
	$cutter = 0;
	foreach $base (@bases) {
	    $cutter += $charcut{$base};
	}
    }
    return $cutter;
}
1;
