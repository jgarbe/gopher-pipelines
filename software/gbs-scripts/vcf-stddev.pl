#!/usr/bin/perl -w

############################
# John Garbe
# February 2017
#
# Calculate variation in coverage at each marker in a multi-sample vcf file
#
#
###############################


$usage = "USAGE: vcf-stddev.pl file.vcf > output.txt";

die $usage unless ($#ARGV == 0);

$vcffile = shift @ARGV;
$metric = "DP";

open IFILE, $vcffile or die "cannot open vcf file $vcffile: $!\n";

while ($line = <IFILE>) {
    next if ($line =~ "^#");
    chomp $line;
    @line = split /\t/, $line;
    @ids = split /:/, $line[8];
    for $i (0..$#ids) {
	$ids{$ids[$i]} = $i;
    }

    $dropoutcount = 0;
    $coveragesum = 0;
    $samplecount = 0;
    @array = ();
    for $sample (9..$#line) {
#	print "metrics: $line[$sample]\n";
	$samplecount++;
	if ($line[$sample] eq ".") {
	    $dropoutcount++;
	    push @array, 0;
	} else {
#	    print "$coveragesum";
#	    print "$metric";
#	    print "$ids{$metric}";
	    @metrics = split /:/, $line[$sample];
	    if ($metrics[$ids{$metric}] eq ".") {
		$dropoutcount++;
		push @array, 0;
	    } else {
		$coveragesum += $metrics[$ids{$metric}];
		push @array, $metrics[$ids{$metric}];
	    }
	}
    }
    
    $stddev = &stdev(\@array);
    $mean = $coveragesum / $samplecount;
    
    print "$mean\t$stddev\t$dropoutcount\n";


}



sub average{
    my($data) = @_;
    if (not @$data) {
	die("Empty arrayn");
    }
    my $total = 0;
    foreach (@$data) {
	$total += $_;
    }
    my $average = $total / @$data;
    return $average;
}
sub stdev{
    my($data) = @_;
    if(@$data == 1){
	return 0;
    }
    my $average = &average($data);
    my $sqtotal = 0;
    foreach(@$data) {
	$sqtotal += ($average-$_) ** 2;
    }
    my $std = ($sqtotal / (@$data-1)) ** 0.5;
    return $std;
}
