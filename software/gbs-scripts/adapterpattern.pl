#!/usr/bin/perl -w

die "USAGE: adapterpattern.pl overhang padding\n" unless ($#ARGV == 1);

my ($overhang, $padding) = @ARGV;
$pattern = &expand($overhang);

# Add padding sequences to pattern
@paddings = split /,/, $padding;
$paddingpattern = join "|", @paddings;
#chop $paddingpattern;
$pattern = "^($paddingpattern)($pattern)";
#print "$paddingpattern\n";

#$pattern = "$paddingpattern$pattern";
print "$pattern\n";

exit;


sub expand {
    my ($pat) = @_;

#    $pat ||= $self->str;
    $pat =~ s/N|X/./g;
#    $pat =~ s/pu/R/ig;
#    $pat =~ s/py/Y/ig;
#    $pat =~ s/U/T/g;
#    $pat =~ s/^</\^/;
#    $pat =~ s/>$/\$/;

    my $PURINES      = 'AG';
    my $PYRIMIDINES  = 'CT';

    ## Avoid nested situations: [ya] --/--> [[ct]a]
    ## Yet correctly deal with: sg[ya] ---> [gc]g[cta]
    if($pat =~ /\[\w*[RYSWMK]\w*\]/) {
$pat =~ s/\[(\w*)R(\w*)\]/\[$1$PURINES$2\]/g;
$pat =~ s/\[(\w*)Y(\w*)\]/\[$1$PYRIMIDINES$2\]/g;
$pat =~ s/\[(\w*)S(\w*)\]/\[$1GC$2\]/g;
$pat =~ s/\[(\w*)W(\w*)\]/\[$1AT$2\]/g;
$pat =~ s/\[(\w*)M(\w*)\]/\[$1AC$2\]/g;
$pat =~ s/\[(\w*)K(\w*)\]/\[$1GT$2\]/g;
$pat =~ s/\[(\w*)V(\w*)\]/\[$1ACG$2\]/g;
$pat =~ s/\[(\w*)H(\w*)\]/\[$1ACT$2\]/g;
$pat =~ s/\[(\w*)D(\w*)\]/\[$1AGT$2\]/g;
$pat =~ s/\[(\w*)B(\w*)\]/\[$1CGT$2\]/g;
$pat =~ s/R/\[$PURINES\]/g;
$pat =~ s/Y/\[$PYRIMIDINES\]/g;
$pat =~ s/S/\[GC\]/g;
$pat =~ s/W/\[AT\]/g;
$pat =~ s/M/\[AC\]/g;
$pat =~ s/K/\[GT\]/g;
$pat =~ s/V/\[ACG\]/g;
$pat =~ s/H/\[ACT\]/g;
$pat =~ s/D/\[AGT\]/g;
$pat =~ s/B/\[CGT\]/g;
    } else {
$pat =~ s/R/\[$PURINES\]/g;
$pat =~ s/Y/\[$PYRIMIDINES\]/g;
$pat =~ s/S/\[GC\]/g;
$pat =~ s/W/\[AT\]/g;
$pat =~ s/M/\[AC\]/g;
$pat =~ s/K/\[GT\]/g;
$pat =~ s/V/\[ACG\]/g;
$pat =~ s/H/\[ACT\]/g;
$pat =~ s/D/\[AGT\]/g;
$pat =~ s/B/\[CGT\]/g;
    }
    $pat =~ s/\((.)\)/$1/g;  ## Doing thses last since:
    $pat =~ s/\[(.)\]/$1/g;  ## Pattern could contain [y] (for example)

    return $pat;
}
