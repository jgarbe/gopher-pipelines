#!/usr/bin/perl 

# create an enzyme perl module that defines a hash with all enzyme names, recognition sequences, and overhagn sequences. Created from enzyme text file.

$enzymefile = "enzymes.txt";
$paddingfile = "padding.txt";

open IFILE, $enzymefile or die "Cannot open enzyme file $enzymefile: $!\n";
while ($line = <IFILE>) {
    chomp $line;
    next if ($line =~ /^#/); # skip commented out enzymes
    ($name, $sequence) = split ' ', $line;
    $enzyme = lc($name);
    $enzymes{$enzyme}{name} = $name;
    $enzymes{$enzyme}{sequence} = $sequence;
    ($a, $b) = split /\|/, $sequence;
    if (length($a) > length($b)) {
       $overhang = rc($a);
    } else {
       $overhang = $b;
    }
    $enzymes{$enzyme}{overhang} = $overhang;
    $length = length($enzymes{$enzyme}{sequence}) - 1; # subtract one for |
    $class = "onecutters" if ($length == 1);
    $class = "twocutters" if ($length == 2);
    $class = "threecutters" if ($length == 3);
    $class = "fourcutters" if ($length == 4);
    $class = "fivecutters" if ($length == 5);
    $class = "sixcutters" if ($length == 6);
    $class = "sevencutters" if ($length == 7);
    $class = "eightcutters" if ($length == 8);
    $class = "ninepluscutters" if ($length > 8);
    if (length($a) == length($b)) {
	$enzymeclasses{"blunt"}{$enzyme} = 1 ;
	$class = "blunt$class";
    }
    $enzymeclasses{$class}{$enzyme} = 1;
    $enzymeclasses{"all"}{$enzyme} = 1;
}
close IFILE;

### Print out enzyme.pm ###
print qq[
package enzymes;

# Don't edit this file directly: edit enzymes.txt then run \"create-enzyme-package.pl > enzymes.pm\" to generate this file

our \@ISA = qw( Exporter );
our \@EXPORT = qw( %enzymes %enzymeclasses cutter %padding);

];

# %enzymes
print "\%enzymes = (\n";
foreach $name (sort {$a->{name} cmp $b->{name} } keys %enzymes) {
    print "\"$name\" => {\n";
    print "name => \"$enzymes{$name}{name}\",\n";
    print "sequence => \"$enzymes{$name}{sequence}\",\n";
    print "overhang => \"$enzymes{$name}{overhang}\",\n";
    print "},\n";
}
print ");\n\n";

# %enzymeclasses
print "\%enzymeclasses = (\n";
foreach $class (sort keys %enzymeclasses) {
    $list = "";
    foreach $enzyme (sort keys %{ $enzymeclasses{$class} }) {
	$list = "$list,$enzyme";
    }
    $list =~ s/^,//; # get rid of first comma
    print "\"$class\" => \"$list\",\n\n";
}
print ");\n\n";

# read in adapter padding sequences, print out to module
open IFILE, $paddingfile or die "Cannot open padding file $padding: $!\n";
print "\%padding = (\n";
while ($line = <IFILE>) {
    chomp $line;
    next if ($line =~ /^#/); # skip comments
    ($enzyme, $padding) = split /\t/, $line;
    next unless ($enzyme and $padding); # skip blank or malformed lines
    print "\"$enzyme\" => \"$padding\",\n";
}
print ");\n\n";

# cutter function
print q(
%charcut = (
    "N" => .5,
    "R" => .5,
    "Y" => .5,
    "M" => .5,
    "K" => ,5,
    "S" => .5,
    "W" => .5,
    "H" => .25,
    "B" => .25,
    "V" => .25,
    "D" => .25,
    "A" => 1,
    "G" => 1,
    "C" => 1,
    "T" => 1,
    );

sub cutter {
    ($enzyme) = @_;
    if ($enzyme =~ /-/) { # double digest
	($e1, $e2) = split /-/, $enzyme;
	$x = &cutter($e1);
	$y = &cutter(\$e2);
	$cutter = ( (4**$x * 4**$y) / (4**$x + 4**$y) )**(1/4); # not correct
	$cutter = 0;
    } else {
	$sequence = $enzymes{$enzyme}{sequence};
	$sequence =~ s/\|//; # remove |
	@bases = split '', $sequence;
	$cutter = 0;
	foreach $base (@bases) {
	    $cutter += $charcut{$base};
	}
    }
    return $cutter;
}
);



# end of module
print "1;\n";

############################ subs #############################

sub rc {
    my $dna = shift;

        # reverse the DNA sequence
    my $revcomp = reverse($dna);

        # complement the reversed DNA sequence
    $revcomp =~ tr/ABCDGHMNRSTUVWXYabcdghmnrstuvwxy/TVGHCDKNYSAABWXRtvghcdknysaabwxr/;
    return $revcomp;
}
