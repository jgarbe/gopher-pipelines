#!/usr/bin/perl -w

#####################################
# vcffilter.pl
# John Garbe
# April 2017
# 
#####################################

=head1 DESCRIPTION

vcffilter.pl -  remove variants with too many missing genotype calls and remove samples with too many missing genotype calls

=head1 SYNOPSIS

vcffilter.pl --vcffile input.vcf --outputfile output.vcf [--samplecutoff 0.95] [--markercutoff 0.95] [--maf 0.01] [--mindp 0] [--thin 0]

=head1 OPTIONS

 --vcffile file : Input vcf file to filter
 --outputfile file : name of output vcf file
 --samplecutoff 0.95 : Samples/individuals with genotyping call rate less than threshold will be excluded
 --markercutoff 0.95 : Markers/variants with genotyping call rate less than threshold will be excluded
 --maf 0.01 : Minimum minor allele frequency
 --mindp 0 : Remove genotype calls based on fewer than this many reads
 --thin 0 : minimum distance between markers (bp)
 --help : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib "$FindBin::RealBin";
use enzymes;
use Cwd 'abs_path';

# set defaults
$args{samplecutoff} = 0.95;
$args{markercutoff} = 0.95;
$args{maf} = 0.01;
$args{mindp} = 0;
$args{thin} = 0;
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
#	   "threads=i" => \$threads,
	   "samplecutoff=f" => \$args{samplecutoff},
	   "markercutoff=f" => \$args{markercutoff},
	   "maf=f" => \$args{maf},
	   "mindp=i" => \$args{mindp},
	   "thin=i" => \$args{thin},
	   "vcffile=s" => \$args{vcffile},
	   "outputfile=s" => \$args{outputfile},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
pod2usage unless ($args{vcffile} and $args{outputfile});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
die "Cannot find VCF file $args{vcffile}\n" if (! -e $args{vcffile});
$args{vcffile} = abs_path($args{vcffile});

# determine genotype call rate per individual, with missing genotype call rate per variant cutoff applied
open IPIPE, "vcftools --vcf $args{vcffile} --max-missing $args{markercutoff} --minDP $args{mindp} --thin $args{thin} --maf $args{maf} --missing-indv --stdout 2>/dev/null | " or die "cannot open pipe from vcftools: $!\n";

# parse output, identify samples to exclude
#INDV    N_DATA    N_GENOTYPES_FILTERED    N_MISS    F_MISS
$remove = "";
$header = <IPIPE>;
chomp $header;
@header = split /\t/, $header;
$badsamplecount = 0;
$samplecount = 0;
while ($line = <IPIPE>) {
    chomp $line;
    @line = split /\t/, $line;
    $samplecount++;
    if ($line[4] > (1-$args{samplecutoff})) {
	$remove .= "--remove-indv $line[0] ";
	$badsamplecount++;
    }
}

# generate new vcffile with bad samples and markers excluded
$result = `vcftools --vcf $args{vcffile} --max-missing $args{markercutoff} --minDP $args{mindp} --thin $args{thin} --maf $args{maf} --recode $remove --stdout > $args{outputfile} 2>/dev/null`;
print $result if ($args{verbose});

$before = `grep -v "^#" $args{vcffile} | wc -l`;
chomp $before;
$after = `grep -v "^#" $args{outputfile} | wc -l`;
chomp $after;

print "After filtering, kept $after out of a possible $before markers\n";
print "$badsamplecount bad samples filtered out from a total of $samplecount samples\n";


