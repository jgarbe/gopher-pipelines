#!/usr/bin/perl -w

use File::Basename;

# run uclust on a bunch of (trimmed gbs) fastq files
$threads = 40;

$fastqfolder = shift @ARGV or die "USAGE: uclust.pl fastqfolder\n";

$identity = $ARGV[0] // .95;

@fastqs = `ls $fastqfolder/*.fastq`;
chomp @fastqs;

die "Cannot find any *.fastq files in $fastqfolder\n" if ($#fastqs < 0);
$files = $#fastqs + 1;
print "Found $files fastq files\n";

my $myscratchfolder = "uclust";
mkdir $myscratchfolder;

# run gnu parallel
$gpout = "$myscratchfolder/gp.out";
open GP, "| parallel --progress -j $threads > $gpout" or die "Cannot open pipe to gnu parallel\n";

# send commands to gnu parallel
# uclust doesn't scale well, so just use threads = 2 all the time
foreach $fastq (@fastqs) {
    ($name) = fileparse($fastq);
    $sample = $name;
    $sample =~ s/\.fastq//;
    push @samples, $sample;
    print GP "usearch -cluster_fast $fastq -id $identity -threads 2 -centroids $myscratchfolder/$sample.centroids -uc $myscratchfolder/$sample.clusters &> $myscratchfolder/$sample.log\n";
}
close GP;
print "error code: $?\n" if ($?);

# process results
$ofile = "$myscratchfolder/uclust.dat";
open OFILE, ">$ofile" or die "cannot open $ofile: $!\n";
print OFILE "Sample\tCount\tValue\n";
$ofile2 = "$myscratchfolder/uclust.samplecounts.dat";
open OFILE2, ">$ofile2" or die "cannot open $ofile2: $!\n";
print OFILE2 "Sample\tCount\n";
foreach $sample (@samples) {
    $total = 0;
    %counts = ();
    $ifile = "$myscratchfolder/$sample.clusters";
    open IFILE, $ifile or die "cannot open $ifile: $!\n";
    while ($line = <IFILE>) {
	next unless ($line =~ /^C/);
	chomp $line;
	@line = split /\t/, $line;
	$count = $line[2];
	$counts{$count}++;
	$total += $count;
    }
    $minimum = 10 * ($total / 1000000);
    foreach $count (keys %counts) {
	if ($count > $minimum) {
	    $stats->{$sample}{uclust}{loci} += $counts{$count};
	    print OFILE "$sample\t$count\t$counts{$count}\n";
	}
    }
    print OFILE2 "$sample\t$stats->{$sample}{uclust}{loci}\n";
}
close OFILE;
close OFILE2;

# generate plots
`uclustplot.pl $ofile $ofile2`;

