#!/usr/bin/perl -w 

die "USAGE: vcflocimarkers.pl file.vcf\n" unless ($#ARGV == 0);
$file = shift @ARGV;
die "Cannot read file $file\n" unless (-r $file);
$result = `grep -v "^#" $file | cut -f 3 | cut -f 1 -d '_' | sort | uniq | wc -l`;
print $result;

