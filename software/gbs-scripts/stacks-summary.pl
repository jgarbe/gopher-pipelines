#!/usr/bin/perl -w

use File::Basename;

$usage = "USAGE: stacks-summary.pl sample.enzyme.tags.tsv\n";
die $usage unless ($#ARGV == 0);

$ifile = shift @ARGV;

$sample = fileparse($ifile);
$sample =~ s/\.tags\.tsv//;

open IFILE, $ifile or die "cannot open $ifile: $!\n";

$notfirst = 0;
$locus = 0;
$count = 0;
while ($line = <IFILE>) {
    next if ($line =~ /^#/); # skip coments
    @line = split /\t/, $line;
    if ($line[6] eq "consensus") {
	next unless ($notfirst);
	$locus++;
#	print "$locus\t$count\n";
	$depth{$count}++;
	$count = 0;
    } elsif ($line[6] eq "primary") {
	$count++;
	$notfirst = 1;
    }
}

# Sample Metric Depth Count
# plot Depth by count, color=Sample, facet_wrap=metric
#print "Loci depth\tCount\n";
#foreach $depth (sort {$a<=>$b} keys %depth) {
#    print "$depth\t$depth{$depth}\n";
#}
for $i (0..20) {
    $value = $depth{$i} // 0;
    print "$sample\tLoci-depth\t$i\t$value\n";
}
$value = 0;
for $i (reverse 0..20) {
    $value += $depth{$i} // 0;
    print "$sample\tLoci-depth-cummulative\t$i\t$value\n";
}


#$ifile = "$enzyme.trim.alleles.tsv";
$ifile =~ s/\.tags\./\.alleles\./;
open IFILE, $ifile or die "cannot open $ifile: $!\n";

$locus = 0;
$allelecount = 0;
$depthcount = 0;
while ($line = <IFILE>) {
    next if ($line =~ /^#/); # skip coments
    chomp $line;
    @line = split /\t/, $line;
    if ($line[2] ne $locus) {
	if ($locus ne "0") {
#	    print "$locus\t$allelecount\t$depthcount\n";
	    $depthcount{$depthcount}++;
	    $allelecount{$allelecount}++;
	}
	$notfirst = 1;
	$locus = $line[2];
	$depthcount = $line[5];
	$allelecount = 1;
    } else {
	$depthcount += $line[5];
	$allelecount++;
    }
}

#print "Alleles\tCount\n";
for $i (0..20) {
    $value = $allelecount{$i} // 0;
    print "$sample\tAlleles\t$i\t$value\n";
}

#print "Depth\tCount\n";
for $i (0..20) {
    $value = $depthcount{$i} // 0;
    print "$sample\tAllele-depth\t$i\t$value\n";
}



