#!/usr/bin/perl -w

#####################################
# tassel-convert.pl 
# John Garbe
# January 2018
# 
#####################################

=head1 DESCRIPTION

tassel-convert.pl - 
 - Generate a tassel key file and symlink fastq files to new tassel-compliant filenames

=head1 SYNOPSIS

tassel-convert.pl --enzyme SbfI [--padding ,A,AG,TGC] --fastqfolder

=head1 OPTIONS

Options:
    --enzyme enzyme : Enzyme used to digest sample
    --help : Display usage information
b
    --padding C,TG,AAG,GCTC : comma-separated list of phasing pads. To include a pad of zero start with a comma: ,C,TG,AAG,GCTC
    --fastqfolder folder : folder with fastq files to process

=cut

##################### Initialize ###############################

use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib "$FindBin::RealBin";
use enzymes;
use File::Basename;

# set defaults

GetOptions("help" => \$help,
	   "verbose" => \$verbose,
#	   "threads=i" => \$threads,
	   "enzyme=s" => \$enzyme,
	   "padding=s" => \$padding,
	   "fastqfolder=s" => \$fastqfolder,
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($help);
pod2usage unless ($fastqfolder);
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}

die "Cannot find fastq folder $fastqfolder\n" if (! -e $fastqfolder);
die "Unknown enzyme $enzyme\n" unless ($enzymes{lc($enzyme)});
$pad = lc($enzyme) . "-r1";
if (! $padding) {
    print "looking for padding for $pad\n" if ($verbose);
    $padding = $padding{$pad};
}
print "padding: $padding\n" if ($verbose);
@padding = split ",", $padding;

# Get list of fastq files
@fastqs = `find $fastqfolder -name "*.fastq"`;
if ($#fastqs < 0) {
    @fastqs = `find $fastqfolder -mindepth 1 -maxdepth 1 -name "*.fastq.gz"`;
    die "No .fastq or .fastq.gz files found in $fastqfolder\n" if ($#fastqs < 0);
}

$keyfile = "keyfile.txt";
open OFILE, ">$keyfile" or die "cannot open keyfile $keyfile: $!\n";
print OFILE "Flowcell\tLane\tBarcode\tFullSampleName\n";
mkdir "tassel-fastq";
chomp @fastqs;
$samplenumber = 1;
foreach $fastq (@fastqs) {
    my ($fname, $path, $mysuffix) = fileparse($fastq, (".fastq", ".fastq.gz"));
    $mysuffix =~ s/^\.//;
    $sample = "";
    $read = "";
    if ($fname =~ /(.*)_S[\d]{1,4}_(R[12])_[0-9]{3}$/) {
	# standard new-style concatenated (no lane number) filenames
	print STDERR "Filename format: new-style concatenated\n" if ($verbose);
	$sample = $1;
	$read = $2;
    } elsif ($fname =~ /(.*)_(R[12])$/) {
	# standard new-style concatenated (no lane number) filenames
	print STDERR "Filename format: new-style concatenated\n" if ($verbose);
	$sample = $1;
	$read = $2;
    } else {
	print "Unable to parse $fname, skipping\n";
	next;
    }

    `ln -s ../$fname.$mysuffix tassel-fastq/Flowcell${samplenumber}_1_$mysuffix`;
    foreach $padding (@padding) {
	print OFILE "Flowcell$samplenumber\t1\t$padding\t$sample\n";
    }

    $samplenumber++;
}

close OFILE;
    
