#!/usr/bin/perl

#####################################
# coverage-summary.pl 
# John Garbe
# February 2017
#####################################

=head1 NAME

coverage-summary.pl - Determine the number of loci with a minimum length of --blockminlength and minimum depth of --blockmindepth

=head1 SYNOPSIS

USAGE: coverage-summary.pl --bamfile bwa.bam --referencefai genome.fa.fai > out.txt"

=head1 DESCRIPTION

Options:
b
    --bamfile bwa.bam : bam  alignment file
    --blockminlength 40 : Mimimum block length
    --blockmindepth 10 : Minimum block coverage depth
    --referencefai genome.fa.fai : Fai index of reference genome
    --outputfile sample.gbstrim.fastq : fastq file of processed reads (not implemented)
    --help : Display usage information
=cut

##################### Initialize ###############################

use Getopt::Long;
use Pod::Usage;

# set defaults
$blockmindepth = 10;
$blockminlength = 40;
GetOptions("help" => \$help,
	   "verbose" => \$verbose,
#	   "threads=i" => \$threads,
	   "bamfile=s" => \$bamfile,
	   "referencefai=s" => \$referencefai,
	   "blockminlength=i" => \$blockminlength,
	   "blockmindepth=i" => \$blockmindepth,
	   "outputfile=s" => \$outputfile,
    ) or pod2usage;
pod2usage(q(-verbose) => 3) if ($help);
pod2usage unless ($referencefai and $bamfile);
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
die "Cannot find bam file $bamfile\n" if (! -e $bamfile);
die "Cannot find fai file $referencefai\n" if (! -e $referencefai);

$coveragefile = "coverage.tmp";
$result = `bedtools genomecov -dz -ibam $bamfile -g $referencefai > $coveragefile`;

$bedfile = "coverage.bed";
open BEDFILE, ">$bedfile" or die "cannot open output file $bedfile: $!\n";
$llfile = "coverage.ll.tmp";
open LLFILE, ">$llfile" or die "cannot open output file $llfile: $!\n";
$lsfile = "coverage.ls.tmp";
open LSFILE, ">$llfile" or die "cannot open output file $lsfile: $!\n";

open IFILE, $coveragefile or die "Cannot open $coveragefile: $!\n";
$inlocus = 0;
$end = 0;
$pos = 0;
while ($line = <IFILE>) {
    chomp $line;
    $lastpos = $pos;
    ($chr, $pos, $cov) = split /\t/, $line;
    # not in a locus
    if (! $inlocus) {
	next if ($cov < $blockmindepth);
	# start of new locus
	$inlocus = 1;
	$locusdepthsum = $cov;
	$locuslength = 1;
    } else {
	# reached end of locus
	if (($cov < $blockmindepth) or ($pos - $lastpos != 1)) {
	    $inlocus = 0;
	    $locusaveragedepth = int($locusdepthsum / $locuslength);
	    $start = $lastpos - $locuslength + 1;
#	    print "$locusaveragedepth\t$locuslength\n";
	    # locus too short
	    if ($locuslength < $blockminlength) {
		$shortlocicounts{$locusaveragedepth}++;
	    # locus long enough
	    } else {
		$locicounts{$locusaveragedepth}++;
		print BEDFILE "$chr\t$start\t$lastpos\n";
	    }
	    print LLFILE "$locuslength\n";
	    $locusspace = $lastpos - $end;
	    $end = $lastpos;
	    print LSFILE "$locusspace\n";
	# in locus
	} else {  
	    $locusdepthsum += $cov;
	    $locuslength++;
	}
    }
}
close IFILE;

$locisum = 0;
$sum = 0;
print "#Loci with minimum coverage $blockmindepth and minimum length $blockminlength:\n";
print "depth\t#loci\n";
foreach $depth (sort {$b <=> $a} keys %locicounts) {
    $sum += $locicounts{$depth};
    print "$depth\t$sum\n";
#    print "$depth\t$locicounts{$depth}\n";
    $locisum += $locicounts{$depth};
}
print "#$locisum total good loci\n";

exit;

$locisum = 0;
print "Loci with minimum coverage $mindepth and length less than $minlength:\n";
foreach $depth (sort {$a <=> $b} keys %shortlocicounts) {
    print "$depth\t$shortlocicounts{$depth}\n";
    $locisum += $shortlocicounts{$depth};
}
print "$locisum total short loci\n";
