#!/usr/bin/perl -w

# plot uclust data

$ifile = shift @ARGV || die "USAGE: uclustplot.pl uclust.dat uclust.samplecounts.dat\n";
$ifile2 = shift @ARGV || die "USAGE: uclustplot.pl uclust.dat uclust.samplecounts.dat\n";

# Run R to generate plot
my $r_script = "uclustplot.r";
my $width = 480;
open(Rcmd,">", $r_script) or die "Cannot open $r_script \n\n";
print Rcmd "options(warn=-1)";
print Rcmd qq(
  library("ggplot2");
  png(filename="uclustplot.png",width=$width);
  datat <- read.table("$ifile", header=T, colClasses=c("Sample"="factor"));

ggplot(datat, aes(Count, Value, color=Sample)) + geom_line() + ggtitle("Title") + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Number of Loci') + xlab('Number of Reads') + xlim(c(0,100))

  #eof) . "\n";

close Rcmd;
system("R --no-restore --no-save --no-readline < $r_script > $r_script.out");

# Run R to generate plot
$r_script = "uclustplot2.r";
$width = 1000;
open(Rcmd,">", $r_script) or die "Cannot open $r_script \n\n";
print Rcmd "options(warn=-1)";
print Rcmd qq(
  library("ggplot2");
  png(filename="uclustplot2.png",width=$width);
  datat <- read.table("$ifile2", header=T, colClasses=c("Sample"="factor"));

ggplot(datat, aes(reorder(Sample, -Count), Count, fill=Sample)) + geom_bar(stat="identity") + ggtitle("Title") + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Number of Loci') + xlab('Sample') 

# + facet_wrap(~Metric, scales='free_y', ncol=1) 

  #eof) . "\n";

close Rcmd;
system("R --no-restore --no-save --no-readline < $r_script > $r_script.out");

exit;

################# subs ###################

