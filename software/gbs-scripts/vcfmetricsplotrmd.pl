#!/usr/bin/perl -w

###############################################################
#
# John Garbe
# March 2017
#
###############################################################

=head1 DESCRIPTION

Generate summary statistics and plots from a vcf file, either single or multi-sample.

=head2 OUTPUT

  plots
  summary metric files

=head1 SYNOPSIS

vcfmetricsplot.pl --vcffile file [--prefix filt] [--samplesheet samplesheet.txt]

vcfmetricsplot.pl --help

=head1 OPTIONS

 --vcffile file : a vcf file
 --prefix string : prefix to attached to png file names
 --samplesheet file : Samplesheet with metadata to add to pca plot
 --help : Print usage instructions and exit
 --verbose : Print more information while running

=cut

###############################################################

use Getopt::Long;
use File::Basename;
use Pod::Usage;

$args{prefix} = "";
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "vcffile:s" => \$args{vcffile},
	   "samplesheet:s" => \$args{samplesheet},
	   "prefix:s" => \$args{prefix},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
pod2usage unless ($args{vcffile});

die "Cannot find vcffile $args{vcffile}\n" unless (-e $args{vcffile});

($vcfname) = fileparse($args{vcffile});
die "Compressed VCF file not supported\n" if ($vcfname =~ /\.gz$/);

foreach $outputoption ("--depth", "--het", "--missing-indv", "--site-depth", "--site-mean-depth", "--hardy", "--site-quality", "--missing-site") {
    $command = "vcftools --vcf $args{vcffile} $outputoption 2>&1";
# consider --SNPdensity integer and --TsTv-summary 

    $result = `$command`;
    print $result if ($args{verbose});;
}

# determine the number of samples
$numsamples = `wc -l < out.het`;
chomp $numsamples;
$numsamples--;

### Plotly Time! ###

### Generate depth per sample plot ###
# out.idepth
# INDV    N_SITES MEAN_DEPTH

$name = $args{prefix}. "sample-depth";
$cleanname = $name;
$cleanname =~ s/[^A-Za-z]/-/g; # periods mess up some rmd functions
$ofile = "$name.dat";
$rfile = "$name.rmd";
$datafile = "out.idepth"; # "$reportfolder/$name.dat";
print "Generating depth per sample plot\n" if ($args->{verbose});
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n";
# read in the data and print it out
open IFILE, $datafile or die "cannot open $datafile: $!\n";
$header = <IFILE>;
print OFILE "sample\tvalue\n";
while ($line = <IFILE>) {
    chomp $line;
    my ($sample, $sites, $mean) = split /\t/, $line;
    $data{$sample}{meandepth} = $mean;
    print OFILE "$sample\t$mean\n";
}
close OFILE;
close IFILE;

# generate order for report sorting
@order = sort {$data{$b}{meandepth} <=> $data{$a}{meandepth}} keys %data;
foreach $sample (@order) {
    $order .= "\"$sample\",";
}
chop $order; # remove last ,
print "ORDER\tMean depth per sample\t$order\n";

$title = "Mean depth per Sample";
$text = "The mean read depth across all variants per sample is shown.";
$updatemenus = "updatemenus = updatemenus,";
$updatemenus = ""; # disable the sort buttons

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$cleanname" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$cleanname" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~value,
 name = "Mean read depth",
 type = "bar", marker = list(color = '#1E77B4'),
hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Depth:",format(datat\$value,big.mark=",",scientific=FALSE))
) %>% 
 layout(xaxis = sampleorder, $updatemenus dragmode='pan', barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Mean depth", fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;


### Generate missingness per sample plot ###
# out.imiss
# INDV    N_DATA    N_GENOTYPES_FILTERED    N_MISS    F_MISS

$name = $args{prefix}. "sample-miss";
$cleanname = $name;
$cleanname =~ s/[^A-Za-z]/-/g; # periods mess up some rmd functions
$ofile = "$name.dat";
$rfile = "$name.rmd";
$datafile = "out.imiss";
print "Generating sample missingness plot\n" if ($args->{verbose});
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n";
# read in the data and print it out
open IFILE, $datafile or die "cannot open $datafile: $!\n";
$header = <IFILE>;
print OFILE "sample\tvalue\n";
while ($line = <IFILE>) {
    chomp $line;
    my ($sample, $junk1, $junk2, $junk3, $miss) = split /\t/, $line;
    $data{$sample}{miss} = $miss;
    print OFILE "$sample\t$miss\n";
}
close OFILE;
close IFILE;

# generate order for report sorting
@order = sort {$data{$b}{miss} <=> $data{$a}{miss}} keys %data;
foreach $sample (@order) {
    $order .= "\"$sample\",";
}
chop $order; # remove last ,
print "ORDER\tMissing genotypes per sample\t$order\n";

$title = "Missing genotypes per Sample";
$text = "The fraction of missing genotype calls per sample is shown.";
$updatemenus = "updatemenus = updatemenus,";
$updatemenus = ""; # disable the sort buttons

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$cleanname" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$cleanname" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~value,
 name = "Mean read depth",
 type = "bar", marker = list(color = '#1E77B4'),
hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Missing:",format(datat\$value,big.mark=",",scientific=FALSE),"%")
) %>% 
 layout(xaxis = sampleorder, $updatemenus dragmode='pan', barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Fraction missing genotype calls in sample", fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;


### Generate missingness per sample plot ###
# out.lmiss
# CHR    POS    N_DATA    N_GENOTYPE_FILTERED    N_MISS    F_MISS

$name = $args{prefix}. "site-miss";
$cleanname = $name;
$cleanname =~ s/[^A-Za-z]/-/g; # periods mess up some rmd functions
$ofile = "$name.dat";
$rfile = "$name.rmd";
$datafile = "out.lmiss";
print "Generating site missingness plot\n" if ($args->{verbose});
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n";
# read in the data and print it out
open IFILE, $datafile or die "cannot open $datafile: $!\n";
$header = <IFILE>;
print OFILE "bin\tvalue\n";
$total = 0;
while ($line = <IFILE>) {
    chomp $line;
    my ($junk1, $junk2, $junk3, $junk4, $junk5, $miss) = split /\t/, $line;
    $bin = int($miss * 100);
    $sitedata[$bin]++;
    $total++;
#    $sitedata{miss} = $miss;
#    print OFILE "$miss\n";
}
for $bin (0..100) {
    $value = $sitedata[$bin] // 0;
    $value = $value / $total;
    print OFILE "$bin\t$value\n";
}
close OFILE;
close IFILE;

# generate order for report sorting
#@order = sort {$data{$b}{miss} <=> $data{$a}{miss}} keys %data;
#foreach $sample (@order) {
#    $order .= "\"$sample\",";
#}
#chop $order; # remove last ,
#print "ORDER\tMissing genotypes per sample\t$order\n";

$title = "Missing genotypes per site";
$text = "The fraction of missing genotype calls per site is shown.";
$updatemenus = "updatemenus = updatemenus,";
$updatemenus = ""; # disable the sort buttons

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$cleanname" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$cleanname" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T);

config(plot_ly(
  data=datat,
  x = ~bin,
  y = ~value,
 name = "Site missingness",
 type = "bar", marker = list(color = '#1E77B4')
) %>% 
 layout(dragmode='pan', xaxis = list(title = "Fraction missing genotypes", range = c(-1,101)), yaxis = list(title = "Number of markers", fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;


### Generate PCA plot ###

$name = $args{prefix}. "pca";
$cleanname = $name;
$cleanname =~ s/[^A-Za-z]/-/g; # periods mess up some rmd functions
$ofile = "$name.dat";
$rfile = "$name.rmd";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

# read in samplesheet
%metacats = ();
if ($args{samplesheet}) {
    open IFILE, $args{samplesheet} or die "Cannot open samplesheet $args{samplesheet}: $!\n";
    $header = <IFILE>;
    chomp $header;
    @header = split /\t/, lc($header);
    while ($line = <IFILE>) {
	chomp $line;
	@line = split /\t/, $line;
	$sample = $line[0];
	push @samples, $sample;
	for $i (1..$#line) {
	    next if ($header[$i] eq "fastqr1"); 
	    next if ($header[$i] eq "fastqr2"); 
	    next if ($header[$i] eq "description"); 
	    $metadata{$sample}{$header[$i]} = $line[$i] // "";
	    $metacats{$header[$i]}{$line[$i]} = 1;
	}
    }
    close IFILE;
}

# generate PLINK format file
`module load plink/1.90b4.1; plink --vcf $args{vcffile} --recode 12 --out plink --allow-extra-chr`;

# run plink to calculate pca
print "Generationg PCA plot\n" if ($args{verbose});
`module load plink/1.90b4.1; plink --pca 5 --file plink --allow-extra-chr -out plink`;
$eigenvec = "plink.eigenvec";
$eigenval = "plink.eigenval";
    
### read in data
# eigenvec = coordinates for each point (Sample)
open IFILE, $eigenvec or die "cannot open $eigenvec: $!\n";
while ($line = <IFILE>) {
    chomp $line;
    @line = split ' ', $line;
    $sample = $line[0];
    $pca{$sample} = [ $line[2], $line[3], $line[4] ];
}
close IFILE;
# eigenval = numbers from which we can caluclate % variation
open IFILE, $eigenval or die "cannot open $eigenval: $!\n";
$total = 0;
$count = 0;
while ($line = <IFILE>) {
    chomp $line;
    $count++;
    $total += $line;
    $eigenvals[$count] = $line;
}
close IFILE;

for $i (1..$#eigenvals) {
    $explained[$i] = round100($eigenvals[$i] / $total * 100);
    $explained[$i] = "$explained[$i]%";
}
close IFILE;

### write out data
print OFILE "sample\tpca1\tpca2\tpca3";
foreach $key (sort keys %metacats) {
    print OFILE "\t$key";
}
print OFILE "\n";
foreach $sample (keys %pca) {
    print OFILE "$sample\t$pca{$sample}[0]\t$pca{$sample}[1]\t$pca{$sample}[2]";
    foreach $key (sort keys %metacats) {
	print OFILE "\t$metadata{$sample}{$key}";
    }
    print OFILE "\n";    
}
close OFILE;

# generate metadata color lists
@colors = ('#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'); # standard plotly color set
foreach $cat (sort keys %metacats) {
    @levels = sort keys %{$metacats{$cat}};
    for $i (0..$#levels) {
	$imod = $i % 10;
	$colors{$cat}{$levels[$i]} = $colors[$imod];
    }
    $colorarray{$cat} = "";
    foreach $sample (@samples) {
	$colorarray{$cat} .= "\"$colors{$cat}{$metadata{$sample}{$cat}}\",";
    }
    chop $colorarray{$cat};
}

if ($#samples > 20) {
    $type = "marker";
    $typename = "markers";
} else {
    $type = "textfont";
    $typename = "text";
}

$buttonlist = "";
$firstcolorarray = "";
if (keys %metacats) {
    foreach $cat (sort keys %metacats) {
	$firstcolorarray = $colorarray{$cat} unless ($firstcolorarray);
	$buttonlist .= "list(label = \"Color by:<br>$cat\", method = \"restyle\", args = list(\"$type.color\", list($colorarray{$cat}))),";
    }
    # add "none"
    $colorarray = "";
    foreach $sample (@samples) {
	$colorarray .= "\"#1f77b4\","; # blue
    }
    chop $colorarray;
    $buttonlist .= "list(label = \"Color by:<br>None\", method = \"restyle\", args = list(\"$type.color\", list($colorarray)))";
}
$firstcolorarray = $colorarray unless ($firstcolorarray);

$text = $args{text} // "This is a Principal Components (PCA) plot. The first three principal components are shown, and the percent of total variation explained by each component is shown in the axis titles. Samples with similar characteristics appear close to each other, and samples with dissimilar characteristics are farther apart. Ideally samples will cluster by experimental condition and not by batch or other technical effects.";

### generate pca plot
open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">PCA plot
<a href="#$cleanname" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$cleanname" class="collapse">
$text
</div>

```{r $name, out.height=500, warning=FALSE}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c(\"sample\"=\"factor\"));

updatemenus <- list(
  list(
    x = 1,
    y = 1,
    xanchor = 'right',
    yanchor = 'top',
    font = list(size = 10),
    buttons = list(
      $buttonlist
    )
  )
)

config(plot_ly(
  height = 500,
  data=datat,
  x = ~pca1,
  y = ~pca2,
  z = ~pca3,
 type = "scatter3d",
 mode = '$typename', 
 color = ~sample,
$type = list(color = c($firstcolorarray)),
hoverinfo='text', text=paste0(datat\$sample))
%>% layout(updatemenus=updatemenus, scene = list(xaxis = list(title = "Axis 1 ($explained[1])", showspikes=FALSE), yaxis = list(title = "Axis 2 ($explained[2])", showspikes=FALSE), zaxis=list(title="Axis 3 ($explained[3])", showspikes=FALSE)), showlegend = FALSE), dragmode='tableRotation', collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosest3d','hoverCompareCartesian','lasso2d','zoom3d','select3d','toggleSpikelines','pan3d', 'resetCameraLastSave3d', 'orbitRotation', 'tableRotation', 'resetCameraDefault3d'))
```
);
close RFILE;







exit;
# TODO: return some stats (mean marker depth)
print "STATS Start\n";

# depth per sample
$datafile = "out.idepth";
open IFILE, $datafile or die "Cannot open $datafile: $!\n";
$header = <IFILE>;
while ($line = <IFILE>) {
    chomp $line;
    my ($sample, $sites, $depth) = split /\t/, $line;
    print "$sample\tmeandepth\t$depth\n";
}

print "STATS End\n";

exit;

# Here lies old GGplot2 plots!
### GGPLOT2 Time! ###

### Plotting ###
#$ofile = "tmp.dat";
$rfile = "tmp.r";
my $height = 480;
my $width = 800;
my $samplewidth = 400;
if ($numsamples > 20) {
    $samplewidth = 400 + (5 * ($numsamples-20));
}


### sample heterozygocity plot ###
$pngfile = $args{prefix} . "sample-het.png";
print "Generating $pngfile\n" if ($args{verbose});
$datafile = "out.het";
# INDV O(HOM) E(HOM) N_SITES F
# ohom and ehom are observed and expected heterozygouos sites, F is inbreeding coefficient

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
  library(ggplot2);
  library(scales);
  datat <- read.table("$datafile", header=T, colClasses=c("INDV"="factor"));
  png(filename="$pngfile", height = $height, width = $samplewidth);

  datat <- transform(datat, INDV = reorder(INDV, F))

  p <- ggplot(datat, aes(x=INDV, y=F, fill=INDV)) 
  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab("Sample (sorted by Y-value)") + ylab("Inbreeding coefficient F") + guides(fill=FALSE)

  dev.off();
  #eof
);
close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### site quality plot ###
$pngfile = $args{prefix} . "site-qual.png";
print "Generating $pngfile\n" if ($args{verbose});
$datafile = "out.lqual";
#CHROM    POS    QUAL

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
  library(ggplot2);
  library(scales);
  datat <- read.table("$datafile", header=T);
  png(filename="$pngfile", height = $height, width = $width);

  p <- ggplot(datat, aes(x=QUAL)) 
  p + geom_histogram(binwidth=10) + theme(axis.text.x = element_text(angle = 90)) + ylab("Number of markers") + xlab("Marker quality") + xlim(c(0,1000))

  dev.off();
  #eof
);
close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### site depth plot ###
$pngfile = $args{prefix} . "site-depth.png";
print "Generating $pngfile\n" if ($args{verbose});
$datafile = "out.ldepth.mean";
#CHROM    POS    SUM_DEPTH    SUMSQ_DEPTH

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
  library(ggplot2);
  library(scales);
  datat <- read.table("$datafile", header=T);
  png(filename="$pngfile", height = $height, width = $width);

  p <- ggplot(datat, aes(x=MEAN_DEPTH)) 
  p + geom_histogram(binwidth=1) + ylab("Number of markers") + xlab("Marker depth") + xlim(c(1,1000)) 

  dev.off();
  #eof
);
close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### min spacing plot ###
$statfile = "markerspacing.txt";
open SFILE, ">>$statfile";
$pngfile = $args{prefix} . "site-space.png";
print "Generating $pngfile\n" if ($args{verbose});
$datafile = $args{prefix} . "space";
open OFILE, ">$datafile" or die "Cannot open $datafile: $!\n";
print OFILE "Space\tMarkers\n";
$ymax = 0;
# get data
foreach $space (0, 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000, 5000, 10000) {
    $command = "vcftools --vcf $args{vcffile} --thin $space 2>&1";
    $result = `$command`;
#    print $result;
    ($kept) = $result =~ /After filtering, kept (\d+) out of a possible \d+ Sites/;
    $ymax = $kept if ($kept > $ymax);
    print OFILE "$space\t$kept\n";
    print SFILE "$space\t$args{prefix}\t$kept\n";
}
close OFILE;

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
  library(ggplot2);
  library(scales);
  datat <- read.table("$datafile", header=T);
  png(filename="$pngfile", height = $height, width = $width);

  p <- ggplot(datat) 
 datasub <- subset(datat, Space %in% c(0,100,200,1000,10000))
  p + geom_line(aes(x=Space, y=Markers)) + ylab("Number of markers") + xlab(paste("Minimum marker spacing (bp) \\nLabeled at 0,100,200,1k and 10k")) +ylim(c(0,$ymax)) + geom_text(data=datasub, aes(x=Space, y=Markers, label=Markers), hjust=0, vjust=0) + geom_point(data=datasub, aes(x=Space, y=Markers))

  dev.off();
  #eof
);
close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### PCA plot ###
# generate PLINK format file
`module load plink/1.90b4.1; plink --vcf $args{vcffile} --recode 12 --out plink --allow-extra-chr`;

# generate PCA plot
print "Generationg PCA plot\n" if ($args{verbose});
`module load plink/1.90b4.1; plink --pca 5 --file plink --allow-extra-chr -out plink`;
    
### Plotting ###
$rfile = "pcaplot.r";
$pngfile = $args{prefix} . "pca.png";
open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
 table <- read.table('plink.eigenvec')
 png(filename="$pngfile",height=800,width=800)
 plot(table[3:4],type="n",main="PCA Plot of $vcfname", xlab="PC1", ylab="PC2")
 text(table[3:4],labels=table[,2],cex=.7)
 dev.off()

  #eof) . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");


# TODO: return some stats (mean marker depth)
print "STATS Start\n";

# depth per sample
$datafile = "out.idepth";
open IFILE, $datafile or die "Cannot open $datafile: $!\n";
my $header = <IFILE>;
while ($line = <IFILE>) {
    chomp $line;
    my ($sample, $sites, $depth) = split /\t/, $line;
    print "$sample\tmeandepth\t$depth\n";
}

print "STATS End\n";


##################### Helper subs ###################

# Round to nearest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
}
