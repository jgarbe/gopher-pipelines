#!/usr/bin/perl -w

###############################################################
#
# John Garbe
# March 2017
#
###############################################################

=head1 DESCRIPTION

Generate plot showing marker locations on each chromosome/contig.

=head2 OUTPUT

  plots
  summary metric files

=head1 SYNOPSIS

chromplot.pl --vcffile file --referencefai ref.fa.fai

chromplot.pl --help

=head1 OPTIONS

 --vcffile file : a vcf file
 --referencefai file : an fai index file of the reference fasta file
 --prefix string : prefix to attached to png file names
 --help : Print usage instructions and exit
 --verbose : Print more information while running

=cut

###############################################################

use Getopt::Long;
use File::Basename;
use Pod::Usage;

$args{prefix} = "";
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "vcffile:s" => \$args{vcffile},
	   "referencefai:s" => \$args{referencefai},
	   "prefix:s" => \$args{prefix},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
pod2usage unless ($args{vcffile} and $args{referencefai});

die "Cannot find vcffile $args{vcffile}\n" unless (-f $args{vcffile} && -f _ );
die "Cannot find referencefai $args{referencefai}\n" unless (-r $args{referencefai} && -f _ );

$datafile = $args{prefix} . "chromcountplot.dat";

# write out chromosome marker counts
# get count of markers on each chromosome
@result = `grep -v "#" $args{vcffile} | cut -f 1 | sort | uniq -c`;
foreach $line (@result) {
    chomp $line;
    $line =~ s/^\s+//;
    ($count, $chr) = split ' ', $line;
#    print "$chr, $count\n";
    $data{$chr}{count} = $count;
}

# get chromosome lengths
@result = `cat $args{referencefai}`;
foreach $line (@result) {
    chomp $line;
    $line =~ s/^\s+//;
    ($chr, $length) = split ' ', $line;
#    print "$chr, $length\n";
    $data{$chr}{length} = $length;
}

open OFILE, ">$datafile" or die "Cannot open $datafile: $!\n";
$count = 0;
foreach $chr (sort { $data{$b}{length} <=> $data{$a}{length} } keys %data) {
    $count++;
    push @topchroms, $chr if ($count <= 40);
    $topchroms{$chr} = 1 if ($count <= 40);
    $data{$chr}{count} = 0 unless ($data{$chr}{count});
    print OFILE "$chr\t$data{$chr}{length}\t$data{$chr}{count}\n";
}
close OFILE;

### Plotting ###
$rfile = "chromplot.r";
$height = 480;
$width = 800;
#my $samplewidth = 400;

### marker count plot ###
$pngfile = $args{prefix} . "chromcountplot.png";
print "Generating $pngfile\n";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
  library(ggplot2);
  library(scales);
  datat <- read.table("$datafile", header=F, col.names=c("chr","X","Y"));
  png(filename="$pngfile", height = $height, width = $width);

 p <- ggplot(datat) + geom_point(aes(X,Y))
 p + xlab("Chromosome length") + ylab("# of markers") + scale_y_continuous(labels = comma) + ggtitle("Markers per chromosome") + aes(xmin=0, ymin=0)

  dev.off();
  #eof
);
close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");


### marker count plot RMD ###
$name = $args{prefix} . "chromcountplot";
$cleanname = $name;
$cleanname =~ s/[^A-Za-z]/-/g; # periods mess up some rmd functions
$rfile = "$name.rmd";
print "Generating $name\n";

$title = "Markers per chromosome";
$text = "Each point in the plot is a chromosome or scaffold in the reference genome assembly, plotted by chromosome length (x axis) and # of markers on the chromosome (y axis). Longer chromosomes should have more markers and shorter chromosmes should have fewer markers. Pseudo-chromosomes composed of unplaced contigs often have unexpectedly high or low numbers of markers.";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$cleanname" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$cleanname" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$datafile", header=F, col.names=c("chr","X", "Y"));

config(plot_ly(
  data=datat,
  x = ~X,
  y = ~Y,
 name = "Markers per chromosome",
 type = "scatter",
hoverinfo="text",
text = paste0(datat\$chr, " has ", datat\$Y, " markers"))
%>% layout(dragmode='pan', legend = list(orientation = 'h'), xaxis = list(title = "Chromosome length"), yaxis = list(title = "# of markers")), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;

# write out marker positions in vcf file to tmp file
$datafile = "chromplot.dat";
`echo -e "Chr\tPos" > $datafile`;
@results = `grep -v "#" $args{vcffile} | cut -f 1-2`;
foreach $line (@results) {
    chomp $line;
    ($chr, $pos) = split /\t/, $line;
    $markers{$chr}{$pos} = 1;
}
open OFILE, ">$datafile" or die "cannot open $datafile: $!\n";
print OFILE "Chr\tPos\n";
for $i (0..$#topchroms) {
    $chr = $topchroms[$i];
    foreach $pos (sort {$a <=> $b} keys %{$markers{$chr}}) {
	print OFILE "$chr\t$pos\n";
    }
}

$datafile2 = "$datafile" . "2";
open OFILE, ">$datafile2" or die "cannot open $datafile2: $!\n";
print OFILE "Chr\tLength\n";
$limits = "";
for $i (0..$#topchroms) {
    $chr = $topchroms[$i];
    $limits = "\"$chr\",$limits";
    print OFILE "$chr\t$data{$chr}{length}\n";
}
chop $limits;

### Plotting ###
$rfile = "chromplot.r";
my $height = 480;
my $width = 800;
$title = "Marker Locations";
$topchroms = ($count > 40) ? 40 : $count;
if ($topchroms > 20) {
    $title = "Marker Locations ($topchroms longest chromosomes/contigs)";
#    $height = 480 + (24 * ($topchroms-20));
}

### marker location plot ###
$pngfile = $args{prefix} . "chromplot.png";
print "Generating $pngfile\n";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
  library(ggplot2);
  library(scales);
  datat <- read.table("$datafile", header=T, colClasses=c("Chr"="factor"));
  datachr <- read.table("$datafile2", header=T, colClasses=c("Chr"="factor"));
  png(filename="$pngfile", height = $height, width = $width);

datat\$Chr=factor(datat\$Chr, levels=c($limits))
datachr\$Chr=factor(datachr\$Chr, levels=c($limits))

p <-  ggplot(datachr, aes(Chr,Length)) + geom_bar(stat="identity", fill="grey70")
 p + geom_segment(data=datat, aes(x=as.integer(Chr) - .44, xend=as.integer(Chr) + .44, y=Pos, yend=Pos, colour="black"), size=0.1) + scale_colour_identity() + coord_flip() + xlab("Chromosome") + ylab("Position") + scale_y_continuous(labels = comma) + ggtitle("$title")

  dev.off();
  #eof
);
close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");



