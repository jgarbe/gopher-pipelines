#!/usr/bin/perl -w

#####################################
# vcf2table.pl
# John Garbe
# April 2017
# 
#####################################

=head1 DESCRIPTION

vcf2table.pl -  take a vcf file and spit out a table that can be opened in Excel. Rows are samples, columns are markers

=head1 SYNOPSIS

vcf2table.pl --vcffile input.vcf --outputfile output.txt [--transpose] [--gzip]

=head1 OPTIONS

 --vcffile file : Input vcf file to convert to a table
 --outputfile file : name of output text file
 --transpose : rows are markers, columns are samples
 --gzip : gzip compress the output file
 --help : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib "$FindBin::RealBin";
use enzymes;
use Cwd 'abs_path';

GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "transpose" => \$args{transpose},
	   "gzip" => \$args{gzip},
#	   "threads=i" => \$threads,
	   "vcffile=s" => \$args{vcffile},
	   "outputfile=s" => \$args{outputfile},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
pod2usage unless ($args{vcffile} and $args{outputfile});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
die "Cannot find VCF file $args{vcffile}\n" if (! -e $args{vcffile});
$args{vcffile} = abs_path($args{vcffile});

# convert to 012 format
$gzip = "--vcf";
$gzip = "--gzvcf" if ($args{vcffile} =~ /\.gz$/);
$result = `/home/umgc-staff/shared/bin/vcftools/bin/vcftools $gzip $args{vcffile} --012 --out vcf2table.tmp`;
print $result;

### paste together the parts ###
$mfile = "vcf2table.tmp.012.pos";
$ifile = "vcf2table.tmp.012.indv";
$gfile = "vcf2table.tmp.012";

# read in marker positions
open IFILE, $mfile or die "cannot open $mfile: $!\n";
$count = 0;
while ($line = <IFILE>) {
    chomp $line;
    ($chr, $pos) = split /\t/, $line;
    $pos[$count] = $pos;
    $chr[$count] = $chr;
    $count++;
}
close IFILE;

# read in individual names
open IFILE, $ifile or die "cannot open $ifile: $!\n";
$count = 0;
while ($line = <IFILE>) {
    chomp $line;
    $ind[$count] = $line;
    $count++;
}
close IFILE;

# read in genotype data
open IFILE, $gfile or die "cannot open $gfile: $!\n";
$ind = 0;
while ($line = <IFILE>) {
    chomp $line;
    @line = split /\t/, $line;
    shift @line; # ignore first column 
    for $genotype (0..$#line) { # skip first column
	$genotype[$ind][$genotype] = $line[$genotype];
    }
    $ind++;
}
close IFILE;

# print out final file
open OFILE, ">$args{outputfile}" or die "cannot open $args{outputfile}: $!\n";

if ($args{transpose}) {
    # genotypes are rows (transpose)
    # header
    print OFILE "Chr\tPos";
    for $ind (0..$#ind) {
	print OFILE "\t$ind[$ind]";
    }
    print OFILE "\n";
    
    # body
    for $genotype (0..$#pos) { 
	print OFILE "$chr[$genotype]\t$pos[$genotype]";
	for $ind (0..$#ind) {
	    print OFILE "\t$genotype[$ind][$genotype]";
	}
	print OFILE "\n";
    }
    close OFILE;
    `gzip $args{outputfile}` if ($args{gzip});

} else {
    # individuals are rows
#    open OFILE, ">$args{outputfile}.transpose" or die "cannot open $ofile: $!\n";

    # header
    print OFILE "SampleID";
    for $genotype (0..$#pos) {
	print OFILE "\t$chr[$genotype]-$pos[$genotype]";
    }
    print OFILE "\n";

    # body
    for $ind (0..$#ind) {
	print OFILE "$ind[$ind]";
	for $genotype (0..$#pos) { 
	    print OFILE "\t$genotype[$ind][$genotype]";
	}
	print OFILE "\n";
    }
    close OFILE;
    `gzip $args{outputfile}` if ($args{gzip});

}
