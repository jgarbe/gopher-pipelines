#!/usr/bin/perl -w

die "USAGE: samparse.pl align.sam reference.fa\n" unless ($#ARGV == 1);

($samfile, $fastafile) = @ARGV;

@results = `fastalengths.pl $fastafile`;
foreach $result (@results) {
    chomp $result;
    ($id, $length) = split /\t/, $result;
    $lengths{$id} = $length;
}

# filter an ampliseq sam file
open IFILE, $samfile or die "cannot open sam file $samfile: $!\n";
$lastread = "";
$alignment = 0;
$count = 0;
while (<IFILE>) {
    if ($_ =~ /^@/) { # reprint header
	print $_;
	next;
    }
    chomp;
    @line = split /\t/, $_, 12;
    $read = $line[1-1];
    $reference = $line[3-1];
    # skip unaligned reads
    if ($reference eq "*") {
	print "$_\n";
	next;
    }
    if ($read eq $lastread) { # second alignment
	$alignment = 2;
	$alignment2 = $_;
	$fields{$alignment}{reference} = $reference;

	# read in fields
	@fields = split /\t/, $line[12-1];
	foreach $field (@fields) {
	    ($id, $junk, $value) = split /:/, $field; 
	    $fields{$alignment}{$id} = $value;
	}

	if ($fields{1}{AS} > $fields{1}{XS}) { # first alignment has higher score than second
	    print "$alignment1\n";
	} else { # equal alignment scores,  print alignment to smaller of the two reference seqs
	    $align1length = $lengths{$fields{1}{reference}};
	    $align2length = $lengths{$fields{2}{reference}};
	    if ($align1length < $align2length) {
		print "$alignment1\n";
	    } else {
		print "$alignment2\n";
	    }
	}
    } else { # first alignment
	if ($alignment == 1) { # first alignment, and previous alignment was a first (and only) alignment as well
	    print "$alignment1\n";
	    $lastread = $read;
	    $count++;
	}

	$alignment = 1;
	$alignment1 = $_;
	%fields = ();
	$fields{$alignment}{reference} = $reference;

	# read in fields
	@fields = split /\t/, $line[12-1];
	foreach $field (@fields) {
	    ($id, $junk, $value) = split /:/, $field; 
	    $fields{$alignment}{$id} = $value;
	}
	$lastread = $read;
    
    }

}

   

#     M04803:182:000000000-C6F4C:1:1102:15177:2680    0       rs7970314-alt-1 1       11      11M2D65M        *       0       0       AS:i:141        XS:i:133        XN:i:0  XM:i:0  XO:i:1  XG:i:2  NM:i:2  MD:Z:11^AC65
#     M04803:182:000000000-C6F4C:1:1102:15177:2680    256     rs7970314-ref   1       11      11M2D65M        *       0       0       AS:i:133        XS:i:133        XN:i:0  XM:i:1  XO:i:1  XG:i:2  NM:i:3  MD:Z:11^AC36
