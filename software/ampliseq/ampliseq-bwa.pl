#!/usr/bin/perl -w

#####################################
# .pl
# John Garbe
# June 2018
#####################################

=head1 DESCRIPTION

ampliseq.pl - what it does

=head1 SYNOPSIS

ampliseq.pl --r1 sample_R1.fastq --r2 sample_R2.fastq --bwaindex index --samplename sample [--threads 10] [--scratchfolder .]

=head1 OPTIONS

This pipeline handles single-end read datasets only. 

Options:

 --help : Print usage instructions and exit
 --verbose : Print more information while running

=cut

##################### Initialize ###############################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;
use File::Temp qw( tempdir );

# set defaults
$args{threads} = 10;
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "threads=i" => \$args{threads},
	   "r1=s" => \$args{r1},
	   "r2=s" => \$args{r2},
	   "samplename=s" => \$args{samplename},
	   "bwaindex=s" => \$args{bwaindex},
	   "scratchfolder=s" => \$args{scratchfolder},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
$args{file} = abs_path($args{file}) if ($args{file});

$myscratchfolder = $args{scratchfolder} // ".";
### Handle Parameters ###

pod2usage unless ($args{r1} && $args{r1} && $args{samplename} && $args{bwaindex}); 


# count starting reads
if ($args{r1} =~ /\.gz$/) {
    $reads = `gunzip -c $args{r1} | wc -l`;
} else {
    $reads = `wc -l < $args{r1}`;
}
chomp $reads;
$reads{raw} = $reads / 4;

### Stitch reads together ###
$command = "pear -f $args{r1} -r $args{r2} -o $myscratchfolder/$args{samplename} --threads $args{threads}";
$result = `$command`;

$r1 = "$myscratchfolder/$args{samplename}" . ".assembled.fastq";

# count stitched reads
$reads = `wc -l < $r1`;
chomp $reads;
$reads{stitch} = $reads / 4;

### Align with BWA index ###
$command = "bwa mem -t $args{threads} $args{bwaindex} $r1 > $myscratchfolder/$args{samplename}.sam 2>$myscratchfolder/$args{samplename}.bwalog";
$result = `$command`;
    
### Count ref and alt alignments ###
@result = `grep -v "^@" $myscratchfolder/$args{samplename}.sam | cut -f 3 | sort | uniq -c`;
chomp @result;
foreach $result (@result) {
#	print "result: $result\n";
    ($count, $allele) = split ' ', $result;
	    
    if ($allele eq "*") { # skip unaligned reads - should count these
	$reads{noalign} = $count;
	next;
    }
#	print "count: $count, allele: $allele\n";
#	($rs) = split /-/, $allele, 2;
    $data{$allele} = $count;
}

# TODO: print out results
$ofile = "$myscratchfolder/$args{samplename}.out";
open OFILE, ">$ofile" or die "cannot open $ofile: $!\n";
foreach $allele (keys %data) {
    print OFILE "$allele\t$data{$allele}\n";
}
foreach $key (keys %reads) {
    print OFILE "$key\t$reads{$key}\n";
}


################################ Helper subs ###############################

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}




