#!/usr/bin/perl -w

#####################################
# samplesheet2manifest.pl
# John Garbe
# Feb 2019
#####################################

=head1 DESCRIPTION

samplesheet2manifest.pl - covert a gtools samplesheet to a qiime2 manifest

=head1 SYNOPSIS

samplesheet2manifest.pl --samplesheet samplesheet.txt --fastqfolder fastq/folder > manifest.txt

=head1 OPTIONS

Options:

 --samplesheet file : samplesheet
 --fastqfolder path : folder containing fastq files listed in the samplesheet
 --help : Print usage instructions and exit
 --verbose : Print more information while running

=cut

##################### Initialize ###############################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;
use File::Temp qw( tempdir );

# set defaults
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "samplesheet=s" => \$args{samplesheet},
	   "fastqfolder=s" => \$args{fastqfolder},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
pod2usage if (@ARGV);
pod2usage unless ($args{samplesheet} && $args{fastqfolder});
$args{samplesheet} = abs_path($args{samplesheet});
$args{fastqfolder} = abs_path($args{fastqfolder});

### Handle Parameters ###

# read in the samplesheet
open $SS, $args{samplesheet} or die "Cannot open samplesheet $args{samplesheet}: $!\n";
$header = <$SS>;
chomp $header;
@header = split /\t/, lc($header);
if ($header =~ /fastqR2/i) {
    $args{pe} = 1;
} else {
    $args{pe} = 0;
}
while ($line = <$SS>) {
    chomp $line;
    @line = split /\t/, $line;
    $sample = $line[0];
    for $i (1..$#line) {
	$samplesheetdata{$sample}{$header[$i]} = $line[$i] // "";
    }
}
close $SS;

print "sample-id,absolute-filepath,direction\n";
foreach $sample (sort keys %samplesheetdata) {
    print "$sample,$args{fastqfolder}$samplesheetdata{$sample}{fastqr1},forward\n";
    print "$sample,$args{fastqfolder}$samplesheetdata{$sample}{fastqr2},reverse\n" if ($args{pe});
}


################################ Helper subs ###############################

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

sub mean {
    my($data) = @_;
    if (not @$data) {
	die("Empty array\n");
    }
    my $total = 0;
    foreach (@$data) {
	$total += $_;
    }
    my $average = $total / @$data;
    return $average;
}

sub stdev {
    my($data) = @_;
    if(@$data == 1) {
	return 0;
    }
    my $average = &average($data);
    my $sqtotal = 0;
    foreach(@$data) {
	$sqtotal += ($average-$_) ** 2;
    }
    my $std = ($sqtotal / (@$data-1)) ** 0.5;
    return $std;
}



