#!/usr/bin/perl -w

###########################################################
# fastamedian.pl
# John Garbe
# November 2014
#
# Compute the median length of sequences in a fasta file
#
###########################################################

=head1 NAME

fastamedian.pl

=head1 SYNOPSIS

fastamedian.pl input.fasta

=head1 DESCRIPTION

Prints out the median length of sequence in a fasta file

=cut

# See this post for a version that uses a module: http://seqanswers.com/forums/showthread.php?t=15856

$usage = "medianlength.pl input.fastq\n";
die $usage unless ($#ARGV == 0);
$ifile = $ARGV[0];
open IFILE, "<$ifile" or die "Cannot open input file $ifile: $!\n";;
$/ = ">"; # define new line separater

my $junkfirstone = <IFILE>;

while (<IFILE>) { # one line equals one sequence entry
    chomp;
    my ($def,@seqlines) = split /\n/, $_;
    my $seq = join '', @seqlines;
    push @lengths, length($seq);
}

if ($#lengths < 0) {
    print "0\n";
} else {
    @sorted = sort {$a <=> $b} @lengths;
    $mid = int(($#sorted+1) / 2);
    print "$sorted[$mid]\n";
}
