#!/usr/bin/perl -w

#############################################################
# start_parallel_jobs_gp.pl
# John Garbe
# June 2014
#
# A Qiime cluster submission script for MSI clusters
# Runs commands using GNU Parallel
# This doesn't create or submit pbs jobs
# It is assumed to be running in a pbs job already
# Supports single-node jobs only
#
# Running the poller task after the main tasks was causing problems, so I removed that part
#
##############################################################

use Getopt::Std;
use feature "switch";
use Env;

our ($opt_m, $opt_s, $opt_q, $opt_j, $opt_w, $opt_c, $opt_n);

foreach $i (@ARGV) {
    print "$i\n";
}

$usage = "USAGE: start_parallel_jobs_gp.pl commands runid\n";
die $usage unless getopts('msj:');
die $usage unless ($#ARGV == 1);
$makejobs = $opt_m // 0;
$submitjobs = $opt_s // 0;
$jobdir = $opt_j // ".";
$commandfile = $ARGV[0];
$runid = $ARGV[1];

################## Determine Resources ####################

# How many cores are available to us?
$threads = $ENV{"PBS_NUM_PPN"} // 1;

############################# SUMBIT #####################################

if ($submitjobs) {
#    `echo "Running GNU Parallel from dir $jobdir"`;
    my $result = `cat $commandfile | parallel --retries 2 --jobs $threads`;
    print $result;
# These lines print exit statuses, I thought it might be useful for debugging, but it isn't
#    print "GNU Parallel exit status: ${^CHILD_ERROR_NATIVE}\n";
    exit;
}

############################# MAKE #######################################

if ($makejobs) {
    # no need to do anything for makejobs because Qiime already provides all commands in a file suitable to cat into parallel
    exit; 
}

exit;


    
