#!/usr/bin/perl -w

#######################################################################
#  Copyright 2016 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 DESCRIPTION

covid-pipeline - Align covid samples to a reference genome and generate consensus sequences

=head1 SYNOPSIS

covid-pipeline --fastqfolder folder [ --bowtie2index index OR --bwaindex index ] --referencefasta file

=head1 OPTIONS

Analyze a set of fastq files

 --hostbwa string : BWA index basename of virus host
 --bowtie2index string : Bowtie2 index basename
 --bwaindex string : BWA index basename
 --referencefasta file : A fasta file containing the reference genome
 --removeduplicates : Remove duplicates using Picard
 --baitbed file : Capture bait file, BED format - turns on PicardHsMetrics
 --targetbed file : Capture target file, BED format - baitbed file is used by default
 --freebayes : Call variants with Freebayes
 --fastqfolder folder : A folder containing fastq files to process
 --samplesheet file : A samplesheet file
 --outputfolder folder : A folder to deposit final results
 --scratchfolder folder : A temporary/scratch folder
 --name string : Name of the analysis, fastqfolder name is default name
 --samplespernode integer : Number of samples to process simultaneously on each node (default = 1)
 --threadspersample integer : Number of threads used by each sample
 --subsample integer : Subsample the specified number of reads from each sample. 0 = no subsampling
 --resume : Continue where a failed/interrupted run left off
 --adapterfile file : A file containing adapter sequences for trimmomatic
 --extraoptionsfile file : File with extra options for trimmomatic, bwa, and bowtie2
 --bam2fastq : convert bam files back to aligned and unaligned fastq files
 --amplicon : amplicon data - remove primers (assumes ARTIC v2)
 --pacbio : PacBio dataset
 --nofastqc : Don't run FastQC
 --help : Print usage instructions and exit
 --verbose : Print more information whie running (verbose)

=cut

##################### Main ###############################

use FindBin;                  # locate this script
use lib "$FindBin::RealBin";  # use the current directory
use Pipelinev4;

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;

my %samples;
my %args;
my %stats;
#my $reportFH; # analysis report filehandle

### Initialize ###
&init(\%samples, \%args, \%stats);

### Singlesample analysis ###
&singlesamples(\%samples, \%args, \%stats);
runtime;

if ($args{freebayes}) {
    &freebayes(\%samples, \%args, \%stats);
    runtime;
}

### All sample processing ###
&allsample(\%samples, \%args, \%stats);
runtime;

### Metrics ###
$args{stageorder} = ["general", "subsample", "fastqc", "trimmomatic", "fastqc-trim", "bwa", "bowtie2", "picardalignmentplot", "ivar", "ivartrim"];
allmetrics(\%samples, \%args, \%stats);

### R markdown report ###
&reportrmd(\%samples, \%args, \%stats, "Covid Report");

print "Finished\n";
runtime;

exit;

##################### Initialize ###############################
sub init {
    print "\n### Initializing run ###\n";

    my ($samples, $args, $stats) = @_;

    # set defaults
    $args->{options} = join " ", @ARGV;
    $args->{threadspersample} = 8;
    if ($ENV{"PBS_NUM_PPN"}) {
        $args->{samplespernode} = int($ENV{"PBS_NUM_PPN"} / $args->{threadspersample} + 0.5);
    } else {
        $args->{samplespernode} = 1;
    }
    $args->{adapterfile} = "$GPRESOURCES/all_adapters.fa";
    $args->{referencefasta} = $ENV{GENOME_FA} // "$GPSOFTWARE/covid-scripts/reference/seq/genome.fa";
#    $args->{bwaindex} = $ENV{BWA_INDEX} // "$GPSOFTWARE/covid-scripts/reference/bwa/genome";
    $args->{bwaindex} = $ENV{BWA_INDEX} // "$GPSOFTWARE/covid-scripts/swift-reference/bwa/genome";
    $args->{stitch} = 1;
    $args->{hostbwaindex} = "/home/umii/public/ensembl/Homo_sapiens/current/bwa/genome";
    GetOptions("help" => \$args->{help},
	       "verbose" => \$args->{verbose},
	       "resume" => \$args->{resume},
               "threadspersample=i" => \$args->{threadspersample},
               "samplespernode=i" => \$args->{samplespernode},
               "outputfolder=s" => \$args->{outputfolder},
               "scratchfolder=s" => \$args->{scratchfolder},
	       "extraoptionsfile=s" => \$args->{extraoptionsfile},
               "subsample=i" => \$args->{subsample},
               "fastqfolder=s" => \$args->{fastqfolder},
               "samplesheet=s" => \$args->{samplesheet},
               "qualitycontrol" => \$args->{qualitycontrol},
               "freebayes" => \$args->{freebayes},
	       "referencefasta=s" => \$args->{referencefasta},
	       "bwaindex=s" => \$args->{bwaindex},
	       "bowtie2index=s" => \$args->{bowtie2index},
	       "hostbwaindex=s" => \$args->{hostbwaindex},
	       "stitch" => \$args{stitch},
	       "removeduplicates" => \$args->{removeduplicates},
               "adapterfile=s" => \$args->{adapterfile},
	       "baitbed=s" => \$args->{baitbed},
	       "targetbed=s" => \$args->{targetbed},
	       "name=s" => \$args->{runname},
	       "pacbio" => \$args->{pacbio},
	       "amplicon" => \$args->{amplicon},
	       "bam2fastq" => \$args->{bam2fastq},
	       "bwamem=s" => \$args->{bwamem},
	       "nofastqc" => \$args->{nofastqc},
	) or pod2usage;
    pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args->{help});
    pod2usage unless ( $args->{fastqfolder} and ($args->{bwaindex} or $args->{bowtie2index}) and $args->{referencefasta} );
    if ($#ARGV >= 0) {
        print "Unknown commandline parameters: @ARGV\n";
        pod2usage;
    }
    $args->{threads} = $ENV{"PBS_NUM_PPN"} // $args->{threadspersample}; # threads used by pipeline stages (not singlesample stages)
    $args->{extraoptionsfile} = abs_path($args->{extraoptionsfile}) if ($args->{extraoptionsfile});

    ### Starttime ###
    my $date = `date`;
    chomp $date;
    print "Starting at $date\n";
    $args->{startdate} = $date;
    &runtime;

    ### Additional Parameters ###
    # add additional parameter processing here
    if (! ($args->{bwaindex} or $args->{bowtie2index}) ) {
	$args->{bwaindex} = $ENV{BWA_INDEX} // "";
    }
    if ($args->{amplicon}) {
#	$args->{amplicon} = "$GPSOFTWARE/covid-scripts/reference/annotation/nCoV-2019.ivar.bed";
	$args->{amplicon} = "$GPSOFTWARE/covid-scripts/swift-reference/annotation/swift_panel.ivar.bed"; 
    }
    $args->{bowtie2index} = bowtie2indexcheck($args->{bowtie2index}) if ($args->{bowtie2index});
    $args->{bwaindex} = bwaindexcheck($args->{bwaindex}) if ($args->{bwaindex});
    $args->{hostbwaindex} = bwaindexcheck($args->{hostbwaindex}) if ($args->{hostbwaindex});
    die "Cannot find reference fasta file $args->{referencefasta}\n" if (!-e $args->{referencefasta} && -f _);
    $args->{referencefasta} = abs_path($args->{referencefasta});
    if ($args->{baitbed}) {
	die "ERROR: Baitbed file $args->{baitbed} not found\n" if (! -e $args->{baitbed});
	$args->{baitbed} = abs_path($args->{baitbed});
	die "ERROR: Targetbed file $args->{targetbed} not found\n" if ($args->{targetbed} and (! -e $args->{targetbed}));
	$args->{targetbed} = abs_path($args->{targetbed} // $args->{baitbed});
    }
    if ($args->{freebayes}) {
	if (! -e "$args->{referencefasta}.fai") {
	    die "Reference fasta index required: $args->{referencefasta}.fai\n";
	}
    }

    ### Finish setup
    requiredprograms(("fastqc", "\$TRIMMOMATIC", "bowtie2", "bwa", "\$PICARD"));
    requiredprograms(("freebayes")) if ($args->{freebayes});
    diegracefully();
    readinextraoptions($args);
    samplesheetandfoldersetup($samples, $args, $stats, "covid");

    hsmetricsprep($args) if ($args->{baitbed}); # turn bed files into interval files

}

######################## Singlesample jobs ##########################
sub singlesamples {
    print "\n### Analyzing samples ###\n";

    my ($samples, $args, $stats) = @_;

    my $myscratchfolder = "$args->{scratchfolder}/singlesamples";
    mkdir $myscratchfolder;

   # set up nodefile
    $nodefile = "$myscratchfolder/nodelist.txt";
    $nodecount = 1;
    my $junk;
    if ($ENV{PBS_NODEFILE}) {
        $result = `sort -u $ENV{PBS_NODEFILE} > $nodefile`;
        print $result;
	$nodecount = `wc -l < $nodefile`;
	chomp $nodecount;
    }

    # pass arguments through to singlesample.pl
    my $subsample = $args->{subsample} ? "--subsample $args->{subsample}" : "";
    my $adapterfile = $args->{adapterfile} ? "--adapterfile $args->{adapterfile}" : "";
    my $extraoptions = $args->{extraoptionsfile} ? "--extraoptionsfile $args->{extraoptionsfile}" : "";
    my $qualitycontrol = $args->{qualitycontrol} ? "--qualitycontrol" : "";
    my $verbose = $args->{verbose} ? "--verbose" : "";
    my $removeduplicates = $args->{removeduplicates} ? "--removeduplicates" : "";
    my $stitch = $args->{stitch} ? "--stitch" : "";
    my $bwaindex = $args->{bwaindex} ? "--bwaindex $args->{bwaindex}" : "";
    my $hostbwaindex = $args->{hostbwaindex} ? "--hostbwaindex $args->{hostbwaindex}" : "";
    my $bwamem = $args->{bwamem} ? "--bwamem $args->{bwamem}" : "";
    my $bowtie2index = $args->{bowtie2index} ? "--bowtie2index $args->{bowtie2index}" : "";
    my $baitfile = $args->{baitfile} ? "--baitfile $args->{baitfile}" : "";
    my $targetfile = $args->{targetfile} ? "--targetfile $args->{targetfile}" : "";
    my $amplicon = $args->{amplicon} ? "--amplicon $args->{amplicon}" : "";
    my $pacbio = $args->{pacbio} ? "--pacbio" : "";
    my $nofastqc = $args->{nofastqc} ? "--nofastqc" : "";

    # print out singlesample commandlines
    my $commandfile = "$myscratchfolder/singlesample-commands.txt";
    open OFILE, ">$commandfile" or die "Cannot open $commandfile: $!\n";
    foreach $sample (keys %{$samples}) {
        if (($args->{resume}) && (-e "$myscratchfolder/$sample/Complete")) {
            print "Skipping completed sample $sample\n";
            next;
        }
	my $readgroup = "--readgroup $sample";
        my $r1 = "--R1 $samples->{$sample}{R1}{fastq}";
        $r2 = ($args->{pe}) ? "--R2 $samples->{$sample}{R2}{fastq}" : "";
        $log = "$args->{scratchfolder}/logs/$sample.log";
	$command = "covid-singlesample.pl $verbose --threads $args->{threadspersample} --referencefasta $args->{referencefasta} $subsample $adapterfile $extraoptions $qualitycontrol $stitch $removeduplicates $bwamem $baitfile $targetfile $amplicon $bowtie2index $bwaindex $pacbio $nofastqc --outputfolder $myscratchfolder/$sample $r1 $r2 $readgroup $hostbwaindex &> $log && echo \"$sample complete\" || echo \"$sample failed\"\n";
        print OFILE $command;
    }
    close OFILE;

   # run jobs in parallel
    mkdir "$args->{scratchfolder}/logs";
    if ($nodecount <= 1) { # single node
        system("cat $commandfile | parallel -j $args->{samplespernode}");
    } else { # multiple node
        system("$commandfile | parallel -j $args->{samplespernode} --sshloginfile $nodefile --workdir $ENV{PWD}");
    }

    # read in stats from each sample
    getsinglesamplestats($samples, $args, $stats);

}

################### All-sample jobs #######################
sub allsample {
    print "\n### Processing results ###\n";

    my ($samples, $args, $stats) = @_;

    my $scratchfolder = $args->{scratchfolder};
    my $myscratchfolder = "$scratchfolder/allsamples";
    mkdir $myscratchfolder;
    my $outputfolder = $args->{outputfolder};
    chdir $myscratchfolder;

    ### generate plots ###
    subsampleplotrmd($samples, $args, $stats);

    fastqcplotrmd($samples, $args, $stats, "short");

    trimmomaticplot($samples, $args, $stats) if ($args->{qualitycontrol});

    alignmentsummarymetricsplotrmd($samples, $args, $stats);

    insertsizeplot($samples, $args, $stats);

    hsmetricsplot($samples, $args, $stats) if ($args->{baitbed});

    vcfmetricsplot($samples, $args, $stats) if ($args->{freebayes});

    ### create folders ###
    # move singlesample logs to log folder
#    compilefolder($samples, $args, $stats, "logs", "log", "-log.txt");
    # copy log files to the output folder
#    `cp -rL $args->{scratchfolder}/logs $args->{outputfolder}`;

    # copy .bam and .bam.bai to bam folder
    compilefiles($samples, $args, $stats, "bam", "bam", ".bam");
    compilefiles($samples, $args, $stats, "bai", "bam", ".bam.bai");

    # compile consensus and tsv files
    compilefiles($samples, $args, $stats, "consensus", "consensus", ".fa");
    compilefiles($samples, $args, $stats, "tsv", "tsv", ".tsv");

    ### bam2fastq ###
    bam2fastq($samples, $args, $stats) if ($args->{bam2fastq});

}

############################ Rmd Report #############################
sub reportrmd {
    print "\n### Generating Analysis Report ###\n";

    my ($samples, $args, $stats, $title) = @_;

    ### report header
    $info{"Library type"} = $args->{library} if ($args->{library});
    reportheader($samples, $args, $stats, $title, \%info);

    ### report body

    print $reportFH $args->{reporttext};

    ### data
    print $reportFH "\n<h2> Data</h2>\n";
    print $reportFH qq(The output folder generated by this analysis pipeline contains the following folders and files:<br>\n);
    print $reportFH qq(bam/ bam alignment files<br>\n);
    print $reportFH qq(fastqc/ FastQC html files<br>\n) unless ($args->{nofastqc});
    print $reportFH qq(fastqc-trim/ FastQC html post-trimming/filtering<br>\n) if ($args->{qualitycontrol} && ! $args->{nofastqc});
    print $reportFH qq(aligned/ fastq files containing reads that aligned to the reference<br>\n) if ($args->{bam2fastq});
    print $reportFH qq(unaligned/ fastq files containing reads that did not align to the reference<br>\n) if ($args->{bam2fastq});

    ### methods
    print $reportFH qq(\n## Methods
$args->{methods}
);
    ### acknowledgements

    ### footer

#    close $reportFH;

    ### generate pdf and html from rmd, copy over to output folder
    &run("Rscript -e \"library('rmarkdown'); render('$reportfolder/report.rmd', output_format='html_document', output_file='$args->{outputfolder}/report.html')\"", $args->{logfile}); # generate html

}

################################ Helper subs ###############################

