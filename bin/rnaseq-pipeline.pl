#!/usr/bin/perl -w

######################################################
# rnaseq-pipeline.pl
# John Garbe
# December 2015
#
#######################################################

=head1 DESCRIPTION

rnaseq-pipeline.pl - RNA-seq Analysis Program: Analyze a set of RNA-seq fastq files using Hisat2 and Subread featureCounts

=head1 SYNOPSIS

rnaseq-pipeline.pl --hisat2index index --gtffile file --fastqfolder folder

=head1 OPTIONS

Analyze a collection of rna-seq fastq files

Options

 --hisat2index index : A hisat2 index basename (for running hisat2)
 --gtffile file : A reference genome gtf gene annotation file
 --library string : truseqrna OR picorna OR pcorna2 OR classic (default)
 --qualitycontrol : Enable quality-control: use trimmomatic to trim adapter sequences, trim low quality bases from the ends of reads, and remove short sequences

Advanced options

 --headcrop integer : crop the specified number of bases off the front of each read, useful for picogreen libraries
 --cufflinks : run cufflinks/cuffquat/cuffnorm
 --maskfile file : GTF file for Cuffquant's --maskfile option

Standard gopher-pipeline options

 --fastqfolder folder : A folder containing fastq files to process
 --subsample integer : Subsample the specified number of reads from each sample. 0 = no subsampling (default = 0)
 --samplesheet file : A samplesheet
 --runname string : Name of the sequencing run
 --projectname string : Name of the experiment (UMGC Project name) 
 --illuminasamplesheet file : An illumina samplesheet, from which extra sample information can be obtained 
 --nofastqc : Don't run FastQC
 --samplespernode integer : Number of samples to process simultaneously on each node (default = 1)
 --threadspersample integer : Number of threads used by each sample
 --scratchfolder folder : A temporary/scratch folder
 --outputfolder folder : A folder to deposit final results
 --extraoptionsfile file : File with extra options for trimmomatic, tophat, cuffquant, or featurecounts
 --resume : Continue where a failed/interrupted run left off
 --verbose : Print more information while running
 --help : Print usage instructions and exit

=cut

##################### Main ###############################

use FindBin;                  # locate this script
use lib "$FindBin::RealBin";  # use the current directory
use Pipelinev4;

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;

my %samples;
my %args;
my %stats;
#my $reportFH; # analysis report filehandle

### Initialize ###
&init(\%samples, \%args, \%stats);

### Singlesample analysis ###
&singlesamples(\%samples, \%args, \%stats);
runtime;

### Cuffnorm ###
if ($args{cufflinks}) {
    &cuffnorm(\%samples, \%args, \%stats);
    runtime;
}

### Differential expression ###
#&differentialexpression(\%samples, \%args, \%stats);
runtime;

### All sample processing ###
&allsample(\%samples, \%args, \%stats);
runtime;

### Metrics ###
$args{stageorder} = ["general", "subsample", "fastqc", "trimmomatic", "hisat2", "insert"];
allmetrics(\%samples, \%args, \%stats);

### R markdown report ###
&reportrmd(\%samples, \%args, \%stats, "RNA-Seq Report");

print "Finished\n";
runtime;

exit;

##################### Initialize ###############################
sub init {
    print "\n### Initializing run ###\n";

    my ($samples, $args, $stats) = @_;

    # set defaults
    $args->{options} = join " ", @ARGV;
    my $help = "";
    $args->{threadspersample} = 8;
    if ($ENV{"PBS_NUM_PPN"}) {
	$args->{samplespernode} = int($ENV{"PBS_NUM_PPN"} / $args->{threadspersample} + 0.5);
    } else {
	$args->{samplespernode} = 1;
    }
    $args->{hisat2index} = $ENV{HISAT2_INDEX} // "";
    $args->{gtffile} = $ENV{ANNOTATION_GTF} // $ENV{ANNOTATION_GFF3} // "";
    GetOptions("help" => \$args->{help},
               "verbose" => \$args->{verbose},
               "resume" => \$args->{resume},
               "extraoptionsfile=s" => \$args->{extraoptionsfile},
               "outputfolder=s" => \$args->{outputfolder},
               "scratchfolder=s" => \$args->{scratchfolder},
               "threadspersample=i" => \$args->{threadspersample},
               "samplespernode=i" => \$args->{samplespernode},
	       "nofastqc" => \$args->{nofastqc},
               "illuminasamplesheet=s" => \$args->{illuminasamplesheet},
               "projectname=s" => \$args->{projectname},
               "runname=s" => \$args->{runname},
               "samplesheet=s" => \$args->{samplesheet},
               "subsample=i" => \$args->{subsample},
               "fastqfolder=s" => \$args->{fastqfolder},
	       # rnaseq options
               "qualitycontrol" => \$args->{qualitycontrol},
               "gtffile=s" => \$args->{gtffile},
               "maskfile=s" => \$args->{maskfile},
               "hisat2index=s" => \$args->{hisat2index},
               "library=s" => \$args->{library},
	       "headcrop=i" => \$args->{headcrop},
	       "cufflinks" => \$args->{cufflinks},
        ) or pod2usage;
    pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args->{help});
    pod2usage unless ( $args->{hisat2index} and $args->{fastqfolder} and $args->{gtffile} );
    if ($#ARGV >= 0) {
	print "Unknown commandline parameters: @ARGV\n";
	pod2usage;
    }
    $args->{threads} = $ENV{"PBS_NUM_PPN"} // $args->{threadspersample}; # threads used by pipeline stages (not singlesample stages)
    $args->{extraoptionsfile} = abs_path($args->{extraoptionsfile}) if ($args->{extraoptionsfile});

    ### Starttime ###
    my $date = `date`;
    chomp $date;
    print "Starting at $date\n";
    $args->{startdate} = $date;
    &runtime;

    ### Handle Parameters ###
    # add parameter processing here

    if ($args->{library}) {
	$args->{library} = lc($args{library});
	die "Unknown --library value: $args->{library}\n Supported values: truseqrna, picorna, pcorna2\n" unless ( ($args->{library} eq "truseqrna") or ($args->{library} eq "picorna") or ($args->{library} eq "picorna2") or ($args->{library} eq "classic"));
    }

    die "ERROR: Could not locate gtffile $args->{gtffile}\n" if (! -e $args->{gtffile});
    # switch to filtered ensembl file if present
#    print "GTF file: $args->{gtffile}\n";
    $filtgtf = $args->{gtffile};
    $filtgtf =~ s/\/gtf$/\/.filt\.gtf/;
#    print "filt GTF file: $filtgtf\n";

    $reference = "";
    if ($ENV{LATIN_NAME}) {
	$reference = $ENV{COMMON_NAME};
    }
    if ($ENV{COMMON_NAME}) {
	if ($reference) {
	    $reference .= " ($ENV{LATIN_NAME})";
	} else {
	    $reference = "$ENV{LATIN_NAME}";
	}
    }
    if ($ENV{BUILD_NAME}) {
	$reference .= " genome assembly \"$ENV{BUILD_NAME}\"";
    }
    if ($ENV{ENSEMBL_RELEASE}) {
	$reference .= " using annotation from Ensembl release $ENV{ENSEMBL_RELEASE}"
    }
    $reference = $args{hisat2index} unless ($reference);

    $args->{methods} = "The RNA-Seq dataset was analyzed using the reference $reference. ";
    if (-e $filtgtf) {
	print "The Ensembl GTF file was filtered to remove annotations for non-protein-coding features.\n";
	$args->{methods} .= "The Ensembl GTF annotation file was filtered to remove annotations for non-protein-coding features. ";
	$args->{gtffile} = $filtgtf;
    }
#    print "using GTF file: $args->{gtffile}\n";
    $args->{gtffile} = abs_path($args->{gtffile});
    if ($args->{maskfile}) {
	die "ERROR: Could not locate maskfile $args->{maskfile}\n" if (! -e $args->{maskfile});
	$args->{maskfile} = abs_path($args->{maskfile});
    }
    if ($args->{hisat2index}) {
	$args->{hisat2index} = hisat2indexcheck($args->{hisat2index});
    }

    ### Finish setup
    requiredprograms(("fastqc", "\$TRIMMOMATIC", "\$PICARD", "parallel", "R", "samtools", "cufflinks", "hisat2")); # check that required programs are installed
    diegracefully();
    readinextraoptions($args);
    samplesheetandfoldersetup($samples, $args, $stats, "rnaseq");

    # create a splice site file
    $args->{hisat2splicefile} = "$args->{scratchfolder}/splicesites.txt";
    &run("hisat2_extract_splice_sites.py $args->{gtffile} > $args->{hisat2splicefile}", $args->{logfile});

}

######################## Singlesample jobs ##########################
sub singlesamples {
    print "\n### Analyzing samples ###\n";

    my ($samples, $args, $stats) = @_;

    my $myscratchfolder = "$args->{scratchfolder}/singlesamples";
    mkdir $myscratchfolder;

    # set up nodefile
    $nodefile = "$myscratchfolder/nodelist.txt";
    $nodecount = 1;
    my $junk;
    if ($ENV{PBS_NODEFILE}) {
	$result = `sort -u $ENV{PBS_NODEFILE} > $nodefile`;
	print $result;
	$nodecount = `wc -l < $nodefile`;
	chomp $nodecount;
    }

    # pass arguments through to singlesample.pl
    my $qc = $args->{qualitycontrol} ? "--qualitycontrol" : "";
    my $subsample = $args->{subsample} ? "--subsample $args->{subsample}" : "";
    my $extraoptions = $args->{extraoptionsfile} ? "--extraoptionsfile $args->{extraoptionsfile}" : "";
    my $mask = $args->{maskfile} ? "--maskfile $args->{maskfile}" : "";
    my $headcrop = $args->{headcrop} ? "--headcrop $args->{headcrop}" : "";
    my $library = $args->{library} ? "--library $args->{library}" : "";
    my $index = "--hisat2index $args->{hisat2index}";
    my $hisat2splicefile = $args->{hisat2splicefile} ? "--hisat2splicefile $args->{hisat2splicefile}" : "";
    my $cufflinks = $args->{cufflinks} ? "--cufflinks" : "";
    my $nofastqc = $args->{nofastqc} ? "--nofastqc" : "";

    # print out singlesample commandlines
    my $commandfile = "$myscratchfolder/singlesample-commands.txt";
    open OFILE, ">$commandfile" or die "Cannot open $commandfile: $!\n";
    foreach $sample (keys %{$samples}) {
	if (($args->{resume}) && (-e "$myscratchfolder/$sample/Complete")) {
	    print "Skipping completed sample $sample\n";
	    next;
	}
	my $r1 = "--R1 $samples->{$sample}{R1}{fastq}";
        $r2 = ($args->{pe}) ? "--R2 $samples->{$sample}{R2}{fastq}" : "";
        $log = "$args->{scratchfolder}/logs/$sample.log";
	$command = "rnaseq-singlesample.pl --threads $args->{threadspersample} $qc $subsample $library $mask $hisat2splicefile --outputfolder $myscratchfolder/$sample --gtffile $args->{gtffile} $index $headcrop $cufflinks $nofastqc $extraoptions $r1 $r2 &> $log && echo \"$sample complete\" || echo \"$sample failed\"\n";
	print OFILE $command;
    }
    close OFILE;

    # run jobs in parallel
    mkdir "$args->{scratchfolder}/logs";
    if ($nodecount <= 1) { # single node
	system("cat $commandfile | parallel -j $args->{samplespernode}");
    } else { # multiple node
	system("cat $commandfile | parallel -j $args->{samplespernode} --sshloginfile $nodefile --workdir $ENV{PWD}");
    }

    # read in stats from each sample
    getsinglesamplestats($samples, $args, $stats);

}

######################## Cuffnorm ##########################
sub cuffnorm {
    print "\n### Running Cuffnorm ###\n";

    my ($samples, $args, $stats) = @_;

    my $myscratchfolder = "$args->{scratchfolder}/cuffnorm";
#    mkdir $myscratchfolder;

    # generate list of cxb files
    @samples = sort keys %{$samples};
    my $cxbfiles = "$args->{scratchfolder}/singlesamples/$samples[0]/abundances.cxb";
    for $i (1..$#samples) {
        $cxbfiles = "$cxbfiles $args->{scratchfolder}/singlesamples/$samples[$i]/abundances.cxb";
    }

    # run cuffnorm
    run("cuffnorm --no-update-check --output-dir $myscratchfolder --quiet -p $args->{threads} $args->{gtffile} $cxbfiles", $args->{logfile});

    # rename samplenames
    open OFILE, ">$myscratchfolder/cufftablerename-samplelist.txt" or die "cannot open $myscratchfolder/cufftablerename-samplelist.txt: $!\n";
    for $i (0..$#samples) {
	$number = $i+1;
        print OFILE "q$number\t$samples[$i]\n";
    }
    close OFILE;

    $result = `cufftablerename.pl $myscratchfolder/genes.fpkm_table $myscratchfolder/cufftablerename-samplelist.txt > $myscratchfolder/genes.fpkm_table.renamed`;
    print $result if ($args->{verbose});
#    `mv *.RDATA $outputfolder`;
    `cp $myscratchfolder/genes.fpkm_table.renamed $args->{outputfolder}`;
    `cp $myscratchfolder/isoforms.fpkm_table $args->{outputfolder}`;

}

################### Differential Gene Expression #######################
sub differentialexpression {
    print "\n### Running differential expression ###\n";

    my ($samples, $args, $stats) = @_;

    my $myscratchfolder = "$args->{scratchfolder}/differentialexpression";
    mkdir $myscratchfolder;

    # get sample groups
    foreach $sample (keys %{$samples}) {
        if ($samples->{$sample}{group}) {
            push @groups, $samples->{$sample}{group};
            push @{$groups{$samples->{$sample}{group}}}, $sample;
        }
    }

    # quit if improper number of groups
    $groupnumber = keys %groups;
    if (($groupnumber < 2) or ($groupnumber > 3)) {
        if ($groupnumber == 0) {
            print "No groups defined in samplesheet, skipping differential expression testing\n";
        } else {
            print "$groupnumber groups defined in samplesheet, differential expression testing is only run if 2 or 3 groups are present\n";
        }
        return;
    }

    ### cuffdiff ###
    # generate command line
    $cuffdifffiles = "";
    foreach $group (keys %groups) {
        foreach $sample (@{$groups{$group}}) {
            $cuffdifffiles .= "$args->{scratchfolder}/singlesamples/$sample/abundances.cxb,";
        }
        chop $cuffdifffiles; # remove trailing ,
        $cuffdifffiles .= " ";
    }
    # run cuffdiff
    run("cuffdiff --output-dir $myscratchfolder --num-threads $args->{threads} $args->{gtffile} $cuffdifffiles", $args->{logfile});

    ### DESeq2 ###
    # TODO...

}

################### All-sample jobs #######################
sub allsample {
    print "\n### Processing results ###\n";

    my ($samples, $args, $stats) = @_;

    my $scratchfolder = $args->{scratchfolder};
    my $myscratchfolder = "$scratchfolder/allsamples";
    mkdir $myscratchfolder;
    my $outputfolder = $args->{outputfolder};
    chdir $myscratchfolder;

    @samples = sort keys %{$samples};

    ### generate plots ###
    subsampleplotrmd($samples, $args, $stats);

    myrinplotrmd($samples, $args, $stats);

    fastqcplotrmd($samples, $args, $stats, "short");

    hisat2plotrmd($samples, $args, $stats);

    insertsizeplot($samples, $args, $stats);

#    myrnaseqmetricsplot($samples, $args, $stats);

    myrrnaplotrmd($samples, $args, $stats);

    mysubreadplot($samples, $args, $stats);

    ### create folders ###
    # move singlesample logs to log folder
#    compilefolder($samples, $args, $stats, "logs", "log", "-log.txt");
    # copy log files to the output folder
#    `cp -rL $args->{scratchfolder}/logs $args->{outputfolder}`;

   # copy .bam and .bam.bai to bam folder
    compilefiles($samples, $args, $stats, "bam", "bam", ".bam");
    compilefiles($samples, $args, $stats, "bai", "bam", ".bam.bai");

    if ($args->{cufflinks}) {
	# cuffquant folder of .cxb files
	compilefolder($samples, $args, $stats, "cuffquant", "abundances.cxb", ".cxb");
	# save a tarball to outputfolder
	$result = `tar -zcf $args->{outputfolder}/cuffquant-cxb-files.tar.gz $args->{scratchfolder}/cuffquant/*.cxb`;
	print $result;
    }

    ### subread merge ###
    print "Running subread-merge.pl\n" if ($args->{verbose});
    open OFILE, ">subread-samplelist.txt" or die "cannot open subread-samplelist.txt: $!\n";
    for $sample (@samples) {
        print OFILE "$scratchfolder/singlesamples/$sample/subread-counts.txt\t$sample\n";
    }
    close OFILE;
    $result = `subread-merge.pl -f subread-samplelist.txt > $myscratchfolder/subread.txt`;
    print $result;
    `cp $myscratchfolder/subread.txt $outputfolder`;

    expressiontableplotrmd($samples, $args, $stats, "$myscratchfolder/subread.txt", "subread");

#    `cp $outputfolder/genes.fpkm_table.renamed .`; # so only the filename shows up in the report, not the full path
#    expressiontableplotrmd(\%samples, \%args, \%stats, "genes.fpkm_table.renamed", "cuffquant");
    
}


############################ Rmd Report #############################
sub reportrmd {
    print "\n### Generating Analysis Report ###\n";

    my ($samples, $args, $stats, $title) = @_;

    ### report header
    $info{"Library type"} = $args->{library} if ($args->{library});
    reportheader($samples, $args, $stats, $title, \%info);

    ### report body
    print $reportFH $args->{reporttext};

    ### data
    print $reportFH "\n<h2> Data</h2>\n";
    print $reportFH qq(This analysis generates these folders and files:<br>\n);
    print $reportFH qq(bam/ Sorted bam alignment files and bai indexes<br>\n);
    print $reportFH qq(fastqc/ FastQC html files<br>\n) unless ($args->{nofastqc});
    print $reportFH qq(fastqc-trim/ FastQC html post-trimming/filtering<br>\n) 	if ($args->{qualitycontrol} && ! $args->{nofastqc});
    print $reportFH qq(cuffquant-cxb-files.tar.gz Cuffnorm-generated cxb files for all samples<br>\n) if ($args->{cufflinks});
    print $reportFH qq(subread.txt Raw count expression table generated by Subread featureCounts<br>\n);
    print $reportFH qq(genes.fpkm_table.renamed FPKM read count table<br>\n) if ($args->{cufflinks});
#    print $reportFH qq(DESeq2-data.RDATA Raw count expression data in DESeq2 format<br>\n);
#    print $reportFH qq(DESeq2.html Instructions for using DESeq2 file<br>\n);

    ### methods
    print $reportFH qq(\n## Methods
$args->{methods}
);
    ### acknowledgements

    ### footer

    close $reportFH;

    ### generate pdf and html from rmd, copy over to output folder
    &run("Rscript -e \"library('rmarkdown'); render('$reportfolder/report.rmd', output_format='html_document', output_file='$args->{outputfolder}/report.html')\"", $args->{logfile}); # generate html

}

####################### helper subs #########################

### rnaseqmetricsplot ###
sub myrnaseqmetricsplot {
    my ($samples, $args, $stats) = @_;

    print "Running rnaseqmetricsplot\n" if ($args->{verbose});
    open OFILE, ">rnaseqmetrics-filelist.txt" or die "cannot open rnaseqmetrics-filelist.txt: $!\n";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "$args->{scratchfolder}/singlesamples/$sample/rnaseqmetrics.txt\t$sample\n";
    }
    close OFILE;
    $result = `rnaseqmetricsplotrmd.pl -f rnaseqmetrics-filelist.txt`;

    # process the stdout, grabbing some stats
    &getstats($args, $stats, $result, "rnaseqmetricsplot");

    ### add to report ###
    `mv rnaseqmetricsplot.* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = 'rnaseqmetricsplot.rmd'}\n```\n);
}

### subreadplot ###
sub mysubreadplot {
    my ($samples, $args, $stats) = @_;

    print "Generating Subread plot\n"; # if ($args->{verbose});
    open OFILE, ">subreadplot-filelist.txt" or die "cannot open subreadplot-filelist.txt: $!\n";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "$args->{scratchfolder}/singlesamples/$sample/subread-counts.txt.summary\t$sample\n";
    }
    close OFILE;
    $result = `subreadplotrmd.pl -f subreadplot-filelist.txt`;

    # process the stdout, grabbing some stats
    &getstats($args, $stats, $result, "subreadplot");

    ### add to report ###
    `mv subreadplot.* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = 'subreadplot.rmd'}\n```\n);
}

### rrnaplotrmd ###
sub myrrnaplotrmd {
    print "Generating rRNA screen plot\n";

    my ($samples, $args, $stats) = @_;

    # generate rrna plot
    $name = "rrnaplot";
    $ofile = "$reportfolder/$name.dat";
    $rfile = "$reportfolder/$name.rmd";
    open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

    # generate order for report sorting
    @order = sort {$stats->{$b}{rrna}{pct_rrna} <=> $stats->{$a}{rrna}{pct_rrna}} keys %{$stats};
    $order = "";
    foreach $sample (@order) {
	$order .= "\"$sample\",";
    }
    chop $order; # remove last ,
    $args->{order}{"rRNA"} = $order;

    # print out the data
    print OFILE "sample\trrna\n";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "$sample\t$stats->{$sample}{rrna}{pct_rrna}\n";
    }
    close OFILE;

    $title = "Ribosomal RNA Content";
    $text = "The percent of reads aligning to a ribosomal database (Silva) is shown. For most RNA-Seq experiments ribosomal RNA is not desired, and significant ribosomal content is an indicator of poor sample quality. Some RNA-Seq library construction methods have higher levels of rRNA.";

    open RFILE, ">$rfile" or die "Cannot open $rfile\n";
    print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~rrna,
 type = "bar",
hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>rRNA",datat\$rrna,"%")
) %>% 
 layout(xaxis = sampleorder, dragmode='pan', barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Percent rRNA", range = c(0, 100), fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
    close RFILE;
    $args->{reporttext} .= qq(```{r test-main, child = '$rfile'}\n```\n);

}

### rinplotrmd ###
sub myrinplotrmd {
    print "Generating RNA RIN plot\n";

    my ($samples, $args, $stats) = @_;

    my $firstsample = (keys %{$stats})[0];
    if (!defined($stats->{$firstsample}{qc}{rin})) {
	print "Skipping RIN plot, no RIN QC data found in Illumina samplesheet\n";
	return;
    }

    # generate rin plot
    $name = "rinplot";
    $ofile = "$reportfolder/$name.dat";
    $rfile = "$reportfolder/$name.rmd";
    open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

    # generate order for report sorting
    @order = sort {$stats->{$b}{qc}{rin} <=> $stats->{$a}{qc}{rin}} keys %{$stats};
    $order = "";
    foreach $sample (@order) {
	$order .= "\"$sample\",";
    }
    chop $order; # remove last ,
    $args->{order}{"RIN"} = $order;

    # print out the data
    print OFILE "sample\trin\tquality\n";
    foreach $sample (sort keys %{$samples}) {
	$rin = $stats->{$sample}{qc}{rin};
	if ($rin < 7) {
	    $quality = "Poor";
	} elsif ($rin < 8) {
	    $quality = "Marginal";
	} else {
	    $quality = "Good";
	}
	
	print OFILE "$sample\t$stats->{$sample}{qc}{rin}\t$quality\n";
    }
    close OFILE;

    $title = "RNA Integrity Number (RIN)";
    $text = "The RIN is a measure of RNA sample integrity generated by the Agilent Bioanalyzer instrument. Lower RIN values indicate lower quality RNA, which negatively impacts the accuracy and reproducibility of gene expression estimates. [More information.](https://en.wikipedia.org/wiki/RNA_integrity_number)";

    open RFILE, ">$rfile" or die "Cannot open $rfile\n";
    print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~rin,
 color = ~quality,
 colors = c(Poor='#d62728',Marginal='#bcbd22',Good='#2ca02c'),
 type = "bar",
hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>RIN",datat\$rin)
) %>% 
 layout(xaxis = sampleorder, dragmode='pan', legend = list(orientation = 'h'), barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "RIN", range = c(0, 10), fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
    close RFILE;
    $args->{reporttext} .= qq(```{r test-main, child = '$rfile'}\n```\n);

}



sub oldreport {

    my ($samples, $args, $stats, $title) = @_;

    # print example cuffdiff command
    my $cuffdifffiles = "";
    foreach $sample (sort keys %{$samples}) {
        $cuffdifffiles .= "$sample.cxb,";
    }
    chop $cuffdifffiles; # remove last comma
#    &line("To run Cuffdiff uncompress the cuffquant-cxb-files.tar.gz file and run the following command in the cuffquant folder. Cxb files in the same experimental group must be listed together and separated by a comma; a space must separate groups::\n\n\tcuffdiff --num-threads 1 $args->{gtffile} $cuffdifffiles\n");


#    &line("$subsample Quality of data in fastq files is assessed using FastQC. $quality Reads are aligned using Hisat2. FPKM expression values are generated using Cuffquant and Cufnorm from the Cufflinks package, and Raw read counts are generated using featureCounts from the Subread R package.");
    &space;    

}


