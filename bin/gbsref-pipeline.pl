#!/usr/bin/perl -w

#######################################################################
#  Copyright 2016 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 DESCRIPTION

gbsref-pipeline - Align gbs fastq files to a reference genome, call variants

=head1 SYNOPSIS

gbsref-pipeline --fastqfolder folder --bwaindex index --referencefasta file

gbsref-pipeline --fastqfolder folder --bowtie2index index --referencefasta file

=head1 OPTIONS

Analyze gbs fastq files

Options:
 --enzyme enzyme : enzyme used to generate GBS library
 --enzyme2 enzyme : Second enzyme used in a double diggest to generate GBS library
 --bowtie2index string : Bowtie2 index basename
 --bwaindex string : BWA index basename
 --referencefasta file : A fasta file containing the reference genome

Advanced options:
 --adapterfile file : A file containing adapter sequences for trimmomatic
 --padding padding : Padding sequences used in GBS library construction
 --padding2 padding : Padding sequences used with second enzyme in double digest in GBS library construction
 --headcrop integer : Remove this many bases off the front of each read (for removing padding sequences), disables padding recognition
 --maxlength integer : Crop reads to this length
 --minlength integer : Reads shorter than this are discarded
 --ploidy integer : Ploidy of the species (default = 2 = diploid)
 --freebayesthreads integer : Number of threads to use when running freebayes
 --coverage : Calculate coverage stats with bedtools (takes a while)
 --samplecutoff 0.95 : Samples/individuals with genotyping call rate less than threshold will be excluded
 --markercutoff 0.95 : Markers/variants with genotyping call rate less than threshold will be excluded
 --maf 0.01 : Minimum minor allele frequency

Standard gopher-pipeline options
 --fastqfolder folder : A folder containing fastq files to process
 --subsample integer : Subsample the specified number of reads from each sample. 0 = no subsampling (default = 0)
 --samplesheet file : A samplesheet
 --runname string : Name of the sequencing run
 --projectname string : Name of the experiment (UMGC Project name)
 --illuminasamplesheet file : An illumina samplesheet, from which extra sample information can be obtained
 --nofastqc : Don't run FastQC
 --samplespernode integer : Number of samples to process simultaneously on each node (default = 1)
 --threadspersample integer : Number of threads used by each sample
 --scratchfolder folder : A temporary/scratch folder
 --outputfolder folder : A folder to deposit final results
 --extraoptionsfile file : File with extra options for trimmomatic, tophat, cuffquant, or featurecounts
 --resume : Continue where a failed/interrupted run left off
 --verbose : Print more information while running
 --help : Print usage instructions and exit

The --padding option will apply the padding to all samples. The padding can alos be provided in a samplesheet (padding and padding2 columngs), or else the default padding sequence (in enzymes.pm) for the enzyme will be used.

The --enzyme will apply the enzyme to all samples. The enzyme can also be provided in a samplesheet.

=cut

##################### Main ###############################

use FindBin;                  # locate this script
use lib "$FindBin::RealBin";  # use the current directory
use Pipelinev4;
use lib "$ENV{GOPHER_PIPELINES}/software/gbs-scripts"; # so we can load enzymes
use enzymes; # so we can assign padding sequences

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;

my %samples;
my %args;
my %stats;

### Initialize ###
&init(\%samples, \%args, \%stats);

### Singlesample analysis ###
&singlesamples(\%samples, \%args, \%stats);
runtime;

### Freebayes variant calling ###
&freebayes(\%samples, \%args, \%stats);
runtime;

### All sample processing ###
&allsample(\%samples, \%args, \%stats);
runtime;

### Metrics ###
$args{stageorder} = ["general", "subsample", "fastqc", "trimmomatic", "fastqc-trim", "bwa", "bowtie2"];
allmetrics(\%samples, \%args, \%stats);

### R markdown report ###
&reportrmd(\%samples, \%args, \%stats, "GBS Reference Report");

print "Finished\n";
runtime;

exit;

##################### Initialize ###############################
sub init {
    print "\n### Initializing run ###\n";

    my ($samples, $args, $stats) = @_;

    # set defaults
    $args->{options} = join " ", @ARGV;
    $args->{threadspersample} = 8;
    if ($ENV{"PBS_NUM_PPN"}) {
        $args->{samplespernode} = int($ENV{"PBS_NUM_PPN"} / $args->{threadspersample} + 0.5);
    } else {
        $args->{samplespernode} = 1;
    }
    $args->{adapterfile} = "$GPRESOURCES/all_adapters.fa";
    $args->{minlength} = 1;
    $args->{maxlength} = 1000;
    $args{samplecutoff} = 0.5;
    $args{markercutoff} = 0.95;
    $args{maf} = 0.01;
    $args->{referencefasta} = $ENV{GENOME_FA} // "";
    $args->{bwaindex} = $ENV{BWA_INDEX} // "";
    GetOptions("help" => \$args->{help},
               "verbose" => \$args->{verbose},
               "resume" => \$args->{resume},
               "extraoptionsfile=s" => \$args->{extraoptionsfile},
               "outputfolder=s" => \$args->{outputfolder},
               "scratchfolder=s" => \$args->{scratchfolder},
               "threadspersample=i" => \$args->{threadspersample},
               "samplespernode=i" => \$args->{samplespernode},
               "nofastqc" => \$args->{nofastqc},
               "illuminasamplesheet=s" => \$args->{illuminasamplesheet},
               "projectname=s" => \$args->{projectname},
               "runname=s" => \$args->{runname},
               "samplesheet=s" => \$args->{samplesheet},
               "subsample=i" => \$args->{subsample},
               "fastqfolder=s" => \$args->{fastqfolder},
	       # gbsref options
               "qualitycontrol" => \$args->{qualitycontrol},
	       "referencefasta=s" => \$args->{referencefasta},
	       "bwaindex=s" => \$args->{bwaindex},
	       "bowtie2index=s" => \$args->{bowtie2index},
               "adapterfile=s" => \$args->{adapterfile},
	       "enzyme=s" => \$args->{enzyme},
	       "enzyme2=s" => \$args->{enzyme2},
	       "padding=s" => \$args->{padding},
	       "padding2=s" => \$args->{padding2},
	       "headcrop=i" => \$args->{headcrop},
	       "minlength=i" => \$args->{minlength},
	       "maxlength=i" => \$args->{maxlength},
	       "freebayesthreads=i" => \$args->{freebayesthreads},
	       "ploidy=i" => \$args->{ploidy}, 
	       "coverage" => \$args->{coverage},
	       "samplecutoff=f" => \$args{samplecutoff},
	       "markercutoff=f" => \$args{markercutoff},
	       "maf=f" => \$args{maf},
	) or pod2usage;
    pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args->{help});
    pod2usage unless ( $args->{fastqfolder} and ($args->{bwaindex} or $args->{bowtie2index}) and $args->{referencefasta} );
    if ($#ARGV >= 0) {
        print "Unknown commandline parameters: @ARGV\n";
	pod2usage;
    }
    $args->{threads} = $ENV{"PBS_NUM_PPN"} // ($args->{threadspersample} * $args->{samplespernode}) / 2; # threads used by pipeline stages (not singlesample stages)
    $args->{extraoptionsfile} = abs_path($args->{extraoptionsfile}) if ($args->{extraoptionsfile});

    ### Starttime ###
    my $date = `date`;
    chomp $date;
    print "Starting at $date\n";
    $args->{startdate} = $date;
    &runtime;

    ### Additional Parameters ###
    # add additional parameter processing here
    $args->{bowtie2index} = bowtie2indexcheck($args->{bowtie2index}) if ($args->{bowtie2index});
    $args->{bwaindex} = bwaindexcheck($args->{bwaindex}) if ($args->{bwaindex});
    die "Cannot find reference fasta file $args->{referencefasta}\n" if (!-e $args->{referencefasta});
    $args->{referencefasta} = abs_path($args->{referencefasta});
    $args->{enzyme} = lc($args->{enzyme}) if ($args->{enzyme});
    $args->{enzyme2} = lc($args->{enzyme2}) if ($args->{enzyme2});

    $reference = "";
    if ($ENV{LATIN_NAME}) {
        $reference = $ENV{COMMON_NAME};
    }
    if ($ENV{COMMON_NAME}) {
        if ($reference) {
            $reference .= " ($ENV{LATIN_NAME})";
        } else {
            $reference = "$ENV{LATIN_NAME}";
        }
    }
    if ($ENV{BUILD_NAME}) {
        $reference .= " genome assembly \"$ENV{BUILD_NAME}\"";
    }
    $reference = $args{bwaindex} unless ($reference);

    $args->{methods} = "The GBS dataset was analyzed using the reference $reference. ";

    ### Finish setup
    requiredprograms(("fastqc", "\$TRIMMOMATIC", "bowtie2", "bwa", "\$PICARD", "freebayes"));
    diegracefully();
    readinextraoptions($args);
    samplesheetandfoldersetup($samples, $args, $stats, "gbsref");
    my @programs = ("fastqc", "trimmomatic", "bowtie2", "bwa", "picard", "samstat", "samtools", "freebayes");

}

######################## Singlesample jobs ##########################
sub singlesamples {
    print "\n### Analyzing samples ###\n";

    my ($samples, $args, $stats) = @_;

    my $myscratchfolder = "$args->{scratchfolder}/singlesamples";
    mkdir $myscratchfolder;

   # set up nodefile
    $nodefile = "$myscratchfolder/nodelist.txt";
    $nodecount = 1;
    my $junk;
    if ($ENV{PBS_NODEFILE}) {
        $result = `sort -u $ENV{PBS_NODEFILE} > $nodefile`;
        print $result;
	$nodecount = `wc -l < $nodefile`;
	chomp $nodecount;
    }

    # pass arguments through to singlesample.pl
    my $subsample = $args->{subsample} ? "--subsample $args->{subsample}" : "";
    my $adapterfile = $args->{adapterfile} ? "--adapterfile $args->{adapterfile}" : "";
    my $extraoptions = $args->{extraoptionsfile} ? "--extraoptionsfile $args->{extraoptionsfile}" : "";
    my $qualitycontrol = $args->{qualitycontrol} ? "--qualitycontrol" : "";
    my $verbose = $args->{verbose} ? "--verbose" : "";
    my $bwaindex = $args->{bwaindex} ? "--bwaindex $args->{bwaindex}" : "";
    my $bowtie2index = $args->{bowtie2index} ? "--bowtie2index $args->{bowtie2index}" : "";
    my $nofastqc = $args->{nofastqc} ? "--nofastqc" : "";
    my $coverage = $args->{coverage} ? "--coverage" : "";
    my $headcrop = $args->{headcrop} ? "--headcrop $args->{headcrop}" : "";
    my $minlength = $args->{minlength} ? "--minlength $args->{minlength}" : "";
    my $maxlength = $args->{maxlength} ? "--maxlength $args->{maxlength}" : "";
    my $ploidy = $args->{ploidy} ? "--ploidy $args->{ploidy}" : "";

    # print out singlesample commandlines
    my $commandfile = "$myscratchfolder/singlesample-commands.txt";
    open OFILE, ">$commandfile" or die "Cannot open $commandfile: $!\n";
    foreach $sample (keys %{$samples}) {
	my $enzyme = $samples->{$sample}{enzyme} ? "--enzyme $samples->{$sample}{enzyme}" : "";
	my $enzyme2 = $samples->{$sample}{enzyme2} ? "--enzyme2 $samples->{$sample}{enzyme2}" : "";
	my $padding = $samples->{$sample}{padding} ? "--padding $samples->{$sample}{padding}" : "";
	my $padding2 = $samples->{$sample}{padding2} ? "--padding2 $samples->{$sample}{padding2}" : "";
	if ($args->{headcrop}) {
	    $padding = "";
	    $padding2 = "";
	}
	$r1 = "--R1 $samples->{$sample}{R1}{fastq}";
	$r2 = $args->{pe} ? "--R2 $samples->{$sample}{R2}{fastq}" : "";

        if (($args->{resume}) && (-e "$myscratchfolder/$sample/Complete")) {
            print "Skipping completed sample $sample\n";
            next;
        }
	my $readgroup = "--readgroup $sample";
	$log = "$args->{scratchfolder}/logs/$sample.log";
	$command = "gbsref-singlesample.pl $verbose --threads $args->{threadspersample} --referencefasta $args->{referencefasta} $subsample $adapterfile $extraoptions $qualitycontrol $enzyme $enzyme2 $padding $padding2 $headcrop $minlength $maxlength $bowtie2index $bwaindex $ploidy $nofastqc --outputfolder $myscratchfolder/$sample $r1 $r2 $readgroup $coverage &> $log && echo \"$sample complete\" || echo \"$sample failed\"\n";
        print OFILE $command;
    }
    close OFILE;

   # run jobs in parallel
    mkdir "$args->{scratchfolder}/logs";
    if ($nodecount <= 1) { # single node
        system("cat $commandfile | parallel -j $args->{samplespernode}");
    } else { # multiple node
        system("cat $commandfile | parallel -j $args->{samplespernode} --sshloginfile $nodefile --workdir $ENV{PWD}");
    }

    # read in stats from each sample
    getsinglesamplestats($samples, $args, $stats);

}

################### All-sample jobs #######################
sub allsample {
    print "\n### Processing results ###\n";

    my ($samples, $args, $stats) = @_;

    my $scratchfolder = $args->{scratchfolder};
    my $myscratchfolder = "$scratchfolder/allsamples";
    mkdir $myscratchfolder;
    my $outputfolder = $args->{outputfolder};
    chdir $myscratchfolder;

    ### generate plots ###
    subsampleplotrmd($samples, $args, $stats);

    fastqcplotrmd($samples, $args, $stats, "short");

#    trimmomaticplot($samples, $args, $stats) if ($args->{qualitycontrol});

    alignmentsummarymetricsplotrmd($samples, $args, $stats);

    insertsizeplot($samples, $args, $stats);

#    freebayesplot($samples, $args, $stats);

#    vcfmetricsplotrmd($samples, $args, $stats, $args->{vcffilter}) if ($args->{vcffilter});
    vcfmetricsplotrmd($samples, $args, $stats, $args->{vcf}) if ($args->{vcf});
    `gzip -c $args->{vcf} > $args->{outputfolder}/variants.vcf.gz` if ($args->{vcf});

    `gzip -c $args->{vcffilter} > $args->{outputfolder}/variants.filt.vcf.gz` if ($args->{vcffilter});

    &coverageplot($samples, $args, $stats) if ($args->{coverage});

    ### create folders ###
    # move singlesample logs to log folder
    compilefolder($samples, $args, $stats, "logs", "log", "-log.txt");
    # copy log files to the output folder
#    `cp -rL $args->{scratchfolder}/logs $args->{outputfolder}`;

    # move .bam and .bam.bai to bam folder
    my $sourcefile = "";
    if ($args->{bwaindex}) {
	$sourcefile = "bwa.bam";
    } elsif ($args->{bowtie2index}) {
	$sourcefile = "bowtie2.bam";
    }

    compilefolder($samples, $args, $stats, "bam", $sourcefile, ".bam");

    compilefolder($samples, $args, $stats, "bam.bai", "${sourcefile}.bai", ".bam.bai");

}

############################ Rmd Report #############################
sub reportrmd {
    print "\n### Generating Analysis Report ###\n";

    my ($samples, $args, $stats, $title) = @_;

    ### report header
    $info{"Library type"} = $args->{library} if ($args->{library});
    reportheader($samples, $args, $stats, $title, \%info);

    ### report body

    print $reportFH $args->{reporttext};

    ### data
    print $reportFH "\n<h2> Data</h2>\n";
    print $reportFH qq(The output folder generated by this analysis pipeline contains the following folders and files:<br>\n);
    print $reportFH qq(fastqc/ FastQC html files<br>\n) unless ($args->{nofastqc});
    print $reportFH qq(fastqc-trim/ FastQC html post-trimming/filtering<br>\n) 	if ($args->{qualitycontrol} && ! $args->{nofastqc});
    print $reportFH qq(aligned/ fastq files containing reads that aligned to the reference<br>\n) if ($args->{bam2fastq});
    print $reportFH qq(unaligned/ fastq files containing reads that did not align to the reference<br>\n) if ($args->{bam2fastq});
    print $reportFH qq(variants.vcf.gz VCF variant file<br>\n) if ($args->{bam2fastq});
    print $reportFH qq(variants.filt.vcf.gz Filtered VCF variant file<br>\n) if ($args->{bam2fastq});

    gbsvcfreport($samples, $args, $stats, $reportFH);

    ### methods
    print $reportFH qq(\n## Methods
$args->{methods}
);
    ### acknowledgements

    ### footer

    close $reportFH;

    ### generate pdf and html from rmd, copy over to output folder
    &run("Rscript -e \"library('rmarkdown'); render('$reportfolder/report.rmd', output_format='html_document', output_file='$args->{outputfolder}/report.html')\"", $args->{logfile}); # generate html

}

################################ Helper subs ###############################

### coverageplot ###
sub coverageplot {
    my ($samples, $args, $stats) = @_;

    print "Generating coverage plot\n" if ($args->{verbose});
    open OFILE, ">coverage-filelist.txt" or die "cannot open coverage-filelist.txt: $!\n";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "$args->{scratchfolder}/singlesamples/$sample/coverage.txt\t$sample\n";
    }
    close OFILE;
    @result = `coverageplot.pl -f coverage-filelist.txt`;
    `mv coverageplot.png $args->{outputfolder}`;
    # save stats from stdout
    $statzone = 0;
    foreach $result (@result) {
	$statzone = 0 if ($result =~ /^STATS End/);
	if ($statzone) {
	    chomp $result;
	    ($sample, $key, $stat) = split /\t/, $result;
	    $stats->{$sample}{coverage}{$key} = $stat;
	}
	$statzone = 1 if ($result =~ /^STATS Start/);
    }

    # add to report
    &h1("Coverage Plot");
    &image("coverageplot.png");
    &space();
}


