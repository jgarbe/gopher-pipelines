#!/usr/bin/perl -w

######################################################
# covid-singlesample.pl
# John Garbe
# May 2015
# Re-write May 2016
#
#######################################################

=head1 NAME

covid-singlesample.pl - Align a fastq file (single- or paired-end) to a reference covid genome, generate consensus and vcf file

=head1 SYNOPSIS

covid-singlesample.pl --referencefasta file --bowtie2index index --bwaindex index --R1 R1.fastq [--R2 R2.fastq] [--outputfolder folder]

=head1 DESCRIPTION

Align a fastq file to a reference using Bowtie2 and/or BWA:

 --bowtie2index string : Bowtie2 index basename
 --bwaindex string : BWA index basename
 --hostbwaindex string : Host BWA index basename
 --referencefasta file : A fasta file containing the reference genome
 --readgroup name : String to use as read group ID and SM in bam file
 --removeduplicates : Remove duplicates using Picard
 --baitbed file : Capture bait file, BED format - turns on PicardHsMetrics
 --targetbed file : Capture target file, BED format - baits file is used by default
 --baitfile file : Capture bait file, interval format - turns on PicardHsMetrics
 --targetfile file : Capture target file, interval format - baits file is used by default
 --outputfolder folder : The output folder
 --threads integer : Number of processors to use (number of threads to run)
 --verbose : Print more information while running
 --subsample integer : Subsample the specified number of reads from each sample. 0 = no subsampling (default = 0)
 --qualitycontrol : Enable quality-control: use trimmomatic to trim adapter sequences, trim low quality bases from the ends of reads, and remove short sequences
 --adapterfile file : A file containing adapter sequences for trimmomatic
 --extraoptionsfile file : File with extra options for trimmomatic, BWA, or Bowtie2
 --help : Print usage instructions and exit
 --gbsenzyme enzyme : enzyme used to generate GBS library
 --gbspadding padding : Padding sequences used in GBS library construction
 --amplicon : amplicon data - remove primers (assumes ARTIC v2)
 --pacbio : PacBio dataset
 --nofastqc : skip FastQC
=cut

##################### Main ###############################

use FindBin;                  # locate this script directory
use lib "$FindBin::RealBin";  # look for modules in the script directory
use Pipelinev4;               # load the pipeline module

use Getopt::Long;
use Pod::Usage;
use Cwd 'abs_path';
use File::Temp qw( tempdir );

my %args;
my %stats;

### Initialize ###
&init(\%args, \%stats);

### Subsample ###
runtime;
subsample(\%args, \%stats);

### FastQC ###
if (! $args{nofastqc}) {
    runtime;
    fastqc(\%args, \%stats); 
}

if ($args{qualitycontrol}) {
    ### Trimmomatic ###
    runtime;
    trimmomatic(\%args, \%stats);

    ### FastQC ###
    runtime;
    fastqc(\%args, \%stats, "fastqc-trim");
}

### host removal ###
#runtime;
#myhostremoval(\%args, \%stats);

if ($args{stitch}) {
    ### stitch ###
    runtime;
    stitch(\%args, \%stats);
}

if ($args{bwaindex}) {
    ### BWA ###
    runtime;
    bwa(\%args, \%stats);
} elsif ($args{bowtie2index}) {
    ### Bowtie2 ###
    runtime;
    bowtie2(\%args, \%stats);
} else {
    die "No BWA, no Bowtie2, no soup!\n";
}

### iVar ###
runtime;
myivar(\%args, \%stats);

# a hack to remove the fasta index because it causes picard to crash???
#$fai = $args{referencefasta} . ".fai";
#`rm $fai` if (-e $fai);

### Picard MultipleMetrics ###
runtime;
collectmultiplemetrics(\%args, \%stats);

### Picard wgs metrics ### - don't need this, multipleMetrics covers it
#runtime;
#wgsmetrics(\%args, \%stats);

if ($args{removeduplicates}) {
    ### Picard Mark (remove) duplicates ###
    runtime;
    markduplicates(\%args, \%stats);

    ### samtools view filter ###
    runtime;
    samtoolsviewfilter(\%args, \%stats);
}

if ($args{baitfile}) {
    ### HsMetrics ###
    runtime;
    hsmetrics(\%args, \%stats);
}

### Write stats to file ###
runtime;
writestats(\%args, \%stats);

`touch $args{scratchfolder}/Complete`;

print "Finished\n";
runtime;

exit;

##################### Initialize ###############################
sub init {
    print "Running Sample Initialize\n";

    my ($args, $stats) = @_;

    ### Usage ###
    $usage = "USAGE: covid-singlesample.pl --referencefasta fasta --bwaindex index --bowtie2index index [--outputfolder folder] --R1 file [--R2 file] [--readgroup name]\n";

    # set defaults - add additional parameter defaults here
    $args->{threads} = $ENV{"PBS_NUM_PPN"} // 1;
    $args->{scratchfolder} = $ENV{PWD} . "/covid-singlesample";
    GetOptions("help" => \$args->{help},
               "verbose" => \$args->{verbose},
	       "R1=s" => \$args->{R1},
	       "R2=s" => \$args->{R2},
               "threads=i" => \$args->{threads},
               "outputfolder=s" => \$args->{scratchfolder},
               "extraoptionsfile=s" => \$args->{extraoptionsfile},
               "subsample=i" => \$args->{subsample},
               "qualitycontrol" => \$args->{qualitycontrol},
               "adapterfile=s" => \$args->{adapterfile},
	       "referencefasta=s" => \$args->{referencefasta},
	       "samplename=s" => \$args->{samplename},
	       "readgroup=s" => \$args->{readgroup},
	       "bwaindex=s" => \$args->{bwaindex},
	       "bowtie2index=s" => \$args->{bowtie2index},
	       "hostbwaindex=s" => \$args->{hostbwaindex},
	       "stitch" => \$args{stitch},
	       "removeduplicates" => \$args->{removeduplicates},
	       "qcplots" => \$args->{qcplots},
	       "baitbed=s" => \$args->{baitbed},
	       "targetbed=s" => \$args->{targetbed},
	       "baitfile=s" => \$args->{baitfile},
	       "targetfile=s" => \$args->{targetfile},
	       "gbsenzyme=s" => \$args->{gbsenzyme},
	       "gbspadding=s" => \$args->{gbspadding},
	       "pacbio" => \$args->{pacbio},
	       "amplicon=s" => \$args->{amplicon},
	       "bwamem=s" => \$args->{bwamem},
	       "nofastqc" => \$args->{nofastqc},
        ) or die $usage;
    pod2usage(q(-verbose) => 3) if ($args->{help});
    die "$usage" unless ($args->{R1} and ($args->{bwaindex} or $args->{bowtie2index}) and $args->{referencefasta} );
    $args->{extraoptionsfile} = abs_path($args->{extraoptionsfile}) if ($args->{extraoptionsfile});

    ### Starttime ###
    $date = `date`;
    chomp $date;
    print "Starting at $date\n";
    &runtime;

    ### Standard parameters ###
    $args->{scratchfolder} = abs_path($args->{scratchfolder});
    die "R1 Fasta file $args->{R1} not found\n" if (!-e $args->{R1});
    $args->{R1} = abs_path($args->{R1});
    $args->{pe} = 0;
    if ($args->{R2}) {
        die "R2 Fasta file $args->{R2} not found\n" if (!-e $args->{R2});
        $args->{R2} = abs_path($args->{R2});
        $args->{pe} = 1;
    }
    $args->{logfile} = "$args->{scratchfolder}/log" unless ($args->{verbose});

    ### non-Standard parameters ###
    # add additional parameters processing here
    $args->{bowtie2index} = bowtie2indexcheck($args->{bowtie2index}) if ($args->{bowtie2index});
    $args->{bwaindex} = bwaindexcheck($args->{bwaindex}) if ($args->{bwaindex});
    die "Cannot file reference fasta file $args->{referencefasta}\n" if (!-e $args->{referencefasta});
    $args->{referencefasta} = abs_path($args->{referencefasta});
    if ($args->{baitfile}) {
	die "ERROR: Baitfile file $args->{baitfile} not found\n" if (! -e $args->{baitfile});
	$args->{baitfile} = abs_path($args->{baitfile});
	die "ERROR: Targetfile file $args->{targetfile} not found\n" if ($args->{targetfile} and (! -e $args->{targetfile}));
	$args->{targetfile} = abs_path($args->{targetfile} // $args->{baitfile});
    } elsif ($args->{baitbed}) {
	die "ERROR: Baitbed file $args->{baitbed} not found\n" if (! -e $args->{baitbed});
	$args->{baitbed} = abs_path($args->{baitbed});
	die "ERROR: Targetbed file $args->{targetbed} not found\n" if ($args->{targetbed} and (! -e $args->{targetbed}));
	$args->{targetbed} = abs_path($args->{targetbed} // $args->{baitbed});
    }

    ### Finish setup
    # check that required programs are installed
    requiredprograms(("fastqc", "\$TRIMMOMATIC", "bowtie2", "bwa", "\$PICARD"));

    diegracefully();
    readinextraoptions($args);
    scratchfoldersetup($args);

    hsmetricsprep($args) if ($args->{baitbed} and ! $args->{baitfile}); # turn bed into interval file

}


######################## iVar ##########################
# run iVar to generate viral consensus sequence and VCF file
sub myivar {
    print "Running iVar\n";

    my ($args, $stats) = @_;

    # trim primers and low quality
    $trimbam = "$args->{scratchfolder}/trimmed.bam";
    $sortbam = "$args->{scratchfolder}/trimmed.sort.bam";
    $ivartrimstats = "$args->{scratchfolder}/ivartrim.stats";
    if ($args->{amplicon}) {
	&run("ivar trim -i $args->{bam} -b $args->{amplicon} -p trimmed > $ivartrimstats", $args->{logfile});
	$args->{methods} .= "iVar was used to quality trim aligned reads and remove primer sequences from aligned reads. ";
	# pull stats 
	open IFILE, $ivartrimstats or die "cannot open $ivartrimstats: $!\n";
	while ($line = <IFILE>) {
	    last if ($line =~ /^Primer Name/);
	}
	$sum = 0;
	while ($line = <IFILE>) {
	    chomp $line;
	    last if ($line eq "");
	    ($primer, $count) = split /\t/, $line;
	    $primer =~ s/(.*)(LEFT|RIGHT).*/$1$2/; # condense alternate primers together
	    $sum += $count;
	    $primerdata{$primer} += $count;
	}
	close IFILE;
	foreach $primer (keys %primerdata) {
	    if ($sum eq 0) {
		$stats->{ivartrim} = 0;
	    } else {
		$pct = $primerdata{$primer} / $sum * 100;
		$stats->{ivartrim}{$primer} = $pct;
	}
	}
	    
    } else {
	&run("ivar trim -i $args->{bam} -p trimmed > $ivartrimstats", $args->{logfile});
	$args->{methods} .= "iVar was used to quality trim aligned reads. ";
    }

#Trimmed primers from 66.6% (359) of reads.
#35.81% (193) of reads were quality trimmed below the minimum length of 30 bp and were not writen to file.
#15.4% (83) of reads that started outside of primer regions were not written to file.
#Trimmed primers from 0% (0) of reads.
#0% (0) of reads were quality trimmed below the minimum length of 30 bp and were not writen to file.
#100% (115) of reads started outside of primer regions. Since there were no primers found in BED file, these reads were written to file.

    # pull some more stats
    $statstext = `cat $ivartrimstats`;
    
    $mapped = $1 if ($statstext =~ /Found (.+) mapped reads/);
    $unmapped = $1 if ($statstext =~ /Found (.+) unmapped reads/);
    $trimmed = $1 if ($statstext =~ /Trimmed primers from (.+)\%/);
    $short = $1 if ($statstext =~ /(.+)\% \(\d+\) of reads were quality trimmed/);
    $outside = $1 if ($statstext =~ /(.+)\% \(\d+\) of reads that started outside of/);
    $stats->{ivar}{"pct-primerstrimmed"} = $trimmed // "undef";
    $stats->{ivar}{"pct-tooshort"} = $short // "undef";
    $stats->{ivar}{"pct-outsideofprimerregions"} = $outside // "undef";
    $stats->{ivar}{"mappedreads"} = $mapped // "undef";
    $stats->{ivar}{"unmappedreads"} = $unmapped // "undef";

    &run("samtools sort $trimbam > trimmed.sort.bam", $args->{logfile});
    &run("samtools index $sortbam", $args->{logfile});

    # calculate coverage
    $coveragefile = "$args->{scratchfolder}/coverage.txt";
    &run("covstats.sh $sortbam > $coveragefile", $args->{logfile});
    @mystats = `cat $coveragefile`;
    foreach $stat (@mystats) {
	chomp $stat;
	($metric, $value) = split ' ', $stat;
	print $stats->{ivar}{$metric} = $value;
    }

    # call variants
    &run("samtools mpileup -aa -A -d 0 -B -Q 0 --reference $args->{referencefasta} $sortbam | ivar variants -p variants -q 20 -t 0.2 -m 00 -r $args->{referencefasta}", $args->{logfile});
    $tsv = "$args->{scratchfolder}/variants.tsv";

    # filter variants
    $filttsv = "$args->{scratchfolder}/variants.filt.tsv";
    &run("tsv-filter.pl $tsv > $filttsv", $args->{logfile});

    # count filtered variants
    $variants = `wc -l < $filttsv`;
    chomp $variants;
    $variants--;
    $stats->{ivar}{variants} = $variants;

    # generate consensus sequence
    $consensus = "$args->{scratchfolder}/consensus.fa";
    &run("samtools mpileup -d 1000 -A -Q 0 $sortbam | ivar consensus -p consensus -q 20 -t 0 -m 00", $args->{logfile});

    $args->{bam} = $sortbam;
    $stats->{files}{tsv} = $tsv;
    $stats->{files}{consensus} = $consensus;
    $stats->{files}{bam} = "$sortbam";
    $stats->{files}{bai} = "$sortbam.bai";

    $args->{methods} .= "Variants were called and a consensus sequence was generated using samtools mpileup and iVar. A filtered tsv variant file was generated, variants with alt allele frequency less than .5 and variants in positions 1-54 and 29836-29903 were removed.";


}

sub myhostremoval {
    print "Running host removal\n";

    my ($args, $stats) = @_;

    my $bam = "$args->{scratchfolder}/host.bam";

    # make tmp folder unique 
    my $srttmp = tempdir("sortXXXX", DIR => "$args->{scratchfolder}/", CLEANUP => 1);
    $srttmp .= "/tmp";

    # figure out how much ram to throw at samtools sort
    my $memory = nodememory();
    my $mem = int($memory / $args->{threads} * .8); # use memory proportional to the number of threads we're supposed to use, then reduce by 20% safety factor
    # calculate size of fastq file(s)
    $fastqsize = `du -DB 1000000000 $args->{R1}`;
    ($fastqsize) = split /\t/, $fastqsize;
    $fastqsize = $fastqsize * 2 if ($args->{pe});
    # set mem to fastq size if less than proportianal memory
    $mem = $fastqsize if ($fastqsize < $mem);
    $mem = "${mem}G";
#    print STDERR "samtools sort mem: $mem\n";
    $mem = $args->{bwamem} // $mem; 
    
    $readgroup = ($args->{readgroup}) ? "-R '\@RG\\tID:$args->{readgroup}\\tSM:$args->{readgroup}'" : "";

    my $logging = ($args->{verbose}) ? "" : "2>>$args->{logfile}";
    if ($args->{pe}) {
        &run("bwa mem -v 3 -t $args->{threads} $readgroup $args->{hostbwaindex} $args->{R1} $args->{R2} $logging | samtools sort -m $mem -T $srttmp -\@ $args->{threads} -O bam -o $bam 1>&2", $args->{logfile});
    } else {
	print "$args->{threads}\n";
	print "$args->{hostbwaindex}\n";
	print "$args->{R1}\n";
	print "$srttmp\n";
	print "$bam\n";
	&run("bwa mem -t $args->{threads} $readgroup $args->{hostbwaindex} $args->{R1} $logging | samtools sort -m $mem -T $srttmp -\@ $args->{threads} -O bam -o $bam 1>&2", $args->{logfile});
    }
    die "BWA failure\n" unless (-e $bam);

    # index bam
    run("samtools index $bam");
    $args->{bam} = "$bam";
    $stats->{files}{bam} = "$bam";
    $stats->{files}{bai} = "$bam.bai";

    # generate unaligned fastq from bam
    if ($args->{pe}) {
	run("samtools fastq -f 4 -1 $args->{scratchfolder}/nohost_R1.fastq.gz -2 $args->{scratchfolder}/nohost_R2.fastq.gz $stats->{files}{bam} -s $args->{scratchfolder}/nohost_singletons.fastq.gz", $args->{logfile});
	$args->{R1} = "$args->{scratchfolder}/nohost_R1.fastq.gz";
	$args->{R2} = "$args->{scratchfolder}/nohost_R2.fastq.gz";
    } else {
	run("samtools fastq -f 4 -0 $args->{scratchfolder}/nohost_R1.fastq.gz $stats->{files}{bam}", $args->{logfile});
	$args->{R1} = "$args->{scratchfolder}/nohost_R1.fastq.gz";
    }
 
   
    # aligned reads: don't print if unmapped or not primary alignment
#    if ($args->{pe}) {
#	$command = "samtools fastq -F 4 -F 256 -1 $myscratchfolder/aligned/${sample}_R1.fastq.gz -2 $myscratchfolder/aligned/${sample}_R2.fastq.gz $stats->{$sample}{files}{bam}\n";
#    } else {
#	$command = "samtools fastq -F 4 -F 256 -0 $myscratchfolder/aligned/${sample}_R1.fastq.gz $stats->{$sample}{files}{bam}\n";
#    }

#    `ln -s ../bam2fastq/aligned $args->{outputfolder}/aligned`;
#    `ln -s ../bam2fastq/unaligned $args->{outputfolder}/unaligned`;

    $args->{methods} .= "BWA mem was used to align reads to a reference host genome ($args->{bwaindex}). Reads aligning to the host were removed. ";



}
