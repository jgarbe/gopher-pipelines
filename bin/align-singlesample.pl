#!/usr/bin/perl -w

######################################################
# align-singlesample.pl
# John Garbe
# May 2015
# Re-write May 2016
#
#######################################################

=head1 NAME

align-singlesample.pl - Align a fastq file (single- or paired-end) to a reference

=head1 SYNOPSIS

align-singlesample.pl --referencefasta file --bowtie2index index --bwaindex index --R1 R1.fastq [--R2 R2.fastq] [--outputfolder folder]

=head1 DESCRIPTION

Align a fastq file to a reference using Bowtie2 and/or BWA:

 --bowtie2index string : Bowtie2 index basename
 --bwaindex string : BWA index basename
 --referencefasta file : A fasta file containing the reference genome
 --readgroup name : String to use as read group ID and SM in bam file
 --removeduplicates : Remove duplicates using Picard
 --baitbed file : Capture bait file, BED format - turns on PicardHsMetrics
 --targetbed file : Capture target file, BED format - baits file is used by default
 --baitfile file : Capture bait file, interval format - turns on PicardHsMetrics
 --targetfile file : Capture target file, interval format - baits file is used by default
 --outputfolder folder : The output folder
 --threads integer : Number of processors to use (number of threads to run)
 --verbose : Print more information while running
 --subsample integer : Subsample the specified number of reads from each sample. 0 = no subsampling (default = 0)
 --qualitycontrol : Enable quality-control: use trimmomatic to trim adapter sequences, trim low quality bases from the ends of reads, and remove short sequences
 --adapterfile file : A file containing adapter sequences for trimmomatic
 --extraoptionsfile file : File with extra options for trimmomatic, BWA, or Bowtie2
 --help : Print usage instructions and exit
 --gbsenzyme enzyme : enzyme used to generate GBS library
 --gbspadding padding : Padding sequences used in GBS library construction
 --pacbio : PacBio dataset
 --nofastqc : skip FastQC
=cut

##################### Main ###############################

use FindBin;                  # locate this script directory
use lib "$FindBin::RealBin";  # look for modules in the script directory
use Pipelinev4;               # load the pipeline module

use Getopt::Long;
use Pod::Usage;
use Cwd 'abs_path';

my %args;
my %stats;

### Initialize ###
&init(\%args, \%stats);

### Subsample ###
runtime;
subsample(\%args, \%stats);

### FastQC ###
if (! $args{nofastqc}) {
    runtime;
    fastqc(\%args, \%stats); 
}

if ($args{qualitycontrol}) {
    ### Trimmomatic ###
    runtime;
    trimmomatic(\%args, \%stats);

    ### FastQC ###
    runtime;
    fastqc(\%args, \%stats, "fastqc-trim");
}

if ($args{gbsenzyme}) {
    ### GBS Trim ###
    runtime;
    gbstrim(\%args, \%stats);
}

if ($args{stitch}) {
    ### stitch ###
    runtime;
    stitch(\%args, \%stats);
}

if ($args{bwaindex}) {
    ### BWA ###
    runtime;
    bwa(\%args, \%stats);
} elsif ($args{bowtie2index}) {
    ### Bowtie2 ###
    runtime;
    bowtie2(\%args, \%stats);
} else {
    die "No BWA, no Bowtie2, no soup!\n";
}

# a hack to remove the fasta index because it causes picard to crash???
#$fai = $args{referencefasta} . ".fai";
#`rm $fai` if (-e $fai);

### Picard MultipleMetrics ###
runtime;
collectmultiplemetrics(\%args, \%stats);

### Picard wgs metrics ### - don't need this, multipleMetrics covers it
#runtime;
#wgsmetrics(\%args, \%stats);

if ($args{removeduplicates}) {
    ### Picard Mark (remove) duplicates ###
    runtime;
    markduplicates(\%args, \%stats);

    ### samtools view filter ###
    runtime;
    samtoolsviewfilter(\%args, \%stats);
}

if ($args{baitfile}) {
    ### HsMetrics ###
    runtime;
    hsmetrics(\%args, \%stats);
}

### Write stats to file ###
runtime;
writestats(\%args, \%stats);

`touch $args{scratchfolder}/Complete`;

print "Finished\n";
runtime;

exit;

##################### Initialize ###############################
sub init {
    print "Running Sample Initialize\n";

    my ($args, $stats) = @_;

    ### Usage ###
    $usage = "USAGE: align-singlesample.pl --referencefasta fasta --bwaindex index --bowtie2index index [--outputfolder folder] --R1 file [--R2 file] [--readgroup name]\n";

    # set defaults - add additional parameter defaults here
    $args->{threads} = $ENV{"PBS_NUM_PPN"} // 1;
    $args->{scratchfolder} = $ENV{PWD} . "/align-singlesample";
    GetOptions("help" => \$args->{help},
               "verbose" => \$args->{verbose},
	       "R1=s" => \$args->{R1},
	       "R2=s" => \$args->{R2},
               "threads=i" => \$args->{threads},
               "outputfolder=s" => \$args->{scratchfolder},
               "extraoptionsfile=s" => \$args->{extraoptionsfile},
               "subsample=i" => \$args->{subsample},
               "qualitycontrol" => \$args->{qualitycontrol},
               "adapterfile=s" => \$args->{adapterfile},
	       "referencefasta=s" => \$args->{referencefasta},
	       "samplename=s" => \$args->{samplename},
	       "readgroup=s" => \$args->{readgroup},
	       "bwaindex=s" => \$args->{bwaindex},
	       "bowtie2index=s" => \$args->{bowtie2index},
	       "stitch" => \$args{stitch},
	       "removeduplicates" => \$args->{removeduplicates},
	       "qcplots" => \$args->{qcplots},
	       "baitbed=s" => \$args->{baitbed},
	       "targetbed=s" => \$args->{targetbed},
	       "baitfile=s" => \$args->{baitfile},
	       "targetfile=s" => \$args->{targetfile},
	       "gbsenzyme=s" => \$args->{gbsenzyme},
	       "gbspadding=s" => \$args->{gbspadding},
	       "pacbio" => \$args->{pacbio},
	       "bwamem=s" => \$args->{bwamem},
	       "nofastqc" => \$args->{nofastqc},
        ) or die $usage;
    pod2usage(q(-verbose) => 3) if ($args->{help});
    die "$usage" unless ($args->{R1} and ($args->{bwaindex} or $args->{bowtie2index}) and $args->{referencefasta} );
    $args->{extraoptionsfile} = abs_path($args->{extraoptionsfile}) if ($args->{extraoptionsfile});

    ### Starttime ###
    $date = `date`;
    chomp $date;
    print "Starting at $date\n";
    &runtime;

    ### Standard parameters ###
    $args->{scratchfolder} = abs_path($args->{scratchfolder});
    die "R1 Fasta file $args->{R1} not found\n" if (!-e $args->{R1});
    $args->{R1} = abs_path($args->{R1});
    $args->{pe} = 0;
    if ($args->{R2}) {
        die "R2 Fasta file $args->{R2} not found\n" if (!-e $args->{R2});
        $args->{R2} = abs_path($args->{R2});
        $args->{pe} = 1;
    }
    $args->{logfile} = "$args->{scratchfolder}/log" unless ($args->{verbose});

    ### non-Standard parameters ###
    # add additional parameters processing here
    $args->{bowtie2index} = bowtie2indexcheck($args->{bowtie2index}) if ($args->{bowtie2index});
    $args->{bwaindex} = bwaindexcheck($args->{bwaindex}) if ($args->{bwaindex});
    die "Cannot file reference fasta file $args->{referencefasta}\n" if (!-e $args->{referencefasta});
    $args->{referencefasta} = abs_path($args->{referencefasta});
    if ($args->{baitfile}) {
	die "ERROR: Baitfile file $args->{baitfile} not found\n" if (! -e $args->{baitfile});
	$args->{baitfile} = abs_path($args->{baitfile});
	die "ERROR: Targetfile file $args->{targetfile} not found\n" if ($args->{targetfile} and (! -e $args->{targetfile}));
	$args->{targetfile} = abs_path($args->{targetfile} // $args->{baitfile});
    } elsif ($args->{baitbed}) {
	die "ERROR: Baitbed file $args->{baitbed} not found\n" if (! -e $args->{baitbed});
	$args->{baitbed} = abs_path($args->{baitbed});
	die "ERROR: Targetbed file $args->{targetbed} not found\n" if ($args->{targetbed} and (! -e $args->{targetbed}));
	$args->{targetbed} = abs_path($args->{targetbed} // $args->{baitbed});
    }

    ### Finish setup
    # check that required programs are installed
    requiredprograms(("fastqc", "\$TRIMMOMATIC", "bowtie2", "bwa", "\$PICARD"));

    diegracefully();
    readinextraoptions($args);
    scratchfoldersetup($args);

    hsmetricsprep($args) if ($args->{baitbed} and ! $args->{baitfile}); # turn bed into interval file

}
