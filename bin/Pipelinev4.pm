#!/usr/bin/perl -w

#######################################################################
#  Copyright 2015 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

Pipeline.pm - A Perl module providing functions for performing a variety of sequence analysis pipeline "stages"

=head1 DESCRIPTION

Put this module file in the same folder as a script using the module, then put these three lines in the script:

    use FindBin;                  # locate this script directory
    use lib "$FindBin::RealBin";  # look for modules in the script directory
    use Pipelinev3;               # load the pipeline module

=cut

######################################################

package Pipelinev4;
use Getopt::Long;
use Pod::Usage;
use Cwd 'abs_path';
use File::Temp qw( tempdir );
use File::Spec;
use File::Basename;
use feature "state";
use warnings;
use Exporter;
use Scalar::Util qw(looks_like_number);
use lib "$ENV{GOPHER_PIPELINES}/software/gbs-scripts"; # so we can load enzymes
use enzymes; # so we can assign padding sequences during samplesheet load

use IPC::Open3;

our @ISA= qw( Exporter );

# these functions CAN be exported.
# update this list with: grep "^sub" Pipelinev4.pm | cut -f2 -d' '
our @EXPORT_OK = qw( subsample subsampleplotrmd allsubsample sizeselect stitch fastqc allfastqc trimmomatic trimmomaticplot hisat2 hisat2indexcheck hisat2plotrmd bowtie2 bowtie2indexcheck bwa bwaindexcheck samtoolssortandindexbam samtoolsaddremoverg cleansam collectmultiplemetrics alignmentsummarymetrics alignmentsummarymetricsplotrmd insertsize insertsizeplot wgsmetrics markduplicates hsmetrics hsmetricsplot hsmetricsprep haplotypecaller bamdownsample samtoolsviewfilter samstat samtoolsstats featurecounts subreadplot cuffquant allgunzip bam2fastq freebayes freebayesplot genotypegvcfs gbsvcfreport vcfmetricsplotrmd scratchfoldersetup writestats samplesheetandfoldersetup foldersetup getsinglesamplestats compilefolder compilefiles fastqcplotrmd expressiontableplotrmd metrics allmetrics fixsamplesheet reportheader diegracefully readinextraoptions round10 round100 printlog run runtime formattime compressed INT_handler commify nodememory getstats requiredprograms titlermd imagermd fieldlistrmd spacermd statprint $SCRATCHLOCAL $SCRATCHGLOBAL $GPRESOURCES $GPSOFTWARE $reportFH $reportfolder);

# these functions are exported by default.
our @EXPORT = @EXPORT_OK;

#$SCRATCHLOCAL = "/scratch.local/"; # some scratches are full
$SCRATCHLOCAL = "/panfs/roc/scratch/";
$SCRATCHGLOBAL = "/panfs/roc/scratch/";
$GPRESOURCES = "$ENV{GOPHER_PIPELINES}/resources"; # gopher-pipelines resources directory
$GPSOFTWARE = "$ENV{GOPHER_PIPELINES}/software"; # gopher-pipelines resources directory

###########################################################################
###################### Single-sample pipeline stages ######################
###########################################################################

############################# Subsample #################################
# Requires: subsample.pl
# Args: subsample
# Stats: #rawreads #subsampledreads 
sub subsample {
    print "Running Subsample\n";

    my ($args, $stats) = @_;

    # determine sequence counts
    $reads = `reads.pl $args->{R1}`;
    ($reads) = split /\s/, $reads;
    $stats->{subsample}{"rawreads"} = $reads;
    $stats->{subsample}{"subsampledreads"} = $reads;

    # skip subsampling if not called for
    if ((! $args->{subsample}) or $args->{subsample} == 0) {
        print "Skipping subsampling\n";
        return;
    }
    print "Subsampling $args->{subsample} reads\n";

    # parse filenames
    $newfiler1 = "$args->{scratchfolder}/R1.ss";
    if ($args->{pe}) {
	$newfiler2 = "$args->{scratchfolder}/R2.ss";
    }
    # symlink
    if ($args->{subsample} > $stats->{subsample}{"rawreads"}) {
	if (compressed($args->{R1})) {
	    $suffix = ".fastq.gz";
	} else {
	    $suffix = ".fastq";
	}
	`ln -s $args->{R1} $newfiler1$suffix`;
	$args->{R1} = "$newfiler1$suffix";
	if ($args->{pe}) {
	    `ln -s $args->{R2} $newfiler2$suffix`; 
	    $args->{R2} = "$newfiler2$suffix";
	}
    # subsample
    } else {
	if ($args->{pe}) {
	    &run("subsample.pl $args->{R2} $args->{subsample} $reads > $newfiler2.fastq & subsample.pl $args->{R1} $args->{subsample} $reads > $newfiler1.fastq && wait \$!", $args->{logfile});
	    $args->{R1} = "$newfiler1.fastq";
	    $args->{R2} = "$newfiler2.fastq";
	} else { # SE
	    &run("subsample.pl $args->{R1} $args->{subsample} $reads > $newfiler1.fastq", $args->{logfile});
	    $args->{R1} = "$newfiler1.fastq";
	}
    }
    if ($args->{subsample} < $stats->{subsample}{"rawreads"}) {
	$stats->{subsample}{"subsampledreads"} = $args->{subsample};
    } else {
	$stats->{subsample}{"subsampledreads"} = $stats->{subsample}{"rawreads"};

    }

    $args->{methods} .= "Fastq files were evenly subsampled down to a maximum of " . commify($args->{subsample}) . " reads per sample. ";

}

############################# All Subsample #################################
sub allsubsample {
    print "\n### Running Subsample on all samples ###\n";

    my ($samples, $args, $stats) = @_;

    push @{$args->{stageorder}}, "subsample";

    # determine sequence counts
    my $myscratchfolder = "$args->{scratchfolder}/fastq-subsampled";
    mkdir $myscratchfolder;
    $gpout = "$myscratchfolder/gp.out";
    $progress = $args->{verbose} ? "--progress" : "";
    open GP, "| parallel $progress -j $args->{threads} > $gpout" or die "Cannot open pipe to gnu parallel\n";
#    open GP, ">$myscratchfolder/parallel.test" or die "Cannot open pipe to gnu parallel\n";

    # send commands to gnu parallel
    foreach $sample (keys %{$samples}) {
	print GP "wc -l $samples->{$sample}{R1}{fastq} >> $myscratchfolder/wc.out\n";
    }
    close GP;
    print "error code: $?\n" if ($?);
    open IFILE, "$myscratchfolder/wc.out" or die "Cannot open file $gpout: $!\n";
    $samplecount = 0;
    $sum = 0;
    while ($line = <IFILE>) {
	chomp $line;
	my ($count, $file) = split / /, $line;
	my ($fname, $path) = fileparse($file);
	my ($sample, $junk) = split /_R[12].fastq/, $fname;
	$stats->{$sample}{subsample}{"rawreads"} = $count / 4;
	$stats->{$sample}{subsample}{"subsampledreads"} = $count / 4;
	$sum += $count / 4;
	$samplecount++;
    }
    close IFILE;

    if ($args->{subsample} eq "auto") {
	$mean = int($sum / $samplecount);
	$max = $mean * 2;
	$min = $mean * .1; # need to do something with this
	print "Setting subsample to two times the mean = $max\n";
	$args->{subsample} = $max;
    }

    # skip subsampling if not called for
    if ($args->{subsample} == 0) {
	print "Skipping subsampling\n";
	return;
    }
    print "Subsampling $args->{subsample} reads from each sample\n";
    $args->{methods} .= "Fastq files were evenly subsampled down to a maximum of " . commify($args->{subsample}) . " reads per sample. ";

    # run gnu parallel
    open GP, "| parallel $progress -j $args->{threads} > $gpout" or die "Cannot open pipe to gnu parallel\n";
#    open GP, ">parallel.test" or die "Cannot open pipe to gnu parallel\n";

    # send commands to gnu parallel
    foreach $sample (keys %{$samples}) {
	$newfile = "$myscratchfolder/${sample}_R1.fastq";
	# rich man's speedy subsampling using subsampler.pl
	$lines = $stats->{$sample}{subsample}{"rawreads"} * 4; # four lines per read
	if ($args->{subsample} > $stats->{$sample}{subsample}{"rawreads"}) {
	    `ln -s $samples->{$sample}{R1}{fastq} $newfile`;
	} else {
	    print GP "subsample.pl $samples->{$sample}{R1}{fastq} $args->{subsample} $lines > $newfile\n";
	}
	$samples->{$sample}{R1}{fastq} = $newfile;
	if ($samples->{$sample}{R2}{fastq}) {
	    $newfile = "$myscratchfolder/${sample}_R2.fastq";
	    # rich man's speedy subsampling using subsampler.pl
	    if ($args->{subsample} > $stats->{$sample}{subsample}{"rawreads"}) {
		`ln -s $samples->{$sample}{R2}{fastq} $newfile`;
	    } else {
		print GP "subsample.pl $samples->{$sample}{R2}{fastq} $args->{subsample} $lines > $newfile\n";
	    }
	    $samples->{$sample}{R2}{fastq} = $newfile;	    
	}
	if ($args->{subsample} < $stats->{$sample}{subsample}{"rawreads"}) {
	    $stats->{$sample}{subsample}{"subsampledreads"} = $args->{subsample};
	} else {
	    $stats->{$sample}{subsample}{"subsampledreads"} = $stats->{$sample}{subsample}{"rawreads"};
	}
    }
    close GP;
    print "error code: $?\n" if ($?);
    $args->{fastqfolder} = "$myscratchfolder";

}

############################# Subsampleplotrmd ################################
sub subsampleplotrmd {
    print "Generating subsample plot\n";

    my ($samples, $args, $stats) = @_;

    # generate reads per sample plot
    $name = "subsampleplot";
    $ofile = "$reportfolder/$name.dat";
    $rfile = "$reportfolder/$name.rmd";
    print "Generating reads per sample plot\n" if ($args->{verbose});
    open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

    # generate order for report sorting
    @order = sort {$stats->{$b}{subsample}{"rawreads"} <=> $stats->{$a}{subsample}{"rawreads"}} keys %{$stats};
    foreach $sample (@order) {
	$order .= "\"$sample\",";
    }
    chop $order; # remove last ,
    $args->{order}{"Reads per sample"} = $order;

    # print out the data
    print OFILE "sample\tsubsampled\traw\ttotal\n";
    foreach $sample (sort keys %{$samples}) {
	$total = $stats->{$sample}{subsample}{"rawreads"};
	$ss = $stats->{$sample}{subsample}{"subsampledreads"} // 0;
	$raw = $total - $ss;

	print OFILE "$sample\t$ss\t$raw\t$total\n";
    }
    close OFILE;

    if ($args->{subsample}) {
	$legend = "legend = list(x = .7, y = 1.2),";
    } else {
	$legend = "showlegend = FALSE,";
    }
    if ($args->{pe}) {
	$readtype = "Read-pairs";
    } else {
	$readtype = "Reads";
    }
    $title = "$readtype per Sample";
    $text = "The number of " . lc($readtype) . " per sample at the start of the analysis is shown.";
    if ($args->{subsample}) {
	$prettysub = commify($args->{subsample});
	$text .= " Samples with more than $prettysub reads were subsampled down to $prettysub reads.";
    }
    $updatemenus = "updatemenus = updatemenus,";
    $updatemenus = ""; # disable the sort buttons

    open RFILE, ">$rfile" or die "Cannot open $rfile\n";
    print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

data2 <- datat[rev(order(datat\$total)),]
valueorder <- list(title="Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
              categoryarray = c(as.character(data2\$sample)))

updatemenus <- list(
  list(
    active = 0,
    font = list(size = 10),
    type = 'buttons',
    xanchor = 'right',
    buttons = list(
      
      list(
        label = "Sort by<br>Sample",
        method = "relayout",
        args = list(list(xaxis = sampleorder))),
      
      list(
        label = "Sort by<br>Value",
        method = "relayout",
        args = list(list(xaxis = valueorder)))
    )
  )
)

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~subsampled,
 name = "Reads after subsampling",
 type = "bar", marker = list(color = '#1E77B4'),
hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Reads:",format(datat\$subsampled,big.mark=",",scientific=FALSE))
) %>% 
add_trace(y = ~raw, name = 'Raw Reads', marker = list(color = '#A5CEE2'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Raw reads:",format(datat\$total,big.mark=",",scientific=FALSE))) %>%
 layout(xaxis = sampleorder, $updatemenus dragmode='pan', $legend barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "$readtype", fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
    close RFILE;
    $args->{reporttext} .= qq(```{r test-main, child = '$rfile'}\n```\n);

}

############################# Size select #################################
# Requires: pear
# Args:
# Stats: ??? 
sub sizeselect {
    print "Running sizeselect\n";

    my ($args, $stats) = @_;

    $min = $args->{sizeselectmin} // 0;
    $max = $args->{sizeselectmax} // 1000;

    ### run pear to stitch reads, run through stitched reads saving IDs of reads with good and bad lengths
    $count = 0;
    $goodcount = 0;
    if ($args->{pe}) {
	&run("pear -f $args->{R1} -r $args->{R2} -o $args->{scratchfolder}/pear ", $args->{logfile});
	$ifile = "$args->{scratchfolder}/pear.assembled.fastq";
	$ofile = "$args->{scratchfolder}/pear.ids.txt";
	open IFILE, $ifile or die "Cannot open $ifile: $!\n";
	open OFILE, $ofile or die "Cannot open $ofile: $!\n";
	while ($id = <IFILE>) {
	    $count++;
	    my $seq = <IFILE>;
	    my $plus = <IFILE>;
	    my $qual = <IFILE>;
	    $length = length($seq) - 1; # minus one for newline character
	    if (($length >= $min) and ($length <= $max)) {
		print OFILE "$id\n";
		$goodcount++;
	    }
	}
	close IFILE;
	close OFILE;

	### pull out good reads from original fastq file
	$newr1 = "$args->{R1}";
	$newr2 = "$args->{R2}";
	&run("extract_fastq.pl $ofile $args->{R1} > $newr1", $args->{logfile});
	&run("extract_fastq.pl $ofile $args->{R2} > $newr2", $args->{logfile});
	$args->{R1} = $newr1;
	$args->{R2} = $newr2;

	# save stats
	if ($args->{pe}) {
	    $stats->{sizeselect}{goodsequences} = $goodcount;
	    $stats->{sizeselect}{"%goodsequences"} = round100($goodcount / $count * 100) if ($count > 0);
	}
	
    } else {
	# how to handle single reads???
    }

}

############################# Size select #################################
# Requires: pear
# Args:
# Stats: ??? 
sub stitch {
    print "Stitching reads with Pear\n";

    my ($args, $stats) = @_;

    ### run pear to stitch reads, run through stitched reads saving IDs of reads with good and bad lengths
    $count = 0;
    $goodcount = 0;
    if ($args->{pe}) {
	&run("pear -f $args->{R1} -r $args->{R2} -o $args->{scratchfolder}/pear ", $args->{logfile});
	$args->{R1} = "$args->{scratchfolder}/pear.assembled.fastq";
	$args->{pe} = 0;

	# save stats - need to add this
#         $stats->{sizeselect}{goodsequences} = $goodcount;
#	  $stats->{sizeselect}{"%goodsequences"} = round100($goodcount / $count * 100) if ($count > 0);
#	}
	
    } else {
	print "Single-end reads data, skipping stitching\n";
    }

    $args->{methods} .= "Paired-reads were stitched together using Pear. ";

}

######################## FastQC ##########################
# Requires: fastqc
# Args: 
# Stats: %gc lastq30baseR1 lastq20baseR1 meanreadqualityR1
sub fastqc {
    print "Running FastQC\n";

    my ($args, $stats) = @_;
    my $stage = $_[2] // "fastqc";

    my $myscratchfolder = $args->{scratchfolder} . "/$stage";
    mkdir $myscratchfolder;
    my $nogroup = ($args->{pacbio}) ? "" : "--nogroup";
    $maxreads = 2000000;
    $maxlines = $maxreads * 4;

    if ($stats->{subsample}{subsampledreads} > $maxreads) {
	$fastqr1 = "$myscratchfolder/fastqc_R1.fastq";
	if (compressed($args->{R1})) {
	    `gunzip -c $args->{R1} | head -n $maxlines  > $fastqr1`;
	} else {
	    `head -n $maxlines $args->{R1} > $fastqr1`;
	}
	if ($args->{pe}) {
	    $fastqr2 = "$myscratchfolder/fastqc_R2.fastq";
	    if (compressed($args->{R2})) {
		`gunzip -c $args->{R2} | head -n $maxlines > $fastqr2`;
	    } else {
		`head -n $maxlines $args->{R2} > $fastqr2`;
	    }
	}
    } else {
	if (compressed($args->{R1})) {
	    $fastqr1 = "$myscratchfolder/fastqc_R1.fastq.gz";
	} else {
	    $fastqr1 = "$myscratchfolder/fastqc_R1.fastq";
	}
	`ln -s $args->{R1} $fastqr1`;

	if ($args->{pe}) {
	    if (compressed($args->{R2})) {
		$fastqr2 = "$myscratchfolder/fastqc_R2.fastq.gz";
	    } else {
		$fastqr2 = "$myscratchfolder/fastqc_R2.fastq";
	    }
	    `ln -s $args->{R2} $fastqr2`;
	}
    }

    if ($args->{pe}) {
        &run("fastqc --extract --threads $args->{threads} $nogroup -o $myscratchfolder $fastqr1 $fastqr2", $args->{logfile});
    } else {
	&run("fastqc --extract $nogroup -o $myscratchfolder $fastqr1", $args->{logfile});
    }
    
#    $stats->{files}{"${stage}R1zip"} = "$myscratchfolder/fastqc_R1_fastqc.zip";
    $stats->{files}{"${stage}R1html"} = "$myscratchfolder/fastqc_R1_fastqc.html";
    if ($args->{pe}) {
#	$stats->{files}{"${stage}R2zip"} = "$myscratchfolder/fastqc_R2_fastqc.zip";
	$stats->{files}{"${stage}R2html"} = "$myscratchfolder/fastqc_R2_fastqc.html";
    }
    if ($stage eq "fastqc-trim") {
	$args->{methods} .= "Quality of data in fastq files after quality control was assessed with FastQC. ";
    } else {
	$args->{methods} .= "Quality of data in fastq files was assessed with FastQC. ";
    }
}

############################# allfastqc #################################
sub allfastqc {
    my ($samples, $args, $stats) = @_;

    print "\n### Running FastQC on all samples ###\n";

    push @{$args->{stageorder}}, "fastqc";

    my $myscratchfolder = "$args->{scratchfolder}/fastqc";
    mkdir $myscratchfolder;
    $gpout = "$myscratchfolder/gp.out";
    $progress = $args->{verbose} ? "--progress" : "";
    open GP, "| parallel $progress -j $args->{threads} > $gpout" or die "Cannot open pipe to gnu parallel\n";
#    open GP, ">$myscratchfolder/parallel.test" or die "Cannot open pipe to gnu parallel\n";

    # send commands to gnu parallel
    foreach $sample (keys %{$samples}) {
	if ($args->{pe}) {
	    print GP "fastqc --extract --nogroup --threads $args->{threads} -o $myscratchfolder $samples->{$sample}{R1}{fastq} $samples->{$sample}{R2}{fastq} 2>&1\n";
	} else {
	    print GP "fastqc --extract --nogroup -o $myscratchfolder $samples->{$sample}{R1}{fastq} 2>&1\n";
	}
    }

    close GP;
    print "error code: $?\n" if ($?);
#    $args->{fastqfolder} = "$myscratchfolder";

}


######################## Trimmomatic ##########################
# Run trimmomatic, supports extraoptions
# Require: java $TRIMMOMATIC
# Args: adapterfile (optional)
# Stats: inputreads surviving dropped %surviving %dropped
# Stats PE: inputreadpairs bothsurviving forwardonlysurvivng reverseonlysurviving dropped %bothsurviving %forwardonlysurvivng %reverseonlysurviving %dropped 
sub trimmomatic {
    print "Running trimmomatic\n";

    my ($args, $stats) = @_;

    # set trimmomatic parameters
    $xmx = "-Xmx2000M"; # java memory
    my $adapterfile = $args->{adapterfile} // "/panfs/roc/msisoft/trimmomatic/0.33/adapters/all_illumina_adapters.fa";
    my $minscore = 16; # sliding window minimum q-score
    # set minimum length after trimming to be half of the original read length
    my $length;
    if ($args->{R1} =~ /\.gz$/) {
	$length = `gunzip -c $args->{R1} | sed '2q;d' | wc -c`;
    } else {
	my $length = `sed '2q;d' $args->{R1} | wc -c`;
    }
    $length--; # subtract newline
    my $minlength = int($length / 2);
    my $headcrop = $args->{headcrop} ? "HEADCROP:$args->{headcrop}" : "";

    my $trimmomaticoptions = $args->{extraoptions}{trimmomatic} // "ILLUMINACLIP:$adapterfile:2:30:10:2:true $headcrop LEADING:3 TRAILING:3 SLIDINGWINDOW:4:$minscore MINLEN:$minlength";

    $newr1 = $args->{R1};
    $newr1 =~ s/\.fastq/.trim.fastq/;
    $newr1single = $newr1;
    $newr1single =~ s/\.fastq/.singleton.fastq/;
    if ($args->{pe}) {
        $newr2 = $args->{R2};
        $newr2 =~ s/\.fastq/.trim.fastq/;
        $newr2single = $newr2;
        $newr2single =~ s/\.fastq/.singleton.fastq/;

        $result = &run("java $xmx -jar \$TRIMMOMATIC/trimmomatic.jar PE -phred33 -threads $args->{threads} $args->{R1} $args->{R2} $newr1 $newr1single $newr2 $newr2single $trimmomaticoptions", $args->{logfile});
    } else {
        $result = &run("java $xmx -jar \$TRIMMOMATIC/trimmomatic.jar SE -threads $args->{threads} $args->{R1} $newr1 $trimmomaticoptions", $args->{logfile});
    }

    # save stdout with all of the stats to a file
    my $ofile = "$args->{scratchfolder}/trimmomatic.log";
    open OFILE, ">$ofile" or print "Cannot open trimmomatic log $ofile: $!\n";
    print OFILE $result;
    close OFILE;

    # set trimmed fastq files as default fastq files
    $args->{R1} = $newr1;
    $args->{R2} = $newr2 if ($args->{pe});

    $args->{methods} .= qq(Trimmomatic was used to trim 3' adapter sequences and low quality bases from the ends of reads using the options '$trimmomaticoptions'. );
}

### Trimmomaticplot ###
sub trimmomaticplot {
    my ($samples, $args, $stats) = @_;

    print "Running trimmomaticplot\n" if ($args->{verbose});
    open OFILE, ">trimmomatic-filelist.txt" or die "cannot open trimmomatic-filelist.txt: $!\n";
    foreach $sample (sort keys %{$samples}) {
            print OFILE "$args->{scratchfolder}/singlesamples/$sample/trimmomatic.log\t$sample\n";
    }
    close OFILE;
    $result = `trimmomaticplot.pl -f trimmomatic-filelist.txt`;

    # process the stdout, grabbing some stats
    getstats($args, $stats, $result, "trimmomatic");

    # add to report
#    &h1("Trimmomatic Plots");  
#    &image("trimmomatic.png");
#    &image("trimmomatic-pct.png");
}

######################## Hisat2 ##########################
# Align using hisat2
# Require: hisat2 samtools
# Args: hisat2index
# Stats: Many, see code for details, may just want to run picard alignment summmary
sub hisat2 {
    print "Running Hisat2\n";

    my ($args, $stats) = @_;

    my $bam = "$args->{scratchfolder}/hisat2.bam";
    my $stranded;

    if ($args->{library}) {
	if ($args->{library} eq "truseqrna") {
            $stranded = $args->{pe} ? "--rna-strandness RF" : "--rna-strandness R";
        } elsif ($args->{library} eq "picorna") {
            $stranded = $args->{pe} ? "--rna-strandness FR --trim5 3" : "--rna-strandness F --trim5 3";
        } elsif ($args->{library} eq "picorna2") {
            $stranded = $args->{pe} ? "--rna-strandness RF --trim5 3" : "--rna-strandness R --trim5 3";
        }
    } elsif ($args->{stranded}) {
        $stranded = $args->{pe} ? "--rna-strandness RF" : "--rna-strandness R";
    }

    if ($args->{hisat2splicefile}) {
	$splicefile = "--known-splicesite-infile $args->{hisat2splicefile}";
    } else {
	$splicefile = "";
    }
    my $hisat2options = $args->{extraoptions}{hisat2} // "--dta-cufflinks $stranded $splicefile";

    # make tmp folder unique
    $srttmp = tempdir("sortXXXX", DIR => "$args->{scratchfolder}/", CLEANUP => 1);
    $srttmp .= "/tmp";

    # figure out how much ram to throw at samtools sort
    my $memory = nodememory();
    my $mem = int($memory / $args->{threads} * .6); # use memory proportional to the number of threads we're supposed to use, then reduce by 20% safety factor
    $mem = "${mem}G";

    my $result = "";
    if ($args->{pe}) {
#        $result = run("hisat2 $hisat2options --threads $args->{threads} -x $args->{hisat2index} -1 $args->{R1} -2 $args->{R2} 2>$args->{scratchfolder}/align_summary.txt | samtools sort -m $mem -T $srttmp -\@ $args->{threads} -O bam -o $bam", $args->{logfile});
        $result = run("hisat2 $hisat2options --threads $args->{threads} -x $args->{hisat2index} -1 $args->{R1} -2 $args->{R2} -S $bam", $args->{logfile});
    } else {
#        $result = run("hisat2 $hisat2options --threads $args->{threads} -x $args->{hisat2index} -U $args->{R1} 2>$args->{scratchfolder}/align_summary.txt | samtools sort -m $mem -T $srttmp -\@ $args->{threads} -O bam -o $bam", $args->{logfile});
        $result = run("hisat2 $hisat2options --threads $args->{threads} -x $args->{hisat2index} -U $args->{R1} -S $bam", $args->{logfile});
    }

    # send hisat output somewhere useful
    $ofile = "$args->{scratchfolder}/align_summary.txt";
    open OFILE, ">$ofile" or print "Cannot open file $ofile: $!\n";
    @results = split /\n/, $result;
    foreach $result (@results) {
	next if ($result =~ /^Warning/);
	print OFILE "$result\n";
    }
    close OFILE;

#    `echo $result > $args->{scratchfolder}/align_summary.txt`;

    die "Hisat2 failure\n" unless (-e $bam);

    $args->{methods} .= "A Hisat2 splicesite file was generated from the GTF file. " if ($args->{hisat2splicefile});
    $args->{methods} .= qq(Hisat2 was used to align reads to a reference genome using the options '$hisat2options -x $args->{hisat2index}'. );

    # index bam
    $args->{bam} = $bam;
    $stats->{files}{bam} = "$bam";
    $stats->{files}{bai} = "$bam.bai";
}

### verify that a hisat2 index is at the specified location, make it's path absolute
sub hisat2indexcheck {
    my ($index) = @_;
    my $extension = ".1.ht2";
    my $indexfile = $index . $extension;
    my $extension2 = ".1.ht2l";
    my $indexfile2 = $index . $extension2;
    if (-e $indexfile) {
	$indexfile = abs_path($indexfile);
    } elsif (-e $indexfile2) {
	$indexfile = abs_path($indexfile2);
    } else {
	die "Unable to find hisat2index $index";
    }
    ($name, $path, $suffix) = fileparse($indexfile, ($extension, $extension2));
    return "$path/$name";
}

### Hisat2plot ###
sub hisat2plotrmd {
    my ($samples, $args, $stats) = @_;

    print "Generating hisat2 plot\n"; # if ($args->{verbose});
    open OFILE, ">hisat-filelist.txt" or die "cannot open hisat-filelist.txt: $!\n";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "$args->{scratchfolder}/singlesamples/$sample/align_summary.txt\t$sample\n";
    }
    close OFILE;
    $result = `hisat2plotrmd.pl -f hisat-filelist.txt`;

    # process the stdout, grabbing some stats
    &getstats($args, $stats, $result, "hisat2alignmentplot");

    ### add to report ###
    `mv hisat2alignmentplot.* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = 'hisat2alignmentplot.rmd'}\n```\n);

}

######################## Bowtie2 ##########################
# Align using bowtie2, pipe output through samtools sort to produce an indexed bam, supports extraoptions
# Require: bowtie2 samtools
# Args: bowtie2index
# Stats: Many, see code for details, may just want to run picard alignment summmary
sub bowtie2 {
    print "Running Bowtie2\n";

    my ($args, $stats) = @_;

    my $bam = "$args->{scratchfolder}/bowtie2.bam";
    my $bowtieoptions = $args->{extraoptions}{bowtie2} // "";

    # make tmp folder unique
    my $srttmp = tempdir("sortXXXX", DIR => "$args->{scratchfolder}/", CLEANUP => 1);
    $srttmp .= "/tmp";

    # figure out how much ram to throw at samtools sort
    my $memory = nodememory();
    $mem = int($memory / $args->{threads} * .8); # use memory proportional to the number of threads we're supposed to use, then reduce by 20% safety factor
    $mem = "${mem}G";

    $readgroup = ($args->{readgroup}) ? "--rg-id $args->{readgroup} --rg SM:$args->{readgroup}" : "";

    ### Run bowtie2 and samtools sort
    # Build commands
    my $bowtiecmd;
    if ($args->{pe}) {
	$bowtiecmd = "bowtie2 $bowtieoptions $readgroup -p $args->{threads} -x $args->{bowtie2index} -1 $args->{R1} -2 $args->{R2}";
    } else {
	$bowtiecmd = "bowtie2 $bowtieoptions $readgroup -p $args->{threads} -x $args->{bowtie2index} -U $args->{R1}";
    }
    my $samtoolscmd = "samtools sort -m $mem -T $srttmp -\@ $args->{threads} -O bam -o $bam >> $args->{logfile}";

    # Be a tricksy hobbit and collect stderr and stdout from bowtie2. 
    # Forward the stdout (sam data) to samtools, and forward the stderr
    # to the log and save a copy of stderr so we can parse out the alignment
    # metrics.
    open SAMTOOLS, "| $samtoolscmd" or die "Cannot start samtools: $!\n";
    open LOG, ">>$args->{logfile}" or die "Cannot open log file $args->{logfile}: $!\n";
    my $pid = open3(\*WRITER, \*READER, \*ERROR, $bowtiecmd);
    $result = "";
    # send stdout to samtools 
    while( my $output = <READER> ) {
	print SAMTOOLS $output; 
    }
    # send stderr to logfile and local variable
    while( my $errout = <ERROR> ) {
	print LOG $errout;
	$result .= $errout;
    }
    waitpid( $pid, 0 ) or die "$!\n";
    my $retval =  $?;
    close SAMTOOLS;
    close LOG;

    die "Bowtie2 failure\n" unless (-e $bam);

    # index bam
    &run("samtools index $bam");
    $args->{bam} = "$bam";
    $stats->{files}{bam} = "$bam";
    $stats->{files}{bai} = "$bam.bai";

    # Parse bowtie alignment stats
    @rawlines = split /\n/, $result;
    # filter out Warning lines
    foreach $line (@rawlines) {
	next if ($line =~ /^Warning/);
	push @lines, $line;
    }
    
    if ($args->{pe}) {
	$lines[0] =~ /^(\d+)/;
	$stats->{bowtie2}{reads} = $1;
	$lines[2] =~ /^\s+(\d+)/;
	$stats->{bowtie2}{concordantzero} = $1;
	$lines[3] =~ /^\s+(\d+)/;
	$stats->{bowtie2}{concordantsingle} = $1;
	$lines[4] =~ /^\s+(\d+)/;
	$stats->{bowtie2}{concordantmulti} = $1;
	$lines[7] =~ /^\s+(\d+)/;
	$stats->{bowtie2}{discordantsingle} = $1;
	
    } else {
	$lines[0] =~ /^(\d+)/;
	$stats->{bowtie2}{reads} = $1;
	$lines[2] =~ /^\s+(\d+)/;
	$stats->{bowtie2}{alignedzero} = $1;
	$lines[3] =~ /^\s+(\d+)/;
	$stats->{bowtie2}{alignedsingle} = $1;
	$lines[4] =~ /^\s+(\d+)/;
	$stats->{bowtie2}{alignedmulti} = $1;
    }

    $args->{methods} .= "Bowtie2 was used to align reads to a reference genome ($args->{bowtie2index}). The bam alignment files were sorted and indexed using samtools sort. ";

}

# verify that a bowtie2 index is at the specified location, make it's path absolute
sub bowtie2indexcheck {
    my ($index) = @_;
    my $extension = ".1.bt2";
    my $indexfile = $index . $extension;
    my $extension2 = ".1.bt2l";
    my $indexfile2 = $index . $extension2;
    if (-e $indexfile) {
	$indexfile = abs_path($indexfile);
    } elsif (-e $indexfile2) {
	$indexfile = abs_path($indexfile2);
    } else {
	die "Unable to find bowtie2index $index";
    }
    ($name, $path, $suffix) = fileparse($indexfile, ($extension, $extension2));
    return "$path/$name";
}

######################## BWA ##########################
# Align using bwa, pipe output through samtools sort to produce an indexed bam
# Require: bwa samtools
# Args: bwaindex
# Stats: none, run picard alignment summary statistics instead
sub bwa {
    print "Running BWA\n";

    my ($args, $stats) = @_;

    my $bam = "$args->{scratchfolder}/bwa.bam";
    my $bwaoptions = $args->{extraoptions}{bwa} // "";

    # make tmp folder unique 
    my $srttmp = tempdir("sortXXXX", DIR => "$args->{scratchfolder}/", CLEANUP => 1);
    $srttmp .= "/tmp";

    # figure out how much ram to throw at samtools sort
    my $memory = nodememory();
    my $mem = int($memory / $args->{threads} * .8); # use memory proportional to the number of threads we're supposed to use, then reduce by 20% safety factor
    # calculate size of fastq file(s)
    $fastqsize = `du -DB 1000000000 $args->{R1}`;
    ($fastqsize) = split /\t/, $fastqsize;
    $fastqsize = $fastqsize * 2 if ($args->{pe});
    # set mem to fastq size if less than proportianal memory
    $mem = $fastqsize if ($fastqsize < $mem);
    $mem = "${mem}G";
#    print STDERR "samtools sort mem: $mem\n";
    $mem = $args->{bwamem} // $mem; 
    
    $readgroup = ($args->{readgroup}) ? "-R '\@RG\\tID:$args->{readgroup}\\tSM:$args->{readgroup}'" : "";

    my $logging = ($args->{verbose}) ? "" : "2>>$args->{logfile}";
    if ($args->{pe}) {
        &run("bwa mem $bwaoptions -v 3 -t $args->{threads} $readgroup $args->{bwaindex} $args->{R1} $args->{R2} $logging | samtools sort -m $mem -T $srttmp -\@ $args->{threads} -O bam -o $bam 1>&2", $args->{logfile});
    } else {
	&run("bwa mem $bwaoptions -t $args->{threads} $readgroup $args->{bwaindex} $args->{R1} $logging | samtools sort -m $mem -T $srttmp -\@ $args->{threads} -O bam -o $bam 1>&2", $args->{logfile});
    }
    die "BWA failure\n" unless (-e $bam);

    # index bam
    run("samtools index $bam");
    $args->{bam} = "$bam";
    $stats->{files}{bam} = "$bam";
    $stats->{files}{bai} = "$bam.bai";

    $args->{methods} .= "BWA mem was used to align reads to a reference genome ($args->{bwaindex}). The bam alignment files were sorted and indexed with Samtools. ";
}

# verify that a bwa index is at the specified location, make it's path absolute
sub bwaindexcheck {
    my ($index) = @_;
    my $extension = ".bwt";
    my $indexfile = $index . $extension;
    if (! -e $indexfile) {
	die "Unable to find bwaindex $index";
    }
    $indexfile = abs_path($indexfile);
    ($name, $path, $suffix) = fileparse($indexfile, ($extension));
    return "$path/$name";
}

########################### samtools sort and index #################
# sort and index a bam file
# Require: samtools
# Args: bam
# Stats: 
sub samtoolssortandindexbam {

    my ($args, $stats) = @_;
    print "Running Samtools sort and index\n";

    ### sort and index
    # make tmp folder unique
    my $srttmp = tempdir("sortXXXX", DIR => "$args->{scratchfolder}/", CLEANUP => 1);

    # figure out how much ram to throw at samtools sort
    my $memory = nodememory();
    my $mem = int($memory / $args->{threads} * .8); # use memory proportional to the number of threads we're supposed to use, then reduce by 20% safety factor
    $mem = "${mem}G";

    my $newbam = $args->{bam};
    $newbam =~ s/\.bam$/.sort.bam/;

    &run("samtools sort -O bam -m $mem -T ${srttmp}/ $args->{bam} -o $newbam", $args->{logfile});
    &run("samtools index $newbam", $args->{logfile});

    $args->{bam} = $newbam;
    die "Sort failure\n" if (! -e $args->{bam});
    $stats->{files}{bam} = $newbam;
    $stats->{files}{bai} = "$newbam.bai";

}

########################### Remove aligned #################
# Generate fastq file with aligned reads removed (for removing contaminants)
# Require: gopher-biotools, samtools
# Args: fastq, bam
# Stats: 
sub removealigned {

    my ($args, $stats) = @_;
    print "Running remove aligned\n";

    $idfile = "$args->{scratchfolder}/removealignedids.txt";

    # save aligned read ids to file
    $cleanedR1 = $args->{R1};
    $cleanedR1 =~ "$args->{scratchfolder}/R1.removealigned.fastq";
    &run("samtools view $args->{bam} | cut -f1 | sort | uniq > $idfile", $args->{logfile});
    # generate new fastq file with reads removed
    $result = &run("extract_fastq.pl -discard $idfile $args->{R1} > $cleanedR1", $args->{logfile});
    print $result if ($args->{verbose});
    $args->{R1} = $cleanedR1;

    if ($args->{R2}) {
	$cleanedR2 = $args->{R2};
	$cleanedR2 =~ "$args->{scratchfolder}/R2.removealigned.fastq";
	# generate new fastq file with reads removed
	$result = &run("extract_fastq.pl -discard $idfile $args->{R1} > $cleanedR2", $args->{logfile});
	print $result if ($args->{verbose});
	$args->{R2} = $cleanedR2;
    }

}


########################### coverage blocks summary #################
# Calculate blocks of coverage across the genome
# Require: bedtools, gbs-scripts
# Args: bam
# Stats: 
sub coverageblocks {

    # determine the number of loci with a minimum length of X bp and minimum depth of Y

    my ($args, $stats) = @_;
    print "Running coverage summary\n";

    &run("coverage-summary.pl --bamfile $args->{bam} --referencefai $args->{referencefasta}.fai", $args->{logfile});

}


########################### samtools add read group #################
# Add a read group to a bam file
# Require: samtools
# Args: bam
# Stats: 
sub samtoolsaddreplacerg {

    my ($args, $stats) = @_;
    print "Running Samtools add read group\n";

    ### add read group
    $tmpbam = $args->{bam} . ".tmp";
    `mv $args->{bam} $tmpbam`;
    &run("samtools addreplacerg -o $args->{bam} $tmpbam", $args->{logfile});

}

###################### Picard CleanSam ######################
# Requires: java, $PICARD
# Args: 
# Stats: 
sub cleansam {
    print "Running Picard CleanSam\n";

    my ($args, $stats) = @_;

    my $newbam = $args->{bam};
    $newbam =~ s/\.bam$/.clean.bam/;

    &run("java -Xmx4g -jar \$PICARD/picard.jar CleanSam INPUT=$args->{bam} OUTPUT=$newbam", $args->{logfile});

    $args->{bam} = $newbam;

}

###################### Picard CollectMultipleMetrics ######################
# Requires: java, $PICARD
# Args: referencefasta
# Stats: 
sub collectmultiplemetrics {
    print "Running Picard CollectMultipleMetrics\n";

    my ($args, $stats) = @_;

    my $defaultmetrics = "PROGRAM=CollectAlignmentSummaryMetrics";
    $defaultmetrics .= " PROGRAM=CollectInsertSizeMetrics" if ($args->{pe});
    
    my $picardmodules = $_[2] // $defaultmetrics;

    &run("java -Xmx4g -jar \$PICARD/picard.jar CollectMultipleMetrics PROGRAM=null $picardmodules INPUT=$args->{bam} OUTPUT=$args->{scratchfolder}/picard REFERENCE_SEQUENCE=$args->{referencefasta}", $args->{logfile});

}

###################### Picard AlignmentSummaryMetrics ######################
# Requires: java, $PICARD
# Args: referencefasta
sub alignmentsummarymetrics {
    print "Running Picard AlignmentSummaryMetrics\n";

    my ($args, $stats) = @_;

    # load java module???
    &run("java -Xmx4g -jar \$PICARD/picard.jar CollectAlignmentSummaryMetrics INPUT=$args->{bam} OUTPUT=$args->{scratchfolder}/picard.alignment_summary_metrics REFERENCE_SEQUENCE=$args->{referencefasta}", $args->{logfile});

}

### picardalignmentsummarymetricsplot ###
sub alignmentsummarymetricsplotrmd {
    my ($samples, $args, $stats) = @_;

    print "Generating Picard alignment summary plot\n";
    open OFILE, ">picardalignmentsummarymetrics-filelist.txt" or die "cannot open picardallignmentsummarymetrics-filelist.txt: $!\n";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "$args->{scratchfolder}/singlesamples/$sample/picard.alignment_summary_metrics\t$sample\n";
    }
    close OFILE;
    $result = `picardalignmentsummarymetricsplotrmd.pl -f picardalignmentsummarymetrics-filelist.txt`;

    # process the stdout, grabbing some stats
    &getstats($args, $stats, $result, "picardalignmentplot");

    ### add to report ###
    `mv picardalignmentplot.* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = 'picardalignmentplot.rmd'}\n```\n);
    $args->{methods} .= qq(Alignment rates were summarized with Picard. );

}


######################## Picard Insertsize Metrics ##########################
# Requires: java, $PICARD
sub insertsize {
    print "Running Picard insert size metrics\n";

    my ($args, $stats) = @_;

    unless ($args->{pe}) {
        print "Skipping Picard insert size metrics - not a paired-end dataset";
    }

    # load java module???
    &run("java -Xmx4g -jar \$PICARD/picard.jar CollectInsertSizeMetrics INPUT=$args->{bam} OUTPUT=$args->{scratchfolder}/picard.insert_size_metrics HISTOGRAM_FILE=$args->{scratchfolder}/picard.insert_size_histogram.pdf", $args->{logfile});
}

### insertsizeplot ###
sub insertsizeplot {
    my ($samples, $args, $stats, $plottype) = @_;

    if ($args->{pe} && (! $args->{stitch})) {
	print "Generating insertsize plot\n"; # if ($args->{verbose});
	open OFILE, ">insertmetrics-filelist.txt" or die "cannot open insertmetrics-filelist.txt: $!\n";
	foreach $sample (sort keys %{$samples}) {
	    print OFILE "$args->{scratchfolder}/singlesamples/$sample/picard.insert_size_metrics\t$sample\n";
	}
	close OFILE;
	$result = `insertplotrmd.pl -f insertmetrics-filelist.txt`;
	# process the stdout, grabbing some stats
	&getstats($args, $stats, $result, "insert");
	
	### add to report ###
	`mv insertplot.* $reportfolder`;
	$args->{reporttext} .= qq(```{r test-main, child = 'insertplot.rmd'}\n```\n);
	$args->{methods} .= qq(Insert sizes were summarized with Picard. );

    }
}


###################### Picard CollectWgsMetrics ######################
# Requires: java, $PICARD
# Args: referencefasta
sub wgsmetrics {
    print "Running Picard CollectWgsMetrics\n";

    my ($args, $stats) = @_;

    # load java module???
    &run("java -Xmx4g -jar \$PICARD/picard.jar CollectWgsMetrics INPUT=$args->{bam} COVERAGE_CAP=null OUTPUT=$args->{scratchfolder}/picard.wgs_metrics REFERENCE_SEQUENCE=$args->{referencefasta}", $args->{logfile});

}

######################## Picard MarkDuplicates ##########################
# Requires: java, $PICARD
sub markduplicates {
    print "Running Picard MarkDuplicates\n";

    my ($args, $stats) = @_;

    my $newbam = $args->{bam};
    $newbam =~ s/\.bam$/.dedup.bam/;

    # load java module???
    &run("java -Xmx4g -jar \$PICARD/picard.jar MarkDuplicates INPUT=$args->{bam} OUTPUT=$newbam CREATE_INDEX=true METRICS_FILE=removeduplicates.txt", $args->{logfile});
    die "Mark Duplicates failure\n" unless (-e $newbam);
    $args->{bam} = $newbam;

    # grab some stats
    $ifile = "$args->{scratchfolder}/removeduplicates.txt";
    open IFILE, "$ifile" or die "Cannot open picard log file $ifile: $!\n";
    while ($line = <IFILE>) {
	if ($line =~ /## METRICS CLASS/) {
	    $line = <IFILE>;
	    chomp $line;
	    @keys = split /\t/, $line;
	    $line = <IFILE>;
	    chomp $line;
	    @values = split /\t/, $line;
	    for $i (0..$#keys) {
		$values[$i] = "" unless (defined($values[$i])); # fill in for empty values
		$data{$keys[$i]} = $values[$i];
	    }
	    last;
	}
    }

    $stats->{removeduplicates}{"\%duplication"} = $data{PERCENT_DUPLICATION} * 100;
    $args->{methods} .= qq(Duplicate reads in the bam alignment files were marked with Picard MarkDuplicates. );

}

######################## Picard HsMetrics ##########################
sub hsmetrics {
    print "Running Picard HsMetrics\n";

    my ($args, $stats) = @_;

    # load java module???
    $result = run("java -Xmx2g -jar \$PICARD/picard.jar CalculateHsMetrics INPUT=$args->{bam} OUTPUT=$args->{scratchfolder}/hsmetrics.txt BAIT_INTERVALS=$args->{baitfile} TARGET_INTERVALS=$args->{targetfile}", $args->{logfile});
# this version uses the reference genome to calculate more stats
#    $result = run("java -Xmx2g -jar \$PICARD/picard.jar CalculateHsMetrics INPUT=$args->{bam} OUTPUT=$args->{scratchfolder}/hsmetrics.txt BAIT_INTERVALS=$args->{scratchfolder}/baits.txt TARGET_INTERVALS=$args->{scratchfolder}/targets.txt REFERENCE_SEQUENCE=$args->{referencefasta} PER_TARGET_COVERAGE=$args->{scratchfolder}/pertargetcoverage.txt", $args->{logfile});

    print $result;
}

### HsMetrics Plot ###
sub hsmetricsplot {
    my ($samples, $args, $stats) = @_;

    print "Running hsmetricsplot\n" if ($args->{verbose});
    open OFILE, ">hsmetrics-filelist.txt" or die "cannot open hsmetrics-filelist.txt: $!\n";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "$args->{scratchfolder}/singlesamples/$sample/hsmetrics.txt\t$sample\n";
    }
    close OFILE;
    @result = `picardhsmetricsplot.pl -f hsmetrics-filelist.txt`;

    # process the stdout, grabbing some stats
    $statzone = 0;
    foreach $result (@result) {
        $statzone = 0 if ($result =~ /^STATS End/);
        if ($statzone) {
            chomp $result;
            ($sample, $key, $stat) = split /\t/, $result;
            $stats->{$sample}{hsmetrics}{$key} = $stat;
        }
        $statzone = 1 if ($result =~ /^STATS Start/);
    }
    `mv *.png $args->{outputfolder}`;

    my $firstsample = (keys %{$stats})[0];

    # add to report
    &h1("Picard HsMetrics Plots");
    &fieldlist("Bait territory", "$stats->{$firstsample}{hsmetrics}{BAIT_TERRITORY}bp");
    &fieldlist("Target territory", "$stats->{$firstsample}{hsmetrics}{TARGET_TERRITORY}bp");
    &fieldlist("Genome size", "$stats->{$firstsample}{hsmetrics}{GENOME_SIZE}bp");
    &space;
    &image("picard-bait.png");
    &image("picard-coverage.png");
    &space;
}

### HsMetrics Prep ###
sub hsmetricsprep {
    my ($args) = @_;

    print "Running hsMetrics prep\n" if ($args->{verbose});

    # look for dict file in a couple obvious locations
    if (! $args->{referencedict}) {
	if ($args->{referencefasta}) {
	    my $tmpdict1 = $args->{referencefasta} . ".dict";
	    my $tmpdict2 = $args->{referencefasta};
	    if ($tmpdict2 =~ s/\.fa$/.dict/) {

	    } elsif ($tmpdict2 =~ s/\.fasta$/.dict/) {

	    } else {
		$tmpdict2 = "";
	    }
	    if (-e $tmpdict1) {
		$args->{referencedict} = $tmpdict1;
	    } elsif (-e $tmpdict2) {
		$args->{referencedict} = $tmpdict2;
	    }
	}
    }

    # create reference dict file if it doesn't already exist
    if (! $args->{referencedict}) {
	$args->{referencedict} = "$args->{scratchfolder}/reference.dict";
	run("java -Xmx2g -jar \$PICARD/picard.jar CreateSequenceDictionary REFERENCE=$args->{referencefasta} OUTPUT=$args->{referencedict}", $args->{logfile});
    }

    # convert BED to interval file
    $args->{baitfile} = "$args->{scratchfolder}/baits.interval_list";
    $args->{targetfile} = "$args->{scratchfolder}/targets.interval_list";

    run("java -Xmx2g -jar \$PICARD/picard.jar BedToIntervalList INPUT=$args->{baitbed} OUTPUT=$args->{baitfile} SEQUENCE_DICTIONARY=$args->{referencedict}", $args->{logfile});
    run("java -Xmx2g -jar \$PICARD/picard.jar BedToIntervalList INPUT=$args->{targetbed} OUTPUT=$args->{targetfile} SEQUENCE_DICTIONARY=$args->{referencedict}", $args->{logfile});

}

######################## Samtools View Filter ##########################
# Requires: samtools
sub samtoolsviewfilter {
    print "Running Samtools View filter\n";

    my ($args, $stats) = @_;

    my $newbam = $args->{bam};
    $newbam =~ s/\.bam$/.filter.bam/;

# include flags: read is paired: 1; read mapped in proper pair: 2 (total: 3)
# exclude flags: read is unmapped: 4; mate is unmapped: 8; read fails vendor QC: 512; read is PCR duplicate: 1024 (total: 1548)
    &run("samtools view -b -F 1548 -o $newbam $args->{bam}", $args->{logfile});
    die "Samtools view filter failure\n" unless (-e $newbam);
    &run("samtools index $newbam");
    $args->{bam} = $newbam;

}

######################## Samstat from 2013-07-08 ##########################
sub samstat {
    print "Running SAMStat\n";

    my ($args, $stats) = @_;

    run("samstat $args->{bam}", $args->{logfile});
    # give output file a better name
    `mv $args->{bam}.html $args->{scratchfolder}/samstat.html`;

}

######################## Samtools stats ##########################
sub samtoolsstats {
    print "Running Samtools stats plot\n";

    my ($args, $stats) = @_;

    my $ref = ($args->{referencefasta}) ? "--ref-seq $args->{referencefasta}" : "";
    my $statsfile = "$args->{scratchfolder}/samtools.stats";
    run("samtools stats $ref $args->{bam} > $statsfile", $args->{logfile});
    run("plot-bamstats -p $args->{scratchfolder}/samtoolsstats/ $statsfile", $args->{logfile});

    # get the peak indel number and %
    @lines = `grep ^IC $statsfile | cut -f 2-`;
    $maxsum = 0;
    foreach $line (@lines) {
	chomp $line;
	@line = split /\t/, $line;
	my $cycle = shift @line;
	$sum = $line[0] + $line[1] + $line[2] + $line[3];
	$maxsum = $sum if ($sum > $maxsum);	
    }

    $stats->{samtoolsstats}{peakindel} = $maxsum;
#    $stats->{samtoolsstats}{"%peakindel"} = $maxsum / $stats->{fastqc}{};

    # get the peak mismatch number and %
    @lines = `grep ^MPC $statsfile | cut -f 2-`;
    $maxsum = 0;
    foreach $line (@lines) {
	chomp $line;
	@line = split /\t/, $line;
	$sum = 0;
	for $index (31..$#line) {
	    $sum += $line[$index];
	}
	$maxsum = $sum if ($sum > $maxsum);
    }

    $stats->{samtoolsstats}{peakmismatch} = $maxsum;

}

##################### Base Quality Score Recalibration#######################
sub bqsr {
    print "Base Quality Score Recalibration with GATK\n";

    my ($args, $stats) = @_;

    $outputbam = "$args->{scratchfolder}/bqsr.bam";

    run("gatk-launch --javaOptions \"-XX:+PrintFlagsFinal -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps \
      -XX:+PrintGCDetails -Xloggc:gc_log.log \
	-XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10 -Xms3000m\" \
      ApplyBQSR \
      --addOutputSAMProgramRecord \
      -R $args->{referencefasta} \
      -I $args->{bam} \
      --useOriginalQualities \
      -O $outputbam \
      -bqsr $args->{scratchfolder}/bqsr-report.txt \
      -SQQ 10 -SQQ 20 -SQQ 30 \
      -L \${sep=\" -L \" sequence_group_interval}");

    if (! -e $outputbam) {
	die "BQSR failure, no bam generated\n";
    }
    $args->{bam} = $outputbam;
}



######################## Subread featurecounts ##########################
sub featurecounts {
    print "Estimating abundances with Subread\n";

    my ($args, $stats) = @_;

    $pairedend = ($args->{pe}) ? "-B -p" : "";
    $stranded = "";
    if ($args->{library} eq "picorna") {
        $stranded = "-s 1"; # FR
    } elsif ($args->{library} eq "picorna2") {
        $stranded = "-s 2"; # RF
    } elsif ($args->{library} eq "truseqrna") {
        $stranded = "-s 2"; # RF
    }

    my $featurecountsoptions = $args->{extraoptions}{featurecounts} // "$stranded $pairedend -Q 10";

    run("featureCounts -T $args->{threads} $featurecountsoptions -a $args->{gtffile} -o $args->{scratchfolder}/subread-counts.txt $args->{bam}", $args->{logfile});

    $args->{methods} .= "Gene-level raw read counts were generated using featureCounts from the Subread R package using the options '$featurecountsoptions'. ";

}

### Subreadplot ### - nobody calls this function
sub subreadplot {

    my ($samples, $args, $stats) = @_;
    print "Generating subread plots\n" if ($args->{verbose});

    open OFILE, ">subread-filelist.txt" or die "cannot open subread-filelist.tx\
t: $!\n";
    foreach $sample (sort keys %{$samples}) {
        print OFILE "$args->{scratchfolder}/rnaseqQC-ss/$sample/subread-counts.txt.summary\t$sample\n";
    }
    close OFILE;
    @result = `subreadplot.pl -f subread-filelist.txt`;

    # process the stdout, grabbing some stats
    $statzone = 0;
    foreach $result (@result) {
	$statzone = 0 if ($result =~ /^STATS End/);
	if ($statzone) {
	    chomp $result;
	    ($sample, $key, $stat) = split /\t/, $result;
	    $stats->{$sample}{subread}{$key} = $stat;
	}
	$statzone = 1 if ($result =~ /^STATS Start/);
    }
    `mv subread.png $args->{outputfolder}`;

    # add to report
    &h2("Subread Plots");
    &image("subread.png");
    &space;
}

######################## Cuffquant ##########################
sub cuffquant {
    print "Estimating abundances with cuffquant\n";

    my ($args, $stats) = @_;

    my $frags = ($stats->{subsample}{"subsampledreads"} > 20000000) ? "--max-bundle-frags 10000000" : "";
    my $mask = ($args->{maskfile}) ? "--mask-file $args->{maskfile}" : "";

    my $cuffquantoptions = $args->{extraoptions}{cuffquant} // "--quiet --multi-read-correct $frags $mask";

    run("cuffquant $cuffquantoptions --no-update-check $args->{gtffile} $args->{bam} -o $args->{scratchfolder} -p $args->{threads}", $args->{logfile});

}

######################## GATK HaplotypeCaller ##########################
sub haplotypecaller {
    print "Running GATK HaplotypeCaller\n";

    my ($args, $stats) = @_;

    my $intervals = ($args->{intervals}) ? "--intervals $args->{intervals}" : "";
    $gvcf = "$args->{scratchfolder}/gatk.g.vcf.gz";
    $padding = "";
    if ($args->{intervals} =~ /\.vcf$/) {
#	$padding = "--interval-padding 40";
    }
    &run("gatk --java-options \"-Xmx4g\" HaplotypeCaller -R $args->{referencefasta} -I $args->{bam} -O $gvcf -ERC GVCF $padding $intervals", $args->{logfile}); # --sparkMaster local[8]  <- use with spark version of HC??

    $stats->{files}{gvcf} = $gvcf;
}

######################## bamdownsample ##########################
sub bamdownsample {
    print "Running BAM downsample\n";

    my ($args, $stats, $depth) = @_;

    $depth = $depth // 500;

    my $newbam = $args->{bam};
    $newbam =~ s/\.bam$/.ds.bam/;
    &run("variant $args->{bam} -m $depth -o $newbam -b", $args->{logfile});
    &run("samtools index $newbam", $args->{logfile});
    $args->{bam} = $newbam;
    $stats->{files}{bam} = $newbam;

    $args->{methods} .= "Regions of bam files with more than $depth reads were downsampled to a depth of $depth reads using VariantBam. ";

}

############################# all Gunzip #################################
sub allgunzip {
    my ($samples, $args, $stats) = @_;

    foreach $sample (keys %{$samples}) {
	$args->{gz} = 1 if (compressed($samples->{$sample}{R1}{fastq}));
	$args->{gz} = 1 if ($samples->{$sample}{R2}{fastq} and compressed($samples->{$sample}{R2}{fastq}));
    }
    if (! $args->{gz}) {
	print "Skipping gunzip, files not compressed\n" if ($args->{verbose});
	return 1;
    }

    print "\n### Running gunzip on all samples ###\n";

    push @{$args->{stageorder}}, "gunzip";

    # gunzip files
    my $myscratchfolder = "$args->{scratchfolder}/fastq-gunzip";
    mkdir $myscratchfolder;
    $gpout = "$myscratchfolder/gp.out";
    $progress = $args->{verbose} ? "--progress" : "";
    open GP, "| parallel $progress -j $args->{threads} > $gpout" or die "Cannot open pipe to gnu parallel\n";
#    open GP, ">$myscratchfolder/parallel.test" or die "Cannot open pipe to gnu parallel\n";

    # send commands to gnu parallel
    foreach $sample (keys %{$samples}) {
	my ($fname, $path) = fileparse($samples->{$sample}{R1}{fastq}, (".gz"));
	$newfile = "$myscratchfolder/$fname";
	print GP "gunzip -c $samples->{$sample}{R1}{fastq} > $newfile\n";
	$samples->{$sample}{R1}{fastq} = $newfile;
	if (defined($samples->{$sample}{R2}{fastq})) {
	    ($fname, $path) = fileparse($samples->{$sample}{R2}{fastq}, (".gz"));
	    $newfile = "$myscratchfolder/$fname";
	    print GP "gunzip -c $samples->{$sample}{R2}{fastq} > $newfile\n";
	    $samples->{$sample}{R2}{fastq} = $newfile;
	}
    }

    close GP;
    print "error code: $?\n" if ($?);
    $args->{fastqfolder} = "$myscratchfolder";
    return 0;
}


############### Single sample setup and helper functions ##############

### Set up a scratch folder, add symlinks to fastq files
sub scratchfoldersetup {
    my ($args) = @_;

    my $scratchfolder = $args->{scratchfolder};

    ### set up scratch folders
    if (-e $scratchfolder) {
	print "scratch folder $scratchfolder exists, deleting it\n";
        `rm -r $scratchfolder`;
	die "Error: Cannot remove existing scratchfolder $scratchfolder\n" if (-e $scratchfolder);
    }
    mkdir $scratchfolder or die "Cannot create scratchfolder $scratchfolder: $!\n";
    chdir $scratchfolder;

    # make local copy of fastq files
    $ext = "fastq";
    if (compressed($args->{R1})) {
	$ext = "fastq.gz";
    }
    `ln -s $args->{R1} $scratchfolder/R1.$ext`;
    $args->{R1} = "$scratchfolder/R1.$ext";
    if ($args->{pe}) {
	`ln -s $args->{R2} $scratchfolder/R2.$ext`;
	$args->{R2} = "$scratchfolder/R2.$ext";
    }
}

# write the %stats hash out to an easily-parsed text file
sub writestats {
    print "Writing stats\n";

    my ($args, $stats) = @_;

    my $ofile = "$args->{scratchfolder}/stats.txt";
    open OFILE, ">$ofile" or die "Cannot open stats file $ofile\n";
    foreach $stage (keys %{$stats}) {
	foreach $stat (keys %{$stats->{$stage}}) {
	    print OFILE "$stage\t$stat\t$stats->{$stage}{$stat}\n";
	}
    }
    close OFILE;

    $ofile = "$args->{scratchfolder}/methods.txt";
    open OFILE, ">$ofile" or die "Cannot open methods file $ofile\n";
    print OFILE $args->{methods};
    close OFILE;

}

############################################################################
############################# All Sample Scripts ###########################
############################################################################

### bam2fastq ###
sub bam2fastq {
    my ($samples, $args, $stats, $vcf) = @_;

    print "Running bam2fastq\n"; # if ($args->{verbose});
    $myscratchfolder = "$args->{scratchfolder}/bam2fastq";
    mkdir $myscratchfolder;
    mkdir "$myscratchfolder/aligned";
    mkdir "$myscratchfolder/unaligned";    

    $gpout = "$myscratchfolder/bam2fastq.gpout";
    open GP, "| parallel -j $args->{threadspersample} > $gpout" or die "Cannot open pipe to gnu parallel\n"; 
    open FILE, ">$myscratchfolder/commands.txt" or die "Cannot open pipe to gnu parallel\n";

    # TODO: add -f or -F flags to include only primary alignments, or only unaligned reads
#    $filter = "-f 4";
    # then add this to align-pipeline
    foreach $sample (keys %{$samples}) {
	# unaligned reads

	if ($args->{pe}) {
	    $command = "samtools fastq -f 4 -1 $myscratchfolder/unaligned/${sample}_R1.fastq.gz -2 $myscratchfolder/unaligned/${sample}_R2.fastq.gz -s $myscratchfolder/$sample_singletons.fastq.gz $stats->{$sample}{files}{bam}\n";
	} else {
	    $command = "samtools fastq -f 4 -0 $myscratchfolder/unaligned/${sample}_R1.fastq.gz $stats->{$sample}{files}{bam}\n";
	}
	print GP $command;
	print FILE $command;
	# aligned reads: don't print if unmapped or not primary alignment
	if ($args->{pe}) {
	    $command = "samtools fastq -F 4 -F 256 -1 $myscratchfolder/aligned/${sample}_R1.fastq.gz -2 $myscratchfolder/aligned/${sample}_R2.fastq.gz $stats->{$sample}{files}{bam}\n";
	} else {
	    $command = "samtools fastq -F 4 -F 256 -0 $myscratchfolder/aligned/${sample}_R1.fastq.gz $stats->{$sample}{files}{bam}\n";
	}
	print GP $command;
	print FILE $command;
    }
    close GP;
    close IFILE;

    `ln -s ../bam2fastq/aligned $args->{outputfolder}/aligned`;
    `ln -s ../bam2fastq/unaligned $args->{outputfolder}/unaligned`;

    $args->{methods} .= "Fastq files for aligned and unaligned reads were generated from the bam files using 'Samtools fastq'. ";

}


# run freebayes on all samples (bam files)
sub freebayes {
    print "\n### Calling variants with Freebayes ###\n";

    my ($samples, $args, $stats) = @_;

    push @{$args->{stageorder}}, "freebayes";

    my $myscratchfolder = "$args->{scratchfolder}/freebayes";
    mkdir $myscratchfolder;
    $mytmpdir = "$myscratchfolder/tmp";
    mkdir $mytmpdir;
    my $vcffolder = "$myscratchfolder/vcffiles";
    mkdir $vcffolder;
    $vcfrawfile = "$myscratchfolder/variants.raw.vcf";
    $vcffile = "$myscratchfolder/variants.vcf";
    $vcffilterfile = "$myscratchfolder/variants.filt.vcf";
    my $ploidy = $args->{ploidy} ? "--ploidy $args->{ploidy}" : "";
    $mincoverage = keys %{$samples};
    $mincoverage = $mincoverage * 2;
    $mincoverage = "--min-coverage $mincoverage";
    # set max coverage to 0.1% of total reads
    $totalreads = 0;
    foreach $sample (keys %{$samples}) {
        $totalreads += $stats->{$sample}{subsample}{"subsampledreads"}
    }
    $maxcoverage = 1000; #int($totalreads * .001);
    $maxcoverage = "--max-coverage $maxcoverage";

    $bamlist = "";
    # make local bam folder to keep freebayes command line length within unix limits
    mkdir "$myscratchfolder/bams";
    foreach $sample (keys %{$samples}) {
#	print "Freebayes bam $sample $stats->{$sample}{files}{bam}\n";
	`ln -s $stats->{$sample}{files}{bam} $myscratchfolder/bams/$sample.bam` unless (-e "$myscratchfolder/bams/$sample.bam");
	`ln -s $stats->{$sample}{files}{bam}.bai $myscratchfolder/bams/$sample.bam.bai` unless (-e "$myscratchfolder/bams/$sample.bam.bai");;
	$bamlist .= " bams/$sample.bam";
#	$bamlist .= " $stats->{$sample}{files}{bam}";
    }
#    print "BAMlist: $bamlist end\n";

    my $originaldir = `pwd`;
    chdir $myscratchfolder;

    $freebayesthreads = $args->{freebayesthreads} // $args->{threads};
    $freebayesoptions = "--genotype-qualities $ploidy $mincoverage";
    if ($freebayesthreads > 1) {

	$gpout = "$myscratchfolder/freebayes.gpout";
	open GP, "| parallel -j $freebayesthreads > $gpout" or die "Cannot open pipe to gnu parallel\n"; 
	open FILE, ">$myscratchfolder/commands.txt" or die "Cannot open pipe to gnu parallel\n";

	@regions = `bedtools makewindows -g $args->{referencefasta}.fai -w 2000000`;

	$scaffoldcount = 0;
	foreach $line (@regions) {
	    chomp $line;
	    my ($chr, $start, $end) = split /\t/, $line;
            $region = "$chr:$start-$end";
            $nicescaffold = "$chr.$start.$end";
            $nicescaffold =~ s/\W/./g;
	    $scaffoldcount++;
	    if ($args->{resume} && -e "$vcffolder/$nicescaffold.complete") {
		print "Skipping completed scaffold $nicescaffold\n";
		next;
	    }
	    $output = "$vcffolder/$nicescaffold.vcf";
	    $command = "freebayes $freebayesoptions --fasta-reference $args->{referencefasta} $bamlist --region \"$region\" > $output && touch $vcffolder/$nicescaffold.complete\n";
	    print GP $command;
	    print FILE $command;
	}
	close IFILE;
	close GP;

	# check that all freebayes completed successfully
	$completecount = `ls $vcffolder/*.complete | wc -l`;
	chomp $completecount;
	print STDERR "Freebayes error, freebayes was successful on only $completecount of $scaffoldcount scaffolds\n" if ($completecount != $scaffoldcount);

	# combine all vcf files into one
	&run("vcf-merge.pl $vcffolder $vcfrawfile", $args->{logfile});
#	$result = `gzip $tmpvcf`;
	print $result if ($args->{verbose});
	
    } else {
	&run("freebayes $freebayesoptions --fasta-reference $args->{referencefasta} $bamlist > $vcfrawfile", $args->{logfile});
    }
    chdir $originaldir;

    # basic filter: variants with QUAL > 20
    &run("vcffilter -f \"QUAL > 20\" $vcfrawfile > $vcffile", $args->{logfile});

    # stringent filter: remove samples with > 5% missing genotypes, and variants with missing calls in more than 5% of samples
    $samplecutoff = $args->{samplecutoff} ? "--samplecutoff $args->{samplecutoff}" : "";
    $markercutoff  = $args->{markercutoff} ? "--markercutoff $args->{markercutoff}" : "";
    $maf  = $args->{maf} ? "--maf $args->{maf}" : "";
    &run("vcffilter.pl $samplecutoff $markercutoff $maf --vcffile $vcffile --output $vcffilterfile", $args->{logfile});

    $args->{vcffilter} = $vcffilterfile;
    $args->{vcf} = $vcffile;

    $args->{methods} .= "Freebayes was used to call variants jointly across all samples using the options '$freebayesoptions'. The raw VCF file generated by Freebayes was filtered to remove the lowest quality variants using vcffilter with the options '-f \"QUAL > 20\"'. ";

}

### freebayes Plot ###
sub freebayesplot {
    my ($samples, $args, $stats) = @_;

# the freebayes subroutine already generates the plots, here we just add the 
# plots to the report, this provides control over where in the report
# the plots appear

    # add to report
    &h2("Freebayes Plots");  
    &image("pcaplot.png");
    &image("pcaplot-filt.png");
    &space;

}

################# GATK joint calling GenotypeGVCFs ###################
sub genotypegvcfs {
    my ($samples, $args, $stats, $vcf) = @_;

    print "Running GATK genotypegvcfs\n"; # if ($args->{verbose});
    $myscratchfolder = "$args->{scratchfolder}/genotypegvcfs";
    mkdir $myscratchfolder;

    $gvcflist = "";
    # make local gvcf folder to keep gatk command line length within unix limits
    mkdir "$myscratchfolder/gvcf";
    foreach $sample (keys %{$samples}) {
	`ln -s $stats->{$sample}{files}{gvcf} $myscratchfolder/gvcf/$sample.g.vcf.gz`;
	`ln -s $stats->{$sample}{files}{gvcf}.tbi $myscratchfolder/gvcf/$sample.g.vcf.gz.tbi`;
	$gvcflist .= " --variant genotypegvcfs/gvcf/$sample.g.vcf.gz";
    }
#    my $intervals = ($args->{intervals}) ? "--intervals $args->{intervals}" : ""; # this hits a bug in GATK, rely on interval list passed to individual sample calling to limit regions called
    $intervals = "";

    $include = "--include-non-variant-sites" if ($args->{ampliseq});
    $gvcffile = "$myscratchfolder/cohort.g.vcf.gz";
    &run("gatk CombineGVCFs -R $args->{referencefasta} $gvcflist $intervals -O $gvcffile", $args->{logfile});


    $vcffile = "$myscratchfolder/gatk.vcf";
    &run("gatk --java-options \"-Xmx4g\" GenotypeGVCFs $include -R $args->{referencefasta} -V $gvcffile -O $vcffile", $args->{logfile});
    $args->{vcf} = $vcffile;

    # filter SNPs
    $vcffiltfile = "$myscratchfolder/gatk.filt.vcf";
    &run("gatk VariantFiltration -R $args->{referencefasta} -V $vcffile -O $vcffiltfile --filter-expression \"QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0\" --filter-name \"gatk-standard\"", $args->{logfile});

# QD < 2.0
# FS > 60.0
# MQ < 40.0
# SOR > 3.0
# MQRankSum < -12.5
# ReadPosRankSum < -8.0
   
}

### gbsvcfreport ###
sub gbsvcfreport {
    my ($samples, $args, $stats, $reportFH) = @_;

    print "Running gbsvcfreport\n" if ($args->{verbose});

    #determine number of samples and SNPs in vcf files
    $vcfmarkers = `grep -v "^#" $args->{vcf} | wc -l`;
    chomp $vcfmarkers;
    $vcfsamples = `grep -m 1 "#CHROM" $args->{vcf} | wc -w`;
    chomp $vcfsamples;
    $vcfsamples -= 9;

    $result = `vcffilter.pl --vcffile $args->{vcf} --outputfile tmp.vcf --samplecutoff 0 --markercutoff 0 --maf 0 --thin 130 2>/dev/null`;
    $result =~ /kept (\d+) out of/;
    $goodmarkers = $1;

    if ($args->{vcffilter}) {
	$vcffiltermarkers = `grep -v "^#" $args->{vcffilter} | wc -l`;
	chomp $vcffiltermarkers;
	$vcffiltersamples = `grep -m 1 "#CHROM" $args->{vcffilter} | wc -w`;
	chomp $vcffiltersamples;
	$vcffiltersamples -= 9;

	$result = `vcffilter.pl --vcffile $args->{vcffilter} --outputfile tmp.vcf --samplecutoff 0 --markercutoff 0 --maf 0 --thin 130 2>/dev/null`;
	$result =~ /kept (\d+) out of/;
	$goodfiltermarkers = $1;

	# build a list of removed samples
	$samplelist = `grep -m 1 "#CHROM" $args->{vcffilter}`;
	chomp $samplelist;
	@samples = split ' ', $samplelist;
	@samples = @samples[ 9 .. $#samples ];
	foreach $sample (@samples) {
	    $seen{$sample} = 1;
	}
#	$ofile = "$args->{outputfolder}/filteredsamples.txt";
#	open OFILE, ">$ofile" or die "cannot open $ofile: $!\n";
	$filteredsamples = 0;
	$filteredsamplelist = "";
	foreach $sample (keys %{$samples}) {
	    if (! $seen{$sample}) {
#		print OFILE "$sample\n";
		$filteredsamples++;
		$filteredsamplelist .= "$sample ";
	    }
	}
#	close OFILE;
    }

    $markers = `grep -v "^#" $args->{vcf} | cut -f 3 | cut -f 1 -d '_' | sort | uniq | wc -l`;
    chomp $markers;
    $filtmarkers = `grep -v "^#" $args->{vcffilter} | cut -f 3 | cut -f 1 -d '_' | sort | uniq | wc -l`;
    chomp $filtmarkers;

    print $reportFH qq(variants.vcf.gz VCF variant file (compressed) containing $vcfsamples samples and $vcfmarkers markers on $goodmarkers loci<br>\n);

    if ($args->{vcffilter}) {
	$maf = $args->{maf} * 100;
	$samplecutoff = $args->{samplecutoff} * 100;
	$markercutoff = $args->{markercutoff} * 100;

	print $reportFH qq(variants.filt.vcf.gz Filtered VCF variant file (compressed) containing $vcffiltersamples samples and $vcffiltermarkers markers on $goodfiltermarkers loci<br>\n);

	print $reportFH qq(- Samples with > ${samplecutoff}\% missing genotypes, and variants with genotype calls in less than ${markercutoff}\% of samples are removed; variants with maf < ${maf}\% are removed<br>\n);
	print $reportFH qq(- $filteredsamples samples removed: $filteredsamplelist<br>\n);
    }

    $padding = $args->{padding} // $padding{$args->{enzyme} . "-r1"};
    if ($padding) {
	$paddingtext = "One of the following adapter sequences is expected to be present at the beginning of each read in the raw fastq files:<br>\n";
	@padding = split /,/, $padding;
	foreach $pad (@padding) {
	    if ($pad eq "") {
		$paddingtext .= "[no adapter sequence present]<br>\n";
	    } else {
		$paddingtext .= "$pad<br>\n";
	    }
	}
	$paddingtext .= "Some reads may read through into the Illumina adapter sequnce which begins with CTGTCTCTTATACACATCTCCGAG<br>\n";
    print $reportFH qq(\n## Fastq files
$paddingtext
);

    }


    ### Write out project.info file ###
    # runtype
    $runtype = ($args->{pe}) ? "paired-end" : "single-end";

    # readlength
    @samples = keys %{$samples};
    $samplenumber = $#samples + 1;
    $firstsample = $samples[0];
    $readlength = $stats->{$firstsample}{general}{readlength};
#    $readlength = `fastqreadlength.pl $samples->{$firstsample}{R1}{fastq}`;
#    chomp $readlength;

    # genomesize
    $genomesize = `awk '{s+=\$2} END {print s}' $args->{referencefasta}.fai`;
    chomp $genomesize;

    # read depth mean
    $rawtotal = 0;
    $subsampledtotal = 0;
    foreach $sample (@samples) {
	$rawtotal += $stats->{$sample}{subsample}{"rawreads"};
	$subsampledtotal += $stats->{$sample}{subsample}{"subsampledreads"};
    }
    $rawmean = round10($rawtotal / $samplenumber);
    $subsampledmean = round10($subsampledtotal / $samplenumber);

    # read dpeth CV
    $rawdiftotal = 0;
    $subsampleddiftotal = 0;
    foreach $sample (@samples) {
	$rawdiftotal += ($stats->{$sample}{subsample}{"rawreads"} - $rawmean)**2;
	$subsampleddiftotal += ($stats->{$sample}{subsample}{"subsampledreads"} - $subsampledmean)**2;
    }
    $rawstd = sqrt($rawdiftotal / $samplenumber);
    $subsampledstd = sqrt($subsampleddiftotal / $samplenumber);
    $rawcv = round10($rawstd / $rawmean * 100);
    $subsampledcv = round10($subsampledstd / $subsampledtotal * 100);

    # enzyme
    $myenzyme = ($args->{enzyme2}) ? "$args->{enzyme}-$args->{enzyme2}" : "$args->{enzyme}";

    # write out info
    $statsfile = "$args->{outputfolder}/project.info";
    open OFILE, ">$statsfile";
    print OFILE qq(Project\t$args->{runname}
Type\t
Size_selection\t
Instrument\t
Species_common_name\t
Genus_species\t
Ploidy\t
Population_type\t
Targeted_read_depth\t

Enzyme\t$myenzyme
Run_type\t$runtype
Read_length\t$readlength
Genome_size\t$genomesize
Mean_read_depth\t$rawmean
Cv_read_depth\t$rawcv
Subsampled_mean_read_depth\t$subsampledmean
Subsampled_cv_read_depth\t$subsampledcv
Sample_number\t$samplenumber
Filtered_marker_number\t$vcffiltermarkers
Filtered_loci_number\t$goodfiltermarkers
Pipeline\tfreebayes
Notes\t
);
    close OFILE;
    
}


### vcfMetrics Plot ###
sub vcfmetricsplotrmd {
    my ($samples, $args, $stats, $vcf) = @_;

    my $vcffile = $vcf // $args->{vcf};

    ($prefix) = fileparse($vcffile);
    ($prefix) = split /vcf$/, $prefix;
    print "Generating VCF metrics plots ($prefix)\n";

    $result = run("vcfmetricsplotrmd.pl --vcffile $vcffile --prefix $prefix", $args->{logfile});

    # process the stdout, grabbing some stats
    &getstats($args, $stats, $result, "vcfmetricsplot$prefix");
    
    # generate chrom plot
    if ($args->{referencefasta}) {
	run("chromplot.pl --vcffile $vcffile --referencefai $args->{referencefasta}.fai --prefix $prefix", $args->{logfile});
    }

    # add to report
    `mv $prefix* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = '${prefix}pca.rmd'}\n```\n);
    $args->{reporttext} .= qq(```{r test-main, child = '${prefix}sample-depth.rmd'}\n```\n);
    $args->{reporttext} .= qq(```{r test-main, child = '${prefix}sample-miss.rmd'}\n```\n);
    $args->{reporttext} .= qq(```{r test-main, child = '${prefix}site-miss.rmd'}\n```\n);
    $args->{reporttext} .= qq(```{r test-main, child = '${prefix}chromcountplot.rmd'}\n```\n);
    $args->{reporttext} .= qq(![](${prefix}chromplot.png)\n);

    my $firstsample = (keys %{$stats})[0];
    ($vcfname) = fileparse($vcffile);

    # create data file of marker numbers at different filter cutoffs
    $ofile = $prefix . "markers";
    if ($args->{referencefasta}) {
	$thin = 100;
    } else {
	$thin = 0
    }
    open OFILE, ">$ofile";
    foreach $cutoff (.99, .95, .9, .8, .7, .6, .5) {
	$result = `vcffilter.pl --vcffile $vcffile --outputfile tmp.vcf --samplecutoff $cutoff --markercutoff $cutoff --thin $thin 2>/dev/null`;
	$result =~ /kept (\d+) out of/;
	$markers = $1;
	if ($thin == 0) {
	    $markers = `grep -v "^#" tmp.vcf | cut -f 3 | cut -f 1 -d '_' | sort | uniq | wc -l`;
	    chomp $markers;
	}
	$result =~ /(\d+) bad samples filtered out from a total of (\d+) samples/;
	$bad = $1;
	$total = $2;
	$good = $total - $bad;
	print OFILE "$cutoff\t$markers\t$good\n";
    }
    close OFILE;
    `mv *.markers $args->{outputfolder}`;
}

# set up scratch and output folder
# read in samplesheet, make sure it is properly formed
sub samplesheetandfoldersetup {
    my ($samples, $args, $stats, $pipeline) = @_;

    print "Processing samplesheet and setting up folders\n" if ($args->{verbose});

    push @{$args->{stageorder}}, "general";

    ### set up folders
    if ($args->{samplesheet}) {
	die "Cannot find samplesheet $args->{samplesheet}\n" if (! -e $args->{samplesheet});
	$args->{samplesheet} = abs_path($args->{samplesheet});
    }
    die "Fastq folder $args->{fastqfolder} not found\n" if (!-e $args->{fastqfolder});
    $args->{fastqfolder} = abs_path($args->{fastqfolder});

    # set runname, projectname, illuminasamplesheet
    $runname = "";
    $projectname = "";
    $illuminasamplesheet = "";
    if ($args->{fastqfolder} =~ m|^/panfs/roc/umgc/illumina_analysis/|) {
# /panfs/roc/umgc/illumina_analysis/190503_A00223_0117_AH7TYLDRXX-analysis/demultiplex_20190507-11-27-47/demultiplex/Chen2_Project_008/	
	@path = split /\//, $args->{fastqfolder};
	shift @path;
	$runname = $path[$#path];
	$runname =~ s/-analysis$//;
	$projectname = "$path[7]";
	$illuminasamplesheet = "$args->{fastqfolder}/../../SampleSheet.csv";
	$illuminasamplesheet = (-e $illuminasamplesheet) ? abs_path($illuminasamplesheet) : "";
    }

    $args->{runname} = $args->{runname} || $runname || fileparse($args->{fastqfolder});
    $args->{projectname} = $args->{projectname} // $projectname;
    $args->{illuminasamplesheet} = $args->{illuminasamplesheet} || $illuminasamplesheet;

    my $user = `whoami`;
    chomp $user;

    if (!$args->{resume}) {
	if (($args->{scratchfolder}) && ($args->{scratchfolder} !~ /$SCRATCHGLOBAL/)) { # user specified scratchfolder, and it's not in scratch
	    if (-e $args->{scratchfolder}) {
		$time = `date +%Y%m%d-%H-%M-%S`;
		chomp $time;
		$args->{scratchfolder} = "$args->{scratchfolder}/$pipeline-$time";
		die "Scratchfolder already exists, remove and re-run: $args->{scratchfolder}\n" if (-e $args->{scratchfolder});
	    }
	} else { # use default scratchfolder - delete if already exists
	    $args->{scratchfolder} = $args->{scratchfolder} // "$SCRATCHGLOBAL/$user-pipelines/$pipeline-$args->{runname}";
	    if (-e $args->{scratchfolder}) {
		print "scratch folder $args->{scratchfolder} exists\n";
		$|++;
		for $i (4..5) { 
		    $remaining = 5-$i;
		    print "\rDELETING $args->{scratchfolder} in $remaining seconds";
		    sleep 1;
		}
		print "\n";
		`rm -r $args->{scratchfolder}`;	    
		die "Error: Cannot remove existing scratchfolder $args->{scratchfolder}\n" if (-e $args->{scratchfolder});
	    }
	}
	
	`mkdir --parents $args->{scratchfolder}`;
	die "Cannot create scratch folder $args->{scratchfolder}\n" if (! -e $args->{scratchfolder});
	$args->{scratchfolder} = abs_path($args->{scratchfolder});
	mkdir "$args->{scratchfolder}/allsamples";
	$reportfolder = "$args->{scratchfolder}/report";
	mkdir $reportfolder;
	
	$args->{outputfolder} = "$args->{scratchfolder}/output";
	mkdir "$args->{outputfolder}";
	die "Cannot create output folder $args->{outputfolder}\n" if (! -e $args->{outputfolder});
	
	mkdir "$args->{scratchfolder}/fastq";
	$args->{logfile} = "$args->{scratchfolder}/log";
	
	### Process samplesheet
	# create samplesheet if not provided
	if (!defined($args->{samplesheet})) {
	    print "Creating samplesheet\n";
	    $args->{samplesheet} = "$args->{scratchfolder}/samplesheet.txt";
	    if ( ($pipeline eq "umgc") or ($pipeline eq "illumina") or ($pipeline eq "ampliseq")) { # require illumina fastq filenames
		$result = `createsamplesheet.pl -i -f $args->{fastqfolder} -o $args->{samplesheet}`;
	    } else {
		$result = `createsamplesheet.pl -f $args->{fastqfolder} -o $args->{samplesheet}`;
	    }
	    print $result;
	    die "Samplesheet could not be created\n" if (! -e $args->{samplesheet});
	} else { # copy over the specified samplesheet
	    $result = `cp $args->{samplesheet} $args->{scratchfolder}/samplesheet.txt`;
	    print $result;
	    $args->{samplesheet} = "$args->{scratchfolder}/samplesheet.txt";
	}
    } else { # resumeing a run
	$args->{scratchfolder} = $args->{scratchfolder} // "$SCRATCHGLOBAL/$user-pipelines/$pipeline-$args->{runname}";
	die "Attempting to resume run but scratch folder doesn't exist: $args->{scratchfolder}\n(Re-run without the --resume option)\n" unless (-e $args->{scratchfolder});
	# copy over samplesheet if it is newer than the copy in scratch
	if (defined($args->{samplesheet})) {
	    if (-M $args->{samplesheet} < -M "$args->{scratchfolder}/samplesheet.txt") {
		$result = `cp $args->{samplesheet} $args->{scratchfolder}/samplesheet.txt`;
		print $result;
	    }
	}
	$args->{outputfolder} = $args->{outputfolder} // "$args->{scratchfolder}/output";
	$args->{outputfolder} = abs_path($args->{outputfolder}) if ($args->{outputfolder});
	$args->{samplesheet} = "$args->{scratchfolder}/samplesheet.txt";
	$args->{outputfolder} = "$args->{outputfolder}";
	$reportfolder = "$args->{scratchfolder}/report";
	$args->{logfile} = "$args->{scratchfolder}/log";
    }	

    chdir $args->{scratchfolder};

    fixsamplesheet($args->{samplesheet});

    # read in the samplesheet
    open MFILE, $args->{samplesheet} or die "Cannot open samplesheet " . $args->{samplesheet};
    $header = <MFILE>;
    chomp $header;
    @header = split /\t/, lc($header);
    if ($header =~ /fastqR2/i) {
	$args->{pe} = 1;
	print "Paired-end read dataset\n";
    } else {
	$args->{pe} = 0;
	print "Single-end read dataset\n";
    }
    while ($line = <MFILE>) {
	chomp $line;
	@line = split /\t/, $line;
	$sample = $line[0];
	for $i (1..$#line) {
	    $samplesheetdata{$sample}{$header[$i]} = $line[$i] // "";
	}
    }
    close MFILE;

    # verify samplesheet file has required columns
    die "Samplesheet must contain a \"fastqR1\" column\n" if ($header !~ /fastqR1/i);
    die "Samplesheet must contain a \"fastqR2\" column\n" if (($header !~ /fastqR2/i) && ($args->{pe}));

    # print out samplesheet info for debugging
    if (0) { 
    foreach $sample (keys %samplesheetdata) {
	print "$sample";
	foreach $key (sort keys %{$samplesheetdata{$sample}}) {
	    print ":$key=$samplesheetdata{$sample}{$key}";
	}
	print "\n";
    }
    }

    ### For each sample save info from the samplesheet for later use
    foreach $sample (keys %samplesheetdata) {
	$file = $samplesheetdata{$sample}{fastqr1};
	if (! -e "$args->{fastqfolder}/$file") {
	    print "Could not find file $args->{fastqfolder}/$file\n";
	    next;
	}
	if (-z "$args->{fastqfolder}/$file") {
	    print "Excluding sample $sample from analysis due to empty fastq file $file\n";
	    next;
	}
	if (compressed($file)) {
	    $result = `gunzip -c $args->{fastqfolder}/$file | head -n 4`;
	    $result =~ s/\s+//g;
	    if ($result eq "") {
		print "Excluding sample $sample from analysis due to empty fastq file $file\n";
		next;
	    }
	}
	$cleansample = $sample;
#	$cleansample =~ s/_/-/g if ($pipeline eq "metagenomics");
	$fullname = $cleansample . "_R1.fastq";
	$fullname .= ".gz" if (compressed($file));
	$samples->{$cleansample}{R1}{fastq} = "$args->{scratchfolder}/fastq/$fullname";
	push @symlinks, " ln -s $args->{fastqfolder}/$file $args->{scratchfolder}/fastq/$fullname" unless (-e "$args->{scratchfolder}/fastq/$fullname" and $args->{resume});
	if ($args->{pe}) {
	    $file = $samplesheetdata{$sample}{fastqr2};
	    if (! -e "$args->{fastqfolder}/$file") {
		print "Could not find file $args->{fastqfolder}/$file\n";
	    }
	    if (-z "$args->{fastqfolder}/$file") {
		print "Excluding sample $sample from analysis due to empty fastq file $file\n";
		next;
	    }
	    if (compressed($file)) {
		$result = `gunzip -c $args->{fastqfolder}/$file | head -n 4`;
		$result =~ s/\s+//g;
		if ($result eq "") {
		    print "Excluding sample $sample from analysis due to empty fastq file $file\n";
		    next;
		}
	    }
	    $fullname = $cleansample . "_R2.fastq";
	    $fullname .= ".gz" if (compressed($file));
	    $samples->{$cleansample}{R2}{fastq} = "$args->{scratchfolder}/fastq/$fullname";
	    push @symlinks, " ln -s $args->{fastqfolder}/$file $args->{scratchfolder}/fastq/$fullname" unless (-e "$args->{scratchfolder}/fastq/$fullname" and $args->{resume});
	}

#	if ($pipeline eq "metagenomics") {
#	    $samples->{$cleansample}{forwardprimer} = $samplesheetdata{$sample}{linkerprimersequence} // "";
#	    $samples->{$cleansample}{reverseprimer} = $samplesheetdata{$sample}{reverseprimer} // "";
#	} 

	if (($pipeline eq "gbs") or ($pipeline eq "gbsref") or ($pipeline eq "tassel") or ($pipeline eq "stacks2")) {
	    $samples->{$cleansample}{enzyme} = $args->{enzyme} // $samplesheetdata{$sample}{enzyme} // "";
	    $samples->{$cleansample}{enzyme} = lc($samples->{$cleansample}{enzyme});
	    $samples->{$cleansample}{enzyme2} = $args->{enzyme2} // $samplesheetdata{$sample}{enzyme2} || $samples->{$cleansample}{enzyme} // "";
	    $samples->{$cleansample}{enzyme2} = lc($samples->{$cleansample}{enzyme2});
	    unless ($args->{nopadding}) {
		$samples->{$cleansample}{padding} = $args->{padding} // $samplesheetdata{$sample}{padding} || $padding{$samples->{$cleansample}{enzyme} . "-r1"} // "";
		$samples->{$cleansample}{padding2} = $args->{padding2} // $samplesheetdata{$sample}{padding2} || $padding{$samples->{$cleansample}{enzyme2} . "-r2"} // $samples->{$cleansample}{padding} // "";
	    }
	} 
    }

    foreach $symlink (@symlinks) {
	`$symlink`;
    }

    $args->{samplenumber} = scalar (keys %{$samples});
    print "Samples found: $args->{samplenumber}\n";
    die "No fastq files could be found\n" if ($args->{samplenumber} == 0);
    foreach $sample (keys %{$samples}) {
	$stats->{$sample}{general}{samplename} = $sample;
	$stats->{$sample}{general}{runname} = $args->{runname};
	$stats->{$sample}{general}{projectname} = $args->{projectname} if ($args->{projectname});
	$readlength = `fastqreadlength.pl $samples->{$sample}{R1}{fastq}`;
	chomp $readlength;
	$stats->{$sample}{general}{readlength} = $readlength;
	if ($args->{pe}) {
	    $readlength = `fastqreadlength.pl $samples->{$sample}{R2}{fastq}`;
	    chomp $readlength;
	    $stats->{$sample}{general}{readlengthr2} = $readlength;
	    $args->{readlengthr2} = $readlength;
	}
	$args->{readlength} = $readlength;
    }

    # read in illumina samplesheet, if available, and save interesting data, if present
    &readilluminasamplesheet($samples, $args, $stats);
    
}

# Read in illumina samplesheet, if available, and save interesting data, if present
# assumes samplesheet has been sanitized (newlines converted, etc)
sub readilluminasamplesheet {
    my ($samples, $args, $stats) = @_;

    return if (!defined($args->{illuminasamplesheet}));

    print "Reading Illumina samplesheet $args->{illuminasamplesheet}\n";
    if (! -e $args->{illuminasamplesheet}) {
	print "illumina samplesheet not found: $args->{illuminasamplesheet}\n";
	return;
    }

    # open samplesheet, skip metadata if present
    $result = `grep "\\[Data\\]" $args->{illuminasamplesheet}`;
    chomp $result;
    open IFILE, $args->{illuminasamplesheet} or die "Cannot open illuminasamplesheet $args->{illuminasamplesheet}: $!\n\
";
    if ($result ne "") {
        while ($line = <IFILE>) {
            last if ($line =~ /\[Data\]/);
        }
    }

    # process header
    $header = <IFILE>;
    chomp $header;
    @header = split(/,/, $header);
    for $i (0..$#header) {
        $columnname = $header[$i];
        $columnname =~ s/^\s+|\s+$//g; # get rid of whitespace around column name
	$header{$i} = lc $columnname;
	$headernames{lc $columnname} = 1;
    }

    # read in data into a hash
    $samplenumber = 1;
    while ($line = <IFILE>) {
        chomp $line;
        @line = split(/,/, $line);

        $sampleid = $line[$headernames{sample_id}];
	$sampleid =~ s/[^a-z0-9]/\./ig; # sanitize sample names like createsamplesheet does

        foreach $i (0..$#line) {
            $data{$sampleid}{$header{$i}} = $line[$i];
        }
        $data{$sampleid}{samplenumber} = $samplenumber;
        $samplenumber++;
    }

    # save interesting data to stats
    if (defined($headernames{rin})) {
	print "Saving RIN from illuminasamplesheet\n";
	foreach $sample (keys %{$samples}) {
	    if (!defined($data{$sample}{rin})) {
		print "no RIN found for sample $sample\n";
	    }
	    $stats->{$sample}{qc}{rin} = $data{$sample}{rin} // "undef";
	}
    }
}

# setup up scratch and output folders for very simple pipelines that don't use a samplesheet or fastq folder
sub foldersetup {
    print "Setting up folders\n";

    my ($samples, $args, $stats, $pipeline) = @_;

    push @{$args->{stageorder}}, "general";

    ### set up folders
    $date = `date -I`;
    chomp $date;
    $args->{runname} = $date unless ($args->{runname});

    my $user = `whoami`;
    chomp $user;

    $args->{scratchfolder} = $args->{scratchfolder} // "$SCRATCHGLOBAL/$user-pipelines/$pipeline-$args->{runname}";

    if (!$args->{resume}) {
	if (-e $args->{scratchfolder}) {
	    print "scratch folder $args->{scratchfolder} exists\n";
	    $|++;
	    for $i (4..5) { 
		$remaining = 5-$i;
		print "\rDELETING $args->{scratchfolder} in $remaining seconds";
		sleep 1;
	    }
	    print "\n";
	    `rm -r $args->{scratchfolder}`;
	    
	    die "Error: Cannot remove existing scratchfolder $args->{scratchfolder}\n" if (-e $args->{scratchfolder});
	}
	
	`mkdir --parents $args->{scratchfolder}`;
	die "Cannot create temporary folder $args->{scratchfolder}\n" if (! -e $args->{scratchfolder});
	$args->{scratchfolder} = abs_path($args->{scratchfolder});
	mkdir "$args->{scratchfolder}/allsamples";
	
	$args->{outputfolder} = "$args->{scratchfolder}/output";
	mkdir "$args->{outputfolder}";
	die "Cannot create output folder $args->{outputfolder}\n" if (! -e $args->{outputfolder});

	$args->{logfile} = "$args->{scratchfolder}/log";
	
    } else { # resumeing a run
	die "Attempting to resume run but scratch folder doesn't exist: $args->{scratchfolder}\n(Re-run without the --resume option)\n" unless (-e $args->{scratchfolder});
	$args->{outputfolder} = $args->{outputfolder} // "$args->{scratchfolder}/output";
	$args->{outputfolder} = abs_path($args->{outputfolder}) if ($args->{outputfolder});
	$args->{outputfolder} = "$args->{outputfolder}";
    }	

    chdir $args->{scratchfolder};

}

############################# Get singlesample stats ###################
sub getsinglesamplestats {
    my ($samples, $args, $stats) = @_;
    print "\nGetting singlesample stats\n" if ($args->{verbose});

    # Slurp up stats ###
    foreach $sample (keys %{$samples}) {
	$ifile = "$args->{scratchfolder}/singlesamples/$sample/stats.txt";
	if (open IFILE, $ifile) {
	    while ($line = <IFILE>) {
		chomp $line;
		($stage, $stat, $value) = split /\t/, $line;
		$stats->{$sample}{$stage}{$stat} = $value;
	    }
	    
	} else {
	    $logfile = "$args->{scratchfolder}/logs/$sample/log";
	    warn "Cannot open stats file $ifile: $!\n";
	    warn "Analysis of sample $sample failed, check log file for errors: $logfile\n";
	}
	$ifile = "$args->{scratchfolder}/singlesamples/$sample/methods.txt";
	if (-e $ifile) {
	    $methods = `cat $ifile`;
#	    $args->{singlesamplemethods} = $methods;
	}
    }
    $args->{methods} .= $methods if ($methods); # add last sample methods to roject methods
}

################### Compile Folder #######################
sub compilefolder {
    my ($samples, $args, $stats, $folder, $sourcefile, $destext) = @_;
    
    print "Compiling $sourcefile files into $folder\n" if ($args->{verbose});

    my $myscratchfolder = "$args->{scratchfolder}/$folder";
    mkdir $myscratchfolder;
    foreach $sample (sort keys %{$samples}) {
	my $source = "../singlesamples/$sample/$sourcefile"; # relative paths
	if (! -e $source) {
	    print "Cannot find file $source\n";
	    next;
	}
	my $dest = "../$folder/$sample$destext"; # relative paths
	$result = `ln -s $source $dest 2>&1` unless (-e $dest); # create symlinks to them
	print $result;
    }
}

################### Compile Files #######################
# this compiles files into the outputfolder
sub compilefiles {
    my ($samples, $args, $stats, $key, $folder, $destext) = @_;
    
    print "Compiling $key files into $folder\n" if ($args->{verbose});

#    my $myscratchfolder = "$args->{scratchfolder}/$folder";
#    mkdir $myscratchfolder;
    $folder = "$args->{outputfolder}/$folder";
    mkdir $folder unless (-e $folder);
    foreach $sample (sort keys %{$samples}) {
	my $source = $stats->{$sample}{files}{$key} // "";
	if (! -e $source) {
	    print "Cannot find file $source\n";
	    next;
	}
	$source =~ s|$args->{scratchfolder}|../..|;
#	my $dest = "../$folder/$sample$destext"; # relative paths
	my $dest = "$folder/$sample$destext"; # relative paths
	if (! -e $dest) {
	    my $result = `ln -s $source $dest 2>&1`; # create symlinks to them
	    print $result;
	}
    }
}

################### fastqcplotrmd #######################
sub fastqcplotrmd {
    print "Generating FastQC plots\n";

    my ($samples, $args, $stats, $mode) = @_;

    if ($args->{nofastqc}) {
	print "FastQC not run, skipping plots\n";
	return;
    }

    # move FastQC htmls to fastqc folder
    compilefiles($samples, $args, $stats, "fastqcR1html", "fastqc", "_R1.html");
    compilefiles($samples, $args, $stats, "fastqcR2html", "fastqc", "_R2.html") if ($args->{pe});

    ### fastqqualityplot ###
    print "Running fastqcplot.pl\n" if ($args->{verbose});
    open OFILE, ">fastqcplot-filelist.txt" or die "cannot open fastqcplot-filelist.txt: $!\n";
    foreach $sample (sort keys %{$samples}) {
	if (-e "$args->{scratchfolder}/singlesamples/$sample/fastqc/fastqc_R1_fastqc/fastqc_data.txt") {
	    print OFILE "$args->{scratchfolder}/singlesamples/$sample/fastqc/fastqc_R1_fastqc/fastqc_data.txt\t$sample\tR1\n";
	}
	if (-e "$args->{scratchfolder}/singlesamples/$sample/fastqc/fastqc_R2_fastqc/fastqc_data.txt") {
	    print OFILE "$args->{scratchfolder}/singlesamples/$sample/fastqc/fastqc_R2_fastqc/fastqc_data.txt\t$sample\tR2\n";
	}
    }
    close OFILE;

    $verbose = "";
    $verbose = "-v" if ($args->{verbose});
    print "fastqcplot.pl $verbose -l fastqcplot-filelist.txt\n" if ($args->{verbose});
    $result = `fastqcplot.pl $verbose -l fastqcplot-filelist.txt`;
    &getstats($args, $stats, $result, "fastqc");

    `mv fastqqualityplotr1.* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = 'fastqqualityplotr1.rmd'}\n```\n);
    if (-e "fastqqualityplotr2.rmd") {
	`mv fastqqualityplotr2.* $reportfolder`;
	$args->{reporttext} .= qq(```{r test-main, child = 'fastqqualityplotr2.rmd'}\n```\n);
    }
    $args->{reporttext} .= qq(<details ontoggle="window.dispatchEvent(new Event('resize'));"><summary>More QC plots</summary><div style=\"padding:0px 20px 0px 20px;\">\n) if (defined($mode));
    `mv fastqadapterplot.* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = 'fastqadapterplot.rmd'}\n```\n);
    `mv fastqduplicationplot.* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = 'fastqduplicationplot.rmd'}\n```\n);
    $args->{reporttext} .="</div></details>\n" if (defined($mode));

    ### Create filtered FastQC folder ###
    if ($args->{qualitycontrol}) {
	
	# move FastQC htmls to fastqc folder
	compilefiles($samples, $args, $stats, "fastqcR1html", "fastqc-trim", "_R1.html");
	compilefiles($samples, $args, $stats, "fastqcR2html", "fastqc-trim", "_R2.html") if ($args->{pe});
	
	### Filtered fastqqualityplot ###
	print "Running filtered fastqcplot.pl\n" if ($args->{verbose});
	open OFILE, ">filtered_fastqcplot-filelist.txt" or die "cannot open filtered_fastqcplot-filelist.txt: $!\n";
	foreach $sample (sort keys %{$samples}) {
	    if (-e "$args->{scratchfolder}/singlesamples/$sample/fastqc-trim/fastqc_R1_fastqc/fastqc_data.txt") {
		print OFILE "$args->{scratchfolder}/singlesamples/$sample/fastqc-trim/fastqc_R1_fastqc/fastqc_data.txt\t$sample\tR1\n";
	    }
	    if (-e "$args->{scratchfolder}/singlesamples/$sample/fastqc-trim/fastqc_R2_fastqc/fastqc_data.txt") {
		print OFILE "$args->{scratchfolder}/singlesamples/$sample/fastqc-trim/fastqc_R2_fastqc/fastqc_data.txt\t$sample\tR2\n";
	    }
	}
	close OFILE;
	
	$verbose = "";
	$verbose = "-v" if ($args->{verbose});
	$result = `fastqcplot.pl $verbose -l filtered_fastqcplot-filelist.txt -f`;
	&getstats($args, $stats, $result, "fastqc-trim");
	
	`mv filteredfastqqualityplotr1.* $reportfolder`;
	$args->{reporttext} .= qq(```{r test-main, child = 'filteredfastqqualityplotr1.rmd'}\n```\n);
	if (-e "filteredfastqqualityplotr2.rmd") {
	    `mv filteredfastqqualityplotr2.* $reportfolder`;
	    $args->{reporttext} .= qq(```{r test-main, child = 'filteredfastqqualityplotr2.rmd'}\n```\n);
	}
	$args->{reporttext} .= qq(<details ontoggle="window.dispatchEvent(new Event('resize'));"><summary>More QC plots</summary><div style=\"padding:0px 20px 0px 20px;\">\n) if (defined($mode));
	`mv filteredfastqadapterplot.* $reportfolder`;
	$args->{reporttext} .= qq(```{r test-main, child = 'filteredfastqadapterplot.rmd'}\n```\n);
	`mv filteredfastqduplicationplot.* $reportfolder`;
	$args->{reporttext} .= qq(```{r test-main, child = 'filteredfastqduplicationplot.rmd'}\n```\n);	
	$args->{reporttext} .="</div></details>\n" if (defined($mode));
    }
    
}

############################## Expressiontable Plot #######################
sub expressiontableplotrmd {
    my ($samples, $args, $stats, $expressiontable, $name) = @_;

    print "Generating expression table plots\n"; # if ($args->{verbose});

    $type = ($name eq "subread") ? "--featuretype gene" : "";
    $result = `expressiontableplotrmd.pl --expressionfile $expressiontable --samplesheet $args->{samplesheet} $type`;
    &getstats($args, $stats, $result, $name);

    # add to report
    `mv expressedplot.* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = 'expressedplot.rmd'}\n```\n);
    `mv distributionplot.* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = 'distributionplot.rmd'}\n```\n);
    if (-e "pcaplot.rmd") {
	`mv pcaplot.* $reportfolder`;
	$args->{reporttext} .= qq(```{r test-main, child = 'pcaplot.rmd'}\n```\n);
    }

    return;

# old stuff
    &h2("$expressiontable plots");
    &line("The dendrogram, mdsplot, and interactive PCA plot are all generated from the top 500 most variable genes. The heatmap is generated from the top 50 most variable genes.");
    &image("${name}_expressedplot.png");
    &image("${name}_expressiondistributionplot.png");
    &image("${name}_dendrogramplot.png");
    &image("${name}_mdsplot.png");
    &image("${name}_heatmapplot.png");
    &space;
    &line("|pcaicon| `Interactive PCA plot <Resources/${name}_emperor/emperor/index.html>`_"); 
    &line(".. |pcaicon| image:: Resources/pcaicon.png"); 
    &space;
}

############################## Metrics #######################
sub metrics {
    print "\nRunning Metrics\n";

    my ($samples, $args, $stats, $metricscolumns) = @_;
    my @columns = @{$metricscolumns};

    my $ofile = "$args->{outputfolder}/metrics.txt";
    open OFILE, ">$ofile" or print "Cannot open file $ofile for writing: $!\n";
    foreach $column (@columns) {
        print OFILE "$column\t";
        print "$column\t";
    }
    print "\n";
    print OFILE "\n";

    foreach $sample (sort keys %{$stats}) {
        foreach $column (@columns) {
            if (defined ($stats->{$sample}{$column})) {
                print $stats->{$sample}{$column} . "\t";
                print OFILE $stats->{$sample}{$column} . "\t";
            } else {
                print "undef\t";
                print OFILE "undef\t";
            }
        }
        print "\n";
        print OFILE "\n";
    }

    # calcluate mean values
    print "$args->{runname}    \tMean    \t";
    print OFILE "$args->{runname}    \tMean    \t";
    foreach $column (@columns[2..$#columns]) {
        $sum = 0;
        $count = 0;
        foreach $sample (sort keys %{$stats}) {
            if (defined ($stats->{$sample}{$column}) and looks_like_number($stats->{$sample}{$column})) {
                $sum += $stats->{$sample}{$column};
                $count++;
            }
        }
        if ($count > 0) {
            print round10($sum / $count);
            print "\t";
            print OFILE round10($sum / $count);
            print OFILE "\t";
        } else {
            print "undef\t";
            print OFILE "undef\t";
        }
    }
    print "\n";
    print OFILE"\n";

}

############################## allmetrics #######################
sub allmetrics {
    print "\nRunning All Metrics\n";

    my ($samples, $args, $stats) = @_;

    # open output file
    my $ofile = "$args->{outputfolder}/metrics.txt";
    open OFILE, ">$ofile" or print "Cannot open file $ofile for writing: $!\n";
    my $ofile2 = "$args->{outputfolder}/metrics.csv";
    open OFILE2, ">$ofile2" or print "Cannot open file $ofile2 for writing: $!\n";

    # get list of all stages and all stats of each stage
    foreach $sample (keys %{$samples}) {
	foreach $stage (keys %{$stats->{$sample}}) {
	    foreach $stat (keys %{$stats->{$sample}{$stage}}) {
		$stages{$stage}{$stat}++;
		$stagelist{$stage} = 1;
	    }
	}
    }
    
    # determine order for printing stages
    # use order recorded in $args->{stageorder}
    foreach $stage (@{$args->{stageorder}}) {
	if (defined($stagelist{$stage})) {
	    push @stageorder, $stage;
	    delete $stagelist{$stage};
	}
    }
    # add any additional stages on to the end
    foreach $stage (sort keys %stagelist) {
	next if ($stage eq "temp");
	next if ($stage eq "files");
	push @stageorder, $stage;
    }
    
    # print header
    $line = "";
    foreach $stage (@stageorder) {
	foreach $stat (sort keys %{$stages{$stage}}) {
	    $line .= "$stage-$stat\t";
	}
    }
    chop $line; # remove trailing \t
#    print "$line\n";
    print OFILE "$line\n";
    $line =~ s/\t/,/g;
    print OFILE2 "$line\n";

    # print data
    foreach $sample (sort keys %{$stats}) {
#	foreach $stage (sort keys %stages) {
	$line = "";
	foreach $stage (@stageorder) {
	    foreach $stat (sort keys %{$stages{$stage}}) {

		if (defined ($stats->{$sample}{$stage}{$stat})) {
		    chomp $stats->{$sample}{$stage}{$stat}; # need this chomp, don't know why 
		    $line .= $stats->{$sample}{$stage}{$stat} . "\t";
		} else {
		    $line .= "undef\t";
		}
	    }
	}
	chop $line; # remove trailing \t
#        print "$line\n";
        print OFILE "$line\n";
	$line =~ s/\t/,/g;
        print OFILE2 "$line\n";
    }

    # calcluate mean values
    print "Mean\tMetric\n";
    foreach $stage (@stageorder) {
#    foreach $stage (sort keys %stages) {
	foreach $stat (sort keys %{$stages{$stage}}) {

	    $sum = 0;
	    $count = 0;
	    foreach $sample (sort keys %{$stats}) {
		if (defined ($stats->{$sample}{$stage}{$stat}) and looks_like_number($stats->{$sample}{$stage}{$stat})) {
		    $sum += $stats->{$sample}{$stage}{$stat};
		    $count++;
		}
	    }
	    if (($stage eq "general") and ($stat eq "runname")) {
#		print "$args->{runname}\t";
		print OFILE "$args->{runname}\t";
		print OFILE2 "$args->{runname},";
	    } elsif (($stage eq "general") and ($stat eq "samplename")) {
#		print "Mean\t";
		print OFILE "Mean\t";
		print OFILE2 "Mean,";
	    } elsif ($count > 0) {
		$value = round100($sum / $count);
		print "$value\t$stage\t$stat\n";
		print OFILE "$value\t";
		print OFILE2 "$value,";
		$args->{mean}{"$stage-$stat"} = $value;
	    } else {
		print "undef\t$stage\t$stat\n";
		print OFILE "undef\t";
		print OFILE2 "undef,";
		$args->{mean}{"$stage-$stat"} = "undef";
	    }
	}
    }
    print "\n";
    print OFILE "\n";
    print OFILE2 "\n";
    close OFILE;
    close OFILE2;

    print "Scratch folder: $args->{scratchfolder}\n";
    print "Output folder: $args->{outputfolder}\n";

}

# make sure columns are tab delimited - convert multiple spaces and tabs to a single tab, allow the last column to contain spaces
sub fixsamplesheet {
    my ($samplesheet) = @_;
 
    # convert mac and dos newlines to unix newlines
    `perl -i -pe 's/\015/\012/g' $samplesheet`;
    `perl -i -pe 's/\015\012/\012/g' $samplesheet`;
   
    open IFILE, "$samplesheet" or die "Cannot open samplesheet $samplesheet: $!\n";
    open OFILE, ">samplesheet.tmp" or die "Cannot open temporary file samplesheet.tmp: $!\n";
    $header = <IFILE>;
    $header =~ s/^\s+|\s+$//g; # remove leading and trailing whitespace
    @header = split ' ', $header;
    $columns = $#header + 1;
    print OFILE join("\t", @header);
    print OFILE "\n";
    while ($line = <IFILE>) {
	$line =~ s/^\s+|\s+$//g; # remove leading and trailing whitespace
	next if ($line eq ""); # skip blank lines
	$line =~ s/ +\t/\t/g; # collapse spaces and tab into a tab
	$line =~ s/\t +/\t/g; # collapse tab and spaces into a tab
	@line = split /[ +|\t]/, $line, $columns;
	print OFILE join("\t", @line);
	print OFILE "\n";
    }
    $result = `mv samplesheet.tmp $samplesheet`;
    print $result;
    close OFILE;
    close IFILE;

}

############################ Report Header #############################
sub reportheader {
    my ($samples, $args, $stats, $title, $info, $qiime2module) = @_;

    ### calculate some things
    $meanreads = $args->{mean}{"subsample-rawreads"};
    $totalreads = int($meanreads * $args->{samplenumber});
    $meanreads = commify(int($meanreads));
    $totalreads = commify($totalreads);
    if ($ENV{LATIN_NAME}) {
	$reference = $ENV{COMMON_NAME};
    }
    if ($ENV{COMMON_NAME}) {
	if ($reference) {
	    $reference .= " ($ENV{LATIN_NAME})";
	} else {
	    $reference = "$ENV{LATIN_NAME}";
	}
    }
    if ($ENV{BUILD_NAME}) {
	$reference .= " genome assembly \"$ENV{BUILD_NAME}\"";
    }
    
    if ($args->{pe}) {
        $readname = "read-pairs";
    } else {
        $readname = "reads";
    }
    
    ($gpversion) = fileparse($ENV{GOPHER_PIPELINES});
    $modulelist = $ENV{LOADEDMODULES};
    @modulelist = split /:/, $modulelist;
    push @modulelist, $qiime2module if ($qiime2module);
    $modulelist = join("<br>", sort @modulelist);

    `cp $args->{outputfolder}/metrics.txt $reportfolder`;
    $metricstable = qq(```{r metrics, results = 'asis'}
library(knitr)
datat <- read.table('metrics.txt', comment.char="", header=TRUE)
row.names(datat) <- datat\$general.samplename
datat\$general.samplename <- NULL
kable(datat)
```
);

    $title = "Analysis Report" unless (defined($title));

    ### header
    open $reportFH, ">$reportfolder/report.rmd" or die "Cannot open file $reportfolder/report.rmd for writing: $!\n";

    print $reportFH qq(---
title: $title
output: html_document
---
);

    $numsamples = keys %{$samples};
    $maxwidth = $numsamples * 40;
    $maxwidth = 800 if ($maxwidth < 800);
    $maxwidth = 3000 if ($maxwidth > 3000);
    print $reportFH qq(<style>
                       .main-container {
                         width: 95%;
                           max-width: ${maxwidth}px;
  }
</style>

```{r setup, include=FALSE}
knitr::opts_chunk\$set(echo=FALSE, message=FALSE, error=TRUE, out.width="100%", out.height=300)
htmltools::tagList(rmarkdown::html_dependency_font_awesome())
```
);

    ### Report details
    &fieldlistrmd("Project name", $args->{projectname}) if ($args->{projectname});
    &fieldlistrmd("Run name", $args->{runname}) if ($args->{runname});

    if ($args->{pe}) {
	if ($args->{readlength} == $args->{readlengthr2}) {
	    $readtype = "Paired-end $args->{readlength}bp";
	} else {
	    $readtype = "Paired-end $args->{readlength}bp-$args->{readlengthr2}bp";
	}
    } else {
	$readtype = "Single-end $args->{readlength}bp";
    }
    &fieldlistrmd("Read type", $readtype);
    &fieldlistrmd("Samples", $args->{samplenumber});
    &fieldlistrmd("Total $readname", $totalreads);
    &fieldlistrmd("Mean $readname per sample", $meanreads);
    &fieldlistrmd("Reference", $reference) if ($reference);
    &fieldlistrmd("Report generated", $args->{startdate});
    &spacermd;

    # pipeline specific stuff
    if ($info) {
	foreach $key (keys %{$info}) {
	    &fieldlistrmd("$key", $info->{$key});
	}
	&spacermd;
    }

    print $reportFH "<details><summary>More details</summary><div style=\"padding:0px 20px 0px 20px;\">\n";

    &fieldlistrmd("Fastq folder", $args->{fastqfolder});
    $program = $0;
    $program =~ s/-pipeline\.pl /-pipeline/;
    &fieldlistrmd("Commandline", "$program $args->{options}");
    &fieldlistrmd("Gopher-pipelines version", "$gpversion");

    print $reportFH "<details><summary>Program versions</summary><div style=\"padding:0px 20px 0px 20px;\">\n";
    print $reportFH "$modulelist</div></details>\n";

    print $reportFH "<details><summary>Metric table</summary><div style=\"padding:0px 20px 0px 20px;\">\n";
    print $reportFH "$metricstable\n</div></details>\n";

    print $reportFH "</div></details>\n";
    &spacermd;
    print $reportFH "-------------------\n";
    &spacermd;

    ### sort menu
    if ($args->{samplenumber} > 6) {
        # generate list of samples ordered alphabetically
        $azorder = "";
        foreach $sample (sort {$a cmp $b} keys %{$samples}) {
            $azorder .= "\"$sample\",";
        }
        chop $azorder; # remove last ,

        $ifthen = "if (order == \"Alphabetical\") {
update = { 'xaxis.categoryarray':[$azorder]};
";
        $menu = "Order samples by <select name=\"plotsort\" onchange=\"resort(this.value)\">
  <option value=\"Alphabetical\">Alphabetical</option>
";
        foreach $category (sort {$a cmp $b} keys %{$args->{order}}) {
            $ifthen .= qq(} else if (order == "$category") {
update = {'xaxis.categoryarray':[$args->{order}{$category}]};
);
            $menu .= "  <option value=\"$category\">$category</option>\n";
        }
        $ifthen .= "}";
        $menu .= "</select>";

        print $reportFH qq(
<script>

function resort(order) {

var update;
$ifthen

plots = document.querySelectorAll("div.plotly");
var string;
for (var i = 0; i < plots.length; i++)       {
  if (plots[i].id) {
    string = string + "<br> " + plots[i].id
    Plotly.relayout(plots[i], update);
  }
}
//  document.getElementById("demo").innerHTML = "Plots ordered by " + order;
}

</script>

$menu

<!--
<div id="demo">
No button has been clicked.
</div>
-->

);
    }

}

############################################################################
################################ Helper subs ###############################
############################################################################

### die gracefully ###
sub diegracefully {
# catch interrupt signals
    $SIG{'INT'} = sub { &runtime; print "WARNING: program exited prematurely, results are incomplete\n"; exit; };
    $SIG{'TERM'} = sub { &runtime; print "WARNING: program exited prematurely, results are incomplete\n"; exit; };
    $SIG{'KILL'} = sub { &runtime; print "WARNING: program exited prematurely, results are incomplete\n"; exit; };
}

### Read in extra options file ###
sub readinextraoptions {
    my ($args) = @_;

    if ($args->{extraoptionsfile}) {
        open $fh, $args->{extraoptionsfile} or die "Cannot open extra command file $args->{extraoptionsfile}: $!\n";
        while ($line = <$fh>) {
            chomp $line;
            next if ($line =~ /^#/);
            next if ($line eq "");
            ($command, $options) = split /\s+/, $line, 2;
            $command = lc($command);
            print "Using extra options for command $command: $options\n";
            $args->{extraoptions}{$command} = $options;
        }
    }
}

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

sub printlog {
    my ($text, $logfile) = @_;
    open LOG, ">>$logfile" or die "Cannot open log file $logfile: $!\n";
    print LOG "# $text\n";
    print "$text\n";
}

# run a system command, pipe stderr to stdout, append stdout to a log file (if defined), and return stdout as one big string
sub run {
    my ($command, $logfile, $output) = @_;

    my ($result, $line);

#    my $redirect = "2>\%1";
#    if ($output) {
#	if ($output eq "stderr") {
#	    $redirect = 

    $loglines = 0;
    if (defined($logfile)) {
	open LOG, ">>$logfile" or die "Cannot open log file $logfile: $!\n";
	print LOG "\n#################################################################\n";
	print LOG "COMMAND: $command\n";
	open COMMAND, "$command 2>\&1 |" or warn "Cannot execute $command\n$!\n";
	$result = "";
	while ($line = <COMMAND>) {
	    $result .= $line;
	    $loglines++;
	    print LOG "Output truncated at 1000 lines\n" if ($loglines eq 1000);
	    next if ($loglines >= 1000);
	    print LOG $line;
	}
	close COMMAND;
    } else {
	print "\n#################################################################\n";
	print "COMMAND: $command\n";
	$result = `$command 2>\&1`;
	print $result;
    }
    return $result;
}

# print how long we've been running
sub runtime {
    state $firsttime = 1;
    state $starttime = time();
    state $checkpointtime = $starttime;
    $now = time();
    my $runtime = $now - $starttime;
    my $checktime = $now - $checkpointtime;
    if ($firsttime) {
	$checkpointtime = $now;
	$firsttime = 0;
	return;
    }
    print "Checkpoint: " . &formattime($checktime) . "\tTotal time: " . &formattime($runtime) . "\n";
    $checkpointtime = $now;
}

# print out checkpoint time info
sub formattime {
    my ($time) = @_;
    if ($time < 60) {
        return "$time seconds";
    } elsif ($time < 7200) { # 2 hours
        my $minutes = &round10($time / 60);
        return "$minutes minutes";
    } else {
	$hours = &round10($time / 3600);
        return "$hours hours";
    }
}

# determine if a file has a .gz extension
sub compressed {
    if ($_[0] =~ /\.gz$/) {
	return 1;
    } else {
	return 0
    }
}

# function to run when an interrupt signal has been received
sub INT_handler {

    # print out a report
    &runtime;

    print "WARNING: program exited prematurely, results are incomplete\n";

    exit;
}

# return the memory available per sample on this node in GB
sub nodememory {

    # determine memory allocated to PBS job
    if ($ENV{"PBS_JOBID"}) {
	$result = `qstat -f $ENV{"PBS_JOBID"} | grep 'Resource_List.mem'`;
	chomp $result;
	$result =~ /= (\d+)(\w+)/;
	$size = $1;
	$unit = lc($2);
	if ($unit eq "gb") {
	    # do nothing
	} elsif ($unit eq "mb") {
	    $size = $size / 1024;
	} else {
	    print STDERR "Cannot determine proper memory per sample, using default of 10gb\n";
	    $size = 10;
	}
	return $size;
    } else {
	# determine memory on local machine (UMGC1/2 ?)
	my $info = `head -n 1 /proc/meminfo | head -n 1`;
	my ($junk, $mem_info) = split /\s+/, $info;
	chomp $mem_info;
	$mem_info = $mem_info / 1024; # turn into mb
	$mem_info = int($mem_info / 1024 * 10 + .5) / 10; # turn into gb
	return $mem_info;
    }

}

# return the memory available on this node in GB
#sub nodememory {

#    my $info = `head -n 1 /proc/meminfo | head -n 1`;
#    my ($junk, $mem_info) = split /\s+/, $info;
#    chomp $mem_info;
#    $mem_info = $mem_info / 1024; # turn into mb
#    $mem_info = int($mem_info / 1024 * 10 + .5) / 10; # turn into gb
#    return $mem_info;

#}

# print numbers with commas
sub commify {
    my $text = reverse $_[0];
    $text =~ s/(\d\d\d)(?=\d)(?!\d*\.)/$1,/g;
    return scalar reverse $text
}

### Parse out stats from stdout
sub getstats {
    my ($args, $stats, $result, $stage) = @_;
    @results = split /\n/, $result;
    $statzone = 0;
    foreach $line (@results) {
        $statzone = 0 if ($line =~ /^STATS End/);
        if ($statzone) {
            chomp $line;
            ($sample, $key, $stat) = split /\t/, $line;
            $stats->{$sample}{$stage}{$key} = $stat;
        }
        $statzone = 1 if ($line =~ /^STATS Start/);
	if ($line =~ /^ORDER/) {
	    chomp $line;
	    $junk = ""; # define junk
	    ($junk, $text, $order) = split /\t/, $line;
	    $args->{order}{$text} = $order;
#	    print "$text: $order\n"; # DEBUG
	}
    }
}

### check that required software is present
sub requiredprograms {
    my @programs = @_;

    my $quit = 0;
    foreach $program (@programs) {
        # check if shell variables are defined (good for checking for java software)
#	print "Checking for $program\n";
        if ($program =~ /^\$/) {
            $result = `echo $program`;
            chomp $result;
            if ($result eq "") {
                print "Shell variable $program must be defined, please load the neccessary module or install the software\n";
                $quit = 1;
            }
	} else {
	    $result = `which $program 2>&1`;
#	    print "$result\n";
	    if ($result =~ "no $program in") {
		print "Required program \"$program\" not found, please load the neccessary module or install the software\n";
		$quit = 1;
	    }
	}
    }
    exit if ($quit);
}

### R markdown report functions
sub titlermd {
    $text = shift @_;
    print $reportFH "# $text\n";
}

sub imagermd {
    $file = shift @_;
    if (! -e $file) {
	print "Image not found: $file\n";
	return;
    }
    `cp $file $reportfolder`;
    print $reportFH "![]($file)\n";
}

sub fieldlistrmd {
    $text1 = $_[0] // "undefined";
    $text2 = $_[1] // "undefined";
    print $reportFH "<b>$text1:</b> $text2<br>\n";
}

sub spacermd {
    print $reportFH "\n";
}

############ Debugging helper subs ##########
sub statprint {
    my ($samples, $args, $stats) = @_;
    print "DEBUG STATS:\n";
    foreach $sample (sort keys %{$stats}) {
	print "$sample";
	foreach $column (sort keys %{$stats->{$sample}}) {
	    print "\t$column";
	}
	print "\n";
    }
    print "\n";
}


# end of Pipline Module
1;
