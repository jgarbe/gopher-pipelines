#!/usr/bin/perl -w

######################################################
# shotgun-pipeline.pl
# John Garbe
# March 2019
#
#######################################################

=head1 DESCRIPTION

qiime2-pipeline.pl - Analyze a set of 16s or ITS microbiome fastq files

=head1 SYNOPSIS

qiime2-pipeline --fastqfolder folder --variableregion V4

=head1 OPTIONS

Analyze a set of microbiome marker gene fastq files using Qiime2:

Options:

Advanced options:
 --refdb database : Path to shogun database
 --flag samplelist : a comma-delimited list of sample names to flag in the report (not tested)

Standard gopher-pipeline options
 --fastqfolder folder : A folder containing fastq files to process
 --subsample integer : Subsample the specified number of reads from each sample. 0 = no subsampling (default = 0)
 --samplesheet file : A samplesheet
 --runname string : Name of the sequencing run
 --projectname string : Name of the experiment (UMGC Project name) 
 --illuminasamplesheet file : An illumina samplesheet, from which extra sample information can be obtained 
 --name string : Name of the analysis, fastqfolder name is default name
 --nofastqc : Don't run FastQC
 --samplespernode integer : Number of samples to process simultaneously on each node (default = 1)
 --threadspersample integer : Number of threads used by each sample
 --scratchfolder folder : A temporary/scratch folder
 --outputfolder folder : A folder to deposit final results
 --extraoptionsfile file : File with extra options for trimmomatic, tophat, cuffquant, or featurecounts
 --resume : Continue where a failed/interrupted run left off
 --verbose : Print more information while running
 --help : Print usage instructions and exit

=cut

############################# Main  ###############################

use FindBin;                  # locate this script
use lib "$FindBin::RealBin";  # use the current directory
use Pipelinev4;

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;

use feature "switch";
use Scalar::Util qw(looks_like_number);
#use Term::ANSIColor;

my %samples;
my %args;
my %stats;
#my $reportFH; # analysis report filehandle

#$qiimeref = "$ENV{GOPHER_PIPELINES}/software/qiime2/";
$qiime2module = "qiime2/2019.1";

### Initialize ###
&init(\%samples, \%args, \%stats);

### Gunzip ###
$rval = &allgunzip(\%samples, \%args, \%stats);
runtime if ($rval);

### Subsample ###
&allsubsample(\%samples, \%args, \%stats);
runtime;

### FastqQC ###
&allfastqc(\%samples, \%args, \%stats) unless ($args{nofastqc});
runtime;

### Shogun ###
&myshogun(\%samples, \%args, \%stats);
runtime;

#exit; # debug

### Qiime2 ###
&myqiime2(\%samples, \%args, \%stats);
runtime;

&myqiime2plots(\%samples, \%args, \%stats);

### Metrics ###
allmetrics(\%samples, \%args, \%stats);
runtime;

### R markdown report ###
&reportrmd(\%samples, \%args, \%stats, "Shotgun Microbiome Report", "UMGC");

print "Finished\n";
runtime;

exit;


############################# Initialize #############################
sub init {
    print "\n### Initializing run ###\n";

    my ($samples, $args, $stats) = @_;

    # set defaults
    $args->{options} = join " ", @ARGV;
    $args->{threadspersample} = 8;
    if ($ENV{"PBS_NUM_PPN"}) {
	$args->{samplespernode} = int($ENV{"PBS_NUM_PPN"} / $args->{threadspersample} + 0.5);
    } else {
        $args->{samplespernode} = 1;
    }
    $args->{subsample} = 0;
    $args->{refdb} = $ENV{'SHOGUN_DB'};
    $args->{reftree} = $ENV{'SHOGUN_TREE'};
    $args->{taxonomyfile} = "$ENV{GOPHER_PIPELINES}/software/shogun-scripts/taxonomy.qza";
    GetOptions("help" => \$args->{help},
               "verbose" => \$args->{verbose},
               "resume" => \$args->{resume},
               "extraoptionsfile=s" => \$args->{extraoptionsfile},
               "outputfolder=s" => \$args->{outputfolder},
               "scratchfolder=s" => \$args->{scratchfolder},
               "threadspersample=i" => \$args->{threadspersample},
               "samplespernode=i" => \$args->{samplespernode},
	       "nofastqc" => \$args->{nofastqc},
               "illuminasamplesheet=s" => \$args->{illuminasamplesheet},
               "projectname=s" => \$args->{projectname},
               "runname=s" => \$args->{runname},
               "samplesheet=s" => \$args->{samplesheet},
               "subsample=i" => \$args->{subsample},
               "fastqfolder=s" => \$args->{fastqfolder},
	       # qiime2 options
               "flag=s" => \$args->{flag},
	       "notmpdata" => \$args->{notmpdata},
	       "refdb=s" => \$args->{refdb},
        ) or pod2usage;
    pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args->{help});
    pod2usage unless ($args->{fastqfolder});
    if ($#ARGV >= 0) {
        print "Unknown commandline parameters: @ARGV\n";
	pod2usage;
    }
    $args->{threads} = $ENV{"PBS_NUM_PPN"} // $args->{threadspersample}; # threads used by pipeline stages (not singlesample stages)
    $args->{extraoptionsfile} = abs_path($args->{extraoptionsfile}) if ($args->{extraoptionsfile});

    ### Starttime ###
    my $date = `date`;
    chomp $date;
    print "Starting at $date\n";
    $args->{startdate} = $date;
    &runtime;

    ### Handle Parameters ###
    # add parameter processing here
    
    ### Finish setup
    diegracefully();
    readinextraoptions($args);
    samplesheetandfoldersetup($samples, $args, $stats, "shotgun");

    # check flagged samples are found
    if ($args->{flag}) {
	@flagsamples = split /,/, $args->{flag};
	foreach $sample (@flagsamples) {
	    $args->{flag}{$sample} = 1;
	    $error = 0;
	    if (! defined($samples->{$sample})) {
		print "Flagged sample $sample not found\n";
		$error = 1;
	    }
	}
	die if ($error);
    }

}

# run full shogun pipeline
sub myshogunfull {
    print "\n### Running Shogun ###\n";

    my ($samples, $args, $stats) = @_;

    push @{$args->{stageorder}}, "shogun";
    my $myscratchfolder = "$args->{scratchfolder}/shogun";
    mkdir $myscratchfolder;

#    if (0) { # debug

    # run shi7 to create combined seqs input for shogun
    &run("shi7_learning.py -i $args->{fastqfolder} -o $myscratchfolder/learnt", $args->{logfile});
    &run("chmod +x $myscratchfolder/learnt/shi7_cmd.sh", $args->{logfile});
    &run("$myscratchfolder/learnt/shi7_cmd.sh", $args->{logfile});

    # run shogun
    &run("shogun pipeline --input $myscratchfolder/learnt/combined_seqs.fna --database $args->{refdb}  --output $myscratchfolder/shogun-output --threads $args->{threads}", $args->{logfile});
    $args->{taxatable} = "$myscratchfolder/shogun-output/taxatable.strain.normalized.txt";
    die "Shogun error\n" if (! -e $args->{taxatable});

    # convert taxa table to biom format
    &runqiime("biom convert -i $myscratchfolder/shogun-output/taxatable.strain.normalized.txt -o $myscratchfolder/shogun-output/taxatable.strain.normalized.biom --table-type=\"OTU table\" --to-json", $args->{logfile}, $myscratchfolder);
    $args->{biomtable} = "$myscratchfolder/shogun-output/taxatable.strain.normalized.biom";
    $args->{methods} .= "Fastq files were processed with Shi7. A biom table was generated using Shogun. ";

    # get # reads per sample from taxa table
    open IFILE, $args->{taxatable} or die "unable to open taxatable $args->{taxatable}: $!\n";
    $header = <IFILE>;
    print $header;
    chomp $header;
    @header = split /\t/, $header;
    shift @header;
    while ($line = <IFILE>) {
	print $line;
	chomp $line;
	@line = split /\t/, $line;
	shift @line;
	for $i (0..$#header) {
	    $sample = $header[$i];
	    $total{$sample} += $line[$i];
	}
    }
    close IFILE;
    $minreads = -1;
    $sumreads = 0;
    $samplecount = 0;
    foreach $sample (keys %total) {
	if (! defined($samples->{$sample})) {
	    print "unknown sample in biom table: $sample\n";
	    next;
	}
	$value = $total{$sample} // 0;
	$minreads = $value if ($minreads == -1 or $minreads > $value);
	$sumreads += $value;
	$samplecount++;
    }
    $args->{meanreads} = int($sumreads / $samplecount);
    $args->{minreads} = $minreads;
    $args->{minreads} = 10 if ($minreads < 10);
    print "meanreads: $args->{meanreads}\n"; # debug
    print "minreads: $args->{minreads}\n"; # debug

#    }
#    $args->{biomtable} = "~/microbiome/kedar_001-shotgun/table.biom";
}

# run align portion of shogun, following WoL protocol
sub myshogun {
    print "\n### Running Shogun ###\n";

    my ($samples, $args, $stats) = @_;

    push @{$args->{stageorder}}, "shogun";
    my $myscratchfolder = "$args->{scratchfolder}/shogun";
    mkdir $myscratchfolder;

    $gpout = "$myscratchfolder/gp.out";
    $progress = $args->{verbose} ? "--progress" : "";
    open GP, "| parallel $progress -j $args->{threads} > $gpout" or die "Cannot open pipe to gnu parallel\n";
#    open GP, ">$myscratchfolder/parallel.test" or die "Cannot open pipe to gnu parallel\n";

    # send commands to gnu parallel
    print "Shi7ing...\n";
    foreach $sample (keys %{$samples}) {
	$sampledir = "$myscratchfolder/$sample";
	mkdir $sampledir;
	`ln -s $samples->{$sample}{R1}{fastq} $sampledir/`;
	`ln -s $samples->{$sample}{R2}{fastq} $sampledir/`;
	print GP "shi7_learning.py -i $sampledir -o $sampledir && bash $sampledir/shi7_cmd.sh\n";
    }
    close GP;
    print "error code: $?\n" if ($?);
    $args->{methods} .= "The software package Shi7 was used with default parameters to trim adapter sequences from reads, merge read pairs, and filter out low quality reads. ";

    # filter - this doesn't seem to do anything
#    print "filter...\n";
#    &run("shogun filter --input $myscratchfolder/learnt/combined_seqs.fna --database $args->{refdb} --output $myscratchfolder/shogun-output --threads $args->{threads}", $args->{logfile});

    $gpout = "$myscratchfolder/gpalign.out";
    $progress = $args->{verbose} ? "--progress" : "";
    open GP, "| parallel $progress -j $args->{samplespernode} > $gpout" or die "Cannot open pipe to gnu parallel\n";
#    open GP, ">$myscratchfolder/parallel.test" or die "Cannot open pipe to gnu parallel\n";

    # send commands to gnu parallel
    print "aligning...\n";
    foreach $sample (keys %{$samples}) {
	$sampledir = "$myscratchfolder/$sample";
print GP qq(bowtie2 --no-unal -x /panfs/roc/groups/12/umgc/public/bin/shogun/WoLr1-db/bowtie2/WoLr1 -S $sampledir/alignment.bowtie2.sam --np 1 --mp "1,1" --rdg "0,1" --rfg "0,1" --score-min "L,0,-0.02" -f $sampledir/combined_seqs.fna --very-sensitive -k 16 -p 10 --no-hd --mm &> $sampledir/align.log\n);

#	print GP "shogun align --input $sampledir/combined_seqs.fna --database $args->{refdb} --aligner bowtie2 --output $sampledir --threads $args->{threadspersample} &> $sampledir/align.log\n";
    }
    close GP;
    print "error code: $?\n" if ($?);
    $args->{methods} .= qq(The WoL reference phylogeny for microbes Release 1 (WoLr1) was used for pylogeneic analysis. Bowtie2 was used to align reads to the WoLr1 bowtie2 index using the parameters '--np 1 --mp "1,1" --rdg "0,1" --rfg "0,1" --score-min "L,0,-0.02" --very-sensitive -k 16 -p 10 --no-hd --mm'. );

    # create bam folder, generate biom file
    print "generating biom...\n";
    $bamfolder = "$myscratchfolder/bams";
    mkdir $bamfolder;
    foreach $sample (keys %{$samples}) {
	$sampledir = "$myscratchfolder/$sample";
	if (-e "$sampledir/alignment.bowtie2.sam") {
	    `ln -s $sampledir/alignment.bowtie2.sam $bamfolder/$sample.sam`;
	} else {
	    print "ALignment failed for sample $sample\n";
	}
    }
    &run("gOTU_from_maps.py $myscratchfolder/bams $myscratchfolder/table -m bowtie2 -e .sam", $args->{logfile});
    $args->{methods} .= qq(The SAM format mapping files were converted into BIOM tables using the WoLr1 script gOTU_from_maps.py. );

    # convert taxa table to biom format
    $biomtable = "$myscratchfolder/table.biom";
    &runqiime("biom convert -i $myscratchfolder/table.uniq.tsv -o $biomtable --table-type=\"OTU table\" --to-json", $args->{logfile}, $myscratchfolder);
    $args->{biomtable} = $biomtable;
    $args->{taxatable} =" $myscratchfolder/table.uniq.tsv";
    $args->{methods} .= qq(The BIOM table based on unique hits per genome (uniq.tsv) was converted to BIOM format using the biom-format python package. );

    # filter biom file
    $biomtable = "$myscratchfolder/table.filt.biom";
    &runqiime("$ENV{GOPHER_PIPELINES}/software/shogun-scripts/filter_otus_per_sample.py $args->{biomtable} 0.0001 $biomtable", $args->{logfile}, $myscratchfolder);
    $args->{biomtable} = $biomtable;
    $args->{methods} .= qq(The WoLr1 script filter_otus_per_sample.py was used to filter out OTUs with less that 0.01% assignments per sample. );

    # get # reads per sample from taxa table
    open IFILE, $args->{taxatable} or die "unable to open taxatable $args->{taxatable}: $!\n";
    $header = <IFILE>;
#    print $header;
    chomp $header;
    @header = split /\t/, $header;
    shift @header;
    while ($line = <IFILE>) {
#	print $line;
	chomp $line;
	@line = split /\t/, $line;
	shift @line;
	for $i (0..$#header) {
	    $sample = $header[$i];
	    $total{$sample} += $line[$i];
	}
    }
    close IFILE;
    $minreads = -1;
    $sumreads = 0;
    $samplecount = 0;
    foreach $sample (keys %total) {
	if (! defined($samples->{$sample})) {
	    print "unknown sample in biom table: $sample\n";
	    next;
	}
	$value = $total{$sample} // 0;
	$minreads = $value if ($minreads == -1 or $minreads > $value);
	$sumreads += $value;
	$samplecount++;
    }
    $args->{meanreads} = int($sumreads / $samplecount);
    $args->{minreads} = $minreads;
    $args->{minreads} = 10 if ($minreads < 10);
    print "meanreads: $args->{meanreads}\n"; # debug
    print "minreads: $args->{minreads}\n"; # debug

#    }
#    $args->{biomtable} = "~/microbiome/kedar_001-shotgun/table.biom";
}

############################# Qiime2 #################################
sub myqiime2 {
    print "\n### Running Qiime2 ###\n";

    my ($samples, $args, $stats) = @_;

    push @{$args->{stageorder}}, "qiime2";
    my $myscratchfolder = "$args->{scratchfolder}/qiime2";
    mkdir $myscratchfolder;

    # generate manifest file
#    &run("samplesheet2manifest.pl --fastqfolder $args->{fastqfolder} --samplesheet $args->{samplesheet} > $manifestfile", $args->{logfile});

    $manifestfile = "$myscratchfolder/manifest.txt";
    open $OFILE, ">$manifestfile" or die "cannot open $manifestfile: $!\n";
    print $OFILE "sample-id,absolute-filepath,direction\n";
    foreach $sample (sort keys %{$samples}) {
	print $OFILE "$sample,$samples->{$sample}{R1}{fastq},forward\n";
	print $OFILE "$sample,$samples->{$sample}{R2}{fastq},reverse\n" if ($args->{pe});
    }
    close $OFILE;

    # qiime2 import
    print "Running qiime2 import\n";
    &runqiime("qiime tools import --type 'FeatureTable[Frequency]' --input-path $args->{biomtable} --output-path $myscratchfolder/table.qza", $args->{logfile}, $myscratchfolder);
    # $manifestfile --output-path $myscratchfolder/rawdata.qza # don't manifest file in import command (I think)
    $args->{methods} .= "The BIOM table was imported into Qiime2. ";


    # summarize and visualize biom results
    &runqiime("qiime feature-table summarize --i-table $myscratchfolder/table.qza --o-visualization $myscratchfolder/table.qzv --m-sample-metadata-file $args->{samplesheet}", $args->{logfile}, $myscratchfolder);
    $args->{methods} .= qq(The BIOM table was summarized with the Qiime2 'feature-table summarize' command. );

#    &runqiime("qiime feature-table tabulate-seqs --i-data $myscratchfolder/rep-seqs.qza --o-visualization $myscratchfolder/rep-seqs.qzv", $args->{logfile}, $myscratchfolder); # no rep-seqs.qza for this

    # Generate a tree for phylogenetic diversity analyses
#    print "Generating phylogenetic tree\n";
#    &runqiime("qiime phylogeny align-to-tree-mafft-fasttree --i-sequences $myscratchfolder/rep-seqs.qza --o-alignment $myscratchfolder/aligned-rep-seqs.qza --o-masked-alignment $myscratchfolder/masked-aligned-rep-seqs.qza --o-tree $myscratchfolder/unrooted-tree.qza --o-rooted-tree $myscratchfolder/rooted-tree.qza", $args->{logfile}, $myscratchfolder);
#    $args->{methods} .= "A phylogenetic tree was created using qiime phylogeny align-to-tree-mafft-fasttree. ";
#    $tree = "myscratchfolder/rooted-tree.qza";
    # use pre-built tree instead
   $tree = $args->{reftree};
    print "tree: $tree\n"; # debug

    # Alpha and beta diversity analysis
    # sampling depth: Review the information presented in the table.qzv file that was created above and choose a value that is as high as possible (so you retain more sequences per sample) while excluding as few samples as possible.
    print "Analyzing diversity\n";
    &runqiime("qiime diversity core-metrics-phylogenetic --i-phylogeny $tree --i-table $myscratchfolder/table.qza --p-sampling-depth $args->{minreads} --m-metadata-file $args->{samplesheet} --output-dir $myscratchfolder/core-metrics-results", $args->{logfile}, $myscratchfolder);
    $args->{methods} .= qq(Diversity analysis was perfomed using the Qiime2 'diversity core-metrics-phylogenetic' command with a sampling depth of $args->{minreads} (the number of reads in the sample with the fewest reads). );
    
    if (0) {
    # Test for associations between categorical metadata columns and alpha diversity data. 
    # Faith Phylogenetic Diversity (a measure of community richness)
    &runqiime("qiime diversity alpha-group-significance --i-alpha-diversity $myscratchfolder/core-metrics-results/faith_pd_vector.qza --m-metadata-file $args->{samplesheet} --o-visualization $myscratchfolder/core-metrics-results/faith-pd-group-significance.qzv", $args->{logfile}, $myscratchfolder);
    # Evenness metrics.
    &runqiime("qiime diversity alpha-group-significance --i-alpha-diversity $myscratchfolder/core-metrics-results/evenness_vector.qza --m-metadata-file $args->{samplesheet} --o-visualization $myscratchfolder/core-metrics-results/evenness-group-significance.qzv", $args->{logfile}, $myscratchfolder);

    # Analyze sample composition in the context of categorical metadata using PERMANOVA - Skipping for now
    # qiime diversity beta-group-significance --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza --m-metadata-file sample-metadata.tsv --m-metadata-column BodySite --o-visualization core-metrics-results/unweighted-unifrac-body-site-significance.qzv --p-pairwise
    }

    # Alpha rarefaction plotting
    # The value that you provide for --p-max-depth should be determined by reviewing the “Frequency per sample” information presented in the table.qzv file that was created above. In general, choosing a value that is somewhere around the median frequency seems to work well, but you may want to increase that value if the lines in the resulting rarefaction plot don’t appear to be leveling out,
    &runqiime("qiime diversity alpha-rarefaction --i-table $myscratchfolder/table.qza --i-phylogeny $tree --p-max-depth $args->{meanreads} --m-metadata-file $args->{samplesheet} --o-visualization $myscratchfolder/alpha-rarefaction.qzv", $args->{logfile}, $myscratchfolder);
    $args->{methods} .= qq(Alpha rarefaction analysis was perfomed using the Qiime2 'diversity alpha-rarefaction' command with a maximum depth of $args->{meanreads} (the mean number of reads per sample). );
    
    # Taxonomic analysis
    print "Assigning taxonomy\n";
    &runqiime("qiime metadata tabulate --m-input-file $args->{taxonomyfile} --o-visualization $myscratchfolder/taxonomy.qzv", $args->{logfile}, $myscratchfolder);

    # Visualize
    &runqiime("qiime taxa barplot --i-table $myscratchfolder/table.qza --i-taxonomy $args->{taxonomyfile} --m-metadata-file $args->{samplesheet} --o-visualization $myscratchfolder/taxa-bar-plots.qzv", $args->{logfile}, $myscratchfolder);
#    $args->{methods} .= "Taxonomy was assigned using qiime feature-classifier classify-sklearn. ";

    # Pcoa
    &runqiime("qiime emperor plot --i-pcoa $myscratchfolder/core-metrics-results/bray_curtis_pcoa_results.qza --m-metadata-file $args->{samplesheet} --o-visualization $myscratchfolder/pcoa-visualization.qzv", $args->{logfile}, $myscratchfolder);
    $args->{methods} .= qq(The Qiime2 command 'emperor plot' was used to generate visualizations of the PCoA results from the core-metrics-analysis. );

    # Export all the qiime data into plain text files
    $exportdir = "$myscratchfolder/exports";
    mkdir $exportdir;
    foreach $data ("table", "rooted-tree", "unrooted-tree", "taxonomy") {
#    foreach $data ("table") {
	&runqiime("qiime tools export --input-path $myscratchfolder/$data.qza --output-path $exportdir/", $args->{logfile}, $myscratchfolder);
    }

    &runqiime("biom convert -i $exportdir/feature-table.biom -o $exportdir/feature-table.txt --to-tsv", $args->{logfile}, $myscratchfolder);
    
#    &run("", $args->{logfile});
#    &run("", $args->{logfile});

}

######################### Qiime2 plots #########################
sub myqiime2plots {
    print "\n### Generating plots ###\n";

    my ($samples, $args, $stats) = @_;

    ### generate plots ###
    subsampleplotrmd($samples, $args, $stats);

#    fastqcplotrmd($samples, $args, $stats); # not ready for this yet

#    mydada2plot($samples, $args, $stats);

    mytaxonplot($samples, $args, $stats);

    # pca plot
    $result = `emperorplot.pl $args->{scratchfolder}/qiime2/core-metrics-results/weighted_unifrac_pcoa_results.qza $args->{samplesheet}`;
    if ($?) {
	print $result;
    } else {
	`mv emperorplot.* $reportfolder`;
	$args->{reporttext} .= qq(```{r test-main, child = 'emperorplot.rmd'}\n```\n);
    }

}

######################### Dada2 Plot #########################
sub mydada2plot {
    print "Generating Dada2 plot\n";

    my ($samples, $args, $stats) = @_;

    # generate dada2 plot
    $name = "dada2plot";
    $ofile = "$reportfolder/$name.dat";
    $rfile = "$reportfolder/$name.r";
    open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

    # generate order for report sorting
    $order = "";
    @order = sort {$stats->{$b}{dada2}{"good"} <=> $stats->{$a}{dada2}{"good"}} keys %{$stats};
    foreach $sample (@order) {
	$order .= "\"$sample\",";
    }
    chop $order; # remove last ,
    $args->{order}{"Read filtering"} = $order;

    # print out the data
    @keys = ("noadapter","filtered","noise","notmerged","chimeric","good");
    print OFILE "sample";
    foreach $key (@keys) {
	print OFILE "\t$key";
    }
    print OFILE "\n";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "$sample";
	foreach $key (@keys) {
	    print OFILE "\t$stats->{$sample}{dada2}{$key}";
	}
	print OFILE "\n";
    }
    close OFILE;

    $title = "Read Filtering";
    $mergedtxt = ($args->{pe}) ? "Not merged: reads discarded because the R1 and R2 read could not be stitched together by Dada2. " : "";
    $text = "The number of reads failing each preprocessing step is shown. No primer: reads removed by Cutadapt because they don't contain the expected PCR primer sequence at the beginning of the R1 read or the end of the R2 read. Filtered: reads removed by Dada2 due to low base quality scores. Noise: reads discarded by the Dada2 denoising algorithm. ${mergedtxt}Chimeric: reads discarded by Dada2 because they contain chimeric sequence. Good: reads passing all filter steps.";
    $updatemenus = "updatemenus = updatemenus,";
    $mergedtrace = ($args->{pe}) ? qq(add_trace(y = ~notmerged, name = 'Not merged', marker = list(color = '#d62728'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Not merged",format(datat\$notmerged,big.mark=",",scientific=FALSE))) %>%) : "";
    $updatemenus = ""; # disable the sort buttons

    open RFILE, ">$rfile" or die "Cannot open $rfile\n";
    print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

data2 <- datat[rev(order(datat\$good)),]
valueorder <- list(title="Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
              categoryarray = c(as.character(data2\$sample)))

updatemenus <- list(
  list(
    active = 0,
    font = list(size = 10),
    type = 'buttons',
    xanchor = 'right',
    buttons = list(
      
      list(
        label = "Sort by<br>Sample",
        method = "relayout",
        args = list(list(xaxis = sampleorder))),
      
      list(
        label = "Sort by<br>Value",
        method = "relayout",
        args = list(list(xaxis = valueorder)))
    )
  )
)

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~noadapter,
 name = "No primer",
 type = "bar",
marker = list(color = '#1f77b4'),
hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>No primer reads",format(datat\$noadapter,big.mark=",",scientific=FALSE))
) %>% 
add_trace(y = ~filtered, name = 'Filtered', marker = list(color = '#ff7f0e'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Filtered reads",format(datat\$filtered,big.mark=",",scientific=FALSE))) %>%
add_trace(y = ~noise, name = 'Noise', marker = list(color = '#2ca02c'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Noise reads",format(datat\$filtered,big.mark=",",scientific=FALSE))) %>%
$mergedtrace
add_trace(y = ~chimeric, name = 'Chimeric', marker = list(color = '#9467bd'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Chimeric reads",format(datat\$chimeric,big.mark=",",scientific=FALSE))) %>%
add_trace(y = ~good, name = 'Good', marker = list(color = '#8c564b'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Good reads",format(datat\$good,big.mark=",",scientific=FALSE))) %>%
 layout(xaxis = sampleorder, $updatemenus dragmode='pan', barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Reads", fixedrange = TRUE), legend = list(orientation = 'h')), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
    close RFILE;
    $args->{reporttext} .= qq(```{r test-main, child = '$rfile'}\n```\n);

}

######################### Taxon Plot #########################
sub mytaxonplot {
    print "Generating taxon plot\n";

    my ($samples, $args, $stats) = @_;

    # generate taxon table for generating taxon plot
    runqiime("qiime taxa collapse --i-table $args->{scratchfolder}/qiime2/table.qza --i-taxonomy $args->{taxonomyfile} --p-level 4 --o-collapsed-table taxonomy-4", $args->{logfile}, $args->{scratchfolder});
    runqiime("qiime tools export --input-path taxonomy-4.qza --output-path .", $args->{logfile}, $args->{scratchfolder});
    runqiime("biom convert -i feature-table.biom -o taxonplot.tmp --to-tsv", $args->{logfile}, $args->{scratchfolder});

    $name = "taxonplot";
    $ofile = "$reportfolder/$name.dat";
    $rfile = "$reportfolder/$name.rmd";
    open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

    # read in the data
    $ifile = "taxonplot.tmp";
#    $ifile = $args->{taxatable}; #"$args->{scratchfolder}/shogun/taxatable.strain.normalized.txt";
    open IFILE, $ifile or die "cannot open $ifile: $!\n";
    my $junk = <IFILE>; # Constructed from biom file
    $header = <IFILE>;
    chomp $header;
    @header = split /\t/, $header;
    $header[0] =~ s/#//;
    @taxons = ();
    while ($line = <IFILE>) {
	chomp $line;
	@line = split /\t/, $line;
	$taxon = "taxon" . ($#taxons+1);
	$taxonname = $line[0];
	push @taxons, "$taxon";
	$taxonname{$taxon} = $taxonname;
	for $i (1..$#line) {
	    $data{$header[$i]}{$taxon} = $line[$i];
	    $sums{$header[$i]} += $line[$i];
	}
    }
    
    # generate order for report sorting
    $order = "";
    @order = sort {$data{$b}{$taxons[0]} <=> $data{$a}{$taxons[0]}} keys %data;
    foreach $sample (@order) {
	$order .= "\"$sample\",";
    }
    chop $order; # remove last ,
    $args->{order}{"Taxonomy"} = $order;

    @taxons = reverse @taxons;

    # print out the data
    print OFILE "sample";
    foreach $taxon (@taxons) {
	print OFILE "\t$taxon";
    }
    print OFILE "\n";
    foreach $sample (sort keys %data) {
	print OFILE "$sample";
	$sums{$sample} = 1 unless ($sums{$sample}); # avoid divide by 0
	foreach $taxon (@taxons) {
	    $freq = $data{$sample}{$taxon} / $sums{$sample} * 100;
	    print OFILE "\t$freq";
	}
	print OFILE "\n";
    }
    close OFILE;

    $title = "Taxonomy";
    $text = "The taxonomic composition of each sample is shown, down to taxon level 4 (Order). The data shown here is a subset of the information that can be seen by loading the taxa-bar-plots.qvz qiime visualization file at the view.qiime2.org website (see the Data section of this report).";
    $updatemenus = "updatemenus = updatemenus,";
    $updatemenus = ""; # disable the sort buttons

$traces = "";
for $i (1..$#taxons) {
    $taxon = $taxons[$i];
    $traces .= qq( %>% add_trace(y=~$taxon, name = "$taxonname{$taxon}", text = paste0(datat\$sample, "<br>", "$taxonname{$taxon}<br>", round(datat\$$taxon,2),"%")));
}

    open RFILE, ">$rfile" or die "Cannot open $rfile\n";
    print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

data2 <- datat[rev(order(datat\$$taxons[0])),]
valueorder <- list(title="Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
              categoryarray = c(as.character(data2\$sample)))

updatemenus <- list(
  list(
    active = 0,
    font = list(size = 10),
    type = 'buttons',
    xanchor = 'right',
    buttons = list(
      
      list(
        label = "Sort by<br>Sample",
        method = "relayout",
        args = list(list(xaxis = sampleorder))),
      
      list(
        label = "Sort by<br>Value",
        method = "relayout",
        args = list(list(xaxis = valueorder)))
    )
  )
)

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~$taxons[0],
 name = "$taxonname{$taxons[0]}",
 type = "bar",
hoverinfo="text", 
text = paste0(datat\$sample, "<br>", "$taxonname{$taxons[0]}<br>", round(datat\$$taxons[0],2),"%")
) $traces %>% 
 layout(xaxis = sampleorder, showlegend = FALSE, $updatemenus dragmode='pan', barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Relative Frequency", fixedrange = TRUE), legend = list(orientation = 'h')), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
    close RFILE;
    $args->{reporttext} .= qq(```{r test-main, child = '$rfile'}\n```\n);

}



######################## Rmd Report ##########################
sub reportrmd {
    print "\n### Generating Analysis Report ###\n";

    my ($samples, $args, $stats, $title) = @_;

    ### report header
    $info{"Variable region"} = uc($args->{variableregion}) if ($args->{variableregion});
    reportheader($samples, $args, $stats, $title, \%info, $qiime2module);

    ### report body

    print $reportFH $args->{reporttext};

    ### data

    $visdir = "$args->{outputfolder}/q2-visualization";
    mkdir $visdir;
    `cp $args->{scratchfolder}/qiime2/*.qzv $visdir`;
    `cp $args->{scratchfolder}/qiime2/core-metrics-results/*.qzv $visdir`;
    @visfiles = `find $visdir -name "*.qzv" -printf "\%f\n"`;
    chomp @visfiles;

    $datdir = "$args->{outputfolder}/q2-artifacts";
    mkdir $datdir;
    `cp $args->{scratchfolder}/qiime2/*.qza $datdir`;
    `cp $args->{scratchfolder}/qiime2/core-metrics-results/*.qza $datdir`;
    @datfiles = `find $datdir -name "*.qza" -printf "\%f\n"`;
    chomp @datfiles;

    $expdir = "$args->{outputfolder}/q2-exports";
    mkdir $expdir;
    `cp $args->{scratchfolder}/qiime2/exports/* $expdir`;
    @expfiles = `find $expdir -type f -printf "\%f\n"`;
    chomp @expfiles;

#    $help = qq(<a href="#datahelp" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></h2>
#<div id="datahelp" class="collapse">
#The data folder accompanying this report contains the following folders and files:</div>
#);
    print $reportFH "\n<h2> Data </h2>\nThe data folder accompanying this report contains the following folders and files:\n";
#    $help = qq(<a href="#data1help" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></h2>
#<div id="data1help" class="collapse">
#Qiime2 visualization files (.qzv) should be viewed using the online browser at <a href="https://view.qiime2.org">https://view.qiime2.org</a>
#</div>
#);
    print $reportFH "\n<h3> q2-visualization/ Qiime2 visualization files </h3>\nQiime2 visualization files (.qzv) should be viewed using the online browser at <a href=\"https://view.qiime2.org\">https://view.qiime2.org</a><br>\n";
    foreach $file (@visfiles) {
	print $reportFH "$file<br>\n";
    }
#    $help = qq(<a href="#data2help" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></h3>
#<div id="data2help" class="collapse">
#Qiime2 artifact files (.qza) contain the data and metadata from an analysis. These files can be analyzed further in Qiime2, or you can view the contents of the files using the online browser at <a href="https://view.qiime2.org">https://view.qiime2.org</a>. The contents of the artifact files have also been exported into standard formats (see q2-exports) for use outside of Qiime2.
#</div>
#);
    print $reportFH "\n<h3> q2-artifact/ Qiime2 artifact files </h3>\nQiime2 artifact files (.qza) contain the data and metadata from an analysis. These files can be analyzed further in Qiime2, or you can view the contents of the files using the online browser at <a href=\"https://view.qiime2.org\">https://view.qiime2.org</a>. The contents of the artifact files have also been exported into standard formats (see q2-exports) for use outside of Qiime2.<br>\n";
    foreach $file (@datfiles) {
	print $reportFH "$file<br>\n";
    }
#    $help = qq(<a href="#data3help" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></h3>
#<div id="data3help" class="collapse">
#The contents of all Qiime2 artifact files have been exported into standard formats for use outside of Qiime2.
#</div>
#);
    print $reportFH "\n<h3> q2-exports/ Qiime2 exported files </h3>\nThe contents of all Qiime2 artifact files have been exported into standard formats for use outside of Qiime2.<br>\n";
    foreach $file (@expfiles) {
	print $reportFH "$file<br>\n";
    }
    
    ### methods

    print $reportFH qq(\n## Methods
$args->{methods}
);

    ### acknowledgements

    ### footer

#    close $reportFH;

    ### generate pdf and html from rmd, copy over to output folder
    &run("Rscript -e \"library('rmarkdown'); render('$reportfolder/report.rmd', output_format='html_document', output_file='$args->{outputfolder}/report.html')\"", $args->{logfile}); # generate html

}


################################ Helper subs ###############################

# run a qiime2 command in a fresh bash session
sub runqiime {
    ($command, $logfile, $tmpdir) = @_;
    
    $result = run("env -i HOME=\"\$HOME\" TMPDIR=\"$tmpdir\" bash -l -c 'module load $qiime2module; $command'", $logfile);

    print "Qiime2 Failure: $result\n" if ($?);

}
