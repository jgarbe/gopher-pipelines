#!/usr/bin/perl -w

#######################################################################
#  Copyright 2016 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 DESCRIPTION

illumina-pipeline - Illumina basicQC pipeline

=head1 SYNOPSIS

illumina-pipeline --fastqfolder folder 

=head1 OPTIONS

Analyze a set of Illumina fastq files

Options:
 --adapterfile file : A file containing adapter sequences for trimmomatic
 --librarymethod string : Library method, to list in report
 --extractionmethod string : Extraction method, to list in report
 --sampletype string : Sample type, to list in report
 --species string : Species, to list in report and include in species screening

Standard gopher-pipeline options
 --fastqfolder folder : A folder containing fastq files to process
 --subsample integer : Subsample the specified number of reads from each sample. 0 = no subsampling (default = 0)
 --samplesheet file : A samplesheet
 --runname string : Name of the sequencing run
 --projectname string : Name of the experiment (UMGC Project name) 
 --illuminasamplesheet file : An illumina samplesheet, from which extra sample information can be obtained 
 --nofastqc : Don't run FastQC
 --samplespernode integer : Number of samples to process simultaneously on each node (default = 1)
 --threadspersample integer : Number of threads used by each sample
 --scratchfolder folder : A temporary/scratch folder
 --outputfolder folder : A folder to deposit final results
 --extraoptionsfile file : File with extra options for trimmomatic, tophat, cuffquant, or featurecounts
 --resume : Continue where a failed/interrupted run left off
 --verbose : Print more information while running
 --help : Print usage instructions and exit


=cut

##################### Main ###############################

use FindBin;                  # locate this script
use lib "$FindBin::RealBin";  # use the current directory
use Pipelinev4;

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;

my %samples;
my %args;
my %stats;
#my $reportFH; # analysis report filehandle

### Initialize ###
&init(\%samples, \%args, \%stats);

### Singlesample analysis ###
&singlesamples(\%samples, \%args, \%stats);
runtime;

### All sample processing ###
&allsample(\%samples, \%args, \%stats);
runtime;

### Metrics ###
$args{stageorder} = ["general", "subsample", "fastqc", "fastqscreen"];
allmetrics(\%samples, \%args, \%stats);

### R markdown report ###
&reportrmd(\%samples, \%args, \%stats, "Illumina BasicQC Report");

print "Finished\n";
runtime;

exit;

##################### Initialize ###############################
sub init {
    print "\n### Initializing run ###\n";

    my ($samples, $args, $stats) = @_;

    # set defaults
    $args->{options} = join " ", @ARGV;
    $args->{threadspersample} = 8;
    if ($ENV{"PBS_NUM_PPN"}) {
        $args->{samplespernode} = int($ENV{"PBS_NUM_PPN"} / $args->{threadspersample} + 0.5);
    } else {
        $args->{samplespernode} = 1;
    }
    $args->{adapterfile} = "$GPRESOURCES/all_adapters.fa";
    GetOptions("help" => \$args->{help},
               "verbose" => \$args->{verbose},
               "resume" => \$args->{resume},
               "extraoptionsfile=s" => \$args->{extraoptionsfile},
               "outputfolder=s" => \$args->{outputfolder},
               "scratchfolder=s" => \$args->{scratchfolder},
               "threadspersample=i" => \$args->{threadspersample},
               "samplespernode=i" => \$args->{samplespernode},
	       "nofastqc" => \$args->{nofastqc},
               "illuminasamplesheet=s" => \$args->{illuminasamplesheet},
               "projectname=s" => \$args->{projectname},
               "runname=s" => \$args->{runname},
               "samplesheet=s" => \$args->{samplesheet},
               "subsample=i" => \$args->{subsample},
               "fastqfolder=s" => \$args->{fastqfolder},
	       # illumina options
               "qualitycontrol" => \$args->{qualitycontrol},
               "adapterfile=s" => \$args->{adapterfile},
               "librarymethod=s" => \$args->{librarymethod},
               "sampletype=s" => \$args->{sampletype},
               "species=s" => \$args->{species},
	) or pod2usage;
    pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args->{help});
    pod2usage unless ( $args->{fastqfolder} );
    if ($#ARGV >= 0) {
        print "Unknown commandline parameters: @ARGV\n";
        pod2usage;
    }
    $args->{threads} = $ENV{"PBS_NUM_PPN"} // $args->{threadspersample}; # threads used by pipeline stages (not singlesample stages)
    $args->{extraoptionsfile} = abs_path($args->{extraoptionsfile}) if ($args->{extraoptionsfile});

    ### Starttime ###
    my $date = `date`;
    chomp $date;
    print "Starting at $date\n";
    $args->{startdate} = $date;
    &runtime;

    ### Additional Parameters ###
    # add additional parameter processing here

    &runinfo($args);

    ### Finish setup
    requiredprograms(("fastqc", "\$TRIMMOMATIC"));
    diegracefully();
    readinextraoptions($args);
    samplesheetandfoldersetup($samples, $args, $stats, "illumina");

}

######################## Singlesample jobs ##########################
sub singlesamples {
    print "\n### Analyzing samples ###\n";

    my ($samples, $args, $stats) = @_;

    my $myscratchfolder = "$args->{scratchfolder}/singlesamples";
    mkdir $myscratchfolder;

   # set up nodefile
    $nodefile = "$myscratchfolder/nodelist.txt";
    $nodecount = 1;
    my $junk;
    if ($ENV{PBS_NODEFILE}) {
        $result = `sort -u $ENV{PBS_NODEFILE} > $nodefile`;
        print $result;
	$nodecount = `wc -l < $nodefile`;
	chomp $nodecount;
    }

    # pass arguments through to singlesample.pl
    my $subsample = $args->{subsample} ? "--subsample $args->{subsample}" : "";
    my $adapterfile = $args->{adapterfile} ? "--adapterfile $args->{adapterfile}" : "";
    my $extraoptions = $args->{extraoptionsfile} ? "--extraoptionsfile $args->{extraoptionsfile}" : "";
    my $qualitycontrol = $args->{qualitycontrol} ? "--qualitycontrol" : "";
    my $verbose = $args->{verbose} ? "--verbose" : "";
    my $species = $args->{species} ? "--species $args->{species}" : "";
    my $nofastqc = $args->{nofastqc} ? "--nofastqc" : "";

    # print out singlesample commandlines
    my $commandfile = "$myscratchfolder/singlesample-commands.txt";
    open OFILE, ">$commandfile" or die "Cannot open $commandfile: $!\n";
    foreach $sample (keys %{$samples}) {
        if (($args->{resume}) && (-e "$myscratchfolder/$sample/Complete")) {
            print "Skipping completed sample $sample\n";
            next;
        }
	my $r1 = "--R1 $samples->{$sample}{R1}{fastq}";
	$r2 = ($args->{pe}) ? "--R2 $samples->{$sample}{R2}{fastq}" : "";
	$log = "$args->{scratchfolder}/logs/$sample.log";
	$command = "illumina-singlesample.pl $verbose --threads $args->{threadspersample} $subsample $adapterfile $extraoptions $qualitycontrol $species $nofastqc --outputfolder $myscratchfolder/$sample $r1 $r2 &> $log && echo \"$sample complete\" || echo \"$sample failed\"\n";
        print OFILE $command;
    }
    close OFILE;

   # run jobs in parallel
    mkdir "$args->{scratchfolder}/logs";
    if ($nodecount <= 1) { # single node
        system("cat $commandfile | parallel -j $args->{samplespernode}");
    } else { # multiple node
	system("$commandfile | parallel -j $args->{samplespernode} --sshloginfile $nodefile --workdir $ENV{PWD}");
    }

    # read in stats from each sample
    getsinglesamplestats($samples, $args, $stats);

}

################### 10x barcode check ###################
sub tenxbarcodecheck {
    print "\n### Checking 10x barcodes ###\n";

    my ($samples, $args, $stats) = @_;

    my $myscratchfolder = "$args->{scratchfolder}/10xbarcodecheck";
    mkdir $myscratchfolder;
    chdir $myscratchfolder;
    $ofile = "$myscratchfolder/barcodes.txt";
    open OFILE, ">$ofile" or die "cannot open $ofile: $!\n";

    my %bcnames = ();
    my %data = ();
    foreach $type ("A", "B/C") {
	foreach $sample (keys %{$samples}) {
	    if ($type eq "A") {
		@results = `fastq-starts.pl $samples->{$sample}{R2}{fastq}`;
	    } else {
		@results = `fastq-starts.pl -i 10 $samples->{$sample}{R2}{fastq}`;
	    }
	    foreach $result (@results) {
		chomp $result;
		if ($result =~ /reads/) {
		    if ($result =~ /Other reads/) {
			($junk, $junk, $count, $pct) = split ' ', $result;
			$data{"$type-Other"}{$sample} = $pct;
			$pct =~ s/\%//;
			$sums{"$type-Other"} += $pct;
		    }
		} else {
		    $pct = "";
		    $seq = "";
		    ($seq, $count, $pct) = split ' ', $result;
		    $seq = "$type-$seq";
		    $data{$seq}{$sample} = $pct;
		    $pct =~ s/\%//;
		    $sums{$seq} += $pct;
		}
	    }
	}
    }
    
    print OFILE "Sequence";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "\t$sample";
    }
    print OFILE "\n";
    foreach $seq (sort {$sums{$b} <=> $sums{$a}} keys %sums) {
	last if ($sums{$seq} / keys %{$samples} < 2);
	print OFILE "$seq";
#	print OFILE ",$table .= "<td>" . ($bcnames{$seq} // "unknown") . "</td>";
	foreach $sample (sort keys %{$samples}) {
	    print OFILE "\t" . ($data{$seq}{$sample} // "0%");
	}
	print OFILE "\n";
    }
    close OFILE;

    ### add to report ###
#    $args->{reporttext} .= qq(
#<b>Cellranger HTO/ADT barcodes</b>: <a href="barcodes.txt">barcodes.txt</a>
#);

    `cp $ofile $reportfolder`;
    chdir $args->{scratchfolder};

    $barcodetable = qq(```{r barcode, results = 'asis'}
library(knitr)
datat <- read.table('barcodes.txt', comment.char="", header=TRUE)
row.names(datat) <- datat\$sequence
datat\$sequence <- NULL
kable(datat)
```
);

return qq(<details><summary>Barcode table</summary><div style="padding:0px 20px 0px 20px;">
$barcodetable\n</div></details>
);


}

################### All-sample jobs #######################
sub allsample {
    print "\n### Processing results ###\n";

    my ($samples, $args, $stats) = @_;

    my $scratchfolder = $args->{scratchfolder};
    my $myscratchfolder = "$scratchfolder/allsamples";
    mkdir $myscratchfolder;
    my $outputfolder = $args->{outputfolder};
    chdir $myscratchfolder;

    ### generate plots ###
    subsampleplotrmd($samples, $args, $stats);

    fastqcplotrmd($samples, $args, $stats);

    trimmomaticplot($samples, $args, $stats) if ($args->{qualitycontrol});

#    insertsizeplot($samples, $args, $stats); # todo: add this if its a paired-end file

#    fastqscreenplotrmd($samples, $args, $stats, "contamination");
    fastqscreenplotrmd($samples, $args, $stats, "species");

    ### create folders ###
    # move singlesample logs to log folder
#    compilefolder($samples, $args, $stats, "logs", "log", "-log.txt");
    # copy log files to the output folder
#    `cp -rL $args->{scratchfolder}/logs $args->{outputfolder}`;

}

######################## Rmd Report ##########################
sub reportrmd {
    print "\n### Generating Analysis Report ###\n";

    my ($samples, $args, $stats, $title) = @_;

    ### report header
    $info{"Species"} = $args->{species} if ($args->{species});
    $info{"Sample type"} = $args->{sampletype} if ($args->{sampletype});
    $info{"Library method"} = $args->{librarymethod} if ($args->{librarymethod});
    $info{"Extraction method"} = $args->{extractionmethod} if ($args->{extractionmethod});
    $info{"Instrument"} = "$args->{type} $args->{mode}" if ($args->{type});
    $info{"Note"} = "This QC report is not designed for the unique read structure of 10X single-cell datasets. Refer to Cellranger analysis reports to review 10X run quality metrics." if ($args->{fastqfolder} =~ /demultiplex-cellranger/);

    if ($args->{projectname} =~ /HTO|ADT/) {
	$barcodes = &tenxbarcodecheck($samples, $args, $stats);
	$info{"10X HTO/ADT Barcodes"} = $barcodes;
    }

    reportheader($samples, $args, $stats, $title, \%info);

    ### report body

    print $reportFH $args->{reporttext};

    ### methods

    ### acknowledgements

    ### footer

#    close $reportFH;

    ### generate pdf and html from rmd, copy over to output folder

    &run("Rscript -e \"library('rmarkdown'); render('$reportfolder/report.rmd', output_format='html_document', output_file='$args->{outputfolder}/report.html')\"", $args->{logfile}); # generate html

}


################################ Helper subs ###############################

### fastqscreenplotrmd ###
sub fastqscreenplotrmd {
    my ($samples, $args, $stats, $type) = @_;

    print "Generating fastq $type screen plot\n" if ($args->{verbose});
    open OFILE, ">fastq$type-filelist.txt" or die "cannot open fastq$type-filelist.txt: $!\n";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "$args->{scratchfolder}/singlesamples/$sample/$type.txt\t$sample\n";
    }
    close OFILE;

    $result = `fastqspeciesplotrmd.pl -f fastq$type-filelist.txt -t $type`;
    # process the stdout, grabbing some stats
    &getstats($args, $stats, $result, "fastq${type}screen");

    ### add to report ###
    `mv fastq${type}plot.* $reportfolder`;
    $args->{reporttext} .= qq(```{r test-main, child = 'fastq${type}plot.rmd'}\n```\n);

}

### runinfo ###
# Get details about from from json and samplesheet, if available
sub runinfo {
    my ($args) = @_;

    print "Getting run info\n" if ($args->{verbose});

    ### calculate some things
#/panfs/roc/umgc/illumina_analysis/180913_M04141_0246_000000000-D4M3B-analysis/demultiplex_20180917-10-59-04/demultiplex/Pragman2_Project_005    
    $json = "$args->{fastqfolder}/../../../illumina-setup.json";
    if (-e $json) {
	$type = `grep ^type $json`;
	if ($type) {
	    chomp $type;
	    ($junk, $type) = split /\t/, $type;
	}
	$mode = `grep ^RunMode $json`;
	if ($mode) {
	    chomp $mode;
	    ($junk, $mode) = split /\t/, $mode;
	}
    }
    # get info from samplesheet
    if ($args->{illuminasamplesheet} && -e $args->{illuminasamplesheet}) {
	$ss = &getsamplesheet($args->{illuminasamplesheet});
	foreach $sample (keys %{$ss}) {
	    if (defined($ss->{$sample}{sample_project}) && (lc($ss->{$sample}{sample_project}) eq lc($args->{projectname}))) {
		$species .= "$ss->{$sample}{species}," if (defined($ss->{sample}{species}));
		$sampletype .= "$ss->{$sample}{sample_type}," if (defined($ss->{sample}{sample_type}));
		$librarymethod .= "$ss->{$sample}{library_method}," if (defined($ss->{sample}{library_method}));
		$extractionmethod .= "$ss->{$sample}{extraction_method}," if (defined($ss->{sample}{extraction_method}));
	    }
	}
	$species =~ s/,$// if (defined($species));
	$sampletype =~ s/,$// if (defined($sampletype));
	$librarymethod =~ s/,$// if (defined($librarymethod));
	$extractionmethod =~ s/,$// if (defined($extractionmethod));
    }

    $args->{mode} = $mode // "";
    $args->{type} = $type // "";
    $args->{species} = $args->{species} // $species // "";
    $args->{sampletype} = $args->{sampletype} // $sampletype // "";
    $args->{librarymethod} = $args->{librarymethod} // $librarymethod // "";
    $args->{extractionmethod} = $args->{extractionmethod} // $extractionmethod // "";

}

### getsamplesheet ###
# Copied from umgc2.pm
# Given a samplesheet filename return a hash of the samplesheet data
# assumes samplesheet has been sanitized (newlines converted, etc)
sub getsamplesheet {
    my ($samplesheet) = @_;

    # open samplesheet, skip metadata if present
    $result = `grep "\\[Data\\]" $samplesheet`;
    chomp $result;
    open IFILE, $samplesheet or die "Cannot open samplesheet $samplesheet: $!\n\
";
    if ($result ne "") {
        while ($line = <IFILE>) {
            last if ($line =~ /\[Data\]/);
        }
    }

    # process header
    $header = <IFILE>;
    chomp $header;
    @header = split(/,/, $header);
    for $i (0..$#header) {
        $columnname = $header[$i];
        $columnname =~ s/^\s+|\s+$//g; # get rid of whitespace around column name
	$header{lc($columnname)} = $i;
    }

    # read in data into a hash
    $samplenumber = 1;
    while ($line = <IFILE>) {
        chomp $line;
        @line = split(/,/, $line);

        $sampleid = $line[$header{sample_id}];
        foreach $i (0..$#line) {
            $data{$sampleid}{$header[$i]} = $line[$i];
        }
        $data{$sampleid}{samplenumber} = $samplenumber;
        $samplenumber++;
    }

    # return hash of data
    return \%data;
}
