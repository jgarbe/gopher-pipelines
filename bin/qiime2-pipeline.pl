#!/usr/bin/perl -w

######################################################
# qiime2-pipeline.pl
# John Garbe
# March 2019
#
#######################################################

=head1 DESCRIPTION

qiime2-pipeline.pl - Analyze a set of 16s or ITS microbiome fastq files

=head1 SYNOPSIS

qiime2-pipeline --fastqfolder folder --variableregion V4

=head1 OPTIONS

Analyze a set of microbiome marker gene fastq files using Qiime2:

Options:
 --variableregion region : Use predefined primer sequences, trunclength, maxee, and reference database. Valid regions: V1V3, V3V4, V3V5, V4, V4V6, V5V6, 18S_V9, ITS1, ITS2 
 --emp : EMP protocol, or any other protocol where primers are not present in the reads
 --bowtie2index index : Bowtie index for (host) contamination detection
 --flag samplelist : a comma-delimited list of sample names to flag in the report

Advanced options:
 --r1adapter string : sequence to trim off the 3' end of R1 reads
 --r2adapter string : sequence to trim off the 5' end of R2 reads
 --crop integer : crop integer bases from the start of every read (neccessary for "IIS" library prep method)
 --refdb database : Valid values include greenegenes, silva128, and its. Default is greengenes.
 --referencefasta file : A fasta file containing reference sequences (default=greengenes 97_otus.fasta)
 --referencetaxonomy file : An id_to_taxonomy_fp file (default=greengenes 97_otu_taxonomy.txt), see Qiime documentation for details
 --referencealignedfasta file : a pynast_template_alignment_fp file (default=greengenes rep_set_aligned/97_otus.fasta), see Qiime documentatino for details
 --trunclength integer : dada2 trunc-len
 --maxee integer : dada2 maxee value

Standard gopher-pipeline options
 --fastqfolder folder : A folder containing fastq files to process
 --subsample integer : Subsample the specified number of reads from each sample. 0 = no subsampling (default = 0)
 --samplesheet file : A samplesheet
 --runname string : Name of the sequencing run
 --projectname string : Name of the experiment (UMGC Project name) 
 --illuminasamplesheet file : An illumina samplesheet, from which extra sample information can be obtained 
 --nofastqc : Don't run FastQC
 --samplespernode integer : Number of samples to process simultaneously on each node (default = 1)
 --threadspersample integer : Number of threads used by each sample
 --scratchfolder folder : A temporary/scratch folder
 --outputfolder folder : A folder to deposit final results
 --extraoptionsfile file : File with extra options for trimmomatic, tophat, cuffquant, or featurecounts
 --resume : Continue where a failed/interrupted run left off
 --verbose : Print more information while running
 --help : Print usage instructions and exit

=cut

############################# Main  ###############################

use FindBin;                  # locate this script
use lib "$FindBin::RealBin";  # use the current directory
use Pipelinev4;

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;

use feature "switch";
use Scalar::Util qw(looks_like_number);
#use Term::ANSIColor;

my %samples;
my %args;
my %stats;
#my $reportFH; # analysis report filehandle

$qiimeref = "$ENV{GOPHER_PIPELINES}/software/qiime2/";
$qiime2module = "qiime2/2019.1";

### Initialize ###
&init(\%samples, \%args, \%stats);

### Gunzip ###
$rval = &allgunzip(\%samples, \%args, \%stats);
runtime if ($rval);

### Subsample ###
&allsubsample(\%samples, \%args, \%stats);
runtime;

### FastqQC ###
&allfastqc(\%samples, \%args, \%stats) unless ($args{nofastqc});
runtime;

### Qiime2Trim ###
&myqiime2trim(\%samples, \%args, \%stats);
runtime;

### Qiime2 ###
&myqiime2(\%samples, \%args, \%stats);
runtime;

&myqiime2plots(\%samples, \%args, \%stats);

### Metrics ###
allmetrics(\%samples, \%args, \%stats);
runtime;

### R markdown report ###
&reportrmd(\%samples, \%args, \%stats, "Microbiome Report", "UMGC");

print "Finished\n";
runtime;

exit;


############################# Initialize #############################
sub init {
    print "\n### Initializing run ###\n";

    my ($samples, $args, $stats) = @_;

    # set defaults
    $args->{options} = join " ", @ARGV;

    $refdbs{greengenes}{referencefasta} =
	"/panfs/roc/rissdb/adhoc/greengenes/gg_13_8_otus/rep_set/97_otus.fasta";
    $refdbs{greengenes}{referencealignedfasta} =
	"/panfs/roc/rissdb/adhoc/greengenes/gg_13_8_otus/rep_set_aligned/85_otus.fasta";
    $refdbs{greengenes}{referencetaxonomy} =
	"/panfs/roc/rissdb/adhoc/greengenes/gg_13_8_otus/taxonomy/97_otu_taxonomy.txt";

    $refdbs{silva128}{referencefasta} =
	"/home/umii/public/gopher-data/SILVA_128_QIIME_release-garbe/rep_set/rep_set_16S_only/97/97_otus_16S.fasta";
    $refdbs{silva128}{referencealignedfasta} =
	"/home/umii/public/gopher-data/SILVA_128_QIIME_release-garbe/rep_set_aligned/97/97_otus_aligned.fasta";
    $refdbs{silva128}{referencetaxonomy} =
    "/home/umii/public/gopher-data/SILVA_128_QIIME_release-garbe/taxonomy/16S_only/97/consensus_taxonomy_7_levels.txt";

    $refdbs{its}{referencefasta} =
	"/home/umii/public/gopher-data/its_12_11_otus/rep_set/97_otus.fasta";
    $refdbs{its}{referencealignedfasta} =
	""; # This file isn't used in ITS analysis
    $refdbs{its}{referencetaxonomy} =
	"/home/umii/public/gopher-data/its_12_11_otus/taxonomy/97_otu_taxonomy.txt";
    
    # set defaults
    $args->{options} = join " ", @ARGV;
    $args->{threadspersample} = 8;
    if ($ENV{"PBS_NUM_PPN"}) {
	$args->{samplespernode} = int($ENV{"PBS_NUM_PPN"} / $args->{threadspersample} + 0.5);
    } else {
        $args->{samplespernode} = 1;
    }
    $args->{referencefasta} = $refdbs{greengenes}{referencefasta};
    $args->{referencealignedfasta} = $refdbs{greengenes}{referencealignedfasta};
    $args->{referencetaxonomy} = $refdbs{greengenes}{referencetaxonomy};
    $args->{subsample} = 0;
    $args->{secondsubsample} = 0;
    $args->{r1adapter} = "CTGTCTCTTATACACATCTCCGAGCCCACGAGAC"; # trans2_rc
    $args->{r2adapter} = "CTGTCTCTTATACACATCTGACGCTGCCGACGA"; # trans1_rc
    GetOptions("help" => \$args->{help},
               "verbose" => \$args->{verbose},
               "resume" => \$args->{resume},
               "extraoptionsfile=s" => \$args->{extraoptionsfile},
               "outputfolder=s" => \$args->{outputfolder},
               "scratchfolder=s" => \$args->{scratchfolder},
               "threadspersample=i" => \$args->{threadspersample},
               "samplespernode=i" => \$args->{samplespernode},
	       "nofastqc" => \$args->{nofastqc},
               "illuminasamplesheet=s" => \$args->{illuminasamplesheet},
               "projectname=s" => \$args->{projectname},
               "runname=s" => \$args->{runname},
               "samplesheet=s" => \$args->{samplesheet},
               "subsample=i" => \$args->{subsample},
               "fastqfolder=s" => \$args->{fastqfolder},
	       # qiime2 options
               "flag=s" => \$args->{flag},
               "bowtie2index=s" => \$args->{bowtie2index},
	       "secondsubsample=i" => \$args->{secondsubsample},
	       "referencefasta=s" => \$args->{referencefasta},
	       "referencealignedfasta=s" => \$args->{referencealignedfasta},
	       "referencetaxonomy=s" => \$args->{referencetaxonomy},
	       "emp" => \$args->{emp},
	       "crop=i" => \$args->{crop},
	       "variableregion=s" => \$args->{variableregion},
	       "r1adapter=s" => \$args->{r1adapter},
	       "r2adapter=s" => \$args->{r2adapter},
	       "notmpdata" => \$args->{notmpdata},
	       "refdb=s" => \$args->{refdb},
	       "trunclength=i" => \$args{trunclength},
	       "maxee=i" => \$args{maxee},
        ) or pod2usage;
    pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args->{help});
    pod2usage unless ($args->{fastqfolder});
    if ($#ARGV >= 0) {
        print "Unknown commandline parameters: @ARGV\n";
	pod2usage;
    }
    $args->{threads} = $ENV{"PBS_NUM_PPN"} // $args->{threadspersample}; # threads used by pipeline stages (not singlesample stages)
    $args->{extraoptionsfile} = abs_path($args->{extraoptionsfile}) if ($args->{extraoptionsfile});

    ### Starttime ###
    my $date = `date`;
    chomp $date;
    print "Starting at $date\n";
    $args->{startdate} = $date;
    &runtime;

    ### Handle Parameters ###
    # add parameter processing here
    $args->{bowtie2index} = bowtie2indexcheck($args->{bowtie2index}) if ($args->{bowtie2index});
#    die "--metadatafile required\n" unless($args->{metadatafile});
    
    ### Finish setup
    diegracefully();
    readinextraoptions($args);
    samplesheetandfoldersetup($samples, $args, $stats, "qiime2");

    # check flagged samples are found
    if ($args->{flag}) {
	@flagsamples = split /,/, $args->{flag};
	foreach $sample (@flagsamples) {
	    $args->{flag}{$sample} = 1;
	    $error = 0;
	    if (! defined($samples->{$sample})) {
		print "Flagged sample $sample not found\n";
		$error = 1;
	    }
	}
	die if ($error);
    }

    # Set up parameters for pre-defined variable regions - need to do this after the samplesheet is read in
    $args->{classifier} = "$qiimeref/silva-132-99-nb-classifier.qza";
    $args->{classifiertext} = "classifier trainined on Silva release 132 99% identity OTUs from full length 16s sequences";
    if ($args->{variableregion}) {
	if (lc($args->{variableregion}) eq "v1v3") {
	    $fprimer = "AGAGTTTGATCMTGGCTCAG"; # V1F
	    $rprimer = "ATTACCGCGGCTGCTGG"; # V3R
	    $trunclength = 270; # Trina tested
#	    $pmin = "440"; $pmax = 490; # was 525
	} elsif (lc($args->{variableregion}) eq "v3") {
	    $fprimer = "CCTACGGGAGGCAGCAG"; # V3F
	    $rprimer = "ATTACCGCGGCTGCTGG"; # V3R
	    $trunclength = 130;
#	    $pmin = "115"; $pmax = "200";
	} elsif (lc($args->{variableregion}) eq "v3v4") {
	    $fprimer = "CCTACGGGAGGCAGCAG"; # V3F
	    $rprimer = "GGACTACHVGGGTWTCTAAT"; # V4R
	    $trunclength = 225; # Trina tested, seems pretty stringent
#	    $pmin = "400"; # "225"; $pmax = "435"; # "275";
	} elsif (lc($args->{variableregion}) eq "v3v5") {
	    $fprimer = "CCTACGGGAGGCAGCAG"; # V3F
	    $rprimer = "CCGTCAATTCMTTTRAGT"; # V5R
	    $trunclength = 300; # not tested
#	    $pmin = "1"; $pmax = "";
	} elsif (lc($args->{variableregion}) eq "v4") {
	    $fprimer = "GTGCCAGCMGCCGCGGTAA"; # V4F
	    $rprimer = "GGACTACHVGGGTWTCTAAT"; # V4R
	    $trunclength = 167; # Trina tested
#	    $pmin = "225"; $pmax = "275";
	    $args->{classifier} = "$qiimeref/silva-132-99-515-806-nb-classifier.qza";
	    $args->{classifiertext} = "classifier trained on Silva release 132 99% identity OTUs from the 515F/806R 16s region"; 
	} elsif (lc($args->{variableregion}) eq "v4v5") {
	    $fprimer = "GTGCCAGCMGCCGCGGTAA"; # V4F
	    $rprimer = "CCGTCAATTCMTTTRAGT"; # V5R
	    $trunclength = 230; # not tested
#	    $pmin = "350"; $pmax = "400";
	} elsif (lc($args->{variableregion}) eq "v4v6") {
	    $fprimer = "GTGCCAGCMGCCGCGGTAA"; # V4F
	    $rprimer = "CGACRRCCATGCANCACCT"; # V6R
	    $trunclength = 282; # not tested
#	    $pmin = "495"; $pmax = "525";
	} elsif (lc($args->{variableregion}) eq "v5v6") {
	    $fprimer = "RGGATTAGATACCC"; # V5F
	    $rprimer = "CGACRRCCATGCANCACCT"; # V6R
	    $trunclength = 167; # not tested
#	    $pmin = "225"; $pmax = "275";
	} elsif (lc($args->{variableregion}) eq "18s_v9") {
	    $fprimer = "GTACACACCGCCCGTC"; # V9F
	    $rprimer = "TGATCCTTCTGCAGGTTCACCTAC"; # V9R
	    $trunclength = 100; # Trina tested
#	    $pmin = "1"; $pmax = "140";
	} elsif (lc($args->{variableregion}) eq "its1") {
	    $args->{refdb} = "its";
	    $fprimer = "CTTGGTCATTTAGAGGAAGTAA"; # ITS1F
	    $rprimer = "GCTGCGTTCTTCATCGATGC"; # ITS1R
	    $trunclength = 162; # Trina tested
	    $args->{maxee} = 20;
#	    $pmin = "10"; $pmax = "600";
	    $args->{classifier} = "$qiimeref/unite-ver8-99-classifier-04.02.2020.qza";
	    $args->{classifiertext} = "classifier trainined on UNITE version 8 99% identity full length ITS sequences";
	} elsif (lc($args->{variableregion}) eq "its2") {
	    $args->{refdb} = "its";
	    $fprimer = "TCGATGAAGAACGCAGCG"; # ITS1F
	    $rprimer = "TCCTCCGCTTATTGATATGC"; # ITS1R
	    $trunclength = 230; # Trina tested
	    $args->{maxee} = 10;
#	    $pmin = "10"; $pmax = "600";
	    $args->{classifier} = "$qiimeref/unite-ver8-99-classifier-04.02.2020.qza";
	    $args->{classifiertext} = "classifier trainined on UNITE version 8 99% identity full length ITS sequences";
	} else {
	    die "Unknown variable region \"$args->{variableregion}\"\n";
	}

	# unset primer seqs if this is an emp library
	if ($args->{emp}) {
	    $fprimer = "";
	    $rprimer = "";
	}
	$args->{forwardprimer} = $fprimer;
	$args->{reverseprimer} = $rprimer;	
    }
    $args->{trunclength} = $args->{trunclength} // $trunclength // 300;

    if ($args->{refdb}) {
	$args->{refdb} = lc($args->{refdb});
	if (! defined($refdbs{$args->{refdb}})) {
	    die "Unknown refdb: $args->{refdb}\n";
	}
	$args->{referencefasta} = $refdbs{$args->{refdb}}{referencefasta};
	$args->{referencealignedfasta} = $refdbs{$args->{refdb}}{referencealignedfasta};
	$args->{referencetaxonomy} = $refdbs{$args->{refdb}}{referencetaxonomy};
    }

    die "ERROR: Cannot find reference fasta file $args->{referencefasta}\n" if (! -e $args->{referencefasta});
    $args->{referencefasta} = abs_path($args->{referencefasta});
    die "ERROR: Cannot find reference aligned fasta file $args->{referencealignedfasta}\n" if (($args->{referencealignedfasta} ne "") and (! -e $args->{referencealignedfasta}) );
    $args->{referencealignedfasta} = abs_path($args->{referencealignedfasta}) unless ($args->{referencealignedfasta} eq "");
    die "ERROR: Cannot find reference taxonomy file $args->{referencetaxonomy}\n" if (! -e $args->{referencetaxonomy});
    $args->{referencetaxonomy} = abs_path($args->{referencetaxonomy});

}

############################# Qiime2 trim ###############################
sub myqiime2trim {
    print "\n### Trimming primers and adapters with cutadapt ###\n";

    my ($samples, $args, $stats) = @_;

    push @{$args->{stageorder}}, "qiime2trim";
    my $myscratchfolder = "$args->{scratchfolder}/trim";
    mkdir $myscratchfolder;

    # run gnu parallel
    $gpout = "$myscratchfolder/gp.out";
    $progress = $args->{verbose} ? "--progress" : "";
    
    # run cutadapt to remove 5' primers
    if ($args->{forwardprimer}) {
	print "Trimming 5' primers\n";
	$tmpscratch = "$myscratchfolder/cutadapt-primertrim";
	mkdir $tmpscratch;
	open GP, "| parallel $progress -j $args->{threads} > $gpout" or die "Cannot open pipe to gnu parallel\n";
#        open GP, ">parallel.test" or die "Cannot open pipe to gnu parallel\n";

	# send commands to gnu parallel
	$firstsample = 1;
	foreach $sample (keys %{$samples}) {
	    $forwardprimer = "";
	    if ($args->{forwardprimer}) {
		$forwardprimer = "-g \"^$args->{forwardprimer}\"";
	    }
	    $reverseprimer = "";
	    if ($args->{reverseprimer}) {
		$reverseprimer = "-G \"^$args->{reverseprimer}\"";
	    }
	    
	    if ($args->{pe}) { # paired-end
		$newr1 = "$tmpscratch/${sample}_R1.fastq";
		$newr2 = "$tmpscratch/${sample}_R2.fastq";
		print GP "cutadapt --report=minimal $forwardprimer $reverseprimer -e .2 --discard-untrimmed -o $newr1 -p $newr2 $samples->{$sample}{R1}{fastq} $samples->{$sample}{R2}{fastq} > $tmpscratch/$sample.log\n";
		$samples->{$sample}{R1}{fastq} = $newr1;
		$samples->{$sample}{R2}{fastq} = $newr2;
		if ($firstsample) {
		    $firstsample = 0;
		    $args->{methods} .= "Primer sequences were trimmed off the 5' ends of reads using cutadapt, reads without a primer were discarded (cutadapt $forwardprimer $reverseprimer -e .2 --discard-untrimmed). "
		}
	    } else { # single-end
		$newr1 = "$tmpscratch/${sample}_R1.fastq";
		print GP "cutadapt --report=minimal $forwardprimer -e .2 --discard-untrimmed -o $newr1 $samples->{$sample}{R1}{fastq} > $tmpscratch/$sample.log\n";
		$samples->{$sample}{R1}{fastq} = $newr1;
		if ($firstsample) {
		    $firstsample = 0;
		    $args->{methods} .= "Primer sequences were trimmed off the 5' ends of reads using cutadapt, reads without a primer were discarded (cutadapt $forwardprimer -e .2 --discard-untrimmed). "
		}
	    }
	    
	}
	close GP;
	print "error code: $?\n" if ($?);
	
	# get cutadapt stats
	foreach $sample (keys %{$samples}) {
	    $ifile = "$tmpscratch/$sample.log";
	    open IFILE, "$ifile" or die "cannot open cutadapt primer log $ifile: $!\n";
	    %pstat = ();
	    $header = <IFILE>;
	    chomp $header;
	    @header = split /\t/, $header;
	    $line = <IFILE>;
	    chomp $line;
	    @line = split /\t/, $line;
	    for $i (0..$#line) {
		$pstat{$header[$i]} = $line[$i];
	    }
	    
#status 
#in_reads 
#in_bp     
#too_short 
#too_long 
#too_many_n 
#out_reads 
#w/adapters 
#qualtrim_bp 
#out_bp 
#w/adapters2 
#qualtrim2_bp 
#out2_bp
	    # preferred metrics
#	$total = $stats->{$sample}{temp}{totalreads};
#	$stats->{$sample}{petrim}{"\%noprimer"} = &round10( $noprimer / $total * 100);
#	$stats->{$sample}{petrim}{noprimer} = $noprimer;
#	$stats->{$sample}{petrim}{"\%nostitch"} = &round10( $nostitch / $total * 100);
#	$stats->{$sample}{petrim}{nostitch} = $nostitch;
#	$stats->{$sample}{temp}{currentreads} = $stats->{$sample}{temp}{currentreads} - $nostitch - $noprimer;
	}
    }

    # run cutadapt to remove 3' adapters
    if ($args->{r1adapter}) {
	print "Trimming 3' adapters\n";
	$tmpscratch = "$myscratchfolder/cutadapt-adaptertrim";
	mkdir $tmpscratch;
	open GP, "| parallel $progress -j $args->{threads} > $gpout" or die "Cannot open pipe to gnu parallel\n";
#        open GP, ">parallel.test" or die "Cannot open pipe to gnu parallel\n";

	# send commands to gnu parallel
	$firstsample = 1;
	foreach $sample (keys %{$samples}) {
	    $adapter = "";
	    if ($args->{r1adapter}) {
		$adapter = "-a \"$args->{r1adapter}\"";
		$adapter .= " -A \"$args->{r2adapter}\"" if (($args->{pe}) && ($args->{r2adapter}));
	    }

	    if ($args->{pe}) { # paired-end
		$newr1 = "$tmpscratch/${sample}_R1.fastq";
		$newr2 = "$tmpscratch/${sample}_R2.fastq";
		print GP "cutadapt --report=minimal $adapter -o $newr1 -p $newr2 $samples->{$sample}{R1}{fastq} $samples->{$sample}{R2}{fastq} > $tmpscratch/$sample.log\n";
		$samples->{$sample}{R1}{fastq} = $newr1;
		$samples->{$sample}{R2}{fastq} = $newr2;
		
		if ($firstsample) {
		    $firstsample = 0;
		    $args->{methods} .= "Adapter sequences were trimmed off the 3' ends of reads using cutadapt (cutadapt $adapter). "
		}
	    } else { # single-end
		$newr1 = "$tmpscratch/${sample}_R1.fastq";
		print GP "cutadapt --report=minimal $adapter -o $newr1 $samples->{$sample}{R1}{fastq} > $tmpscratch/$sample.log\n";
		$samples->{$sample}{R1}{fastq} = $newr1;
		if ($firstsample) {
		    $firstsample = 0;
		    $args->{methods} .= "Adapter sequences were trimmed off the 3' ends of reads using cutadapt (cutadapt $adapter). "
		}
	    }
	    
	}
	close GP;
	print "error code: $?\n" if ($?);
	
	# get cutadapt stats
	foreach $sample (keys %{$samples}) {
	    $ifile = "$tmpscratch/$sample.log";
	    open IFILE, "$ifile" or die "cannot open cutadapt primer log $ifile: $!\n";
	    %pstat = ();
	    $header = <IFILE>;
	    chomp $header;
	    @header = split /\t/, $header;
	    $line = <IFILE>;
	    chomp $line;
	    @line = split /\t/, $line;
	    for $i (0..$#line) {
		$pstat{$header[$i]} = $line[$i];
	    }
	    
	    # preferred metrics
#	$total = $stats->{$sample}{temp}{totalreads};
#	$stats->{$sample}{petrim}{"\%noprimer"} = &round10( $noprimer / $total * 100);
#	$stats->{$sample}{petrim}{noprimer} = $noprimer;
#	$stats->{$sample}{petrim}{"\%nostitch"} = &round10( $nostitch / $total * 100);
#	$stats->{$sample}{petrim}{nostitch} = $nostitch;
#	$stats->{$sample}{temp}{currentreads} = $stats->{$sample}{temp}{currentreads} - $nostitch - $noprimer;
	}
    }

}

############################# Qiime2 #################################
sub myqiime2 {
    print "\n### Running Qiime2 ###\n";

    my ($samples, $args, $stats) = @_;

    push @{$args->{stageorder}}, "qiime2";
    my $myscratchfolder = "$args->{scratchfolder}/qiime2";
    mkdir $myscratchfolder;

    # generate manifest file
#    &run("samplesheet2manifest.pl --fastqfolder $args->{fastqfolder} --samplesheet $args->{samplesheet} > $manifestfile", $args->{logfile});

    $manifestfile = "$myscratchfolder/manifest.txt";
    open $OFILE, ">$manifestfile" or die "cannot open $manifestfile: $!\n";
    print $OFILE "sample-id,absolute-filepath,direction\n";
    foreach $sample (sort keys %{$samples}) {
	print $OFILE "$sample,$samples->{$sample}{R1}{fastq},forward\n";
	print $OFILE "$sample,$samples->{$sample}{R2}{fastq},reverse\n" if ($args->{pe});
    }
    close $OFILE;

    # qiime2 import
    $type = "SequencesWithQuality";
    $format = "SingleEndFastqManifestPhred33";
    if ($args->{pe}) {
	$type = "PairedEndSequencesWithQuality";
	$format = "PairedEndFastqManifestPhred33";
    }

    print "Running dada2 denoise\n";
    &runqiime("qiime tools import --type 'SampleData[$type]' --input-path $manifestfile --output-path $myscratchfolder/rawdata.qza --input-format $format", $args->{logfile}, $myscratchfolder);

    # dada2
    if ($args->{pe}) { # paired-end
	$trunclength = $args->{trunclength};
	$trunclength = 0 if ($trunclength > $args->{readlength});
	$maxee = ($args->{maxee}) ? "--p-max-ee-f $args->{maxee} --p-max-ee-r $args->{maxee}" : "";

	&runqiime("qiime dada2 denoise-paired --i-demultiplexed-seqs $myscratchfolder/rawdata.qza --p-trim-left-f 0 --p-trim-left-r 0 --p-trunc-len-f $trunclength --p-trunc-len-r $trunclength $maxee --o-representative-sequences $myscratchfolder/rep-seqs.qza --o-table $myscratchfolder/table.qza --o-denoising-stats $myscratchfolder/stats-dada2.qza --p-n-threads $args->{threads} --verbose", $args->{logfile}, $myscratchfolder);
	$args->{methods} .= "Samples were denoised using dada2 (qiime dada2 denoise-paired --p-trim-left-f 0 --p-trim-left-r 0 --p-trunc-len-f $trunclength --p-trunc-len-r $trunclength $maxee). ";
    } else { # single-end
	&runqiime("qiime dada2 denoise-single --i-demultiplexed-seqs $myscratchfolder/rawdata.qza --p-trim-left 0 --p-trunc-len 0 --o-representative-sequences $myscratchfolder/rep-seqs.qza --o-table $myscratchfolder/table.qza --o-denoising-stats $myscratchfolder/stats-dada2.qza --p-n-threads $args->{threads} --verbose", $args->{logfile}, $myscratchfolder);
	$args->{methods} .= "Samples were denoised using dada2 (qiime dada2 denoise-single --p-trim-left 0 --p-trunc-len 0). ";
    }

    # Grab dada2 stats
    # Export all the stats into plain text file
    $exportdir = "$myscratchfolder/exports";
    mkdir $exportdir;
    &runqiime("qiime tools export --input-path $myscratchfolder/stats-dada2.qza --output-path $exportdir/", $args->{logfile}, $myscratchfolder);

    # read in the data
    $ifile = "$myscratchfolder/exports/stats.tsv";
    open IFILE, $ifile or die "cannot open dada2 stats file $ifile: $!\n";
    $junk = <IFILE>; # header
    $junk = <IFILE>; # comment line
    $minreads = -1;
    $sumreads = 0;
    $samplecount = 0;
    while ($line = <IFILE>) {
	$line =~ s/\r\n?/\n/g; # get rid of stupid windows newlines	
	chomp $line;

	if ($args->{pe}) {
	    ($sample, $input, $filtered, $junk, $denoised, $merged, $junk, $nonchimeric) = split /\t/, $line;
	} else {
	    ($sample, $input, $filtered, $junk, $denoised, $nonchimeric) = split /\t/, $line; # haven't tested this
	    $merged = $denoised;
	}
	$stats->{$sample}{dada2}{noadapter} = $stats->{$sample}{subsample}{subsampledreads} - $input;
	$stats->{$sample}{dada2}{filtered} = $input - $filtered;
	$stats->{$sample}{dada2}{noise} = $filtered - $denoised;
	$stats->{$sample}{dada2}{notmerged} = $denoised - $merged;
	$stats->{$sample}{dada2}{chimeric} = $merged - $nonchimeric;
	$stats->{$sample}{dada2}{good} = $nonchimeric;
	$minreads = $nonchimeric if ($minreads == -1 or $minreads > $nonchimeric);
	$sumreads += $nonchimeric;
	$samplecount++;
    }
    $meanreads = int($sumreads / $samplecount);
    $minreads = 10 if ($minreads < 10);

    # summarize and visualize dada2 results
    &runqiime("qiime feature-table summarize --i-table $myscratchfolder/table.qza --o-visualization $myscratchfolder/table.qzv --m-sample-metadata-file $args->{samplesheet}", $args->{logfile}, $myscratchfolder);
    &runqiime("qiime feature-table tabulate-seqs --i-data $myscratchfolder/rep-seqs.qza --o-visualization $myscratchfolder/rep-seqs.qzv", $args->{logfile}, $myscratchfolder);
    $args->{methods} .= qq(The BIOM table was summarized with the Qiime2 'feature-table summarize' command. );

    # Generate a tree for phylogenetic diversity analyses
    print "Generating phylogenetic tree\n";
    &runqiime("qiime phylogeny align-to-tree-mafft-fasttree --i-sequences $myscratchfolder/rep-seqs.qza --o-alignment $myscratchfolder/aligned-rep-seqs.qza --o-masked-alignment $myscratchfolder/masked-aligned-rep-seqs.qza --o-tree $myscratchfolder/unrooted-tree.qza --o-rooted-tree $myscratchfolder/rooted-tree.qza", $args->{logfile}, $myscratchfolder);
    $args->{methods} .= "A phylogenetic tree was created using qiime phylogeny align-to-tree-mafft-fasttree. ";


    # Alpha and beta diversity analysis
    # sampling depth: Review the information presented in the table.qzv file that was created above and choose a value that is as high as possible (so you retain more sequences per sample) while excluding as few samples as possible.
    print "Analyzing diversity\n";
    &runqiime("qiime diversity core-metrics-phylogenetic --i-phylogeny $myscratchfolder/rooted-tree.qza --i-table $myscratchfolder/table.qza --p-sampling-depth $minreads --m-metadata-file $args->{samplesheet} --output-dir $myscratchfolder/core-metrics-results", $args->{logfile}, $myscratchfolder);
    $args->{methods} .= qq(Diversity analysis was perfomed using the Qiime2 'diversity core-metrics-phylogenetic' command with a sampling depth of $minreads (the number of reads in the sample with the fewest reads). );
    
    if (0) {
    # Test for associations between categorical metadata columns and alpha diversity data. 
    # Faith Phylogenetic Diversity (a measure of community richness)
    &runqiime("qiime diversity alpha-group-significance --i-alpha-diversity $myscratchfolder/core-metrics-results/faith_pd_vector.qza --m-metadata-file $args->{samplesheet} --o-visualization $myscratchfolder/core-metrics-results/faith-pd-group-significance.qzv", $args->{logfile}, $myscratchfolder);
    # Evenness metrics.
    &runqiime("qiime diversity alpha-group-significance --i-alpha-diversity $myscratchfolder/core-metrics-results/evenness_vector.qza --m-metadata-file $args->{samplesheet} --o-visualization $myscratchfolder/core-metrics-results/evenness-group-significance.qzv", $args->{logfile}, $myscratchfolder);

    # Analyze sample composition in the context of categorical metadata using PERMANOVA - Skipping for now
    # qiime diversity beta-group-significance --i-distance-matrix core-metrics-results/unweighted_unifrac_distance_matrix.qza --m-metadata-file sample-metadata.tsv --m-metadata-column BodySite --o-visualization core-metrics-results/unweighted-unifrac-body-site-significance.qzv --p-pairwise
    }

    # Alpha rarefaction plotting
    # The value that you provide for --p-max-depth should be determined by reviewing the “Frequency per sample” information presented in the table.qzv file that was created above. In general, choosing a value that is somewhere around the median frequency seems to work well, but you may want to increase that value if the lines in the resulting rarefaction plot don’t appear to be leveling out,
    &runqiime("qiime diversity alpha-rarefaction --i-table $myscratchfolder/table.qza --i-phylogeny $myscratchfolder/rooted-tree.qza --p-max-depth $meanreads --m-metadata-file $args->{samplesheet} --o-visualization $myscratchfolder/alpha-rarefaction.qzv", $args->{logfile}, $myscratchfolder);
    $args->{methods} .= qq(Alpha rarefaction analysis was perfomed using the Qiime2 'diversity alpha-rarefaction' command with a maximum depth of $meanreads (the mean number of reads per sample). );
    
    # Taxonomic analysis
    print "Assigning taxonomy\n";
    &runqiime("qiime feature-classifier classify-sklearn --i-classifier $args->{classifier} --i-reads $myscratchfolder/rep-seqs.qza --o-classification $myscratchfolder/taxonomy.qza", $args->{logfile}, $myscratchfolder);
    &runqiime("qiime metadata tabulate --m-input-file $myscratchfolder/taxonomy.qza --o-visualization $myscratchfolder/taxonomy.qzv", $args->{logfile}, $myscratchfolder);
    $args->{methods} .= qq(Taxonomy was assigned using the Qiime2 'feature-classifier classify-sklearn' command using a $args->{classifiertext}. );
    # Visualize
    &runqiime("qiime taxa barplot --i-table $myscratchfolder/table.qza --i-taxonomy $myscratchfolder/taxonomy.qza --m-metadata-file $args->{samplesheet} --o-visualization $myscratchfolder/taxa-bar-plots.qzv", $args->{logfile}, $myscratchfolder);

    # Pcoa
    &runqiime("qiime emperor plot --i-pcoa $myscratchfolder/core-metrics-results/bray_curtis_pcoa_results.qza --m-metadata-file $args->{samplesheet} --o-visualization $myscratchfolder/pcoa-visualization.qzv", $args->{logfile}, $myscratchfolder);
    $args->{methods} .= qq(The Qiime2 command 'emperor plot' was used to generate visualizations of the PCoA results from the core-metrics-analysis. );

    # Export all the qiime data into plain text files
    $exportdir = "$myscratchfolder/exports";
    mkdir $exportdir;
    foreach $data ("table", "rep-seqs", "rooted-tree", "unrooted-tree", "taxonomy") {
	&runqiime("qiime tools export --input-path $myscratchfolder/$data.qza --output-path $exportdir/", $args->{logfile}, $myscratchfolder);
    }

    &runqiime("biom convert -i $exportdir/feature-table.biom -o $exportdir/feature-table.txt --to-tsv", $args->{logfile}, $myscratchfolder);
    
#    &run("", $args->{logfile});
#    &run("", $args->{logfile});

}

######################### Qiime2 plots #########################
sub myqiime2plots {
    print "\n### Generating plots ###\n";

    my ($samples, $args, $stats) = @_;

    ### generate plots ###
    subsampleplotrmd($samples, $args, $stats);

#    fastqcplotrmd($samples, $args, $stats); # not ready for this yet

    mydada2plot($samples, $args, $stats);

    mytaxonplot($samples, $args, $stats);

    # pca plot
    $result = &run("emperorplot.pl $args->{scratchfolder}/qiime2/core-metrics-results/weighted_unifrac_pcoa_results.qza $args->{samplesheet}", $args->{logfile});
    if ($?) {
	print $result;
    } else {
	`mv emperorplot.* $reportfolder`;
	$args->{reporttext} .= qq(```{r test-main, child = 'emperorplot.rmd'}\n```\n);
    }

}

######################### Dada2 Plot #########################
sub mydada2plot {
    print "Generating Dada2 plot\n";

    my ($samples, $args, $stats) = @_;

    # generate dada2 plot
    $name = "dada2plot";
    $ofile = "$reportfolder/$name.dat";
    $rfile = "$reportfolder/$name.r";
    open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

    # generate order for report sorting
    $order = "";
    @order = sort {$stats->{$b}{dada2}{"good"} <=> $stats->{$a}{dada2}{"good"}} keys %{$stats};
    foreach $sample (@order) {
	$order .= "\"$sample\",";
    }
    chop $order; # remove last ,
    $args->{order}{"Read filtering"} = $order;

    # print out the data
    @keys = ("noadapter","filtered","noise","notmerged","chimeric","good");
    print OFILE "sample";
    foreach $key (@keys) {
	print OFILE "\t$key";
    }
    print OFILE "\n";
    foreach $sample (sort keys %{$samples}) {
	print OFILE "$sample";
	foreach $key (@keys) {
	    print OFILE "\t$stats->{$sample}{dada2}{$key}";
	}
	print OFILE "\n";
    }
    close OFILE;

    $title = "Read Filtering";
    $mergedtxt = ($args->{pe}) ? "Not merged: reads discarded because the R1 and R2 read could not be stitched together by Dada2. " : "";
    $text = "The number of reads failing each preprocessing step is shown. No primer: reads removed by Cutadapt because they don't contain the expected PCR primer sequence at the beginning of the R1 read or the end of the R2 read. Filtered: reads removed by Dada2 due to low base quality scores. Noise: reads discarded by the Dada2 denoising algorithm. ${mergedtxt}Chimeric: reads discarded by Dada2 because they contain chimeric sequence. Good: reads passing all filter steps.";
    $updatemenus = "updatemenus = updatemenus,";
    $mergedtrace = ($args->{pe}) ? qq(add_trace(y = ~notmerged, name = 'Not merged', marker = list(color = '#d62728'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Not merged",format(datat\$notmerged,big.mark=",",scientific=FALSE))) %>%) : "";
    $updatemenus = ""; # disable the sort buttons

    open RFILE, ">$rfile" or die "Cannot open $rfile\n";
    print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

data2 <- datat[rev(order(datat\$good)),]
valueorder <- list(title="Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
              categoryarray = c(as.character(data2\$sample)))

updatemenus <- list(
  list(
    active = 0,
    font = list(size = 10),
    type = 'buttons',
    xanchor = 'right',
    buttons = list(
      
      list(
        label = "Sort by<br>Sample",
        method = "relayout",
        args = list(list(xaxis = sampleorder))),
      
      list(
        label = "Sort by<br>Value",
        method = "relayout",
        args = list(list(xaxis = valueorder)))
    )
  )
)

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~noadapter,
 name = "No primer",
 type = "bar",
marker = list(color = '#1f77b4'),
hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>No primer reads",format(datat\$noadapter,big.mark=",",scientific=FALSE))
) %>% 
add_trace(y = ~filtered, name = 'Filtered', marker = list(color = '#ff7f0e'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Filtered reads",format(datat\$filtered,big.mark=",",scientific=FALSE))) %>%
add_trace(y = ~noise, name = 'Noise', marker = list(color = '#2ca02c'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Noise reads",format(datat\$filtered,big.mark=",",scientific=FALSE))) %>%
$mergedtrace
add_trace(y = ~chimeric, name = 'Chimeric', marker = list(color = '#9467bd'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Chimeric reads",format(datat\$chimeric,big.mark=",",scientific=FALSE))) %>%
add_trace(y = ~good, name = 'Good', marker = list(color = '#8c564b'), hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>Good reads",format(datat\$good,big.mark=",",scientific=FALSE))) %>%
 layout(xaxis = sampleorder, $updatemenus dragmode='pan', barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Reads", fixedrange = TRUE), legend = list(orientation = 'h')), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
    close RFILE;
    $args->{reporttext} .= qq(```{r test-main, child = '$rfile'}\n```\n);

}

######################### Taxon Plot #########################
sub mytaxonplot {
    print "Generating taxon plot\n";

    my ($samples, $args, $stats) = @_;

    # generate taxon table for generating taxon plot
    runqiime("qiime taxa collapse --i-table $args->{scratchfolder}/qiime2/table.qza --i-taxonomy $args->{scratchfolder}/qiime2/taxonomy.qza --p-level 4 --o-collapsed-table taxonomy-4", $args->{logfile}, $args->{scratchfolder});
    runqiime("qiime tools export --input-path taxonomy-4.qza --output-path .", $args->{logfile}, $args->{scratchfolder});
    runqiime("biom convert -i feature-table.biom -o taxonplot.tmp --to-tsv", $args->{logfile}, $args->{scratchfolder});

    $name = "taxonplot";
    $ofile = "$reportfolder/$name.dat";
    $rfile = "$reportfolder/$name.rmd";
    open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

    # read in the data
    $ifile = "taxonplot.tmp";
    open IFILE, $ifile or die "cannot open $ifile: $!\n";
    my $junk = <IFILE>; # Constructed from biom file
    $header = <IFILE>;
    chomp $header;
    @header = split /\t/, $header;
    $header[0] =~ s/#//;
    @taxons = ();
    while ($line = <IFILE>) {
	chomp $line;
	@line = split /\t/, $line;
	$taxon = "taxon" . ($#taxons+1);
	$taxonname = $line[0];
	push @taxons, "$taxon";
	$taxonname{$taxon} = $taxonname;
	for $i (1..$#line) {
	    $data{$header[$i]}{$taxon} = $line[$i];
	    $sums{$header[$i]} += $line[$i];
	}
    }
    
    # generate order for report sorting
    $order = "";
    @order = sort {$data{$b}{$taxons[0]} <=> $data{$a}{$taxons[0]}} keys %data;
    foreach $sample (@order) {
	$order .= "\"$sample\",";
    }
    chop $order; # remove last ,
    $args->{order}{"Taxonomy"} = $order;

    @taxons = reverse @taxons;

    # print out the data
    print OFILE "sample";
    foreach $taxon (@taxons) {
	print OFILE "\t$taxon";
    }
    print OFILE "\n";
    foreach $sample (sort keys %data) {
	print OFILE "$sample";
	$sums{$sample} = 1 unless ($sums{$sample}); # avoid divide by 0
	foreach $taxon (@taxons) {
	    $freq = $data{$sample}{$taxon} / $sums{$sample} * 100;
	    print OFILE "\t$freq";
	}
	print OFILE "\n";
    }
    close OFILE;

    $title = "Taxonomy";
    $text = "The taxonomic composition of each sample is shown, down to taxon level 4 (Order). The data shown here is a subset of the information that can be seen by loading the taxa-bar-plots.qvz qiime visualization file at the view.qiime2.org website (see the Data section of this report).";
    $updatemenus = "updatemenus = updatemenus,";
    $updatemenus = ""; # disable the sort buttons

$traces = "";
for $i (1..$#taxons) {
    $taxon = $taxons[$i];
    $traces .= qq( %>% add_trace(y=~$taxon, name = "$taxonname{$taxon}", text = paste0(datat\$sample, "<br>", "$taxonname{$taxon}<br>", round(datat\$$taxon,2),"%")));
}

    open RFILE, ">$rfile" or die "Cannot open $rfile\n";
    print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

data2 <- datat[rev(order(datat\$$taxons[0])),]
valueorder <- list(title="Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
              categoryarray = c(as.character(data2\$sample)))

updatemenus <- list(
  list(
    active = 0,
    font = list(size = 10),
    type = 'buttons',
    xanchor = 'right',
    buttons = list(
      
      list(
        label = "Sort by<br>Sample",
        method = "relayout",
        args = list(list(xaxis = sampleorder))),
      
      list(
        label = "Sort by<br>Value",
        method = "relayout",
        args = list(list(xaxis = valueorder)))
    )
  )
)

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~$taxons[0],
 name = "$taxonname{$taxons[0]}",
 type = "bar",
hoverinfo="text", 
text = paste0(datat\$sample, "<br>", "$taxonname{$taxons[0]}<br>", round(datat\$$taxons[0],2),"%")
) $traces %>% 
 layout(xaxis = sampleorder, showlegend = FALSE, $updatemenus dragmode='pan', barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Relative Frequency", fixedrange = TRUE), legend = list(orientation = 'h')), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
    close RFILE;
    $args->{reporttext} .= qq(```{r test-main, child = '$rfile'}\n```\n);

}



######################## Rmd Report ##########################
sub reportrmd {
    print "\n### Generating Analysis Report ###\n";

    my ($samples, $args, $stats, $title) = @_;

    ### report header
    $info{"Variable region"} = uc($args->{variableregion}) if ($args->{variableregion});
    reportheader($samples, $args, $stats, $title, \%info, $qiime2module);

    ### report body

    print $reportFH $args->{reporttext};

    ### data

    $visdir = "$args->{outputfolder}/q2-visualization";
    mkdir $visdir;
    `cp $args->{scratchfolder}/qiime2/*.qzv $visdir`;
    `cp $args->{scratchfolder}/qiime2/core-metrics-results/*.qzv $visdir`;
    @visfiles = `find $visdir -name "*.qzv" -printf "\%f\n"`;
    chomp @visfiles;

    $datdir = "$args->{outputfolder}/q2-artifacts";
    mkdir $datdir;
    `cp $args->{scratchfolder}/qiime2/*.qza $datdir`;
    `cp $args->{scratchfolder}/qiime2/core-metrics-results/*.qza $datdir`;
    @datfiles = `find $datdir -name "*.qza" -printf "\%f\n"`;
    chomp @datfiles;

    $expdir = "$args->{outputfolder}/q2-exports";
    mkdir $expdir;
    `cp $args->{scratchfolder}/qiime2/exports/* $expdir`;
    @expfiles = `find $expdir -type f -printf "\%f\n"`;
    chomp @expfiles;

#    $help = qq(<a href="#datahelp" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></h2>
#<div id="datahelp" class="collapse">
#The data folder accompanying this report contains the following folders and files:</div>
#);
    print $reportFH "\n<h2> Data </h2>\nThe data folder accompanying this report contains the following folders and files:\n";
#    $help = qq(<a href="#data1help" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></h2>
#<div id="data1help" class="collapse">
#Qiime2 visualization files (.qzv) should be viewed using the online browser at <a href="https://view.qiime2.org">https://view.qiime2.org</a>
#</div>
#);
    print $reportFH "\n<h3> q2-visualization/ Qiime2 visualization files </h3>\nQiime2 visualization files (.qzv) should be viewed using the online browser at <a href=\"https://view.qiime2.org\">https://view.qiime2.org</a><br>\n";
    foreach $file (@visfiles) {
	print $reportFH "$file<br>\n";
    }
#    $help = qq(<a href="#data2help" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></h3>
#<div id="data2help" class="collapse">
#Qiime2 artifact files (.qza) contain the data and metadata from an analysis. These files can be analyzed further in Qiime2, or you can view the contents of the files using the online browser at <a href="https://view.qiime2.org">https://view.qiime2.org</a>. The contents of the artifact files have also been exported into standard formats (see q2-exports) for use outside of Qiime2.
#</div>
#);
    print $reportFH "\n<h3> q2-artifact/ Qiime2 artifact files </h3>\nQiime2 artifact files (.qza) contain the data and metadata from an analysis. These files can be analyzed further in Qiime2, or you can view the contents of the files using the online browser at <a href=\"https://view.qiime2.org\">https://view.qiime2.org</a>. The contents of the artifact files have also been exported into standard formats (see q2-exports) for use outside of Qiime2.<br>\n";
    foreach $file (@datfiles) {
	print $reportFH "$file<br>\n";
    }
#    $help = qq(<a href="#data3help" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></h3>
#<div id="data3help" class="collapse">
#The contents of all Qiime2 artifact files have been exported into standard formats for use outside of Qiime2.
#</div>
#);
    print $reportFH "\n<h3> q2-exports/ Qiime2 exported files </h3>\nThe contents of some Qiime2 artifact files have been exported into standard formats for use outside of Qiime2.<br>\n";
    foreach $file (@expfiles) {
	print $reportFH "$file<br>\n";
    }
    
    ### methods

    print $reportFH qq(\n## Methods
$args->{methods}
);

    ### acknowledgements

    ### footer

#    close $reportFH;

    ### generate pdf and html from rmd, copy over to output folder
    &run("Rscript -e \"library('rmarkdown'); render('$reportfolder/report.rmd', output_format='html_document', output_file='$args->{outputfolder}/report.html')\"", $args->{logfile}); # generate html

}


################################ Helper subs ###############################

# run a qiime2 command in a fresh bash session
sub runqiime {
    ($command, $logfile, $tmpdir) = @_;
    
#    $result = run("env -i HOME=\"\$HOME\" TMPDIR=\"$tmpdir\" bash -l -c 'module load $qiime2module; $command'", $logfile);
    $result = run("export TMPDIR=\"$tmpdir\"; $command", $logfile);

    print "Qiime2 Failure: $result\n" if ($?);

}
