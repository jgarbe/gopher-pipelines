#!/usr/bin/perl -w

######################################################
# align-singlesample.pl
# John Garbe
# May 2015
# Re-write May 2016
#
#######################################################

=head1 NAME

illumina-singlesample.pl - run basic qc on a single fastq file (single- or paired-end)

=head1 SYNOPSIS

illumina-singlesample.pl --R1 R1.fastq [--R2 R2.fastq] [--outputfolder folder]

=head1 DESCRIPTION

Run basic QC on a fastq file

 --outputfolder folder : The output folder
 --threads integer : Number of processors to use (number of threads to run)
 --verbose : Print more information while running
 --subsample integer : Subsample the specified number of reads from each sample. 0 = no subsampling (default = 0)
 --qualitycontrol : Enable quality-control: use trimmomatic to trim adapter sequences, trim low quality bases from the ends of reads, and remove short sequences
 --adapterfile file : A file containing adapter sequences for trimmomatic
 --extraoptionsfile file : File with extra options for trimmomatic, BWA, or Bowtie2
 --species human : Comma separated list of one or more species
 --help : Print usage instructions and exit
=cut

##################### Main ###############################

use FindBin;                  # locate this script directory
use lib "$FindBin::RealBin";  # look for modules in the script directory
use Pipelinev4;               # load the pipeline module

use Getopt::Long;
use Pod::Usage;
use Cwd 'abs_path';
use File::Basename;
use File::Temp qw( tempdir );

my %args;
my %stats;

### Initialize ###
&init(\%args, \%stats);

### Subsample ###
runtime;
subsample(\%args, \%stats);

### FastQC ###
runtime;
fastqc(\%args, \%stats);

if ($args{qualitycontrol}) {
    ### Trimmomatic ###
    runtime;
    trimmomatic(\%args, \%stats);

    ### FastQC ###
    runtime;
    fastqc(\%args, \%stats, "fastqc-trim");
}

### fastq contamination ###
#runtime;
#fastqcontaminationscreen(\%args, \%stats);

### fastq species ###
runtime;
fastqspeciesscreen(\%args, \%stats);

### Write stats to file ###
runtime;
writestats(\%args, \%stats);

`touch $args{scratchfolder}/Complete`;

# remove uncompressed fastq files to save space
`rm $args{scratchfolder}/R1.fastq` if (-e "$args{scratchfolder}/R1.fastq");
`rm $args{scratchfolder}/R2.fastq` if (-e "$args{scratchfolder}/R2.fastq");

print "Finished\n";
runtime;

exit;

##################### Initialize ###############################
sub init {
    print "Running Sample Initialize\n";

    my ($args, $stats) = @_;

    ### Usage ###
    $usage = "USAGE: illumina-singlesample.pl [--outputfolder folder] --R1 file [--R2 file]\n";

    # set defaults - add additional parameter defaults here
    $args->{threads} = $ENV{"PBS_NUM_PPN"} // 1;
    $args->{scratchfolder} = $ENV{PWD} . "/illumina-singlesample";
    GetOptions("help" => \$args->{help},
               "verbose" => \$args->{verbose},
	       "R1=s" => \$args->{R1},
	       "R2=s" => \$args->{R2},
               "threads=i" => \$args->{threads},
               "outputfolder=s" => \$args->{scratchfolder},
               "extraoptionsfile=s" => \$args->{extraoptionsfile},
               "subsample=i" => \$args->{subsample},
               "qualitycontrol" => \$args->{qualitycontrol},
               "adapterfile=s" => \$args->{adapterfile},
	       "referencefasta=s" => \$args->{referencefasta},
	       "samplename=s" => \$args->{samplename},
	       "species=s" => \$args->{species},
        ) or die $usage;
    pod2usage(q(-verbose) => 3) if ($args->{help});
    die "$usage" unless ($args->{R1});
    $args->{extraoptionsfile} = abs_path($args->{extraoptionsfile}) if ($args->{extraoptionsfile});

    ### Starttime ###
    $date = `date`;
    chomp $date;
    print "Starting at $date\n";
    &runtime;

    ### Standard parameters ###
    $args->{scratchfolder} = abs_path($args->{scratchfolder});
    die "R1 Fasta file $args->{R1} not found\n" if (!-e $args->{R1});
    $args->{R1} = abs_path($args->{R1});
    $args->{pe} = 0;
    if ($args->{R2}) {
        die "R2 Fasta file $args->{R2} not found\n" if (!-e $args->{R2});
        $args->{R2} = abs_path($args->{R2});
        $args->{pe} = 1;
    }

    ### non-Standard parameters ###
    # add additional parameters processing here


    ### Finish setup
    # check that required programs are installed
    requiredprograms(("fastqc", "\$TRIMMOMATIC"));

    diegracefully();
    readinextraoptions($args);
    scratchfoldersetup($args);

}

######################## Fastq-species-screen.pl ##########################
# Requires: fastq-species-bowtie2.pl
# Args: 
# Stats: ??
sub fastqspeciesscreen {
    print "Running Fastq species screen\n";

    my ($args, $stats) = @_;
    my $stage = $_[2] // "fastq-species";
    $species = "human,mouse,ecoli,rRNA,PhiX,Chloroplast,Mitochondria,Adapters,Vectors";
    if ($args->{species}) {
	$species = "$args->{species},$species";
    }

    &run("module load ensembl; module load genomes; module load genomes-other; fastq-species-bowtie2.pl -n 10000 -s $species $args->{R1} > $args->{scratchfolder}/species.txt");
}
