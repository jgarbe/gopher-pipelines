#!/usr/bin/perl -w

######################################################
# gbsref-singlesample.pl
# John Garbe
# May 2015
# Re-write May 2016
#
#######################################################

=head1 DESCRIPTION

gbsref-singlesample.pl - Align an GBS fastq file (single- or paired-end) to a reference after trimming and filtering

=head1 SYNOPSIS

gbsref-singlesample.pl [--enzyme enzyme] [--enzyme2] [--gbspadding A,AG,TGA] --referencefasta file --bowtie2index index --R1 R1.fastq [--R2 R2.fastq] [--outputfolder folder]
gbsref-singlesample.pl [--enzyme enzyme] [--enzyme2] [--gbspadding A,AG,TGA] --referencefasta file --bwaindex index --R1 R1.fastq [--R2 R2.fastq] [--outputfolder folder]

=head1 OPTIONS

Align an GBS fastq file to a reference using Bowtie2 or BWA:

 --bowtie2index string : Bowtie2 index basename
 --bwaindex string : BWA index basename
 --referencefasta file : A fasta file containing the reference genome
 --enzyme enzyme : enzyme used to generate GBS library
 --enzyme2 enzyme : Second enzyme used in a double diggest to generate GBS library
 --padding padding : Padding sequences used in GBS library construction
 --padding2 padding : Padding sequences used with second enzyme in double digest in GBS library construction
 --headcrop integer : Remove this many bases off the front of each read (for removing padding sequences), disables padding recognition
 --maxlength integer : Crop reads to this length
 --minlength integer : Reads shorter than this are discarded
 --ploidy integer : Ploidy of the species (default = 2 = diploid)
 --readgroup name : String to use as read group ID and SM in bam file
 --outputfolder folder : The output folder
 --threads integer : Number of processors to use (number of threads to run)
 --verbose : Print more information while running
 --subsample integer : Subsample the specified number of reads from each sample. 0 = no subsampling (default = 0)
 --qualitycontrol : Enable quality-control: use trimmomatic to trim adapter sequences, trim low quality bases from the ends of reads, and remove short sequences
 --adapterfile file : A file containing adapter sequences for trimmomatic
 --extraoptionsfile file : File with extra options for trimmomatic, BWA, or Bowtie2
 --coverage : Calculate coverage stats with bedtools (takes a while)
 --nofastqc : skip FastQC
 --help : Print usage instructions and exit

=cut

##################### Main ###############################

use FindBin;                  # locate this script directory
use lib "$FindBin::RealBin";  # look for modules in the script directory
use Pipelinev4;               # load the pipeline module

use Getopt::Long;
use Pod::Usage;
use Cwd 'abs_path';

#use enzymes;

my %args;
my %stats;

### Initialize ###
&init(\%args, \%stats);

### Subsample ###
runtime;
subsample(\%args, \%stats);

### FastQC ###
if (! $args{nofastqc}) {
    runtime;
    fastqc(\%args, \%stats); 
}

if ($args{qualitycontrol}) {
    ### Trimmomatic ###
    runtime;
    trimmomatic(\%args, \%stats);

    ### FastQC ###
    runtime;
    fastqc(\%args, \%stats, "fastqc-trim");
}

### GBS Trim ###
if (! $args{headcrop}) {
    runtime;
    gbstrim(\%args, \%stats);
} else {

### Trimmomatic (custom) ###
    runtime;
    my $adapterfile = "/panfs/roc/msisoft/trimmomatic/0.33/adapters/all_illumina_adapters.fa";
    $minscore = 16;
    $args{extraoptions}{trimmomatic} = "ILLUMINACLIP:$adapterfile:2:30:10:2:true HEADCROP:$args{headcrop} LEADING:3 TRAILING:3 SLIDINGWINDOW:4:$minscore MINLEN:$args{minlength} CROP:$args{maxlength}";
    trimmomatic(\%args, \%stats);
}

if ($args{bwaindex}) {
    ### BWA ###
    runtime;
    bwa(\%args, \%stats);
} elsif ($args{bowtie2index}) {
    ### Bowtie2 ###
    runtime;
    bowtie2(\%args, \%stats);
} else {
    die "No BWA, no Bowtie2, no soup!\n";
}

### gbscoverage ###
if ($args{coverage}) {
    runtime;
    &gbscoverage(\%args, \%stats);
}

# a hack to remove the fasta index because it causes picard to crash???
#$fai = $args{referencefasta} . ".fai";
#`rm $fai` if (-e $fai);

### Picard MultipleMetrics ###
runtime;
collectmultiplemetrics(\%args, \%stats);

### Bam downsample ###
runtime;
bamdownsample(\%args, \%stats);

### Write stats to file ###
runtime;
writestats(\%args, \%stats);

`touch $args{scratchfolder}/Complete`;

print "Finished\n";
runtime;

exit;

##################### Initialize ###############################
sub init {
    print "Running Sample Initialize\n";

    my ($args, $stats) = @_;

    # set defaults - add additional parameter defaults here
    $args->{threads} = $ENV{"PBS_NUM_PPN"} // 1;
    $args->{scratchfolder} = $ENV{PWD} . "/gbsref-singlesample";
    GetOptions("help" => \$args->{help},
               "verbose" => \$args->{verbose},
	       "R1=s" => \$args->{R1},
	       "R2=s" => \$args->{R2},
               "threads=i" => \$args->{threads},
               "outputfolder=s" => \$args->{scratchfolder},
               "extraoptionsfile=s" => \$args->{extraoptionsfile},
               "subsample=i" => \$args->{subsample},
               "qualitycontrol" => \$args->{qualitycontrol},
               "adapterfile=s" => \$args->{adapterfile},
	       "referencefasta=s" => \$args->{referencefasta},
	       "samplename=s" => \$args->{samplename},
	       "readgroup=s" => \$args->{readgroup},
	       "bwaindex=s" => \$args->{bwaindex},
	       "bowtie2index=s" => \$args->{bowtie2index},
	       "enzyme=s" => \$args->{enzyme},
	       "enzyme2=s" => \$args->{enzyme2},
	       "padding=s" => \$args->{padding},
	       "padding2=s" => \$args->{padding2},
	       "headcrop=i" => \$args->{headcrop},
	       "minlength=i" => \$args->{minlength},
	       "maxlength=i" => \$args->{maxlength},
	       "ploidy=i" => \$args->{ploidy}, 
	       "coverage" => \$args->{coverage},
	       "nofastqc" => \$args->{nofastqc},
        ) or pod2usage;
    pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
    pod2usage unless ($args->{R1} and ($args->{bwaindex} or $args->{bowtie2index}) and $args->{referencefasta});
    $args->{extraoptionsfile} = abs_path($args->{extraoptionsfile}) if ($args->{extraoptionsfile});

    ### Starttime ###
    $date = `date`;
    chomp $date;
    print "Starting at $date\n";
    &runtime;

    ### Standard parameters ###
    $args->{scratchfolder} = abs_path($args->{scratchfolder});
    die "R1 Fasta file $args->{R1} not found\n" if (!-e $args->{R1});
    $args->{R1} = abs_path($args->{R1});
    $args->{pe} = 0;
    if ($args->{R2}) {
        die "R2 Fasta file $args->{R2} not found\n" if (!-e $args->{R2});
        $args->{R2} = abs_path($args->{R2});
        $args->{pe} = 1;
    }
    $args->{logfile} = "$args->{scratchfolder}/log" unless ($args->{verbose});

    ### non-Standard parameters ###
    # add additional parameters processing here
    $args->{bowtie2index} = bowtie2indexcheck($args->{bowtie2index}) if ($args->{bowtie2index});
    $args->{bwaindex} = bwaindexcheck($args->{bwaindex}) if ($args->{bwaindex});
    die "Cannot file reference fasta file $args->{referencefasta}\n" if (!-e $args->{referencefasta});
    $args->{referencefasta} = abs_path($args->{referencefasta});

    ### Finish setup
    # check that required programs are installed
    requiredprograms(("fastqc", "\$TRIMMOMATIC", "bowtie2", "bwa", "\$PICARD", "gbstrim.pl"));

    diegracefully();
    readinextraoptions($args);
    scratchfoldersetup($args);

}

### gbscoverage ###
sub gbscoverage {
    print "Running gbs coverage \n";

    my ($args, $stats) = @_;

    run("module load bedtools; coverage-summary.pl --bamfile $args->{bam} --referencefai $args->{referencefasta}.fai > $args->{scratchfolder}/coverage.txt", $args->{logfile});

}
