DESeq2 Instructions
===================

The rnaseq pipelineprovides your dataset in an RDATA file that can easily be loaded into R and analyzed with DESeq2. Other R packages to consider using are EdgeR and Limma/VOOM. DESeq2 reference manuals and user guides are available on the `Bioconductor website <http://bioconductor.org/packages/release/bioc/html/DESeq2.html>`_

Load the RData file into R
---------------------------

#. Copy the DESeq2-data.RDATA file to the computer where you will run R
#. Start up R and install the DESeq2 package::

	source("http://bioconductor.org/biocLite.R")
	biocLite("DESeq2")

Or, load the library if it is already installed::

	library("DESeq2")

#. Load the RDATA file::

	load("C:/path/to/file/DESeq2-data.RDATA")

Create a DESeq2 dataset
-----------------------

Your metatdata (taken from the mapping file) is stored in the "meta" table and your raw expression data is stored in the "data" table. Both tables are used to create a DESeq2 dataset. When creating the dataset you must provide a design formula. The design formula tells which variables in the column metadata table (colData) specify the experimental design and how these factors should be used in the analysis. You can use R’s formula notation to express any experimental design that can be described within an ANOVA-like framework. Refer to the `DESeq2 manual <http://www.bioconductor.org/help/workflows/rnaseqGene/#construct>`_ for more information.

#. Inspect the "meta" table to see what experimental data is available::

	meta

#. Construct the DESeq2 dataset. In this example a simple design using the "Group" column in the meta table is used::

	dds <- DESeqDataSetFromMatrix(countData = data, colData = meta, design = ~ Group)

#. Follow the instructions in the `DESeq2 manual <http://www.bioconductor.org/help/workflows/rnaseqGene/#eda>`_ to visually explore your dataset. (Note: using the rlog function is not recommended with single-cell data, it usually hangs)

#. Follow the instructions in the `DESeq2 manual <http://www.bioconductor.org/help/workflows/rnaseqGene/#de>`_ to complete a differential expression analysis

