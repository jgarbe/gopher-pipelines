# Gopher-pipelines change log

### 2020-6-16
- gopher-pipelines 2.1 created
- shotgun-pipeline added
- covid-pipeline added

### 2020-1-21
- illumina-pipeline now generates a table of CITE-Seq HTO barcodes if the project name ends with HTO or ADT (illumina-pipeline)
- rnaseq-pipeline: bug fixes affecting combination of --qualitycontrol and --subsample when input fastq files are .gz compressed

### 2019-10-29
- rnaseq-pipeline now reports meanfragmentsize and stdevfragmentsize in the metrics (rnaseq-pipeline)

### 2019-7-31
- Updated Qiime2 trunclen settings (qiime2-pipeline)

### 2019-4-24
- Removed unused stuff from resources and software directories

### 2019-4-23
- Illumina-pipeline: more metadata pulled from samplesheets (library and sample types, species)

### 2019-2-21
- Version 2.0 created: removed metagenomics-pipeline, added qiime2-pipeline
- Plots generated with plotly instead of ggplot2
- Reports generated with Rmd/knitr/pandoc instead of Rst/sphinx
- createsamplesheet.pl script cleaned up, removed code specific for qiime1 and the metagenomics-pipeline
- gopher-pipelines/2.0 depends on gtools/2.0
- Added paired-end read support (gbsx-pipeline)
- Stacks2 now checks for samples that fail the ustacks step and removes them from further analysis (stacks2-pipeline)

### 2019-2-19
- Filtered ensembl gtf files (with only protein_coding entries) are now used by default when using references in /home/umii/public/ensembl (rnaseq-pipeline)
- A table of variant flanking sequence is reported for GBS_SNP_CROP variants (gbs-pipeline)

### 2019-1-24
- R1 and R2 fastq.gz files are now uncompressed and/or subsampled at the same time, speeding up processing of large samples

### 2018-12-11
- Adapter.txt file is created correctly (gbs-pipeline, stacks2-pipeline)
- Padding sequences are reported correctly in metrics file (stacks2-pipeline)

### 2018-11-19
- metagenomics pipeline can now handle samplesheets that have underscores in the samplenames

### 2018-11-7
- set default krakendb to minikraken (kraken-pipeline)
- added ribosomal database to fastqscreen (illumina-pipeline)

### 2018-10-10
- fixed/updated color schemes on some plots
- added methods section to rnaseq-pipeline

## 2018-10-02
- Added first draft of gatk-pipeline
- Added support for ensembl module (all pipelines)
- Fixed reporting of readlength in metrics files (all pipelines)
- Removed dependency on riss_util module (all pipelines)

## 2018-8-02
- Piplines use default python2 module instead of a version that MSI dropped.
- Added gbsx pipeline

## 2018-7-09
- All GBS pipelines now generate project.info files
- Updated GBS pipeline padding sequence for MspI

## 2018-6-26
- Added --bwamem option to align-pipeline to allow control of how much memory samtools sort shoudl use
- stacks2-pipeline PCA plot should work now

## 2018-5-31
- Various improvements to the gbs pipelines
- GBS pipelines now use gbs instead of sbg in the pipeline names and everywhere else
- Piplines now use the pipeline scratch folder for samtools sort temporary files instead of /panfs/roc/scratch

## 2018-1-17
- Various improvements to the sbg pipelines

## 2017-11-21
- module version 1.6 created

## 2017-09-08
- Reworked how Sphinx generates reports. Cleaned up the report template a little bit.
- Moved list of links to FastQC files to a collapsible element, collapsed by default

## 2017-08-04
- Added --pacbio flag to align pipeline to enable pipeline to run on pacbio datasets. Previously the pipeline would hang when running FastQC. (thanks Juan)
- Added --headcrop option to rnaseq-pipeline, useful for removing first three bases of SMARTer Stranded Pico rna-seq libraries: --headcrop 3 (thanks Juan)
- Fixed a bug in the calculation of read alignment rates for single-end datasets (all pipelines)
- Optimized processing of FastQC output to reduce runtime (all pipelines)

## 2017-07-11
- Added workaround to get pynast alignments to work correctly for the V1V3 variable region (metagenomics-pipeline, thanks Trina and Trevor)

## 2017-04-19
- Improved fastq quality plot so it shows per-base mean quality, instead of just high, medium, or low quality (green, yellow, red) (metagenomics pipeline)
- Added v4v5 region (metagenomics pipeline)
- added createsamplesheet.pl to bin directory

## 2017-04-13
- Modified metagenomics pipeline so the qiime config details in the programversions.txt file reflect the config used for the analysis (metagenomics pipeline)
- Fixed percent metrics plot so samples with zero denovoOTUs plot correctly, incstead of missing the refOTU portion of the bar (metagenomics pipeline)

## 2017-04-12
- Fixed --variableregion V4V6 primer definitions (metagenomics pipeline)
- Changed name/title of metagenomics report from 16s to microbiome
- Added second fragment length plot showing fragment length distribution after chimera and host sequence removal

## 2017-03-28
- tweeked samplesheet parsing so spurious spaces in tab-delimited files are handled properly

## 2017-03-27 - 1.5 release
- pipeline report files are now named index.html, instead of UMII_UMGC_*_QC.html (all pipelines)
- fixed a bug preventing pipelines from running on multiple nodes (all pipelines, thanks Milcah!)
- improved display of help/usage messages (all pipelines)
- improved robustness of hisat2 summary metric plotting (thanks Christy!) (rnaseq-pipeline)
- added support for "--stranded" stranded library option with tophat2 (rnaseq-pipeline), support for hisat2 is already present 
- fixed parsing of bowtie2 alignment stats (align-pipeline) (thanks Juan!)
- added --notmpdata option to metagenomics-pipeline which suppresses description of temp files in the report
- metagenomics-pipeline now properly generates fragmentlength plot when all samplenames are numeric
- Added freebayes varaint calling to align-pipeline
- Bowtie2 and BWA now add read group info to bam files (align-pipeline). ID and SM are set to the sample name
- Added samtools addremoverg function to pipeline library
- Command line commands are printed to STDOUT when --verbose option is in use (all pipelines)
- The subsample-#reads metric has been renamed subsample-#rawreads (all pipelines)
- The metric subsample-#subsampledreads has been added (all pipelines)
- Changed copyright to Regents of the University of Minnesota
- Fixed type in hisat2 --rna-strandness option so the rnaseq-pipeline --stranded option now works (thanks Ying!)
- Fixed parameter checking in rnaseq2-pipeline (thanks Christy and Rendong!)

## 2016-10-25
- fixed emperor link in rnaseq-pipeline report
- --pandaa option (metagenomics-pipeline) no longer requires an option, it is just a flag
- Default pandamin value changed from 0 to 1 to avoid empty sequences (metagenomics-pipeline)
- umgc-pipeline passes verbose flag on to umgc-singlesample
- empty fastq files are excluded from analysis (all pipelines)
- added primers to 16s-primers.fa primer file (V3F, V5R, 18s-V9, ITS2) (metagenomics-pipeline)
- fixed mock-%mock metric, had been always reporting zero (metagenomics-pipeline)
- fixed bam sorting bug that caused tophat2 alignment (--bowtie2index option) in rnaseq-pipeline to fail
- upgraded Krona to version 2.7 (kraken-pipeline)

## 2016-08-22
- Added program version tracking to all pipelines
- Added level 7 to metagenomics otu plot
- Updated fastq_screen to v0.7.0, which results in more accurate fastq_screen plots
- Added metrics (all pipelines): fastq-%adapter
- Added metrics (aling-pipeline): samtoolsstats-peakmismatch, samtoolstats-peakindel
