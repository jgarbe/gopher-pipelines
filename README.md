![UMN Gopher](resources/images/goldy.png) Gopher-pipelines
=======================

Automated analysis pipelines for the analysis of high-throughput sequence datasets.

INSTALLATION
------------

Gopher-pipelines is a collection of Perl and bash scripts designed to run on compute resources at the Minnesota Supercomputing Institute (MSI). They are installed as the "gopher-pipelines" module at MSI.

USAGE
-----

Use the "--help" option to get usage information for any Gopher-pipeline script. 

Gopher-pipelines documentation is located in the associated Bitbucket wiki https://bitbucket.org/jgarbe/gopher-pipelines/wiki/Home

ISSUES
------

Use the associated Bitbucket issue tracker to report issues: https://bitbucket.org/jgarbe/gopher-pipelines/issues

AKNOWLEDGEMENTS
---------------

Gopher-pipelines is developed by the University of Minnesota Genomics Center in collaboration with the University of Minnesota Informatics Institute and the University of Minnesota Supercomputing Institute. It is released under the terms of the GNU General Public License.

Copyright (c) 2013-2020 Regents of the University of Minnesota

![UMN Logo](resources/images/umnlogo.gif)